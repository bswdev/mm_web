Jaguar:S-Type:2000-2008:sedan
Jaguar:S-Type R:2003-2008:sedan
Jaguar:X-Type Sedan:2002-2008:sedan
Jaguar:X-Type Wagon:2005-2008:wagon
Jaguar:XE:2013:convertible
Jaguar:XF:2009-2012:sedan
Jaguar:XFR:2010-2012:sedan
Jaguar:XJ Convertible:1990-1996:convertible
Jaguar:XJ Coupe:1990-1995:coupe
Jaguar:XJ Sedan:1990-2009,2011-2012:sedan
Jaguar:XJ Supersport:2011-2012:sedan
Jaguar:XJL Supersport:2011-2012:sedan
Jaguar:XJR:1995-2009:sedan
Jaguar:XJR 100:2002:sedan
Jaguar:XK Convertible:1997-2012:convertible
Jaguar:XK Coupe:1997-2012:coupe
Jaguar:XKR:2001-2012:coupe
Jaguar:XKR 100:2002:convertible
Jaguar:XKR Convertible:2000:convertible
Jaguar:XKR Silverstone:2001:coupe
Jaguar:XKR-S:2012:coupe
Jaguar:XKR 175:2011:coupe
Jeep:Cherokee:1990-2001:suv
Jeep:Comanche:1990-1992:truck
Jeep:Commander:2006-2010:crossover,suv
Jeep:Compass:2007-2012:crossover,suv
Jeep:Grand Cherokee SRT8:2006-2010,2012:crossover,suv
Jeep:Grand Cherokee:1993-2012:crossover,suv
Jeep:Grand Wagoneer:1990-1991,1993:suv
Jeep:Liberty:2002-2012:crossover,suv
Jeep:Patriot:2007-2012:crossover,suv
Jeep:Wagoneer:1990:suv
Jeep:Wrangler:1990-2012:suv
