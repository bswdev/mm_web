Saab:9-2X:2005-2006:wagon
Saab:9-3 Convertible:1999-2009:convertible
Saab:9-3 Hatchback:1999-2002:hatchback
Saab:9-3 Sedan:2003-2011:sedan
Saab:9-3 Turbo X:2008:sedan
Saab:9-3 Turbo X SportCombi:2008:wagon
Saab:9-3 Viggen:1999-2002:hatchback
Saab:9-3 Viggen Convertible:2000-2002:convertible,hatchback
Saab:9-3 Wagon:2006-2011:wagon
Saab:9-4X:2011:crossover,suv
Saab:9-5 Sedan:1999-2011:sedan
Saab:9-5 Wagon:1999-2009:wagon
Saab:9-7X:2005-2009:suv
Saab:900 Convertible:1990-1998:convertible
Saab:900 Hatcback:1990-1998:hatchback
Saab:900 SPG:1990-1991:hatchback
Saab:900 Sedan:1990-1993:sedan
Saab:9000 Aero:1993-1997:hatchback
Saab:9000 Hatchback:1990-1998:hatchback
Saab:9000 Sedan:1990-1995:sedan
Saturn:Astra:2008:hatchback
Saturn:Aura Hybrid:2007-2009:hybrid/electric,sedan
Saturn:Aura Sedan:2007-2009:sedan
Saturn:ION Coupe:2003-2007:coupe
Saturn:ION Red Line:2004-2007:coupe
Saturn:ION Sedan:2003-2007:sedan
Saturn:L-Series Sedan:2000-2003:sedan
Saturn:L-Series Wagon:2000-2003:wagon
Saturn:L300 Sedan:2004-2005:sedan
Saturn:L300 Wagon:2004:wagon
Saturn:Outlook:2007-2009:crossover,suv
Saturn:Relay:2005-2007:van/minivan
Saturn:S-Series Coupe:1991-2002:coupe
Saturn:S-Series Sedan:1991-2002:sedan
Saturn:S-Series Wagon:1993-2001:wagon
Saturn:Sky Convertible:2007-2009:convertible
Saturn:Sky Red Line:2007-2009:convertible
Saturn:Sky Red Line Carbon Flash SE:2008:convertible
Saturn:Sky Red Line Hydro Blue LE:2009:convertible
Saturn:Sky Red Line Ruby Red SE:2009:convertible
Saturn:VUE:2002-2009:crossover,suv
Saturn:VUE Hybrid:2007-2009:crossover,hybrid/electric,suv
Scion:FR-S:2012:coupe
Scion:iQ:2012:hatchback
Scion:tC:2005-2012:hatchback
Scion:xA:2004-2006:hatchback
Scion:xB:2004-2006,2008-2012:wagon
Scion:xD:2008-2011:hatchback
Smart:Fortwo Convertible:2008-2011:convertible
Smart:Fortwo Hatchback:2008-2012:hatchback
Spyker:C8 Convertible:2009:convertible
Spyker:C8 Coupe:2009:coupe
Subaru:B9 Tribeca:2006-2007:crossover,suv
Subaru:Baja:2003-2006:crossover,truck
Subaru:BRZ:2013:coupe
Subaru:Forester:1998-2011:crossover,suv,wagon
Subaru:Impreza Coupe:1995-2001:coupe
Subaru:Impreza Hatchback:2008-2012:hatchback
Subaru:Impreza RS:1998-2001:coupe,sedan
Subaru:Impreza STi:2004-2006:sedan
Subaru:Impreza Sedan:1993-2012:sedan
Subaru:Impreza WRX STI:2007-2011:hatchback,sedan
Subaru:Impreza WRX STI Limited:2007,2011:sedan
Subaru:Impreza WRX STI Special Edition:2010:hatchback
Subaru:Impreza Wagon:1993-2007:wagon
Subaru:Justy:1990-1994:hatchback
Subaru:Legacy Sedan:1990-2011:sedan
Subaru:Legacy Wagon:1990-2007:wagon
Subaru:Loyale Hatchback:1990:hatchback
Subaru:Loyale Sedan:1990-1993:sedan
Subaru:Loyale Wagon:1990-1994:wagon
Subaru:Outback Sedan:2000-2007:crossover,sedan
Subaru:Outback Wagon:2000-2011:crossover,wagon
Subaru:SVX:1992-1997:coupe
Subaru:Tribeca:2008-2011:crossover,suv
Subaru:XT Coupe:1991:coupe
Subaru:XV:2013:crossover,suv
Suzuki:Aerio Sedan:2002-2007:sedan
Suzuki:Aerio Wagon:2002-2006:wagon
Suzuki:Equator Crew Cab:2009-2011:truck
Suzuki:Equator Extended Cab:2009-2011:truck
Suzuki:Esteem Sedan:1995-2002:sedan
Suzuki:Esteem Wagon:1998-2002:wagon
Suzuki:Forenza Sedan:2004-2008:sedan
Suzuki:Forenza Wagon:2005-2008:wagon
Suzuki:Grand Vitara:1999-2011:crossover,suv
Suzuki:Kizashi:2010-2011:sedan
Suzuki:Reno:2005-2008:hatchback
Suzuki:Samurai:1990-1995:suv
Suzuki:Sidekick:1990-1998:suv
Suzuki:Swift Hatchback:1990-2001:hatchback
Suzuki:Swift Sedan:1990-1994:sedan
Suzuki:SX4 Hatchback:2007-2011:crossover,hatchback
Suzuki:SX4 Sedan:2008-2011:sedan
Suzuki:Verona:2004-2006:sedan
Suzuki:Vitara:1999-2004:suv
Suzuki:X-90:1996-1998:suv
Suzuki:XL7:2001-2009:crossover,suv
Spyker:C8 Convertible:2009:convertible
Spyker:C8 Coupe:2009:coupe
Subaru:B9 Tribeca:2006-2007:crossover,suv
Subaru:Baja:2003-2006:crossover,truck
Subaru:BRZ:2013:coupe
Subaru:Forester:1998-2011:crossover,suv,wagon
Subaru:Impreza Coupe:1995-2001:coupe
Subaru:Impreza Hatchback:2008-2012:hatchback
Subaru:Impreza RS:1998-2001:coupe,sedan
Subaru:Impreza STi:2004-2006:sedan
Subaru:Impreza Sedan:1993-2012:sedan
Subaru:Impreza WRX STI:2007-2011:hatchback,sedan
Subaru:Impreza WRX STI Limited:2007,2011:sedan
Subaru:Impreza WRX STI Special Edition:2010:hatchback
Subaru:Impreza Wagon:1993-2007:wagon
Subaru:Justy:1990-1994:hatchback
Subaru:Legacy Sedan:1990-2011:sedan
Subaru:Legacy Wagon:1990-2007:wagon
Subaru:Loyale Hatchback:1990:hatchback
Subaru:Loyale Sedan:1990-1993:sedan
Subaru:Loyale Wagon:1990-1994:wagon
Subaru:Outback Sedan:2000-2007:crossover,sedan
Subaru:Outback Wagon:2000-2011:crossover,wagon
Subaru:SVX:1992-1997:coupe
Subaru:Tribeca:2008-2011:crossover,suv
Subaru:XT Coupe:1991:coupe
Subaru:XV:2013:crossover,suv
Suzuki:Aerio Sedan:2002-2007:sedan
Suzuki:Aerio Wagon:2002-2006:wagon
Suzuki:Equator Crew Cab:2009-2011:truck
Suzuki:Equator Extended Cab:2009-2011:truck
Suzuki:Esteem Sedan:1995-2002:sedan
Suzuki:Esteem Wagon:1998-2002:wagon
Suzuki:Forenza Sedan:2004-2008:sedan
Suzuki:Forenza Wagon:2005-2008:wagon
Suzuki:Grand Vitara:1999-2011:crossover,suv
Suzuki:Kizashi:2010-2011:sedan
Suzuki:Reno:2005-2008:hatchback
Suzuki:Samurai:1990-1995:suv
Suzuki:Sidekick:1990-1998:suv
Suzuki:Swift Hatchback:1990-2001:hatchback
Suzuki:Swift Sedan:1990-1994:sedan
Suzuki:SX4 Hatchback:2007-2011:crossover,hatchback
Suzuki:SX4 Sedan:2008-2011:sedan
Suzuki:Verona:2004-2006:sedan
Suzuki:Vitara:1999-2004:suv
Suzuki:X-90:1996-1998:suv
Suzuki:XL7:2001-2009:crossover,suv
