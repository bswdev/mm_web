package com.boosed.mm.shared.db;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.Transient;

import com.boosed.mm.shared.db.enums.MessageType;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;
import com.googlecode.objectify.condition.IfFalse;
import com.googlecode.objectify.condition.IfNotNull;
import com.googlecode.objectify.condition.IfNull;
import com.googlecode.objectify.condition.IfTrue;

/**
 * Account for all users of the system.
 * 
 * @author dsumera
 * 
 */
@Unindexed
public class Account implements HasKey<Account>, Sanitizable, Serializable {

	@Id
	public String id;

	// display name (alphabetized)
	@Indexed
	@NotSaved(IfDefault.class)
	public String name = "";

	// email (from user service)
	@NotSaved(IfDefault.class)
	public String email = "";

	@SuppressWarnings("unused")
	@NotSaved(IfDefault.class)
	private String gmail = "";
	
	/** whether email is displayed with ad */
	@NotSaved(IfDefault.class)
	public Boolean showEmail = false;
	
	// by providing any of these fields you consent to disclosure
	
	// phone
	@NotSaved(IfDefault.class)
	public String phone = null;

	// address
	@NotSaved(IfDefault.class)
	public String address = null;

	// basic locale info for user
	//@NotSaved(IfDefault.class)
	//public Key<Location> location = null;
	@NotSaved(IfDefault.class)
	public String postal = null;

	/** account latitude */
	@NotSaved(IfDefault.class)
	public double latitude = 0;

	/** account longtidue */
	@NotSaved(IfDefault.class)
	public double longitude = 0;
	
	// web url
	@NotSaved(IfDefault.class)
	public String url = null;
	
	// end disclosure
	
	// cars (should we not keep these keys in account? or maybe serializable them?)
	//public Set<Key<Car>> selling = new LinkedHashSet<Key<Car>>();
	//public Set<Key<Car>> buying = new LinkedHashSet<Key<Car>>();

	/** dealer flag */
	@Indexed(IfTrue.class)
	@NotSaved(IfFalse.class)
	public Boolean dealer = false;

	// dealer location, always 5
	@Indexed(IfNotNull.class)
	@NotSaved(IfNull.class)
	public List<String> geocells = null;
	
	/** distance */
	@Transient
	public double distance;
	
	/** notification by email of messages received */
	private Set<MessageType> notify;

	/** check whether the account privileges revoked */
	@NotSaved(IfDefault.class)
	public Boolean enabled = true;
	
	public Long created;

	/** number of credits for ad purchases */
	@NotSaved(IfDefault.class)
	public Integer credits = 0;

	///** number of alerts created */
	//@NotSaved(IfDefault.class)
	//public Integer alerts = 0;

	/** number of unread messages */
	@NotSaved(IfDefault.class)
	public Integer count = 0;

	/** time of the last count update */
	@NotSaved(IfDefault.class)
	public Long update = 0L;
	
	public Account() {
		// default no-arg constructor
		created = System.currentTimeMillis();
		notify = new HashSet<MessageType>();
	}

	public Account(String id, String name, String email) {
		this();
		this.id = id;
		this.name = name;
		this.email = email;
		
		// the user's original email
		gmail = email;
	}

	@Override
	public Key<Account> getKey() {
		return new Key<Account>(Account.class, id);
	}

	public boolean getNotify(MessageType type) {
		return notify.contains(type);
	}
	
	public void setNotify(MessageType type, boolean enabled) {
		if (enabled)
			notify.add(type);
		else
			notify.remove(type);
	}
	
	@Override
	public void sanitize() {
		// scrub account info
		if (!showEmail)
			email = null;
		showEmail = null;

		// null out unnecessary account fields
		//id = null;
		count = null;
		created = null;
		credits = null;
		notify = null;
	}
}