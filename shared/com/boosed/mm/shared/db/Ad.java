package com.boosed.mm.shared.db;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;
import com.googlecode.objectify.condition.IfNotNull;

/**
 * Relation Index Entity for <code>Car</code>;
 * http://novyden.blogspot.com/2011/02
 * /efficient-keyword-search-with-relation.html.
 * 
 * @author dsumera
 */
@Unindexed
public class Ad implements HasKey<Ad>, Sanitizable, Serializable {

	@Id
	public Long id;

	public Key<Account> owner;

	/** stringified key for emailing details */
	public String key;
	
	public Key<Car> car;

	/** map of blobkeys to boolean/urls */
	@Serialized
	private Map<String, Tuple<Boolean, String>> images;

	/** the actual content of the ad */
	@NotSaved(IfDefault.class)
	private String content = null;

	/** the unapproved content of the ad */
	@NotSaved(IfDefault.class)
	private String pending = null;

	/* will not be indexed if edit is null */
	@NotSaved(IfDefault.class)
	@Indexed(IfNotNull.class)
	public Boolean edit = null;

	/** the key of the primary image */
	@NotSaved(IfDefault.class)
	public String imageKey = null;

	/** number of ad views */
	@NotSaved(IfDefault.class)
	public Integer views = 0;
	
	public Ad() {
		// default no-arg constructor
		images = new LinkedHashMap<String, Tuple<Boolean, String>>();
	}

	public Ad(Car car) {
		this();
		this.car = car.getKey();
		owner = car.owner;
	}

	// public void addPicture(String blobkey, String url) {
	// pics.put(blobkey, new Picture(url));
	// }

	public void approveContent() {
		content = pending;
		pending = null;
		// setEdit();
	}

	public void rejectContent() {
		pending = null;
		// setEdit();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		pending = content.trim();
		// setEdit();
	}

	public String getPending() {
		return pending;
	}

	// public void unapprove(String blobkey) {
	// //pics.get(blobkey).type = PictureType.UNAPPROVED;
	// approved.put(blobkey, unapproved.remove(blobkey));
	// }

	public void addImage(String blobkey, String url) {
		images.put(blobkey, new Tuple<Boolean, String>(false, url));
		// unapproved.put(blobkey, url);
		//edit = Boolean.TRUE;
	}

	public boolean removeImage(String blobkey) {
		// image to be removed is main ad image, do not permit removal
		if (blobkey.equals(imageKey))
			return false;

		images.remove(blobkey);
		// approved.remove(blobkey);
		// unapproved.remove(blobkey);
		// setEdit();

		return true;
	}

	public boolean approveImage(String blobkey) {
		// pics.get(blobkey).type = PictureType.APPROVED;
		Tuple<Boolean, String> image = images.get(blobkey);
		// String value = unapproved.remove(blobkey);
		if (image != null) {
			// approved.put(blobkey, value);
			image.a = true;
			// setEdit();
			return true;
		}

		return false;
	}

	public String getUrl(String blobkey) {
		Tuple<Boolean, String> image = images.get(blobkey);
		return image.a ? image.b : null;
		// return approved.get(blobkey);
	}

	/**
	 * Get a <code>List</code> of all image entries for the <code>Ad</code>.
	 * 
	 * @return <code>List</code> of entries containing the blobkey and
	 *         <code>Tuple</code> of approval and image url.
	 */
	public Set<Entry<String, Tuple<Boolean, String>>> getImages() {
		return images.entrySet();
	}

	public Map<String, String> getImages(boolean approved) {
		Map<String, String> rv = new LinkedHashMap<String, String>();
		for (Entry<String, Tuple<Boolean, String>> entry : images.entrySet())
			if (approved == entry.getValue().a)
				rv.put(entry.getKey(), entry.getValue().b);

		// return approved ? this.approved : unapproved;
		return rv;
	}

	// public void removePicture(String blobkey) {
	// pics.remove(blobkey);
	// }

	@Override
	public Key<Ad> getKey() {
		return new Key<Ad>(Ad.class, id);
	}

	@Override
	public void sanitize() {
		// null out fields
		id = null;

		// keep this since it is used to detect contiguous ads with the
		// same owner in the client
		// car = null;

		owner = null;
		key = null;
		edit = null;
		pending = null;
		imageKey = null;
		views = null;

		// remove all unapproved images
		for (Iterator<Entry<String, Tuple<Boolean, String>>> it = images.entrySet().iterator(); it.hasNext();) {
			if (!it.next().getValue().a)
				it.remove();
		}
	}

	/**
	 * Sets the edit status before being persisted to the datastore.
	 */
	@SuppressWarnings("unused")
	@PrePersist
	private void setEdit() {
		for (Tuple<Boolean, String> image : images.values())
			// an image is not approved
			if (!image.a) {
				edit = Boolean.TRUE;
				return;
			}

		// pending content exists
		edit = pending != null ? Boolean.TRUE : null;
	}
}