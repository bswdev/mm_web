package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * Entity type for a cg continent value.
 * 
 * @author dsumera
 */
@Cached(expirationSeconds=600)
@Unindexed
public class CLContinent implements HasKey<CLContinent>, Serializable {

	@Id
	public String name = null;

	public CLContinent() {
		// default no-arg constructor
	}

	public CLContinent(String name) {
		this.name = name;
	}

	@Override
	public Key<CLContinent> getKey() {
		return new Key<CLContinent>(CLContinent.class, name);
	}
}