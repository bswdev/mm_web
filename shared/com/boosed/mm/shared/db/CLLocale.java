package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;

/**
 * Entity type for a cg locale value.
 * 
 * @author dsumera
 */
@Cached(expirationSeconds=600)
@Unindexed
public class CLLocale implements HasKey<CLLocale>, Serializable {

	@Id
	public String name;

	@Parent
	public Key<CLState> state;

	@NotSaved(IfDefault.class)
	public String url = null;

	public CLLocale() {
		// default no-arg constructor
	}

	public CLLocale(Key<CLState> parent, String name, String url) {
		state = parent;
		this.name = name;
		this.url = url;
	}

	@Override
	public Key<CLLocale> getKey() {
		return new Key<CLLocale>(state, CLLocale.class, name);
	}
}