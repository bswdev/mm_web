package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * Entity type for a cg state value.
 * 
 * @author dsumera
 */
@Cached(expirationSeconds=600)
@Unindexed
public class CLState implements HasKey<CLState>, Serializable {

	@Id
	public String name;

	@Parent
	public Key<CLContinent> continent;

	public CLState() {
		// default no-arg constructor
	}

	public CLState(Key<CLContinent> parent, String name) {
		continent = parent;
		this.name = name;
	}

	@Override
	public Key<CLState> getKey() {
		return new Key<CLState>(continent, CLState.class, name);
	}
}