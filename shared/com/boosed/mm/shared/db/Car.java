package com.boosed.mm.shared.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.Transient;

import com.boosed.mm.shared.db.enums.AdNetwork;
import com.boosed.mm.shared.db.enums.AdType;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.MileageType;
import com.boosed.mm.shared.db.enums.PriceType;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;
import com.googlecode.objectify.condition.IfNotNull;

@Unindexed
public class Car implements Comparable<Car>, HasKey<Car>, HasCoords, Serializable {

	@Id
	public Long id;

	// sorted fields

	/**
	 * all search queries use this field; car will not be searchable unless it
	 * is paid for (time != null)
	 */
	@NotSaved(IfDefault.class)
	@Indexed(IfNotNull.class)
	public Long time = null;

	@Indexed
	public Integer mileage;

	@Indexed
	public Integer price;

	@Indexed
	public int year;

	// end sorted

	// filtered fields

	@Indexed
	public Set<MileageType> mileages;

	@Indexed
	public Set<PriceType> prices;

	@Indexed
	public List<ConditionType> conditions;

	// up to 3
	@Indexed
	public Set<Key<Type>> types;

	// always 5
	@Indexed
	public List<String> geocells;

	@Indexed
	public Key<Make> make;

	@Indexed
	public Key<Model> model;

	@Indexed
	public int priority = 0;

	// end filtered

	/** latitude of car */
	public double latitude = 0;

	/** longitude of car */
	public double longitude = 0;

	/** whether car's location has been approximated or not */
	@NotSaved(IfDefault.class)
	public boolean explicit = false;

	/** car's postal code, used to determine location */
	public String postal;

	/** display string for car's location */
	public String location;

	/** user/owner of this vehicle */
	public Key<Account> owner;

	// fields

	public int doors;

	public Key<Engine> engine;

	public Key<Transmission> transmission;

	public Key<Drivetrain> drivetrain;

	public Key<Color> interior;

	public Key<Color> exterior;

	public Key<Ad> ad;

	// optional

	@NotSaved(IfDefault.class)
	public String vin = null;

	@NotSaved(IfDefault.class)
	public String trim = null;

	@NotSaved(IfDefault.class)
	public String stock = "";

	// end fields

	/** this is the default blank image; change this if image changes */
	@NotSaved(IfDefault.class)
	public String image = "http://lh5.ggpht.com/tqi4BRBbb8PCEBt02OAZSNO7v39eI0KBD3ganjozE6BlG5XHr4AlioyMYIvzQZj3G2L8ilW3iL151ORhqTHlOh12";

	/** type of ad associated with car */
	@NotSaved(IfDefault.class)
	public AdType adType = AdType.BASIC;

	/** other ad networks where this car is posted */
	private Set<AdNetwork> networks;

	/** whether the ad has been activated before */
	@NotSaved(IfDefault.class)
	private boolean active = false;

	/** used when search is performed wrt location and radius */
	@Transient
	public double distance;

	public Car() {
		// default no-arg constructor
		types = new HashSet<Key<Type>>();
		mileages = new HashSet<MileageType>();
		prices = new HashSet<PriceType>();
		conditions = new ArrayList<ConditionType>();
		networks = new HashSet<AdNetwork>();
	}

	/**
	 * Determine whether the car is active or not. Once set, it is always true.
	 * 
	 * @return
	 */
	public boolean isActive() {
		// return time != null;
		return active;
	}

	/**
	 * Activation also causes time to be updated. Once a <code>Car</code> has
	 * been activated, it is always activated.
	 * 
	 * @param active
	 */
	public void setActive(boolean active) {
		// once activated, always activated
		this.active |= active;
		time = active ? System.currentTimeMillis() : null;
	}

	@Override
	public Key<Car> getKey() {
		return new Key<Car>(Car.class, id);
	}

	@Override
	public double getLatitude() {
		return latitude;
	}

	@Override
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Return whether <code>Car</code> has been published to the given
	 * <code>AdNetwork</code>.
	 * 
	 * @param network
	 * @return
	 */
	public boolean getNetwork(AdNetwork network) {
		return networks.contains(network);
	}

	/**
	 * Mark or unmark the <code>Car</code> with the specified
	 * <code>AdNetwork</code>.
	 * 
	 * @param network
	 * @param enabled
	 */
	public void setNetwork(AdNetwork network, boolean enabled) {
		if (enabled)
			networks.add(network);
		else
			networks.remove(network);
		// if (mark)
		// this.state |= (1 << state.ordinal());
		// else
		// this.state &= ~(1 << state.ordinal());
	}

	@Override
	public int compareTo(Car car) {
		return new Long(car.time == null ? 0 : car.time).compareTo(new Long(time == null ? 0 : time));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	/**
	 * Provides make/model & trim of this vehicle; does not include the year.
	 */
	public String toString() {
		return model.getName() + (trim == null ? "" : (" " + trim));
	}
}