package com.boosed.mm.shared.db;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * Relation Index Entity for <code>Car</code>;
 * http://novyden.blogspot.com/2011/02
 * /efficient-keyword-search-with-relation.html.
 * 
 * @author dsumera
 */
@Unindexed
public class CarTypes {

	@Id
	Long id;

	@Parent
	public Key<Car> car;

	@Indexed
	public Set<Key<Type>> types = new HashSet<Key<Type>>();

	public CarTypes() {
		// default no-arg constructor
	}

	public CarTypes(Key<Car> car, Set<Key<Type>> types) {
		super();
		this.car = car;
		this.types = types;
	}
}