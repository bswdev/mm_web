package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

@Cached(expirationSeconds=600)
@Unindexed
public class Color implements HasKey<Color>, Serializable {

	@Id
	public String desc;

	public Color() {
		// default no-arg constructor
	}

	public Color(String desc) {
		this.desc = desc;
	}

	public Key<Color> getKey() {
		return new Key<Color>(Color.class, desc);
	}
}