package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

@Cached(expirationSeconds=600)
@Unindexed
public class Drivetrain implements HasKey<Drivetrain>, Serializable {

	// FWD, RWD, AWD, 4WD

	@Id
	public String desc;

	public Drivetrain() {
		// default no-arg constructor
	}

	public Drivetrain(String desc) {
		this.desc = desc;
	}

	public Key<Drivetrain> getKey() {
		return new Key<Drivetrain>(Drivetrain.class, desc);
	}
}