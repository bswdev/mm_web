package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

@Cached(expirationSeconds=600)
@Unindexed
public class Engine implements HasKey<Engine>, Serializable {

	@Id
	public String desc;

	public Engine() {
		// default no-arg constructor
	}

	public Engine(String desc) {
		this.desc = desc;
	}

	public Key<Engine> getKey() {
		return new Key<Engine>(Engine.class, desc);
	}
}