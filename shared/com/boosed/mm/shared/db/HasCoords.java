package com.boosed.mm.shared.db;

/**
 * Interface for coordinate-based entities
 * 
 * @author dsumera
 */
public interface HasCoords {

	public double getLatitude();

	public double getLongitude();
}