package com.boosed.mm.shared.db;

import com.googlecode.objectify.Key;

public interface HasKey<T> {

	public Key<T> getKey();
}
