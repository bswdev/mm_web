package com.boosed.mm.shared.db;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

@Cached(expirationSeconds=600)
@Unindexed
public class Location implements Comparable<Location>, HasKey<Location>, HasCoords, Serializable {

	@Id
	public String postal;

	public double latitude;

	public double longitude;

	public String city;

	public Key<State> state;
	
	public List<String> geocells;

	public Location() {
		// default no-arg constructor
	}

	public Location(String postal, double latitude, double longitude) {
		super();
		this.postal = postal;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Location(String postal, double latitude, double longitude, List<String> geocells) {
		this(postal, latitude, longitude);
		this.geocells = geocells;
	}

	@Override
	public int compareTo(Location location) {
		return getKey().compareTo(location.getKey());
	}

	@Override
	public Key<Location> getKey() {
		return new Key<Location>(Location.class, postal);
	}

	@Override
	public double getLatitude() {
		return latitude;
	}

	@Override
	public double getLongitude() {
		return longitude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((postal == null) ? 0 : postal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (postal == null) {
			if (other.postal != null)
				return false;
		} else if (!postal.equals(other.postal))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return city + ", " + state.getName();
		//return postal;
	}
}