package com.boosed.mm.shared.db;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class Make implements Comparable<Make>, HasKey<Make>, Serializable {

	@Id
	public String desc;

	/** type (& make) to ad total; note some cars may have more than one type */
	@Serialized
	public Map<Key<Type>, Integer> totals;

	/** total number of ads with this make */
	@Indexed
	public int total = 0;

	public Make() {
		totals = new HashMap<Key<Type>, Integer>();
	}

	public Make(String desc) {
		this();
		this.desc = desc;
	}

	public Key<Make> getKey() {
		return new Key<Make>(Make.class, desc);
	}

	@Override
	public int compareTo(Make make) {
		return getKey().compareTo(make.getKey());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Make other = (Make) obj;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		return true;
	}

	public String getCount() {
		return desc + " (" + total + ")";
	}

	public String getCount(Key<Type> type) {
		if (type == null)
			return getCount();

		Integer t = totals.get(type);
		return desc + " (" + (t == null ? total : t) + ")";
	}

	@Override
	public String toString() {
		return desc;
	}
}