package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.boosed.mm.shared.db.enums.MarkerType;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;

/**
 * Relation index entity for <code>Account</code>. Reduces key fetches on
 * <code>Account</code>. Represents a <code>Car</code> a user has bookmarked.
 * 
 * @author dsumera
 */
@Unindexed
public class Marker implements HasKey<Marker>, Serializable {

	@Id
	public Long id;

	//@Indexed
	@Parent
	public Key<Car> car;
	
	@Indexed
	public Key<Account> account;

	@Indexed
	public MarkerType type;

	// sorted

	@Indexed
	public long time;
	
	@Indexed
	public int year;
	
	@Indexed
	public String model;
	
	@Indexed
	public int price = 0;
	
	// price alert fields
	
	@NotSaved(IfDefault.class)
	public long created = 0;
	
	//@Indexed(IfNotNull.class)
	@NotSaved(IfDefault.class)
	public boolean active = false;
	
	@NotSaved(IfDefault.class)
	public int point = 0;

	@NotSaved(IfDefault.class)
	public int delta = 0;
	
	public Marker() {
		// default no-arg constructor
		time = System.currentTimeMillis();
	}

	public Marker(Key<Account> account, Car car, Model model, MarkerType type) {
		this();
		this.account = account;
		this.car = car.getKey();
		this.type = type;
		
		// set price alert variables
		if (type == MarkerType.PRICE) {
			active = true;
			created = time;
		}
		
		// sorts
		price = car.price;
		year = car.year;
		this.model = model.toString();
	}

	@Override
	public Key<Marker> getKey() {
		return new Key<Marker>(car, Marker.class, id);
	}
}