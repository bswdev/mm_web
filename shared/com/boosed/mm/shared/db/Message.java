package com.boosed.mm.shared.db;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.persistence.Id;

import com.boosed.mm.shared.db.enums.MessageState;
import com.boosed.mm.shared.db.enums.MessageType;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;
import com.googlecode.objectify.condition.IfNotNull;
import com.googlecode.objectify.condition.IfNull;

@Unindexed
public class Message implements Comparable<Message>, Comparator<Long>, HasKey<Message>, Serializable {

	@Id
	public Long id;

	@NotSaved(IfNull.class)
	// @Indexed(IfNotNull.class)
	public Key<Account> sender = null;

	@NotSaved(IfDefault.class)
	public String senderName = "system";

	@Indexed
	public Key<Account> recipient;

	@NotSaved(IfDefault.class)
	public String recipientName = "";

	/** seller/owner */
	@NotSaved(IfDefault.class)
	@Indexed(IfNotNull.class)
	public Key<Account> owner = null;

	/** buyer/customer */
	@NotSaved(IfDefault.class)
	@Indexed(IfNotNull.class)
	public Key<Account> customer = null;
	
	/** sorted on time */
	@Indexed
	public long time;

	/** index to reference */
	@Indexed(IfNotNull.class)
	public Key<Car> reference = null;

	/** year, make & model of vehicle (e.g., "2008 VW Rabbit") */
	@NotSaved(IfDefault.class)
	public String referenceName = "";

	/** stock & vin description */
	@NotSaved(IfDefault.class)
	public String identifier = null;

	// @NotSaved(IfDefault.class)
	// public String message = null;
	@Serialized
	public Map<Long, String> messages;

	/** the state of the message */
	private Set<MessageState> state;
	//public int state = 0;

	/** message to delete if current message is sent a response */
	@NotSaved(IfDefault.class)
	@Indexed(IfNotNull.class)
	public Long responseTo = null;

	/** the number of inquiry/response cycles */
	private int count = 1;

	public static final char PRE_SND = (char) 175;
	public static final char PRE_RCP = (char) 174;

	public Message() {
		messages = new TreeMap<Long, String>(this);
		state = new HashSet<MessageState>();
		// default no-arg constructor
		// time = System.currentTimeMillis();
		//messages = new TreeMap<Long, String>(this);// new LinkedHashMap<Long,
													// String>();
	}

	// /**
	// * Default private constructor.
	// *
	// * @param sender
	// * @param recipient
	// * @param message
	// */
	// private Message(Account sender, Account recipient, String message) {
	// this();
	//
	// // check if sender != null for non-system messages
	// if (sender != null) {
	// this.sender = sender.getKey();
	// this.senderName = sender.name;
	// } else
	// // this is a system/admin message
	// this.senderName = "admin";
	//
	// this.recipient = recipient.getKey();
	// this.recipientName = recipient.name;
	// this.message = message;
	// }

	/**
	 * Constructor for alerts.
	 * 
	 * @param sender
	 * @param recipient
	 * @param alert
	 * @param reference
	 */
	public Message(Key<Account> recipient, Car reference, String alert) {
		this();

		this.recipient = recipient;
		// this.recipientName = recipient.name;
		addMessage(alert, null);
		this.reference = reference.getKey();

		// set the referenceName/subject
		StringBuilder sb = new StringBuilder();
		sb.append(reference.year).append(" ").append(/*reference.model.getName()*/reference.toString());
		referenceName = sb.toString();

		sb = new StringBuilder();
		sb.append("st: ").append(reference.stock.equals("") ? "n/a" : reference.stock);
		sb.append(", vin: ").append(reference.vin == null ? "n/a" : reference.vin);
		identifier = sb.toString();
	}

	/**
	 * Constructor for an inquiry.
	 * 
	 * @param sender
	 * @param recipient
	 * @param inquiry
	 * @param reference
	 */
	public Message(Account sender, Car reference, String inquiry) {
		this(reference.owner, reference, inquiry);

		// check if sender != null for non-system messages
		// if (sender != null) {
		// set the sender info
		this.sender = sender.getKey();
		senderName = sender.name;
		// this.referenceName = "inquiry: " + reference.year + " "
		// + reference.model.getName();
		// }
		// else {
		// // this is a system/admin message
		// this.referenceName = "alert: " + reference.year + " "
		// + reference.model.getName();
		// }

		// set the owner & customer
		owner = /* this.recipient */reference.owner;
		customer = this.sender;
	}

	/**
	 * Constructor for a reply to an original <code>Message</code>.
	 * 
	 * @param original
	 * @param message
	 */
	public Message(Account sender, Message original) {
		this();

		// set the sender as the original recipient
		this.sender = sender.getKey();
		senderName = /* original.recipientName */sender.name;

		// set the recipient as the original sender
		recipient = original.sender;
		// this.recipientName = /*original.senderName*/recipientName;
		recipientName = original.senderName;

		// set the original parameters
		identifier = original.identifier;
		owner = original.owner;
		customer = original.customer;
		reference = original.reference;
		referenceName = original.referenceName;
		// referenceName = (owner.equals(recipient) ? "inquiry: " : "reply: ")
		// + original.referenceName;

		// set the thread count
		count = original.count;

		// increment count if the recipient is the owner
		if (owner.equals(recipient))
			count++;

		// copy the messages
		// message = reply + "\r\n\r\n" + /*recipientName*/original.senderName +
		// " wrote:" + "\r\n" + original.message;
		// messages.putAll(original.messages);
		copy(original);

		// set the message to delete if current one is responded to
		responseTo = original.id;
	}

	// /**
	// * Sets the appopriate <code>target</code> according to the current user's
	// * id.
	// *
	// * @param accountId
	// */
	// public void setTarget(String accountId) {
	//
	// boolean isRecipient = recipient.getName().equals(accountId);
	//
	// // reset the target
	// owner = isRecipient ? sender : recipient;
	//
	// // set the reference name
	// referenceName = (isRecipient ? "re: " : "inq: ")
	// + referenceName.substring(referenceName.indexOf(":") + 1);
	//
	// // set read status to false
	// read = false;
	// }

	/**
	 * Typically looks like "inquiry (2): 2008 VW Rabbit"
	 */
	public String getReference() {
		switch (getType()) {
		case ALERTS:
			// no sender, this is a system message
			return "alert: " + referenceName;
		case INQUIRIES:
			// owner is recipient
			return "inquiry" + (count > 1 ? (" (" + count + "): ") : ": ") + referenceName;
		case RESPONSES:
			// owner is sender
			return "reply" + (count > 1 ? (" (" + count + "): ") : ": ") + referenceName;
		default:
			return "";
		}
	}

	public MessageType getType() {
		if (sender == null)
			// no sender, this is a system message
			return MessageType.ALERTS;
		else if (recipient.equals(owner))
			// owner is recipient
			return MessageType.INQUIRIES;
		else
			// owner is sender
			return MessageType.RESPONSES;
	}

	/**
	 * Return whether the <code>MessageState</code> is marked or not.
	 * @param state
	 * @return
	 */
	public boolean getState(MessageState state) {
		//return (this.state & (1 << state.ordinal())) > 0;
		return this.state.contains(state);
	}
	
	/**
	 * Mark or unmark the <code>Message</code> with the specified
	 * <code>MessageState</code>.
	 * 
	 * @param state
	 * @param enabled
	 */
	public void setState(MessageState state, boolean enabled) {
		if (enabled)
			this.state.add(state);
		else
			this.state.remove(state);
//		if (mark)
//			this.state |= (1 << state.ordinal());
//		else
//			this.state &= ~(1 << state.ordinal());
	}

	public void addMessage(String message, Key<Account> sender) {
		if (sender == null || sender.equals(this.sender))
			message = PRE_SND + message;
		else
			message = PRE_RCP + message;

		// update the time
		time = System.currentTimeMillis();

		// mark unread
		setState(MessageState.READ, false);

		// queue the message
		messages.put(time, message);
	}

	public Key<Message> getKey() {
		return new Key<Message>(Message.class, id);
	}

	@Override
	public int compare(Long o1, Long o2) {
		// store in reverse order
		return o2.compareTo(o1);
	}

	@Override
	public int compareTo(Message message) {
		return getKey().compareTo(message.getKey());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * Switch the recipient and sender for all messages copied from original.
	 * 
	 * @param original
	 */
	private void copy(Message original) {
		for (Entry<Long, String> entry : original.messages.entrySet()) {
			String msg = entry.getValue();
			if (msg.charAt(0) == PRE_RCP)
				messages.put(entry.getKey(), PRE_SND + msg.substring(1));
			else
				messages.put(entry.getKey(), PRE_RCP + msg.substring(1));
		}
	}
}