package com.boosed.mm.shared.db;

import java.io.Serializable;
import java.util.List;

public class MobResult implements Serializable {

	// Tuple<Make, Tuple<Car, Tuple<Key<Model>, List<Model>>>>

	public List<Type> types;
	
	public List<Make> makes;

	public List<Car> cars;

	public List<Model> models;

	public MobResult() {
		// default no-arg constructor
	}

	public MobResult(List<Type> types, List<Make> makes, List<Car> cars, List<Model> models) {
		super();
		this.types = types;
		this.makes = makes;
		this.cars = cars;
		//this.selected = selected;
		this.models = models;
	}
}
