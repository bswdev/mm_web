package com.boosed.mm.shared.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class Model implements Comparable<Model>, HasKey<Model>, Serializable {

	@Id
	public String desc;

	@Indexed
	public Key<Make> make;

	@Indexed
	public int total = 0;

	// model types (hatchback, coupe, etc.)
	@Indexed
	public Set<Key<Type>> types;

	// model years
	public List<Integer> years;

	// doors
	public List<Integer> doors;

	// drivetrains
	public List<Key<Drivetrain>> drivetrains;

	// engines
	public List<Key<Engine>> engines;

	// transmissions
	public List<Key<Transmission>> transmissions;

	public Model() {
		// default no-arg constructor
		types = new HashSet<Key<Type>>();
		years = new ArrayList<Integer>();
		doors = new ArrayList<Integer>();
		drivetrains = new ArrayList<Key<Drivetrain>>();
		engines = new ArrayList<Key<Engine>>();
		transmissions = new ArrayList<Key<Transmission>>();
	}

	// public Model(String desc, Key<Make> make) {
	// this();
	// this.desc = desc;
	// this.make = make;
	// }

	public Model(String desc) {
		this();
		this.desc = desc;
	}

	// public void addTypes(List<Key<Type>> types) {
	// this.types.addAll(types);
	// }
	//
	// public void addTypes(Key<Type>... types) {
	// addTypes(Arrays.asList(types));
	// }
	//
	// public void addYear(int year) {
	// if (!years.contains(year)) {
	// years.add(year);
	// Collections.sort(years);
	// }
	// }

	public Key<Model> getKey() {
		return new Key<Model>(Model.class, desc);
	}

	@Override
	public int compareTo(Model model) {
		// return getKey().compareTo(model.getKey());
		return desc.compareTo(model.desc);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((make == null) ? 0 : make.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Model other = (Model) obj;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		if (make == null) {
			if (other.make != null)
				return false;
		} else if (!make.equals(other.make))
			return false;
		return true;
	}

	public String getCount() {
		return toString() + " (" + total + ")";
	}

	/**
	 * Returns the name of the <code>Model</code> without the <code>Make</code>
	 * prefix.
	 */
	@Override
	public String toString() {
		return desc.replaceFirst(make.getName(), "").substring(1);
	}
}