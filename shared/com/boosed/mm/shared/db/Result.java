package com.boosed.mm.shared.db;

import java.io.Serializable;

/**
 * A result with an attached <code>Cursor</code> for paged fetching.
 * 
 * @author dsumera
 * 
 * @param <T>
 */
public class Result<T> implements Serializable {

	public T result;

	public String cursor;

	public Result() {
		// default no-arg constructor
	}

	public Result(T result) {
		super();
		this.result = result;
	}

	public Result(T result, String cursor) {
		super();
		this.result = result;
		this.cursor = cursor;
	}
}