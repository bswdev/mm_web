package com.boosed.mm.shared.db;

/**
 * Interface for objects that potentially has important data that can be
 * nullified before presentation.
 * 
 * @author dsumera
 */
public interface Sanitizable {

	/**
	 * Clear out any privacy data.
	 */
	public void sanitize();
}
