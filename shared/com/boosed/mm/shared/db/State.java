package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

@Cached(expirationSeconds=600)
@Unindexed
public class State implements Comparable<State>, HasKey<State>, Serializable {

	@Id
	public String abbr;

	public String state;

	public State() {
		// default no-arg constructor
	}

	public State(String abbr, String state) {
		super();
		this.abbr = abbr;
		this.state = state;
	}

	@Override
	public int compareTo(State location) {
		return getKey().compareTo(location.getKey());
	}

	@Override
	public Key<State> getKey() {
		return new Key<State>(State.class, abbr);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((abbr == null) ? 0 : abbr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (abbr == null) {
			if (other.abbr != null)
				return false;
		} else if (!abbr.equals(other.abbr))
			return false;
		return true;
	}
}