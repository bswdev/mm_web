package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * Track transactions against a user's <code>Account</code>.
 * 
 * @author dsumera
 */
@Unindexed
public class Transaction implements HasKey<Transaction>, Serializable {

	@Id
	public String id;

	public Transaction() {
		// default no-arg constructor
	}

	public Transaction(String id) {
		this.id = id;
	}

	public Key<Transaction> getKey() {
		return new Key<Transaction>(Transaction.class, id);
	}
}