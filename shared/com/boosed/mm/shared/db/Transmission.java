package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

@Cached(expirationSeconds=600)
@Unindexed
public class Transmission implements HasKey<Transmission>, Serializable {

	@Id
	public String desc;

	public Transmission() {
		// default no-arg constructor
	}

	public Transmission(String desc) {
		this.desc = desc;
	}

	public Key<Transmission> getKey() {
		return new Key<Transmission>(Transmission.class, desc);
	}
}