package com.boosed.mm.shared.db;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Transient;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

@Cached(expirationSeconds=600)
@Unindexed
public class Type implements Comparable<Type>, HasKey<Type>, Serializable {

	// suv, sedan, truck, van/minivan, convertible, coupe, wagon, luxury,
	// hybrid/electric, diesel, crossover, hatchback

	@Id
	public String desc;

	@Transient
	public int total;

	public Type() {
		// default no-arg constructor
	}

	public Type(String desc) {
		this.desc = desc;
	}

	public Key<Type> getKey() {
		return new Key<Type>(Type.class, desc);
	}

	@Override
	public int compareTo(Type type) {
		return desc.compareTo(type.desc);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Type other = (Type) obj;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		return true;
	}

	public String getCount() {
		return desc + " (" + total + ")";
	}

	@Override
	public String toString() {
		return desc;
	}
}