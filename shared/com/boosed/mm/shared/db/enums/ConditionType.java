package com.boosed.mm.shared.db.enums;

public enum ConditionType {

	// NEW, CERTIFIED, USED, YEAR5, YEAR10, YEAR15, YEAR20, ANY;
	ANY, NEW, CERTIFIED, USED, YEAR5, YEAR10;

	private static final int[] CONDITIONS = new int[] { Integer.MAX_VALUE /*-1*/, -1, -1, -1, 5, 10 };

	public int value() {
		return CONDITIONS[ordinal()];
	}

	public boolean isPrimary() {
		switch (this) {
		case NEW:
		case CERTIFIED:
		case USED:
			return true;
		default:
			return false;
		}
	}

	@Override
	public String toString() {
		int ordinal = ordinal();
		switch (ordinal) {
		case 0:
			return "any condition";
		case 1:
			return "new";
		case 2:
			return "certified";
		case 3:
			return "used";
		default:
			return CONDITIONS[ordinal] + " years or newer";
		}
	}
}