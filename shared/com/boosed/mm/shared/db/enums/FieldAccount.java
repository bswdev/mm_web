package com.boosed.mm.shared.db.enums;

public enum FieldAccount {

	// CAR_SELL, CAR_BUY, CAR_REMOVE
	NAME, EMAIL, EML_SHOW, NOTIFY_ALT, NOTIFY_INQ, NOTIFY_RES, PHONE, ADDRESS, LOCATION, URL, DEALER, LATITUDE, LONGITUDE
}
