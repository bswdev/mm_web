package com.boosed.mm.shared.db.enums;

/**
 * Fields used to mark changes back to the database.
 * 
 * @author dsumera
 * 
 */
public enum FieldAd {

	/**
	 * ID_AD - of ad
	 * ID_CAR - id of car
	 * IMG_DELETE - delete photo
	 * IMG_CAR - set photo on car/ad
	 * IMG_APPROVE - approve image
	 * IMG_REJECT - reject an image
	 * AD - content of ad
	 * AD_APPROVE - approve ad
	 * AD_REJECT - reject ad
	 */
	ID_AD, ID_CAR, IMG_CAR, IMG_APPROVE, IMG_DELETE, IMG_REJECT, AD, AD_APPROVE, AD_REJECT
}
