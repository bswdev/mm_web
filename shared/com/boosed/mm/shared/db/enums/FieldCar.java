package com.boosed.mm.shared.db.enums;

/**
 * Fields used to mark changes back to the database.
 * 
 * @author dsumera
 * 
 */
public enum FieldCar {

	ID, MILEAGE, PRICE, YEAR, CONDITION, MAKE, MODEL, LOCATION, TRIM, DOORS, ENGINE, TRANSMISSION, DRIVETRAIN, INTERIOR, EXTERIOR, LATITUDE, LONGITUDE, STOCK, VIN, AD_TYPE
}
