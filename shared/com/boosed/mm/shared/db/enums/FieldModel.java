package com.boosed.mm.shared.db.enums;

/**
 * Fields used to mark <code>Model</code> changes back to the database.
 * 
 * @author dsumera
 * 
 */
public enum FieldModel {

	ID, MAKE, TYPES, YEARS, DOORS, DRIVETRAINS, ENGINES, TRANSMISSIONS
}
