package com.boosed.mm.shared.db.enums;

/**
 * Type of <code>Marker</code> used to annotate a <code>Car</code> wrt an
 * <code>Account</code>.
 * 
 * @author dsumera
 */
public enum MarkerType {

	BOOKMARK, STOCK, PRICE
}
