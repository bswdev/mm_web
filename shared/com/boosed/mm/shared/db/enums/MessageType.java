package com.boosed.mm.shared.db.enums;

public enum MessageType {

	INQUIRIES, RESPONSES, ALERTS, ALL
}
