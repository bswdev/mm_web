package com.boosed.mm.shared.db.enums;

public enum MileageType {

	// MILE15K, MILE30K, MILE45K, MILE60K, MILE75K, MILE100K, ANY;
	ANY, MILE15K, MILE30K, MILE50K, MILE80K;

	private static final int[] MILEAGES = new int[] { Integer.MAX_VALUE, 15000, 30000, 50000, 80000 };

	public int value() {
		return MILEAGES[ordinal()];
		
	}
	@Override
	public String toString() {
		int ordinal = ordinal();

		//return (ordinal < 6) ? ("up to " + MILEAGES[ordinal] + " miles") : "any mileage";
		return ordinal == 0 ? "any mileage" : ("up to " + MILEAGES[ordinal] + " miles");
	}
}