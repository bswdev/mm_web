package com.boosed.mm.shared.db.enums;

public enum PriceType {

	// PRICE15K, PRICE25K, PRICE35K, PRICE45K, PRICE55K, PRICE85K, ANY;
	ANY, PRICE15K, PRICE25K, PRICE40K, PRICE60K;

	private static final int[] PRICES = new int[] { Integer.MAX_VALUE, 15000, 25000, 40000, 60000 };

	public int value() {
		return PRICES[ordinal()];
	}

	@Override
	public String toString() {
		int ordinal = ordinal();

		// return ordinal < 6 ? ("up to $" + PRICES[ordinal]) : "any price";
		return ordinal == 0 ? "any price" : ("up to $" + PRICES[ordinal]);
	}
}