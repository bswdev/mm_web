package com.boosed.mm.shared.db.enums;

/**
 * Fields used to indicate filter type.
 * 
 * @author dsumera
 * 
 */
public enum SortType {

	NONE, LOCATION, MILEAGE, MILEAGE_DESC, MODEL, MODEL_DESC, PRICE, PRICE_DESC, TIME, TIME_DESC, YEAR, YEAR_DESC, NAME, NAME_DESC;

	@Override
	public String toString() {
		switch (this) {
		case MILEAGE:
			return "mileage";
		case MILEAGE_DESC:
			return "-mileage";
		case MODEL:
			return "model";
		case MODEL_DESC:
			return "-model";
		case PRICE:
			return "price";
		case PRICE_DESC:
			return "-price";
		case YEAR:
			return "year";
		case YEAR_DESC:
			return "-year";
		case TIME:
			return "time";
		case TIME_DESC:
			return "-time";
		case NAME:
			return "name";
		case NAME_DESC:
			return "-name";
		case LOCATION:
		case NONE:
			// doesn't do any kind of real sort, just a placeholder
			return "";
		default:
			return super.toString();
		}
	}

//	public String getProperty() {
//		switch (this) {
//		case MILEAGE:
//		case MILEAGE_DESC:
//			return "mileage";
//		case PRICE:
//		case PRICE_DESC:
//			return "price";
//		case YEAR:
//		case YEAR_DESC:
//			return "year";
//		case NONE:
//		default:
//			return super.toString();
//		}
//	}
}