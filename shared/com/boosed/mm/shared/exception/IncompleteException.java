package com.boosed.mm.shared.exception;

/**
 * Exception thrown when a user edit is incomplete.
 * 
 * @author dsumera
 */
public class IncompleteException extends RemoteServiceFailureException {

	public IncompleteException() {
		// default no-arg constructor
	}
}
