package com.boosed.mm.shared.exception;

/**
 * Exception if user does not have sufficient credit.
 * 
 * @author dsumera
 */
public class InsufficientCreditException extends RemoteServiceFailureException {

	public InsufficientCreditException() {
		// default no-arg constructor
	}
}
