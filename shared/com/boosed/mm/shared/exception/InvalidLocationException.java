package com.boosed.mm.shared.exception;

/**
 * Exception thrown when postal code cannot be resolved.
 * 
 * @author dsumera
 */
public class InvalidLocationException extends RemoteServiceFailureException {

	public InvalidLocationException() {
		// default no-arg constructor
	}
}
