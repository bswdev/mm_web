package com.boosed.mm.shared.exception;

/**
 * Exception thrown when setting price point for <code>PriceAlert</code> already
 * triggers alert condition.
 * 
 * @author dsumera
 */
public class InvalidPriceException extends RemoteServiceFailureException {

	public InvalidPriceException() {
		// default no-arg constructor
	}
}
