package com.boosed.mm.shared.exception;

/**
 * Exception thrown when a <code>Message</code> cannot be replied to.
 * 
 * @author dsumera
 */
public class InvalidRecipientException extends RemoteServiceFailureException {

	public InvalidRecipientException() {
		// default no-arg constructor
	}
}
