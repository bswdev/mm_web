package com.boosed.mm.shared.exception;

/**
 * Throw whenever a redundant operation should not be run.
 * 
 * @author dsumera
 */
public class NoOpException extends RemoteServiceFailureException {

	public NoOpException() {
		// default no-arg constructor
	}
}
