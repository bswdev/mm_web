package com.boosed.mm.shared.exception;

/**
 * Exception thrown when an entity does not exist for the desired query.
 * 
 * @author dsumera
 */
public class NotFoundException extends RemoteServiceFailureException {

	public NotFoundException() {
		// default no-arg constructor
	}
}
