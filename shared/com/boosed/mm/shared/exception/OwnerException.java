package com.boosed.mm.shared.exception;

/**
 * Exception when an action is taken against own inventory.
 * 
 * @author dsumera
 */
public class OwnerException extends RemoteServiceFailureException {

	public OwnerException() {
		// default no-arg constructor
	}
}
