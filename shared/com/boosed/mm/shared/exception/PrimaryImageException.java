package com.boosed.mm.shared.exception;

/**
 * Exception thrown when a user attempts to delete the <code>Ad</code>'s primary
 * image.
 * 
 * @author dsumera
 */
public class PrimaryImageException extends RemoteServiceFailureException {

	public PrimaryImageException() {
		// default no-arg constructor
	}
}
