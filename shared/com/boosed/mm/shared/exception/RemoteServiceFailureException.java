package com.boosed.mm.shared.exception;

/**
 * Generic base exception for all system generated errors.
 * 
 * @author dsumera
 */
public class RemoteServiceFailureException extends Exception {

	public RemoteServiceFailureException() {
		// default no-arg constructor
	}

	public RemoteServiceFailureException(Throwable throwable) {
		super(throwable);
	}
}
