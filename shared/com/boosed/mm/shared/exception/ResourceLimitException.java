package com.boosed.mm.shared.exception;

/**
 * Exception thrown when a user exceeds alloted resources.
 * 
 * @author dsumera
 */
public class ResourceLimitException extends RemoteServiceFailureException {

	public ResourceLimitException() {
		// default no-arg constructor
	}
}
