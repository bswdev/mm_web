package com.boosed.mm.shared.exception;

/**
 * Exception thrown when user's privileges have been restricted.
 * 
 * @author dsumera
 */
public class RestrictedException extends RemoteServiceFailureException {

	public RestrictedException() {
		// default no-arg constructor
	}
}
