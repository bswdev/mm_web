package com.boosed.mm.shared.exception;

/**
 * Exception for an expired user session.
 * 
 * @author dsumera
 */
public class SessionTimeoutException extends RemoteServiceFailureException {

	public SessionTimeoutException() {
		// default no-arg constructor
	}
}
