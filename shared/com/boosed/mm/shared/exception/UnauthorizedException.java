package com.boosed.mm.shared.exception;

/**
 * Exception thrown when user attempts an invalid action against another user's
 * items.
 * 
 * @author dsumera
 */
public class UnauthorizedException extends RemoteServiceFailureException {

	/**
	 * Invalid action against another user's item(s).
	 */
	public UnauthorizedException() {
		// default no-arg constructor
	}
}
