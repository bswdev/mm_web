package com.boosed.mm.shared.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.CLLocale;
import com.boosed.mm.shared.db.CLState;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Color;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.db.MobResult;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.FieldAccount;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.boosed.mm.shared.db.enums.FieldCar;
import com.boosed.mm.shared.db.enums.FieldModel;
import com.boosed.mm.shared.db.enums.MarkerType;
import com.boosed.mm.shared.db.enums.MessageState;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.db.enums.MileageType;
import com.boosed.mm.shared.db.enums.PriceType;
import com.boosed.mm.shared.db.enums.SortType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.googlecode.objectify.Key;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("rpc")
public interface MobimotosRpcService extends RemoteService {

	// EDIT LOADS

	// /**
	// * Load the <code>Model</code> and <code>Ad</code> for the given ids.
	// *
	// * @param modelId
	// * @param adId
	// * @throws RemoteServiceFailureException
	// */
	// Tuple<Model, Ad> loadEdit(String modelId, long adId) throws
	// RemoteServiceFailureException;

	// Tuple<Model, Ad> loadEdit(Key<Model> model, Key<Ad> ad) throws
	// RemoteServiceFailureException;

	// /**
	// * Populate the cg values.
	// */
	// void populate();

	/**
	 * Load all possible (US) states for craigslist.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	List<Key<CLState>> loadStates() throws RemoteServiceFailureException;

	/**
	 * Load all locales associated with the given state.
	 * 
	 * @param state
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	List<Key<CLLocale>> loadLocales(Key<CLState> state) throws RemoteServiceFailureException;

	/**
	 * Load all the edits at once.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Map<FieldCar, List<?>> loadEdits() throws RemoteServiceFailureException;

	/**
	 * Load a <code>Location</code> given its postal code.
	 * 
	 * @param postal
	 * @return
	 * @throws RemoteServiceFailureException
	 *             if postal code is not found
	 */
	Location loadLocation(String postal) throws RemoteServiceFailureException;

	// /**
	// * Load <code>Color</code>s for selection.
	// *
	// * @return
	// * @throws RemoteServiceFailureException
	// */
	// List<Key<Color>> loadColors() throws RemoteServiceFailureException;
	//
	// /**
	// * Load <code>Drivetrain</code>s for selection.
	// *
	// * @return
	// * @throws RemoteServiceFailureException
	// */
	// List<Key<Drivetrain>> loadDrivetrains() throws
	// RemoteServiceFailureException;
	//
	// /**
	// * Load <code>Engine</code>s for selection.
	// *
	// * @return
	// * @throws RemoteServiceFailureException
	// */
	// List<Key<Engine>> loadEngines() throws RemoteServiceFailureException;
	//
	// /**
	// * Load <code>Transmission</code>s for selection.
	// *
	// * @return
	// * @throws RemoteServiceFailureException
	// */
	// List<Key<Transmission>> loadTransmissions() throws
	// RemoteServiceFailureException;

	// END EDIT LOADS

	/**
	 * Load action for uploading a photo.
	 */
	String loadAction() throws RemoteServiceFailureException;

	/**
	 * Retrieve the <code>Account</code> for the user.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Account loadAccount() throws RemoteServiceFailureException;

	/**
	 * Loads the <code>Ad</code> given the id. The user must be an admin or
	 * owner.
	 * 
	 * @param adId
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Ad loadAd(long adId) throws RemoteServiceFailureException;

	/**
	 * Retrieve the <code>Ad</code> given the <code>Key</code>. The result is
	 * sanitized regardless of <code>sanitize</code> flag if the user is neither
	 * the owner or an admin.
	 * 
	 * @param adId
	 * @param sanitize
	 * @return
	 */
	Tuple<Ad, Account> loadAd(long adId, boolean sanitize) throws RemoteServiceFailureException;

	/**
	 * Load <code>Type</code>s for selection.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	List<Type> loadTypes() throws RemoteServiceFailureException;

	/**
	 * Load a <code>Model</code> by its given <code>Key</code>.
	 * 
	 * @param modelId
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	// Model loadModel(Key<Model> model) throws RemoteServiceFailureException;
	Model loadModel(String modelId) throws RemoteServiceFailureException;

	/**
	 * Load <code>Model</code>s of a given <code>Make</code>.
	 * 
	 * @param makeId
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	List<Model> loadModels(String makeId) throws RemoteServiceFailureException;

	/**
	 * Load all <code>Ad</code>s with editions pending approval.
	 * 
	 * @param cursor
	 * @param limit
	 * @throws RemoteServiceFailureException
	 */
	Result<List<Car>> loadPending(String cursor, int limit) throws RemoteServiceFailureException;

	/**
	 * Load <code>Make</code>s for initial query.
	 * 
	 * @param filterEmpty
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	List<Make> loadMakes(boolean filterEmpty) throws RemoteServiceFailureException;

	/**
	 * Load a single reference to a <code>Car</code> entity by its id.
	 * 
	 * @param carId
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Car loadCar(long carId) throws RemoteServiceFailureException;

	/**
	 * Load <code>Car</code>s by the provided criteria.
	 * 
	 * @param offset
	 * @param limit
	 * @param type
	 * @param makes
	 * @param models
	 * @param price
	 * @param mileage
	 * @param condition
	 * @param sort
	 * @param postal
	 * @param distanceIndex
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	MobResult loadCars(int offset, int limit, Key<Type> type, List<Key<Make>> makes, List<Key<Model>> models,
			PriceType price, MileageType mileage, ConditionType condition, SortType sort, String postal,
			int distanceIndex) throws RemoteServiceFailureException;

	/**
	 * Load <code>Car</code>s by the provided criteria. Please use the other
	 * version of <code>loadCars</code>.
	 * 
	 * @param offset
	 * @param limit
	 * @param types
	 * @param makes
	 * @param models
	 * @param filter
	 * @param arg0
	 *            - the low argument or postal
	 * @param arg1
	 *            - the high argument or distance
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	@Deprecated
	MobResult loadCars(int offset, int limit, List<Key<Type>> types, List<Key<Make>> makes, List<Key<Model>> models,
			PriceType price, MileageType mileage, ConditionType condition, String postal, int distance, SortType sort)
			throws RemoteServiceFailureException;

	/**
	 * Load all dealers within specified radius.
	 * 
	 * @param offset
	 * @param limit
	 * @param postal
	 * @param distance
	 * @param sort
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	List<Account> loadDealers(int offset, int limit, String postal, int distance, SortType sort) throws RemoteServiceFailureException;
	
	/**
	 * Load all the keys for <code>Car</code>s the user has "bookmarked."
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Set<Key<Car>> loadBookmarks() throws RemoteServiceFailureException;

	// /**
	// * Load a <code>List</code> of <code>Car</code>s the user has saved.
	// *
	// * @param cursor
	// * @param limit
	// * @param sort
	// * @return
	// * @throws RemoteServiceFailureException
	// */
	// Result<List<Car>> loadBuying(String cursor, int limit, SortType sort)
	// throws RemoteServiceFailureException;
	//
	// /**
	// * Load a <code>List</code> of <code>Car</code>s the user has bookmarked.
	// *
	// * @param accountId
	// * @param cursor
	// * @param limit
	// * @return
	// * @throws RemoteServiceFailureException
	// */
	// Result<List<Car>> loadBuying(String accountId, String cursor, int limit)
	// throws RemoteServiceFailureException;
	//
	// /**
	// * Load a <code>List</code> of <code>Car</code>s the user has listed.
	// *
	// * @param cursor
	// * @param limit
	// * @param sort
	// * @return
	// * @throws RemoteServiceFailureException
	// */
	// Result<List<Car>> loadSelling(String cursor, int limit, SortType sort)
	// throws RemoteServiceFailureException;
	//
	// /**
	// * Load a <code>List</code> of <code>Car</code>s the user has listed.
	// *
	// * @param accountId
	// * @param cursor
	// * @param limit
	// * @return
	// * @throws RemoteServiceFailureException
	// */
	// Result<List<Car>> loadSelling(String accountId, String cursor, int limit)
	// throws RemoteServiceFailureException;

	/**
	 * Load a <code>List</code> of <code>Car</code>s the user has listed.
	 * 
	 * @param cursor
	 * @param limit
	 * @param sort
	 * @param type
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Result<List<Tuple<Marker, Car>>> loadInventory(String cursor, int limit, SortType sort, MarkerType type)
			throws RemoteServiceFailureException;

	/**
	 * Load a <code>List</code> of <code>Car</code>s the user has listed.
	 * 
	 * @param accountId
	 * @param cursor
	 * @param limit
	 * @param sort
	 * @param type
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Result<List<Tuple<Marker, Car>>> loadInventory(String accountId, String cursor, int limit, SortType sort,
			MarkerType type) throws RemoteServiceFailureException;

	/**
	 * Load a price <code>Marker</code> associated with the given account and
	 * car.
	 * 
	 * @param carId
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Marker loadPrice(long carId) throws RemoteServiceFailureException;

	/**
	 * Post an <code>Ad</code> to the given advertising network.
	 * 
	 * @param carId
	 * @param locale
	 * @throws RemoteServiceFailureException
	 */
	void postCg(long carId, Key<CLLocale> locale) throws RemoteServiceFailureException;

	// UPDATES

	/**
	 * Activate the <code>Ad</code> corresponding to the given
	 * <code>carId</code>.
	 * 
	 * @param carId
	 * @return the current time
	 * @throws RemoteServiceFailureException
	 */
	Long activate(long carId, boolean active) throws RemoteServiceFailureException;

	// /**
	// * Add a <code>Car</code> to the user's list of bookmarks.
	// *
	// * @param carId
	// * @throws RemoteServiceFailureException
	// */
	// Set<Key<Car>> addBookmark(long carId) throws
	// RemoteServiceFailureException;

	/**
	 * Remove a price <code>Marker</code>.
	 * 
	 * @param carId
	 * @throws RemoteServiceFailureException
	 */
	void removeAlert(long carId) throws RemoteServiceFailureException;

	/**
	 * Remove a <code>Car</code> (and other related elements) from the
	 * datastore.
	 * 
	 * @param carId
	 * @throws RemoteServiceFailureException
	 */
	void removeCar(long carId) throws RemoteServiceFailureException;

	/**
	 * Add a <code>Marker</code> of type "bookmark" for this <code>Car</code> to
	 * user's account.
	 * 
	 * @param carId
	 * @throws RemoteServiceFailureException
	 */
	void addBookmark(long carId) throws RemoteServiceFailureException;

	/**
	 * Remove <code>Marker</code> of type "bookmark" for this <code>Car</code>
	 * from user's account.
	 * 
	 * @param carId
	 * @throws RemoteServiceFailureException
	 */
	void removeBookmark(long carId) throws RemoteServiceFailureException;

	/**
	 * Suggest a <code>Location</code> to the server.
	 * 
	 * @param postal
	 * @param locality
	 * @param admin
	 * @param latitude
	 * @param longitude
	 */
	void suggest(String postal, String locality, String admin, double latitude, double longitude);

	// List<Model> loadModels(int offset, int limit, List<Key<Type>> types,
	// List<Key<Make>> makes)
	// throws RemoteServiceFailureException;

	/**
	 * Save/edit an <code>Account</code>.
	 * 
	 * @param fields
	 * @throws RemoteServiceFailureException
	 */
	void updateAccount(Map<FieldAccount, Serializable> fields) throws RemoteServiceFailureException;

	/**
	 * Save/edit an <code>Ad</code>.
	 * 
	 * @param fields
	 * @throws RemoteServiceFailureException
	 */
	void updateAd(Map<Serializable, FieldAd> fields) throws RemoteServiceFailureException;

	/**
	 * Create or update a <code>PriceAlert</code>.
	 * 
	 * @param carId
	 * @param point
	 *            price point at which user desires to be notified
	 * @throws RemoteServiceFailureException
	 */
	void updateAlert(long carId, int point) throws RemoteServiceFailureException;

	/**
	 * Save/edit a <code>Car</code>.
	 * 
	 * @param fields
	 * @throws RemoteServiceFailureException
	 */
	void updateCar(Map<FieldCar, Serializable> fields) throws RemoteServiceFailureException;

	/**
	 * Save/edit a <code>Model</code>.
	 * 
	 * @param fields
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	void updateModel(Map<FieldModel, Serializable> fields) throws RemoteServiceFailureException;

	/**
	 * Upgrade an <code>Ad</code> to paid status. The <code>Ad</code> must
	 * already be activated.
	 * 
	 * @param carId
	 * @throws RemoteServiceFailureException
	 */
	void upgrade(long carId) throws RemoteServiceFailureException;

	// END UPDATES

	// MESSAGING

	void inquire(long carId, String message) throws RemoteServiceFailureException;

	void reply(long messageId, String reply) throws RemoteServiceFailureException;

	/**
	 * Mark the <code>Message</code> as being read.
	 * 
	 * @param messageId
	 * @param state
	 * @param enabled
	 * @throws RemoteServiceFailureException
	 */
	void mark(long messageId, MessageState state, boolean enabled) throws RemoteServiceFailureException;

	/**
	 * Load the unread message count for the user.
	 * 
	 * @return tuple of time and count
	 * @throws RemoteServiceFailureException
	 */
	Tuple<Long, Integer> loadCount() throws RemoteServiceFailureException;

	Result<List<Message>> loadMessages(String cursor, int limit, MessageType type, Long carId)
			throws RemoteServiceFailureException;

	void removeMessages(List<Long> messageIds) throws RemoteServiceFailureException;

	// END MESSAGING

	// /**
	// * Lightweight method for checking if user is logged in.
	// *
	// * @throws RemoteServiceFailureException
	// */
	// void checkCreds() throws RemoteServiceFailureException;

	/**
	 * Checks if the user exists, adds a record if it doesn't.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	String initialize() throws RemoteServiceFailureException;

	// public List<Car> loadCars(int offset, int limit, List<Key<Type>> types,
	// List<Key<Make>> makes);
	//
	// public List<Model> loadModels(int offset, int limit, List<Key<Type>>
	// types, List<Key<Make>> makes);
	//
	// public List<Key<Make>> loadMakeKeys();
	//
	// public List<Key<Model>> loadModelKeys();
	//
	// public List<Key<Type>> loadTypeKeys(boolean filterEmpty);

	// public List<Car> loadByMakes(List<Key<Make>> makes);
	//
	// public List<Car> loadByTypes(List<Key<Type>> types);

	// MARK FOR DELETION

	Model renameModel(String modelId, String make, String model) throws RemoteServiceFailureException;

	// List<Location> loadLocations(String zip, int distance) throws
	// RemoteServiceFailureException;

	List<Key<Drivetrain>> createDrive(String drive) throws RemoteServiceFailureException;

	List<Key<Engine>> createEngine(String engine) throws RemoteServiceFailureException;

	List<Key<Color>> createColor(String color) throws RemoteServiceFailureException;

	void createLocation(String postal, String state, String city, double latitude, double longitude)
			throws RemoteServiceFailureException;

	List<Key<Make>> createMake(String make) throws RemoteServiceFailureException;

	List<Key<Transmission>> createTransmission(String transmission) throws RemoteServiceFailureException;

	List<Key<Type>> createType(String type) throws RemoteServiceFailureException;

	// List<Model> createModel(String model) throws
	// RemoteServiceFailureException;

	// void payAd(long carId, boolean enabled) throws
	// RemoteServiceFailureException;

	// END DELETION
}