package com.boosed.mm.shared.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.CLLocale;
import com.boosed.mm.shared.db.CLState;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Color;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.db.MobResult;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.FieldAccount;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.boosed.mm.shared.db.enums.FieldCar;
import com.boosed.mm.shared.db.enums.FieldModel;
import com.boosed.mm.shared.db.enums.MarkerType;
import com.boosed.mm.shared.db.enums.MessageState;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.db.enums.MileageType;
import com.boosed.mm.shared.db.enums.PriceType;
import com.boosed.mm.shared.db.enums.SortType;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.googlecode.objectify.Key;

/**
 * The async counterpart.
 */
public interface MobimotosRpcServiceAsync {

	// MARK DELETE

	//void populate(AsyncCallback<Void> callback);
	
	void createMake(String make, AsyncCallback<List<Key<Make>>> callback);

	void createType(String type, AsyncCallback<List<Key<Type>>> callback);

	// void createModel(String model, AsyncCallback<List<Model>> callback);

	void createDrive(String drive, AsyncCallback<List<Key<Drivetrain>>> callback);

	void createEngine(String engine, AsyncCallback<List<Key<Engine>>> callback);

	void createColor(String color, AsyncCallback<List<Key<Color>>> callback);

	void createTransmission(String transmission, AsyncCallback<List<Key<Transmission>>> callback);

	void createLocation(String postal, String state, String city, double latitude, double longitude,
			AsyncCallback<Void> callback);

	// void loadLocations(String zip, int distance,
	// AsyncCallback<List<Location>> callback);

	void renameModel(String modelId, String make, String model, AsyncCallback<Model> callback);

	// void payAd(long carId, boolean enabled, AsyncCallback<Void> callback);

	// END DELETE

	// LOAD EDITS

	// void loadEdit(String modelId, long adId, AsyncCallback<Tuple<Model, Ad>>
	// callback);

	// void loadEdit(Key<Model> model, Key<Ad> ad, AsyncCallback<Tuple<Model,
	// Ad>> callback);

	void loadEdits(AsyncCallback<Map<FieldCar, List<?>>> callback);

	void loadLocation(String postal, AsyncCallback<Location> callback);

	void loadStates(AsyncCallback<List<Key<CLState>>> callback);
	
	void loadLocales(Key<CLState> state, AsyncCallback<List<Key<CLLocale>>> callback);
	
	// void loadColors(AsyncCallback<List<Key<Color>>> callback);
	//
	// void loadDrivetrains(AsyncCallback<List<Key<Drivetrain>>> callback);
	//
	// void loadEngines(AsyncCallback<List<Key<Engine>>> callback);
	//
	// void loadTransmissions(AsyncCallback<List<Key<Transmission>>> callback);

	// END LOAD EDITS

	// void createCar(Car car, AsyncCallback<Void> callback);

	void loadAction(AsyncCallback<String> callback);

	void loadAccount(AsyncCallback<Account> callback);

	void loadAd(long adId, AsyncCallback<Ad> callback);

	void loadAd(long carId, boolean sanitize, AsyncCallback<Tuple<Ad, Account>> callback);

	// void loadCars(int offset, int limit, List<Key<Type>> types,
	// List<Key<Make>> makes,
	// List<Key<Model>> models, FilterType filter, String arg0, String
	// arg1/*String zip, int distance, String sort, int lo, int hi*/,
	// AsyncCallback</*Tuple<Make, Tuple<Car, Tuple<Key<Model>,
	// List<Model>>>>*/MobResult> callback);

	void loadCar(long carId, AsyncCallback<Car> callback);

	void loadCars(int offset, int limit, List<Key<Type>> types, List<Key<Make>> makes, List<Key<Model>> models,
			PriceType price, MileageType mileage, ConditionType condition, String postal, int distance, SortType sort,
			AsyncCallback<MobResult> callback);

	void loadCars(int offset, int limit, Key<Type> type, List<Key<Make>> makes, List<Key<Model>> models,
			PriceType price, MileageType mileage, ConditionType condition, SortType sort, String postal,
			int distanceIndex, AsyncCallback<MobResult> callback);

	void loadDealers(int offset, int limit, String postal, int distance, SortType sort, AsyncCallback<List<Account>> callback);
	
	// void loadModel(Key<Model> model, AsyncCallback<Model> callback);
	void loadModel(String modelId, AsyncCallback<Model> callback);

	void loadModels(String makeId, AsyncCallback<List<Model>> callback);

	void loadMakes(boolean filterEmpty, AsyncCallback<List<Make>> callback);

	void loadPending(String cursor, int limit, AsyncCallback<Result<List<Car>>> callback);

	void loadTypes(AsyncCallback<List<Type>> callback);

	void loadBookmarks(AsyncCallback<Set<Key<Car>>> callback);

	// void loadBuying(String cursor, int limit, SortType sort,
	// AsyncCallback<Result<List<Car>>> callback);
	//
	// void loadBuying(String accountId, String cursor, int limit,
	// AsyncCallback<Result<List<Car>>> callback);
	//
	// void loadSelling(String cursor, int limit, SortType sort,
	// AsyncCallback<Result<List<Car>>> callback);
	//
	// void loadSelling(String accountId, String cursor, int limit,
	// AsyncCallback<Result<List<Car>>> callback);

	void loadInventory(String cursor, int limit, SortType sort, MarkerType type,
			AsyncCallback<Result<List<Tuple<Marker, Car>>>> callback);

	void loadInventory(String accountId, String cursor, int limit, SortType sort, MarkerType type,
			AsyncCallback<Result<List<Tuple<Marker, Car>>>> callback);

	void loadPrice(long carId, AsyncCallback<Marker> callback);

	void postCg(long carId, Key<CLLocale> locale, AsyncCallback<Void> callback);
	
	// void loadModels(int offset, int limit, List<Key<Type>> types,
	// List<Key<Make>> makes,
	// AsyncCallback<List<Model>> callback);

	// UPDATES

	void activate(long carId, boolean active, AsyncCallback<Long> callback);

	void removeAlert(long carId, AsyncCallback<Void> callback);

	void removeCar(long carId, AsyncCallback<Void> callback);

	void addBookmark(long carId, AsyncCallback<Void> callback);

	void removeBookmark(long carId, AsyncCallback<Void> callback);

	void suggest(String postal, String locality, String admin, double latitude, double longitude,
			AsyncCallback<Void> callback);

	void updateAccount(Map<FieldAccount, Serializable> fields, AsyncCallback<Void> callback);

	void updateAd(Map<Serializable, FieldAd> fields, AsyncCallback<Void> callback);

	void updateAlert(long carId, int point, AsyncCallback<Void> callback);

	void updateCar(Map<FieldCar, Serializable> fields, AsyncCallback<Void> callback);

	void updateModel(Map<FieldModel, Serializable> fields, AsyncCallback<Void> callback);

	void upgrade(long carId, AsyncCallback<Void> callback);

	// END UPDATES

	// MESSAGING

	void loadCount(AsyncCallback<Tuple<Long, Integer>> callback);

	void loadMessages(String cursor, int limit, MessageType type, Long carId,
			AsyncCallback<Result<List<Message>>> callback);

	void mark(long messageId, MessageState state, boolean enabled, AsyncCallback<Void> callback);

	void removeMessages(List<Long> messageIds, AsyncCallback<Void> callback);

	void inquire(long carId, String message, AsyncCallback<Void> callback);

	void reply(long messageId, String reply, AsyncCallback<Void> callback);

	// END MESSAGING

	// MISC

	// void checkCreds(AsyncCallback<Void> callback);

	void initialize(AsyncCallback<String> callback);

	// END MISC

	// void loadCars(int offset, int limit, List<Key<Type>> types,
	// List<Key<Make>> makes, List<Key<Model>> models,
	// AsyncCallback<Tuple<Make, Car, Key<Model>, Model>> callback);

	// void loadCars(int offset, int limit, List<Key<Type>> types,
	// List<Key<Make>> makes, AsyncCallback<List<Car>> callback);
	//
	// //void loadModels(AsyncCallback<List<Model>> callback);
	//
	// void loadMakeKeys(AsyncCallback<List<Key<Make>>> callback);
	//
	// void loadModelKeys(AsyncCallback<List<Key<Model>>> callback);
	//
	// void loadTypeKeys(boolean filterEmpty, AsyncCallback<List<Key<Type>>>
	// callback);
	//
	// void loadModels(int offset, int limit, List<Key<Type>> types,
	// List<Key<Make>> makes,
	// AsyncCallback<List<Model>> callback);
}
