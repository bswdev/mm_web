package com.boosed.mm.shared.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.HasCoords;
import com.boosed.mm.shared.db.HasKey;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.MileageType;
import com.boosed.mm.shared.db.enums.PriceType;
import com.googlecode.objectify.Key;

public final class DataUtil {

	/**
	 * Return the index at which the <code>value</code> is found in
	 * <code>items</code>.
	 * 
	 * @param items
	 * @param value
	 * @return -1 if <code>value</code> not found
	 */
	public static int getIndex(int[] items, int value) {
		int i = 0;
		for (int item : items)
			if (item == value)
				return i;
			else
				i++;

		// for (int i = items.length; --i > -1;)
		// if (items[i] == value)
		// return i;

		return -1;
	}

	/**
	 * Retrieve a <code>List</code> of <code>Key</code>s from a
	 * <code>List</code> of <code>HasKey</code> items.
	 * 
	 * @param <T>
	 * @param items
	 * @return
	 */
	public static <T extends HasKey<T>> List<Key<T>> getKeys(List<T> items) {
		List<Key<T>> rv = new ArrayList<Key<T>>();
		for (HasKey<T> item : items)
			rv.add(item.getKey());
		return rv;
	}

	/**
	 * Generate a <code>List</code> of <code>Type</code>s from a
	 * <code>List</code> of <code>Make</code>s.
	 * 
	 * @param makes
	 * @return
	 */
	public static List<Type> getTypes(List<Make> makes) {
		List<Type> rv = new ArrayList<Type>();

		// walk makes
		for (Make make : makes)
			// walk types
			for (Entry<Key<Type>, Integer> entry : make.totals.entrySet()) {

				// create the Type object from key
				Type type = new Type(entry.getKey().getName());

				// find type in result
				int index = rv.indexOf(type);
				if (index == -1) {
					// type does not exist, add to result
					type.total = entry.getValue();
					rv.add(type);
				} else
					// type exists, increment total
					rv.get(index).total += entry.getValue();
			}

		return rv;
	}

	/**
	 * Generate a <code>List</code> of <code>Make</code>s from a selected
	 * <code>Type</code>.
	 * 
	 * @param type
	 * @param makes
	 * @return
	 */
	public static List<Make> getMakes(Key<Type> type, List<Make> makes) {
		if (type == null)
			// return all makes if a type is not specified
			return makes;

		List<Make> rv = new ArrayList<Make>();
		for (Make make : makes)
			if (make.totals.containsKey(type))
				rv.add(make);

		return rv;
	}

	/**
	 * Retrieve the distance between two <code>HasCoords</code>. This is a
	 * haversine implementation.
	 * 
	 * @param coord1
	 * @param coord2
	 * @return
	 */
	public static int getDistance(HasCoords coord1, HasCoords coord2) {
		double lt1 = coord1.getLatitude(), lt2 = coord2.getLatitude();
		return new Double(2 * 3958.75587 * Math.asin(Math.sqrt(Math.pow(Math.sin(Math.toRadians(lt2 - lt1) / 2), 2)
				+ Math.cos(Math.toRadians(lt1)) * Math.cos(Math.toRadians(lt2))
				* Math.pow(Math.sin(Math.toRadians(coord2.getLongitude() - coord1.getLongitude()) / 2), 2))))
				.intValue();
	}

	/**
	 * Get all the <code>ConditionType</code>s for the given age in years.
	 * 
	 * @param age
	 * @return
	 */
	public static List<ConditionType> getConditions(int age) {
		// check the age bounds
		if (age < 0)
			age = 0;

		// List<ConditionType> rv = Arrays.asList(ConditionType.values());
		List<ConditionType> rv = new ArrayList<ConditionType>();
		rv.addAll(Arrays.asList(ConditionType.values()));

		// remove all conditions less than input
		for (Iterator<ConditionType> it = rv.iterator(); it.hasNext();)
			if (age > it.next().value())
				it.remove();

		return rv;
		// int size = rv.size();
		// int index = 0;

		// for (ConditionType type : ConditionType.values())
		// if (age > type.value())
		// rv.remove(type);
		// for (int c : ConditionType.CONDITIONS)
		// if (age <= c)
		// return rv.subList(index + 3, size);
		// else
		// index++;

		// return rv.subList(index + 3, size);
	}

	/**
	 * Get all the <code>MileageType</code>s for the given mileage.
	 * 
	 * @param price
	 * @return
	 */
	public static Set<MileageType> getMileages(int miles) {
		Set<MileageType> rv = new HashSet<MileageType>(Arrays.asList(MileageType.values()));
		// List<MileageType> rv = Arrays.asList(MileageType.values());
		// int size = rv.size();
		// int index = 0;

		// remove all mileages less than input
		for (Iterator<MileageType> it = rv.iterator(); it.hasNext();)
			if (miles > it.next().value())
				it.remove();

		// for (int m : MileageType.MILEAGES)
		// for (MileageType type : MileageType.values())
		// if (miles > type.value())
		// rv.remove(type);
		// else
		// break;
		// if (miles <= m)
		// return new HashSet<MileageType>(rv.subList(index, size));
		// else
		// index++;

		return rv;
		// return new HashSet<MileageType>(rv.subList(index, size));
	}

	/**
	 * Get all the <code>PriceType</code>s for the given price.
	 * 
	 * @param price
	 * @return
	 */
	public static Set<PriceType> getPrices(int price) {
		Set<PriceType> rv = new HashSet<PriceType>(Arrays.asList(PriceType.values()));

		// List<PriceType> rv = Arrays.asList(PriceType.values());
		// int size = rv.size();
		// int index = 0;

		// remove all prices less than input
		for (Iterator<PriceType> it = rv.iterator(); it.hasNext();)
			if (price > it.next().value())
				it.remove();

		return rv;
		// return new HashSet<PriceType>(rv);
		// if (price > type.)
		// for (int p : PriceType.PRICES)
		// if (price <= p)
		// return new HashSet<PriceType>(rv.subList(index, size));
		// else
		// index++;
		// return new HashSet<PriceType>(rv.subList(index, size));
	}

	/**
	 * Trims string and returns a <code>null</code> if the value is of length 0.
	 * 
	 * @param value
	 * @return
	 */
	public static String getNullString(String value) {
		// remove any whitespaces
		value = value.trim();

		// return null if the string is empty
		return value.length() == 0 ? null : value;
	}

	/**
	 * Check if this <code>Car</code> has been activated for longer than the
	 * provided number of <code>hours</code>.
	 * 
	 * @param car
	 * @param hours
	 * @return
	 */
	public static boolean exceededTime(Car car, int hours) {
		// car has not been activated
		if (!car.isActive())
			return false;

		// set expiration duration to given number of hours
		// Calendar expire = Calendar.getInstance();
		// expire.setTimeInMillis(car.time);
		// expire.add(Calendar.HOUR, hours);

		// hours times ms per hour
		// long time = (car.time + (hours * 3600000));

		// check if current time is after expire time
		return (new Date(System.currentTimeMillis())).after(new Date(car.time + (hours * 3600000)));
	}

	/**
	 * Take two <code>Collection</code>s of differing type and combine into a
	 * single list of <code>Tuple&lt;A, B&gt;</code>. Must be same length.
	 * 
	 * @param <A>
	 * @param <B>
	 * @param a
	 * @param b
	 * @return
	 */
	public static <A, B> List<Tuple<A, B>> stitch(Collection<A> as, Collection<B> bs) throws IllegalArgumentException {
		if (as.size() != bs.size())
			throw new IllegalArgumentException();

		List<Tuple<A, B>> rv = new ArrayList<Tuple<A, B>>();
		// walk a
		for (A a : as)
			// walk b
			for (Iterator<B> it = bs.iterator(); it.hasNext();) {
				// add a and b
				rv.add(new Tuple<A, B>(a, it.next()));
				// remove b
				it.remove();
				// start from a loop
				break;
			}

		return rv;
	}

	/**
	 * Return a <code>List</code> of the first item in a <code>List</code> of
	 * <code>Tuple</code>s.
	 * 
	 * @param <T>
	 * @param <P>
	 * @param list
	 * @return
	 */
	public static <T, P> List<T> getFirstTupleList(List<Tuple<T, P>> list) {
		List<T> rv = new ArrayList<T>();
		for (Tuple<T, P> tuple : list)
			rv.add(tuple.a);
		return rv;
	}

	/**
	 * Return a <code>List</code> of the second item in a <code>List</code> of
	 * <code>Tuple</code>s.
	 * 
	 * @param <T>
	 * @param <P>
	 * @param list
	 * @return
	 */
	public static <T, P> List<P> getSecondTupleList(List<Tuple<T, P>> list) {
		List<P> rv = new ArrayList<P>();
		for (Tuple<T, P> tuple : list)
			rv.add(tuple.b);
		return rv;
	}

	private static final String VIN = "([0-9]|[A-H]|[J-N]|P|[R-Z]){17}";
	private static final int[] VIN_WEIGHT = new int[] { 8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2 };
	private static final int[] VIN_DECODE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5, 0, 7, 0, 9, 2, 3, 4,
			5, 6, 7, 8, 9 };
	private static final String CHECK_DIGIT = "0123456789X";

	/**
	 * Perform check on vin value.
	 * 
	 * @param vin
	 * @return
	 */
	public static boolean vinCheck(String vin) {
		// check length
		if (vin.length() != 17)
			return false;
		else if (!vin.matches(VIN))
			return false;

		// check digit
		int total = 0;
		for (int i = 0; i < 17; i++) {
			int d = vin.charAt(i) - '0';

			// check if value is a character
			total += VIN_WEIGHT[i] * (d > 10 ? VIN_DECODE[d - 17] : d);
		}

		// get mod
		total %= 11;

		// compare check digit to mod
		return CHECK_DIGIT.charAt(total) == vin.charAt(8);
	}

	// borrowed from K-9
	public static final String EMAIL_ADDRESS_PATTERN = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
			+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+";

	public static boolean emailCheck(String email) {
		return email.matches(EMAIL_ADDRESS_PATTERN); // EMAIL_ADDRESS_PATTERN.matcher(email).matches();
	}

	public static void main(String[] args) {
		System.out.println(vinCheck("1M8GDM9AXKP042788"));
		System.out.println(vinCheck("WVWAA71K38W187270"));
	}
}