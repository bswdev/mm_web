package com.boosed.gae.server.criterion;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Query;

/**
 * <code>Criterion</code> which specifies results should be descendants of the
 * provided ancestor.
 * 
 * @author dsumera
 * 
 * @param <T>
 */
public class AncestorCriterion<T> implements Criterion {

	public Key<T> ancestor;

	public AncestorCriterion() {
		// default no-arg constructor
	}

	public AncestorCriterion(Key<T> ancestor) {
		super();
		this.ancestor = ancestor;
	}

	@Override
	public void toQuery(Query<?> query) {
		if (ancestor != null)
			query.ancestor(ancestor);
	}
}