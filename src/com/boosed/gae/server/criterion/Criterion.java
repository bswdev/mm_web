package com.boosed.gae.server.criterion;

import com.googlecode.objectify.Query;

/**
 * Interface for setting options on an Objectify <code>Query</code> in lieu of
 * direct access to the query. Use with <code>GenericDao</code>.
 * 
 * @author dsumera
 */
public interface Criterion {

	/**
	 * Configure the provided <code>query</code>.
	 * 
	 * @param query
	 */
	public void toQuery(Query<?> query);
}