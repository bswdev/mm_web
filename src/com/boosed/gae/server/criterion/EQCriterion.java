package com.boosed.gae.server.criterion;

import com.googlecode.objectify.Query;

/**
 * <code>Criterion</code> for asserting a property is equal to a value. Do not
 * use this for null checking, instead use <code>NullCriterion</code>.
 * 
 * @author dsumera
 * @see NullCriterion
 */
public class EQCriterion implements Criterion {

	public String property;

	public Object value;

	public EQCriterion() {
		// default no-arg constructor
	}

	public EQCriterion(String property, Object value) {
		super();
		this.property = property;
		this.value = value;
	}

	@Override
	public void toQuery(Query<?> query) {
		if (value != null)
			query.filter(property, value);
	}
}