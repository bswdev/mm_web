package com.boosed.gae.server.criterion;

import com.googlecode.objectify.Query;

/**
 * Checks for <code>property</code> &gt; <code>value</code>.
 * 
 * @author dsumera
 */
public class GTCriterion implements Criterion {

	public String property;

	public Object value;

	public GTCriterion() {
		// default no-arg constructor
	}

	public GTCriterion(String property, Object value) {
		super();
		this.property = property;
		this.value = value;
	}

	@Override
	public void toQuery(Query<?> query) {
		query.filter(property + " >", value);
	}
}