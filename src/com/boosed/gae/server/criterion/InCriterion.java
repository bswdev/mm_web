package com.boosed.gae.server.criterion;

import java.util.List;

import com.googlecode.objectify.Query;

public class InCriterion<T> implements Criterion {

	public String property;

	public List<T> items;

	public InCriterion() {
		// default no-arg constructor
	}

	public InCriterion(String property, List<T> items) {
		super();
		this.property = property;
		this.items = items;
	}

	@Override
	public void toQuery(Query<?> query) {
		// only filter query if items is not empty
		if (!items.isEmpty())
			query.filter(property + " in", items);
	}
}