package com.boosed.gae.server.criterion;

import com.googlecode.objectify.Query;

public class NECriterion implements Criterion {

	public String property;

	public Object value;

	public NECriterion() {
		// default no-arg constructor
	}

	public NECriterion(String property, Object value) {
		super();
		this.property = property;
		this.value = value;
	}

	@Override
	public void toQuery(Query<?> query) {
		query.filter(property + " !=", value);
	}
}