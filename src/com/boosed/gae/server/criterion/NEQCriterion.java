package com.boosed.gae.server.criterion;

import com.googlecode.objectify.Query;

public class NEQCriterion extends EQCriterion {

	public NEQCriterion() {
		super();
	}

	public NEQCriterion(String property, Object value) {
		super(property, value);
	}
	
	@Override
	public void toQuery(Query<?> query) {
		if (value != null)
			query.filter(property + " !=", value);
	}
}