package com.boosed.gae.server.criterion;

/**
 * Checks for <code>property</code> != 0.
 * 
 * @author dsumera
 */
public class NotZeroCriterion extends GTCriterion {

	public NotZeroCriterion() {
		// default no-arg constructor
	}

	public NotZeroCriterion(String property) {
		super(property, 0);
	}
}