package com.boosed.gae.server.criterion;

import com.googlecode.objectify.Query;

/**
 * <code>Criterion</code> for asserting a property is <code>null</code>.
 * 
 * @author dsumera
 */
public class NullCriterion extends EQCriterion {

	public NullCriterion() {
		// default no-arg constructor
	}

	public NullCriterion(String property) {
		super(property, null);
	}

	@Override
	public void toQuery(Query<?> query) {
		query.filter(property, null);
	}
}