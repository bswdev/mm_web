package com.boosed.gae.server.criterion;

import com.googlecode.objectify.Query;

public class RangeCriterion implements Criterion {

	public String property;

	public Object hi;

	public Object lo;

	public RangeCriterion() {
		// default no-arg constructor
	}

	public RangeCriterion(String property, Object lo, Object hi) {
		super();
		this.property = property;
		this.lo = lo;
		this.hi = hi;
	}

	@Override
	public void toQuery(Query<?> query) {
		if (lo != null)
			query.filter(property + " >=", lo);

		if (hi != null)
			query.filter(property + " <=", hi);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((property == null) ? 0 : property.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RangeCriterion other = (RangeCriterion) obj;
		if (property == null) {
			if (other.property != null)
				return false;
		} else if (!property.equals(other.property))
			return false;
		return true;
	}
}