package com.boosed.gae.server.criterion;

import com.googlecode.objectify.Query;

/**
 * Add an "order by" property to the <code>Query</code>.
 * 
 * @author dsumera
 */
public class SortCriterion implements Criterion {

	public String property;

	public SortCriterion() {
		// default no-arg constructor
	}

	public SortCriterion(String property) {
		super();
		this.property = property;
	}

	@Override
	public void toQuery(Query<?> query) {
		if (property != null && !property.isEmpty())
		query.order(property);
	}
}