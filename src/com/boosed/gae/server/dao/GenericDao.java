package com.boosed.gae.server.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.boosed.gae.server.criterion.Criterion;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.Tuple;
import com.googlecode.objectify.Key;

public interface GenericDao<T> {

	/**
	 * Get a count of the total items matching <code>criteria</code>
	 * 
	 * @param criteria
	 * @return
	 */
	public int countByCriteria(Criterion... criteria);

	/**
	 * Get a count of the total items matching <code>criteria</code>
	 * 
	 * @param criteria
	 * @return
	 */
	public int countByCriteria(List<Criterion> criteria);

	/**
	 * Find all of <code>T</code>.
	 * 
	 * @return
	 */
	public List<T> findAll();

	/**
	 * Find all of <code>T</code> for the given offset and limit.
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 */
	public List<T> findAll(int offset, int limit);

	/**
	 * Find all of <code>T</code> for the given cursor and limit.
	 * 
	 * @param cursor
	 * @param limit
	 * @return
	 */
	public Result<List<T>> findAll(String cursor, int limit);

	/**
	 * Find all <code>Key</code>s of <code>T</code>.
	 * 
	 * @return
	 */
	public List<Key<T>> findAllKeys();

	/**
	 * Find all <code>Key</code>s for <code>T</code> for the given offset and
	 * limit.
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 */
	public List<Key<T>> findAllKeys(int offset, int limit);

	/**
	 * Find all <code>Key</code>s for <code>T</code> for the given cursor and
	 * limit.
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 */
	public Result<List<Key<T>>> findAllKeys(String cursor, int limit);

	/**
	 * Return all <code>Key</code>s and <code>T</code> for the given offset and
	 * limit.
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 */
	public Map<Key<T>, T> findAllWithKeys(int offset, int limit);

	/**
	 * Return all <code>Key</code>s and <code>T</code> for the given cursor and
	 * limit.
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 */
	public Result<Map<Key<T>, T>> findAllWithKeys(String cursor, int limit);

	/**
	 * Find <code>T</code> by a long id.
	 * 
	 * @param id
	 * @return
	 */
	public T findById(Long id);

	/**
	 * Find <code>Map</code> of <code>T</code> given long ids.
	 * 
	 * @param ids
	 * @return
	 */
	public Map<Long, T> findByIds(Long... ids);

	/**
	 * Find <code>T</code> by a string id.
	 * 
	 * @param id
	 * @return
	 */
	public T findById(String id);

	/**
	 * Find <code>Map</code> of <code>T</code> given String ids.
	 * 
	 * @param ids
	 * @return
	 */
	public Map<String, T> findByIds(String... ids);

	/**
	 * Find <code>T</code> given a <code>Key</code>.
	 * 
	 * @param key
	 * @return
	 */
	public T findByKey(Key<T> key);

	/**
	 * Find a <code>Map</code> of <code>T</code> given an <code>Iterable</code>
	 * of <code>Key</code>s.
	 * 
	 * @param keys
	 * @return
	 */
	public Map<Key<T>, T> findByKeys(Iterable<Key<T>> keys);

	/**
	 * Find a <code>List</code> of <code>Key</code>s given the literal value of
	 * the <code>Key</code>.
	 * 
	 * @param literal
	 * @param offset
	 * @param limit
	 * @return
	 */
	public List<Key<T>> findByKeyLiteral(String literal, int offset, int limit);

	/**
	 * Find a <code>Result</code> of <code>Key</code>s given the literal value
	 * of the <code>Key</code>.
	 * 
	 * @param literal
	 * @param cursor
	 * @param limit
	 * @return
	 */
	public Result<List<Key<T>>> findByKeyLiteral(String literal, String cursor, int limit);

	// public List<T> findByConditions(int offset, int limit, List<Criterion>
	// criteria, String... sorts);

	// public Result<List<T>> findByConditions(String cursor, int limit,
	// List<Criterion> criteria, String... sorts);

	/**
	 * Retrieve an instance of <code>T</code> by multiple <code>Criterion</code>
	 * .
	 * 
	 * @param criteria
	 * @return
	 */
	public T findByCriteria(Criterion... criteria);

	/**
	 * Retrieve an instance of
	 * <code>T</codee> by multiple <code>Criterion</code>.
	 * 
	 * @param criteria
	 * @return
	 */
	public T findByCriteria(List<Criterion> criteria);

	/**
	 * Retrieve a <code>List</code> of <code>T</code> by multiple
	 * <code>Criterion</code>.
	 * 
	 * @param criteria
	 * @return
	 */
	public List<T> findAllByCriteria(Criterion... criteria);

	/**
	 * Retrieve a <code>List</code> of
	 * <code>T</codee> by multiple <code>Criterion</code>.
	 * 
	 * @param criteria
	 * @return
	 */
	public List<T> findAllByCriteria(List<Criterion> criteria);

	/**
	 * Retrieve a <code>List</code> of <code>T</code> by multiple
	 * <code>Criterion</code>.
	 * 
	 * @param offset
	 * @param limit
	 * @param criteria
	 * @return
	 */
	public List<T> findAllByCriteria(int offset, int limit, List<Criterion> criteria);

	/**
	 * Retrieve a <code>Result</code> of <code>T</code> by multiple
	 * <code>Criterion</code>.
	 * 
	 * @param cursor
	 * @param limit
	 * @param criteria
	 * @return
	 */
	public Result<List<T>> findAllByCriteria(String cursor, int limit, List<Criterion> criteria);

	// public Result<List<T>> findByConditions(List<Criterion> criteria, int
	// limit);

	// public List<Key<T>> findKeysByConditions(int offset, int limit,
	// List<Criterion> criteria, String... sorts);

	/**
	 * Retrieve a <code>List</code> of <code>Key</code> of <code>T</code> by
	 * multiple <code>Criterion</code>.
	 * 
	 * @param criteria
	 * @return
	 */
	public List<Key<T>> findKeysByCriteria(Criterion... criteria);

	/**
	 * Retrieve a <code>List</code> of <code>Key</code> of <code>T</code> by
	 * multiple <code>Criterion</code>.
	 * 
	 * @param criteria
	 * @return
	 */
	public List<Key<T>> findKeysByCriteria(List<Criterion> criteria);

	/**
	 * Retrieve a <code>List</code> of <code>Key</code> of <code>T</code> by
	 * multiple <code>Criterion</code>.
	 * 
	 * @param offset
	 * @param limit
	 * @param criteria
	 * @return
	 */
	public List<Key<T>> findKeysByCriteria(int offset, int limit, List<Criterion> criteria);

	/**
	 * Retrieve a <code>Result</code> of <code>Key</code> of <code>T</code> by
	 * multiple <code>Criterion</code>.
	 * 
	 * @param offset
	 * @param limit
	 * @param criteria
	 * @return
	 */
	public Result<List<Key<T>>> findKeysByCriteria(String cursor, int limit,
			List<Criterion> criteria);

	/**
	 * Retrieve all the parents of entities identified by the given criteria.
	 * 
	 * @param <P>
	 * @param criteria
	 * @return
	 */
	public <P> Map<Key<P>, P> findParentsByCriteria(List<Criterion> criteria);

	/**
	 * Retrieve all parents of entities identified by criteria starting from a
	 * given <code>cursor</code>.
	 * 
	 * @param <P>
	 * @param cursor
	 * @param limit
	 * @param criteria
	 * @return a <code>List</code> of <code>Tuple</code>s comprised of entity
	 *         and associated parent
	 */
	public <P> Result<List<Tuple<T, P>>> findParentsByCriteria(String cursor, int limit,
			List<Criterion> criteria);

	/**
	 * Retrieve all the parent <code>Key</code>s of entities identified by the
	 * given criteria.
	 * 
	 * @param criteria
	 */
	public <P> Set<Key<P>> findParentKeysByCriteria(List<Criterion> criteria);

	// public List<Key<T>> findKeysByConditions(List<Criterion> criteria,
	// String... sorts);

	// public int findConditionsSize(String property, List<Criterion> criteria,
	// String... sorts);

	// public List<T> findByLimits(int start, int end, String property, int low,
	// int high, String... sorts);

	// public int findLimitsSize(String property, int low, int high, String...
	// sorts);

	public List<T> findByStartsWith(int offset, int limit, String property, String pattern,
			String... sorts);

	public int findStartsWithSize(String property, String pattern, String... sorts);

	public void delete(T... t);

	public void deleteById(Long... ids);

	public void deleteById(String... ids);

	public void deleteByKey(Key<T> key);

	public void deleteByKeys(Iterable<Key<T>> keys);

	public void deleteByKeys(Key<T>... keys);

	public void persist(T t);

	public void persist(T... t);

	// transactional methods

	/**
	 * Executes the task in the transactional context of this DAO/ofy.
	 */
	public void doTransaction(Transactable<T> task);

	/**
	 * Run this <code>Transactable</code> through transactions until it succeeds
	 * without an optimistic concurrency failure.
	 */
	public void repeatInTransaction(Transactable<T> task);
}