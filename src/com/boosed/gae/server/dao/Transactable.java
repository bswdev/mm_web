package com.boosed.gae.server.dao;


public interface Transactable<T> {

	public void run(GenericDao<T> dao);
}
