package com.boosed.gae.server.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.dao.GenericDao;
import com.boosed.gae.server.dao.Transactable;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.util.DataUtil;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.ObjectifyOpts;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;
import com.googlecode.objectify.helper.DAOBase;

/**
 * Find operations return <code>null</code>. Get operations throw
 * <code>NotFoundException</code>.
 * 
 * @author assessor
 * 
 * @param <T>
 * @see NotFoundException
 */
public class GenericDaoImpl<T> extends DAOBase implements GenericDao<T> {

	protected Logger log = Logger.getLogger(getClass().getName());

	protected Class<T> clazz;

	// private static final String CURSOR_KEY = "com.boosed.crowds.cursor";
	private static final String KEY = "__key__";

	// terminating, non-character matching value
	private static final String END_CHAR = "\ufffd";

	public GenericDaoImpl(Class<T> clazz) {
		// super(new ObjectifyOpts().setGlobalCache(true));
		this.clazz = clazz;
		ObjectifyService.register(clazz);
	}

	public GenericDaoImpl(Class<T> clazz, ObjectifyOpts opts) {
		super(opts);
		this.clazz = clazz;
		ObjectifyService.register(clazz);
	}

	protected Class<T> getClazz() {
		return clazz;
	}

	@Override
	public int countByCriteria(Criterion... criteria) {
		return findByCriteriaQuery(criteria).count();
	}

	@Override
	public int countByCriteria(List<Criterion> criteria) {
		return findByCriteriaQuery(criteria).count();
	}

	@Override
	public List<T> findAll() {
		return ofy().query(clazz).list();
	}

	@Override
	public List<T> findAll(int offset, int limit) {
		return ofy().query(clazz).offset(offset).limit(limit).list();
	}

	@Override
	public Result<List<T>> findAll(String cursor, int limit) {
		return getResult(ofy().query(clazz).startCursor(Cursor.fromWebSafeString(cursor)).limit(limit).iterator(),
				limit);
	}

	@Override
	public List<Key<T>> findAllKeys() {
		return ofy().query(clazz).listKeys();
	}

	@Override
	public List<Key<T>> findAllKeys(int offset, int limit) {
		return ofy().query(clazz).offset(offset).limit(limit).listKeys();
	}

	@Override
	public Result<List<Key<T>>> findAllKeys(String cursor, int limit) {
		return getResult(ofy().query(clazz).startCursor(Cursor.fromWebSafeString(cursor)).limit(limit).fetchKeys()
				.iterator(), limit);
	}

	@Override
	public Map<Key<T>, T> findAllWithKeys(int offset, int limit) {
		return ofy().get(findAllKeys(offset, limit));
	}

	@Override
	public Result<Map<Key<T>, T>> findAllWithKeys(String cursor, int limit) {
		Result<List<Key<T>>> result = findAllKeys(cursor, limit);
		return new Result<Map<Key<T>, T>>(ofy().get(result.result), result.cursor);
	}

	// public List<Key<T>> findByCursor(String cursor, int limit) {
	// // the maximum number of results + 1 for the cursor
	// // Map<Key<T>, T> rv = new HashMap<Key<T>, T>(limit + 1);
	// List<Key<T>> rv = new ArrayList<Key<T>>(limit + 1);
	//
	// Query<T> query = ofy().query(clazz);
	//
	// if (cursor != null)
	// // if the provided cursor is not null, start from the decoded cursor
	// query.startCursor(Cursor.fromWebSafeString(cursor));
	//
	// // set the limit
	// query.limit(limit);
	//
	// QueryResultIterator<T> it = query.iterator();
	// while (it.hasNext()) {
	// // populate the map
	// T item = it.next();
	// rv.add(getKey(item));
	// }
	//
	// if (rv.size() == limit)
	// // filled up map, need to add a mapped cursor
	// // rv.put(new Key<T>(clazz, it.getCursor().toWebSafeString()),
	// // null);
	// rv.add(new Key<T>(clazz, it.getCursor().toWebSafeString()));
	//
	// return rv;
	// }

	@Override
	public T findById(Long id) {
		return ofy().find(clazz, id);
	}

	@Override
	public Map<Long, T> findByIds(Long... ids) {
		return ofy().get(clazz, ids);
	}

	@Override
	public T findById(String id) {
		return ofy().find(clazz, id);
	}

	@Override
	public Map<String, T> findByIds(String... ids) {
		return ofy().get(clazz, ids);
	}

	@Override
	public T findByKey(Key<T> key) {
		return ofy().find(key);
	}

	@Override
	public Map<Key<T>, T> findByKeys(Iterable<Key<T>> keys) {
		return ofy().get(keys);
	}

	// public Map<Key<T>, T> findByKeys(Set<Key<T>> keys) {
	// return ofy().get(keys);
	// }

	@Override
	public List<Key<T>> findByKeyLiteral(String literal, int offset, int limit) {
		Query<T> q = ofy().query(clazz);
		literal = literal.toLowerCase();

		// set start
		q.filter(KEY + " >=", new Key<T>(clazz, literal));

		// set end
		q.filter(KEY + " <", new Key<T>(clazz, literal + END_CHAR));

		// offset
		q.offset(offset);

		// limit
		q.limit(limit);

		// // sorts
		// q.order(property);
		// for (String sort : sorts)
		// q.order(sort);

		List<Key<T>> keys = new ArrayList<Key<T>>();
		QueryResultIterator<Key<T>> it = q.fetchKeys().iterator();
		while (it.hasNext())
			keys.add(it.next());

		// List<Key<T>> keys = q.listKeys();

		// add the cursor if necessary
		if (keys.size() == limit)
			keys.add(new Key<T>(clazz, it.getCursor().toWebSafeString()));

		return keys;
	}

	@Override
	public Result<List<Key<T>>> findByKeyLiteral(String literal, String cursor, int limit) {
		Query<T> q = ofy().query(clazz);
		literal = literal.toLowerCase();

		// set start
		q.filter(KEY + " >=", new Key<T>(clazz, literal));

		// set end
		q.filter(KEY + " <", new Key<T>(clazz, literal + END_CHAR));

		// cursor
		if (cursor != null)
			// if the provided cursor is not null, start from the decoded cursor
			q.startCursor(Cursor.fromWebSafeString(cursor));

		// limit
		q.limit(limit);

		// // sorts
		// q.order(property);
		// for (String sort : sorts)
		// q.order(sort);

		return getResult(q.fetchKeys().iterator(), limit);

		// List<Key<T>> keys = new ArrayList<Key<T>>();
		// QueryResultIterator<Key<T>> it = q.fetchKeys().iterator();
		// while (it.hasNext())
		// keys.add(it.next());
		//
		// // List<Key<T>> keys = q.listKeys();
		//
		// // add the cursor
		// if (keys.size() == limit)
		// keys.add(new Key<T>(clazz, it.getCursor().toWebSafeString()));
		//
		// return keys;
	}

	// public List<T> findByCondition(int start, int limit, String property,
	// FilterOperator operator, Object value,
	// String... sorts) {
	// Query<T> q = ofy().query(clazz);
	//
	// // this must be correct type-wise or it won't work correctly (i.e.,
	// // using a String will NOT work)
	// q.filter(property + " " + operator.toString(), value);
	//
	// // range
	// q.offset(start);
	// q.limit(limit);
	//
	// // sorts
	// q.order("-" + property);
	// for (String sort : sorts)
	// q.order(sort);
	//
	// // prepare the return
	// return q.list();
	// }
	//
	// public Result<List<T>> findByCondition(String cursor, int limit, String
	// property, FilterOperator operator,
	// Object value, String... sorts) {
	// Query<T> q = ofy().query(clazz);
	//
	// // this must be correct type-wise or it won't work correctly (i.e.,
	// // using a String will NOT work)
	// q.filter(property + " " + operator.toString(), value);
	//
	// // range
	// // q.offset(start);
	// // cursor
	// if (cursor != null)
	// // if the provided cursor is not null, start from the decoded cursor
	// q.startCursor(Cursor.fromWebSafeString(cursor));
	//
	// q.limit(limit);
	//
	// // sorts
	// q.order("-" + property);
	// for (String sort : sorts)
	// q.order(sort);
	//
	// // prepare the return
	// return getResult(q.iterator(), limit);
	// }

	@Override
	public List<T> findAllByCriteria(Criterion... criteria) {
		return findByCriteriaQuery(criteria).list();
	}

	@Override
	public List<T> findAllByCriteria(List<Criterion> criteria) {
		return findByCriteriaQuery(criteria).list();
	}
	
	@Override
	public List<T> findAllByCriteria(int offset, int limit, List<Criterion> criteria) {
		Query<T> query = findByCriteriaQuery(criteria);
		query.offset(offset);
		query.limit(limit);
		return query.list();
	}

	@Override
	public Result<List<T>> findAllByCriteria(String cursor, int limit, List<Criterion> criteria) {
		Query<T> query = findByCriteriaQuery(criteria);
		// cursor
		if (cursor != null)
			// if the provided cursor is not null, start from the decoded cursor
			query.startCursor(Cursor.fromWebSafeString(cursor));

		// limit
		query.limit(limit);
		return getResult(query.iterator(), limit);
	}

	@Override
	public T findByCriteria(Criterion... criteria) {
		return findByCriteriaQuery(criteria).get();
	}
	
	@Override
	public T findByCriteria(List<Criterion> criteria) {
		return findByCriteriaQuery(criteria).get();
	}
	
	@Override
	public List<Key<T>> findKeysByCriteria(Criterion... criteria) {
		return findByCriteriaQuery(criteria).listKeys();
	}

	@Override
	public List<Key<T>> findKeysByCriteria(List<Criterion> criteria) {
		return findByCriteriaQuery(criteria).listKeys();
	}

	@Override
	public List<Key<T>> findKeysByCriteria(int offset, int limit, List<Criterion> criteria) {
		Query<T> query = findByCriteriaQuery(criteria);
		query.offset(offset);
		query.limit(limit);
		return query.listKeys();
	}

	@Override
	public Result<List<Key<T>>> findKeysByCriteria(String cursor, int limit, List<Criterion> criteria) {
		Query<T> query = findByCriteriaQuery(criteria);
		// cursor
		if (cursor != null)
			// if the provided cursor is not null, start from the decoded cursor
			query.startCursor(Cursor.fromWebSafeString(cursor));

		// limit
		query.limit(limit);
		return getResult(query.fetchKeys().iterator(), limit);
	}

	@Override
	public <P> Map<Key<P>, P> findParentsByCriteria(List<Criterion> criteria) {
		return findByCriteriaQuery(criteria).fetchParents();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <P> Result<List<Tuple<T, P>>> findParentsByCriteria(String cursor, int limit, List<Criterion> criteria) {
		// query entities using cursor w/ criteria
		Result<List<T>> result = /* findKeysByCriteria */findAllByCriteria(cursor, limit, criteria);

		// set query up again using the same cursor, limit & criterion
		Query<T> query = findByCriteriaQuery(criteria);
		
		// cursor
		if (cursor != null)
			// if the provided cursor is not null, start from the decoded cursor
			query.startCursor(Cursor.fromWebSafeString(cursor));

		// limit
		query.limit(limit);

		// prepare result
		// List<Tuple<T, P>> rv = new ArrayList<Tuple<T, P>>();
		
		// fetch the parents using query
		//Collection<P> parents = (Collection<P>) query.fetchParents().values();
		// int i = 0;

		// check that these are the same length
		//log.warning("length of entities: " + result.result.size() + ", size of parents: " + parents.size());

		// for (P parent : (Collection<P>) query.fetchParents().values())
		// rv.add(new Tuple<T, P>(result.result.get(i++), parent));
		//DataUtil.combine(result.result, (Collection<P>) query.fetchParents().values());

		return new Result<List<Tuple<T, P>>>(DataUtil.stitch(result.result, (Collection<P>) query.fetchParents()
				.values()), result.cursor);
		// return a list of parents with the result cursor from the first
		// iterated search
		// return new Result<List<P>>(new ArrayList<P>((Collection<P>)
		// query.fetchParents().values()), result.cursor);

		// replace the list of result with a list of parents
		// List<P> parents = new ArrayList<P>(ofy().get(result.result));

		// return result;
	}

	@Override
	public <P> Set<Key<P>> findParentKeysByCriteria(List<Criterion> criteria) {
		return findByCriteriaQuery(criteria).fetchParentKeys();
	}

	// public List<T> findByConditions(int offset, int limit, List<Criterion>
	// criteria,
	// String... sorts) {
	// return findByConditionsQuery(offset, limit, criteria, sorts).list();
	// }

	// public List<Key<T>> findKeysByConditions(int offset, int limit,
	// List<Criterion> criteria,
	// String... sorts) {
	// return findByConditionsQuery(offset, limit, criteria, sorts).listKeys();
	// }

	// public Result<List<T>> findByConditions(String cursor, int limit,
	// List<Criterion> criteria,
	// String... sorts) {
	// return getResult(findByConditionsQuery(cursor, limit, criteria,
	// sorts).iterator(), limit);
	// }
	//
	// public Result<List<Key<T>>> findKeysByConditions(String cursor, int
	// limit,
	// List<Criterion> criteria, String... sorts) {
	// return getResult(findByConditionsQuery(cursor, limit, criteria,
	// sorts).fetchKeys()
	// .iterator(), limit);
	// }

	// public Query<T> findByConditionsQuery(List<Criterion> criteria, String...
	// sorts) {
	// Query<T> q = ofy().query(clazz);
	//
	// // set criteria
	// for (Criterion criterion : criteria)
	// criterion.toQuery(q);
	//
	// // sorts
	// for (String sort : sorts)
	// q.order(sort);
	//
	// return q;
	// }

	// public List<T> findByConditions(List<Criterion> criteria, String...
	// sorts) {
	// // return findByConditionsQuery(criteria, sorts).list();
	// Query<T> q = ofy().query(clazz);
	//
	// // set criteria
	// for (Criterion criterion : criteria)
	// criterion.toQuery(q);
	//
	// // sorts
	// for (String sort : sorts)
	// q.order(sort);
	//
	// return q.list();
	// }

	// @Override
	// public Result<List<T>> findByConditions(List<Criterion> criteria, int
	// limit) {
	// //return findByConditionsQuery(criteria, sorts).list();
	// Query<T> q = ofy().query(clazz);
	//
	// // set criteria
	// for (Criterion criterion : criteria)
	// criterion.toQuery(q);
	//
	// return getResult(q.iterator(), limit);
	// }

	// public List<Key<T>> findKeysByConditions(List<Criterion> criteria,
	// String... sorts) {
	// return findByConditionsQuery(criteria, sorts).listKeys();
	// }

	// public int findConditionsSize(String property, List<Criterion> criteria,
	// String... sorts) {
	// Query<T> q = ofy().query(clazz);
	//
	// // set criteria
	// for (Criterion criterion : criteria)
	// // q.filter(criterion.property + " =", criterion.value);
	// criterion.toQuery(q);
	//
	// // sorts
	// for (String sort : sorts)
	// q.order(sort);
	//
	// return q.count();
	// }

	// public int findConditionSize(String property, FilterOperator operator,
	// Object value, String... sorts) {
	// Query<T> q = ofy().query(clazz);
	// q.filter(property + " " + operator.toString(), value);
	//
	// // sorts - do this so we don't have to create new indexes
	// q.order("-" + property);
	// for (String sort : sorts)
	// q.order(sort);
	//
	// return q.count();
	// }

	// public List<T> findByLimits(int start, int limit, String property, int
	// low, int high, String... sorts) {
	// Query<T> q = ofy().query(clazz);
	// q.filter(property + " >=", low);
	// q.filter(property + " <=", high);
	//
	// // range
	// q.offset(start);
	// q.limit(limit);
	//
	// // sorts
	// q.order("-" + property);
	// for (String sort : sorts)
	// q.order(sort);
	//
	// // prepare the return
	// return q.list();
	// }
	//
	// public int findLimitsSize(String property, int low, int high, String...
	// sorts) {
	// Query<T> q = ofy().query(clazz);
	// q.filter(property + " >=", low);
	// q.filter(property + " <=", high);
	//
	// // sorts - do this so we don't have to create new indexes
	// q.order("-" + property);
	// for (String sort : sorts)
	// q.order(sort);
	//
	// return q.count();
	// }

	@Override
	public List<T> findByStartsWith(int offset, int limit, String property, String pattern, String... sorts) {
		Query<T> q = ofy().query(clazz);
		pattern = pattern.toUpperCase();
		q.filter(property + " >=", pattern);
		// hopefully this will catch all begins with
		// q.filter(property + " <=", pattern + "ZZZ");
		q.filter(property + " <=", pattern + END_CHAR);

		// range
		q.offset(offset);
		q.limit(limit);

		// sorts
		q.order(property);
		for (String sort : sorts)
			q.order(sort);

		// prepare the return
		return q.list();
	}

	@Override
	public int findStartsWithSize(String property, String pattern, String... sorts) {
		Query<T> q = ofy().query(clazz);
		pattern = pattern.toUpperCase();
		q.filter(property + " >=", pattern);
		// hopefully this will catch all begins with
		// q.filter(property + " <=", pattern + "ZZZ");
		q.filter(property + " <=", pattern + END_CHAR);

		// sorts - do this so we don't have to create new indexes
		q.order(property);
		for (String sort : sorts)
			q.order(sort);

		return q.count();
	}

	@Override
	public void delete(T... t) {
		ofy().delete(t);
	}

	@Override
	public void deleteById(Long... ids) {
		List<Key<T>> keys = new ArrayList<Key<T>>();

		for (long id : ids)
			keys.add(new Key<T>(clazz, id));

		ofy().delete(keys);
	}

	@Override
	public void deleteById(String... ids) {
		List<Key<T>> keys = new ArrayList<Key<T>>();

		for (String id : ids)
			keys.add(new Key<T>(clazz, id));

		ofy().delete(keys);
	}

	@Override
	public void deleteByKey(Key<T> key) {
		ofy().delete(key);
	}

	@Override
	public void deleteByKeys(Iterable<Key<T>> keys) {
		ofy().delete(keys);
	}

	@Override
	public void deleteByKeys(Key<T>... keys) {
		// ofy().delete(Arrays.asList(keys));
		ofy().delete((Object[]) keys);
	}

	@Override
	public void persist(T t) {
		ofy().put(t);
	}

	@Override
	public void persist(T... t) {
		// Objectify o = ofy();
		ofy().put(t);

		// return o.get(key);

		// the object should now have the appropriate id like with JPA
		// return t;
	}

	// transactional

	@Override
	public void doTransaction(Transactable<T> task) {
		try {
			task.run(this);
			ofy().getTxn().commit();
		} finally {
			if (ofy().getTxn().isActive())
				ofy().getTxn().rollback();
		}
	}

	@Override
	public void repeatInTransaction(Transactable<T> task) {
		while (true) {
			try {
				runInTransaction(task);

				// break loop only when task is completed within transaction
				break;
			} catch (ConcurrentModificationException e) {
				if (log.isLoggable(Level.WARNING))
					log.warning("Optimistic concurrency failure for " + task + ": " + e);
			}
		}
	}

	/**
	 * Provides a place to put the result too. Note that the result is only
	 * valid if the transaction completes successfully; otherwise it should be
	 * ignored because it is not necessarily valid.
	 */
	// abstract public static class Transact<T> implements Transactable
	// {
	// protected T result;
	// public T getResult() { return this.result; }
	// }

	/**
	 * Run the <code>Transactable</code> through a new <code>GenericDao</code>
	 * with transactions enabled.
	 */
	private void runInTransaction(Transactable<T> task) {
		new GenericDaoImpl<T>(clazz, new ObjectifyOpts().setBeginTransaction(true)).doTransaction(task);
	}

	// /**
	// * Executes the task in the transactional context of this DAO/ofy.
	// */
	// public void doTransaction(final Runnable task)
	// {
	// this.doTransaction(new Transactable<T>() {
	// @Override
	// public void run(DAOT daot)
	// {
	// task.run();
	// }
	// });
	// }

	// END transactional

	private Query<T> findByCriteriaQuery(Criterion... criteria) {
		Query<T> q = ofy().query(clazz);

		// set criteria
		for (Criterion criterion : criteria)
			criterion.toQuery(q);

		return q;
	}

	private Query<T> findByCriteriaQuery(List<Criterion> criteria) {
		Query<T> q = ofy().query(clazz);

		// set criteria
		for (Criterion criterion : criteria)
			// q.filter(criterion.property + " =", criterion.value);
			criterion.toQuery(q);

		// limit
		// q.offset(offset);
		// q.limit(limit);

		// sorts
		// for (String sort : sorts)
		// q.order(sort);

		return q;
	}

	// private Query<T> findByCriteriaQuery(String cursor, int limit,
	// List<Criterion> criteria) {
	// Query<T> q = ofy().query(clazz);
	//
	// // set criteria
	// for (Criterion criterion : criteria)
	// // q.filter(criterion.property + " =", criterion.value);
	// criterion.toQuery(q);
	//
	// // cursor
	// if (cursor != null)
	// // if the provided cursor is not null, start from the decoded cursor
	// q.startCursor(Cursor.fromWebSafeString(cursor));
	//
	// // limit
	// q.limit(limit);
	//
	// // sorts
	// // for (String sort : sorts)
	// // q.order(sort);
	//
	// return q;
	// }

	private <K> Result<List<K>> getResult(QueryResultIterator<K> it, int limit) {
		List<K> rv = new ArrayList<K>();

		// QueryResultIterator<T> it = query.iterator();
		while (it.hasNext())
			rv.add(it.next());

		// create the result
		Result<List<K>> result = new Result<List<K>>(rv);

		// add the cursor
		result.cursor = rv.size() == limit ? it.getCursor().toWebSafeString() : null;

		return result;
	}

	// private <K> Result<List<K>> getResult(Iterator<K> it, int limit) {
	// List<K> rv = new ArrayList<K>();
	//
	// // QueryResultIterator<T> it = query.iterator();
	// while (it.hasNext())
	// rv.add(it.next());
	//
	// // create the result
	// Result<List<K>> result = new Result<List<K>>(rv);
	//
	// // add the cursor
	// result.cursor = rv.size() == limit ? it.getCursor().toWebSafeString() :
	// null;
	//
	// return result;
	// }
}