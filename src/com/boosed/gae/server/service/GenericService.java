package com.boosed.gae.server.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.boosed.gae.server.criterion.Criterion;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.Tuple;
import com.googlecode.objectify.Key;

public interface GenericService<T> {

	// stats

	int count(Criterion... criteria);

	int count(List<Criterion> criteria);

	// change

	void drop(T... items);

	void drop(Long... ids);

	void drop(String... ids);

	void drop(Key<T> key);

	void drop(Key<T>... keys);

	// Key<T> generateKey(T t);

	T get(long id);

	T get(String id);

	T get(Key<T> key);

	Map<Key<T>, T> get(Key<T>... key);

	Map<Key<T>, T> get(Iterable<Key<T>> keys);

	// Map<Key<T>, T> get(Set<Key<T>> keys);

	Map<Long, T> get(Long... ids);

	Map<String, T> get(String... ids);

	T get(Criterion... criteria);

	T get(List<Criterion> criteria);

	List<T> getAll();

	List<T> getAll(Criterion... criteria);

	List<T> getAll(List<Criterion> criteria);

	List<T> getAll(int offset, int limit);

	List<T> getAll(int offset, int limit, List<Criterion> criteria);

	Result<List<T>> getAll(String cursor, int limit, List<Criterion> criteria);

	<P> Result<List<Tuple<T, P>>> getAllParents(String cursor, int limit, List<Criterion> criteria);

	List<Key<T>> getAllKeys();

	List<Key<T>> getAllKeys(Criterion... criteria);

	List<Key<T>> getAllKeys(List<Criterion> criteria);

	List<Key<T>> getAllKeys(int offset, int limit);

	<P> Map<Key<P>, P> getAllParents(List<Criterion> criteria);

	<P> Set<Key<P>> getAllParentKeys(List<Criterion> criteria);

	Map<Key<T>, T> getAllWithKeys(int offset, int limit);

	Result<List<Key<T>>> getAllWithKeys(String cursor, int limit);

	Result<List<Key<T>>> getByKeyLiteral(String literal, String cursor, int limit);

	T[] save(T... t);

	T save(T t);
}
