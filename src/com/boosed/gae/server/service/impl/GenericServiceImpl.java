package com.boosed.gae.server.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.dao.GenericDao;
import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.Tuple;
import com.googlecode.objectify.Key;

public class GenericServiceImpl<T, D extends GenericDao<T>> implements GenericService<T> {

	protected final Logger log = Logger.getLogger(getClass().getName());

	public GenericServiceImpl(D dao) {
		this.dao = dao;
	}

	@Override
	public int count(Criterion... criteria) {
		return dao.countByCriteria(criteria);
	}

	@Override
	public int count(List<Criterion> criteria) {
		return dao.countByCriteria(criteria);
	}

	@Override
	public void drop(T... items) {
		dao.delete(items);
	}

	@Override
	public void drop(Long... ids) {
		dao.deleteById(ids);
	}

	@Override
	public void drop(String... ids) {
		dao.deleteById(ids);
	}

	@Override
	public void drop(Key<T> key) {
		dao.deleteByKey(key);
	}

	@Override
	public void drop(Key<T>... keys) {
		dao.deleteByKeys(keys);
	}

	// @Override
	// public Key<T> generateKey(T t) {
	// return dao.getKey(t);
	// }

	@Override
	public T get(long id) {
		return dao.findById(new Long(id));
	}

	@Override
	public T get(String id) {
		return dao.findById(id);
	}

	@Override
	public T get(Key<T> key) {
		return dao.findByKey(key);
	}

	@Override
	public Map<Key<T>, T> get(Key<T>... keys) {
		return dao.findByKeys(Arrays.asList(keys));
	}

	@Override
	public Map<Key<T>, T> get(Iterable<Key<T>> keys) {
		return dao.findByKeys(keys);
	}

	// @Override
	// public Map<Key<T>, T> get(Set<Key<T>> keys) {
	// return dao.findByKeys(keys);
	// }

	@Override
	public Map<Long, T> get(Long... ids) {
		return dao.findByIds(ids);
	}

	@Override
	public Map<String, T> get(String... ids) {
		return dao.findByIds(ids);
	}

	@Override
	public T get(Criterion... criteria) {
		return dao.findByCriteria(criteria);
	}
	
	@Override
	public T get(List<Criterion> criteria) {
		return dao.findByCriteria(criteria);
	}
	
	@Override
	public List<T> getAll() {
		return dao.findAll();
	}

	@Override
	public List<T> getAll(Criterion... criteria) {
		return dao.findAllByCriteria(criteria);
	}

	@Override
	public List<T> getAll(List<Criterion> criteria) {
		return dao.findAllByCriteria(criteria);
	}

	@Override
	public List<T> getAll(int offset, int limit, List<Criterion> criteria) {
		return dao.findAllByCriteria(offset, limit, criteria);
	}

	@Override
	public Result<List<T>> getAll(String cursor, int limit, List<Criterion> criteria) {
		return dao.findAllByCriteria(cursor, limit, criteria);
	}

	// @Override
	// public List<T> getAll(List<Criterion> criteria) {
	// return dao.findByConditions(criteria);
	// }
	//
	// @Override
	// public Result<List<T>> getAll(List<Criterion> criteria, int limit) {
	// return dao.findByConditions(criteria, limit);
	// }

	@Override
	public List<T> getAll(int offset, int limit) {
		return dao.findAll(offset, limit);
	}

	@Override
	public List<Key<T>> getAllKeys() {
		return dao.findAllKeys();
	}

	@Override
	public List<Key<T>> getAllKeys(Criterion... criteria) {
		return dao.findKeysByCriteria(criteria);
	}

	@Override
	public List<Key<T>> getAllKeys(List<Criterion> criteria) {
		return dao.findKeysByCriteria(criteria);
	}

	@Override
	public List<Key<T>> getAllKeys(int offset, int limit) {
		return dao.findAllKeys(offset, limit);
	}

	@Override
	public <P> Map<Key<P>, P> getAllParents(List<Criterion> criteria) {
		return dao.findParentsByCriteria(criteria);
	}
	
	@Override
	public <P> Result<List<Tuple<T, P>>> getAllParents(String cursor, int limit, List<Criterion> criteria) {
		return dao.findParentsByCriteria(cursor, limit, criteria);
	}
	
	@Override
	public <P> Set<Key<P>> getAllParentKeys(List<Criterion> criteria) {
		return dao.findParentKeysByCriteria(criteria);
	}
	
	@Override
	public Map<Key<T>, T> getAllWithKeys(int offset, int limit) {
		return dao.findAllWithKeys(offset, limit);
	}

	@Override
	public Result<List<Key<T>>> getAllWithKeys(String cursor, int limit) {
		return dao.findAllKeys(cursor, limit);
	}

	@Override
	public Result<List<Key<T>>> getByKeyLiteral(String literal, String cursor, int limit) {
		return dao.findByKeyLiteral(literal, cursor, limit);
	}

	public T save(T t) {
		dao.persist(t);
		return t;
	}

	@Override
	public T[] save(T... t) {
		dao.persist(t);
		return t;
	}

	protected final D dao;
}
