package com.boosed.mm.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.boosed.mm.client.enums.ResultMode;
import com.boosed.mm.client.widget.AccountPanel;
import com.boosed.mm.client.widget.CarPanel;
import com.boosed.mm.client.widget.CheckboxList;
import com.boosed.mm.client.widget.ColorPanel;
import com.boosed.mm.client.widget.DataTable;
import com.boosed.mm.client.widget.EditPanel;
import com.boosed.mm.client.widget.EnginePanel;
import com.boosed.mm.client.widget.LabelBox;
import com.boosed.mm.client.widget.LabelList;
import com.boosed.mm.client.widget.LocationPanel;
import com.boosed.mm.client.widget.MakePanel;
import com.boosed.mm.client.widget.MessagePanel;
import com.boosed.mm.client.widget.ModelPanel;
import com.boosed.mm.client.widget.TransmissionPanel;
import com.boosed.mm.client.widget.ViewPanel;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Color;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.db.MobResult;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.FieldAccount;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.boosed.mm.shared.db.enums.FieldCar;
import com.boosed.mm.shared.db.enums.FieldModel;
import com.boosed.mm.shared.db.enums.MarkerType;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.db.enums.MileageType;
import com.boosed.mm.shared.db.enums.PriceType;
import com.boosed.mm.shared.db.enums.SortType;
import com.boosed.mm.shared.service.MobimotosRpcService;
import com.boosed.mm.shared.service.MobimotosRpcServiceAsync;
import com.boosed.mm.shared.util.DataUtil;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.objectify.Key;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Admin implements EntryPoint, ValueChangeHandler<Model> {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	// private static final String SERVER_ERROR = "An error occurred while "
	// + "attempting to contact the server. Please check your network "
	// + "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting
	 * service.
	 */
	private final MobimotosRpcServiceAsync mobService = GWT.create(MobimotosRpcService.class);

	/** callback for all void results, only need error to be reported */
	private AsyncCallback<Void> vc = new AsyncCallback<Void>() {
		@Override
		public void onFailure(Throwable caught) {
			Window.alert("an error occurred " + caught.getClass());
		}

		@Override
		public void onSuccess(Void result) {
			// not implemented

		}
	};

	private final CheckboxList<Type> typesQuery = new CheckboxList<Type>("Types") {
		public String getName(Type item) {
			return item.desc + " (" + item.total + ")";
		}
	};

	private final CheckboxList<Make> makesQuery = new CheckboxList<Make>("Makes") {
		public String getName(Make item) {
			Type type = typesQuery.getItem();
			return item.getCount(type == null ? null : type.getKey());
			//return item.desc + " (" + (type == null ? item.total : item.totals.get(type.getKey())) + ")";
		}
	};

	private final CheckboxList<Model> modelsQuery = new CheckboxList<Model>("Models") {
		public String getName(Model item) {
			return item.desc + " (" + item.total + ")";
		}
	};

	// states for the data tables
	private MessageType messageType = MessageType.ALL;
	private Long carId = null;
	private ResultMode resultMode = ResultMode.INVENTORY;
	private String accountId = null;

	private final DataTable<Car> results = new DataTable<Car>("Results", "load", "inventory", "delete") {
		@Override
		public String format(Car item) {
			return /* item.make.getName() + " " + */item.model.getName();
		}

		@Override
		public void processItem(final Widget source, final Car item) {

			// load ad associated with car
			mobService.loadAd(item.ad.getId(), false, new AsyncCallback<Tuple<Ad, Account>>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("failed to load ad");
				}

				public void onSuccess(Tuple<Ad, Account> result) {
					// for (String key : result.getImages(false).keySet())
					// Window.alert("unapproved: " + key);
					//
					// for (String key : result.getImages(true).keySet())
					// Window.alert("approved: " + key);
					//
					// Window.alert("edit status: " + result.edit +
					// ", content: " +
					// result.getContent());

					vp.setCar(item);
					vp.setAd(result.a);
					vp.setAccount(result.b);
					pp.showRelativeTo(source);

					// load inquiries if this is not from a query (i.e., this is
					// the user's inventory)
					if (resultMode == ResultMode.INVENTORY)
						loadInquiries(item);

					mobService.loadAction(new AsyncCallback<String>() {
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("could not get upload");
						}

						@Override
						public void onSuccess(String result) {
							vp.setAction(result);
						}
					});
				}
			});
		}

		class CarCallback implements AsyncCallback<Result<List<Tuple<Marker, Car>>>> {

			private final String title;
			
			public CarCallback(String title) {
				this.title = title;
			}
			
			public void onFailure(Throwable caught) {
				Window.alert("loading account bookmarks failed: " + caught.getClass());
			}

			public void onSuccess(Result<List<Tuple<Marker, Car>>> result) {
				setTitle(title);
				setItems(DataUtil.getSecondTupleList(result.result));
			}
		}
		
		public void load(int page) {
			switch (resultMode) {
			case QUERY:
				doSearch(page);
				break;
			case INVENTORY:
				mobService.loadInventory(/*page * 25*/null, 500, SortType.NONE, MarkerType.STOCK, new AsyncCallback<Result<List<Tuple<Marker, Car>>>>() {
					public void onFailure(Throwable caught) {
						Window.alert("loading selling failed: " + caught.getClass());
					}

					public void onSuccess(Result<List<Tuple<Marker, Car>>> result) {
						setTitle("Results (inventory)");
						setItems(DataUtil.getSecondTupleList(result.result));
					}
				});
				break;
			case SELLING:
				CarCallback cb = new CarCallback("Results (selling)"); 
				if (accountId == null)
					mobService.loadInventory(/*page * 25*/null, 500, SortType.NONE, MarkerType.STOCK, cb);
				else
					mobService.loadInventory(accountId, /*page * 25*/null, 500, SortType.NONE, MarkerType.STOCK, cb);
				break;
			case BUYING:
				cb = new CarCallback("Results (buying)"); 
				if (accountId == null)
					mobService.loadInventory(/*page * 25*/null, 500, SortType.NONE, MarkerType.BOOKMARK, cb);
				else
					mobService.loadInventory(accountId, /*page * 25*/null, 500, SortType.NONE, MarkerType.BOOKMARK, cb);
				break;
			default:
				break;
			}
		}

		public void reset() {
			resultMode = ResultMode.INVENTORY;
			setTitle("Results (inventory)");
		}

		public void process(List<Car> items) {
			if (!Window.confirm("delete car?"))
				return;

			switch (resultMode) {
			case QUERY:
			case INVENTORY:
			case SELLING:
				// just remove the first one selected
				mobService.removeCar(items.get(0).id, new AsyncCallback<Void>() {
					public void onFailure(Throwable caught) {
						Window.alert("failed to delete vehicle " + caught.getClass());
					}

					public void onSuccess(Void result) {
						// get();
					}
				});
				break;
			case BUYING:
				// remove the bookmark
				//Map<FieldAccount, Serializable> fields = new HashMap<FieldAccount, Serializable>();
				//fields.put(FieldAccount, car.id);
				//fields.put(FieldAccount.CAR_REMOVE, items.get(0).id);
				//updateAccount(fields);
			
				break;
			default:
				break;
			}

		}
	};

	private final DataTable<Message> messages = new DataTable<Message>("Messages (general)", "load", "general",
			"delete") {

		private DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm aa, MM/dd/yy");

		@Override
		public String format(Message item) {
			return item.senderName + ", " + item.getReference();
		}

		@Override
		public void processItem(Widget source, Message item) {
			// Window.alert(item.senderName + " " + item.referenceName + " "
			// + item.message);
			// set the contents
			msgv.setReference(item.id);
			msgv.setHeader("from: " + item.senderName + " @ " + dtf.format(new Date(item.time)));
			
			String message = "";
			for (Iterator<Entry<Long, String>> it = item.messages.entrySet().iterator(); it.hasNext();) {
				Entry<Long, String> entry = it.next();
				message += entry.getKey() + " " + entry.getValue() + "\n\n";
			}
			
			msgv.setText(message);

			// set the reply just in case
			repp.setReference(item.id);
			repp.setHeader("to: " + item.senderName + " wrt carId " + item.reference.getId());

			// msgView.sho
			// msgView.setPopupPosition(100, 100);
			msgView.showRelativeTo(source);
		}

		public void reset() {
			messageType = MessageType.ALL;
			carId = null;
			setTitle("Messages (general)");
		}

		public void process(List<Message> items) {
			if (!Window.confirm("delete messages?"))
				return;

			// get all the message ids
			List<Long> messageIds = new ArrayList<Long>();
			for (Message item : items)
				messageIds.add(item.id);

			mobService.removeMessages(messageIds, new AsyncCallback<Void>() {
				public void onFailure(Throwable caught) {
					Window.alert("failed to delete messages " + caught.getClass());
				}

				public void onSuccess(Void result) {
					load(0);
				}
			});
		}

		public void load(int page) {
			// load messages
			mobService.loadMessages(/*page * 25*/null, 500, messageType, carId, new AsyncCallback<Result<List<Message>>>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("failed to load messages " + caught.getClass());
				}

				public void onSuccess(Result<List<Message>> result) {
					messages.setItems(result.result);
					// cursor = result.cursor;
				}
			});
		}
	};

	// private final Set<Key<Model>> keys = new HashSet<Key<Model>>();

	// private final CheckPanel<Type> typesQuery = new CheckPanel<Type>(){
	// @Override
	// public String getName(Type item) {
	// return item.desc;
	// }
	// };
	//
	// private final CheckPanel<Make> makesQuery = new CheckPanel<Make>(){
	// @Override
	// public String getName(Make item) {
	// return item.desc;
	// }
	// };

	/** The master list of all <code>Make</code>s. Use to reset the enumeration. */
	private List<Make> master;

	private LabelList<SortType> sorts;
	private LabelList<ConditionType> condition;
	private LabelList<PriceType> price;
	private LabelList<MileageType> mileage;
	private LabelBox postal;
	private LabelList<Integer> distance;

	/** the account id of the user logged in */
	//private String userId;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		// initialize the user
		mobService.initialize(new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("could not init user");
			}

			@Override
			public void onSuccess(String result) {
				//userId = result;
				ap.setTitle("Account (" + result + ")");
			}
		});

		// VerticalPanel testPanel = new VerticalPanel();

		// final LabelList<Integer> distances = new
		// LabelList<Integer>("distance: ") {
		// @Override
		// public String getName(Integer item) {
		// return item.toString();
		// }
		// };
		// distances.setItems(Arrays.asList(-1, 25, 50, 100, 250, 500));

		sorts = new LabelList<SortType>("sort: ", false) {
			@Override
			public String getName(SortType item) {
				return item.toString();
			}
		};
		sorts.setItems(Arrays.asList(SortType.values()));

		condition = new LabelList<ConditionType>("condition: ", false) {
			@Override
			public String getName(ConditionType item) {
				return item.toString();
			}
		};
		//List<ConditionType> conditions = new ArrayList<ConditionType>(Arrays.asList(ConditionType.values()));
		//conditions.add(0, conditions.remove(conditions.size() - 1));
		condition.setItems(/*conditions*/Arrays.asList(ConditionType.values()));

		price = new LabelList<PriceType>("price: ", false) {
			@Override
			public String getName(PriceType item) {
				return item.toString();
			}
		};
		//List<PriceType> prices = new ArrayList<PriceType>(Arrays.asList(PriceType.values()));
		//prices.add(0, prices.remove(prices.size() - 1));
		price.setItems(/*prices*/Arrays.asList(PriceType.values()));

		mileage = new LabelList<MileageType>("mileage: ", false) {
			@Override
			public String getName(MileageType item) {
				return item.toString();
			}
		};
		//List<MileageType> mileages = new ArrayList<MileageType>(Arrays.asList(MileageType.values()));
		//mileages.add(0, mileages.remove(mileages.size() - 1));
		mileage.setItems(/*mileages*/Arrays.asList(MileageType.values()));

		// final LabelBox makes = new LabelBox("makes: ");
		// final LabelBox types = new LabelBox("types: ");
		final Button makb = new Button("query");
		final Button refresh = new Button("refresh");
		// testPanel.add(typesQuery);
		// testPanel.add(makesQuery);
		postal = new LabelBox("postal: ");
		distance = new LabelList<Integer>("distance: ", false) {
			private final Integer[] distances = new Integer[] { 10, 25, 50, 75, 100, 150, 200, 250, 300, 500 };

			@Override
			public String getName(Integer item) {
				return distances[item] + " miles";
			}
		};
		distance.setItems(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));

		// testPanel.add(zipQuery);
		// testPanel.add(distances);
		// testPanel.add(sorts);
		// testPanel.add(makb);

		mobService.loadMakes(true, new AsyncCallback<List<Make>>() {
			public void onFailure(Throwable caught) {
				Window.alert("failed loading makes");
			}

			public void onSuccess(List<Make> result) {
				// GWT.log("types size is: " + result.size());
				master = result;
				makesQuery.setItems(master);

				// set types query against the master set
				List<Make> values = makesQuery.getItems();
				// setQuery(types, DataUtil.getTypes(values.isEmpty() ? master :
				// values));

				// Make make = makesQuery.getItem();
				// typesQuery.setItems(DataUtil.getTypes(make == null ? result :
				// Arrays.asList(make)));
				typesQuery.setItems(DataUtil.getTypes(values.isEmpty() ? master : values));
			}
		});

		// final TextBox query = new TextBox();
		// final TextBox distance = new TextBox();
		// final Button locq = new Button("query");
		ClickHandler handler = new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Object source = event.getSource();
				// if (source.equals(locq)) {
				// mobService.loadLocations(query.getText(),
				// Integer.parseInt(distance.getText()),
				// new AsyncCallback<List<Location>>() {
				// public void onFailure(Throwable caught) {
				// Window.alert("failed to load locations");
				// }
				//
				// public void onSuccess(java.util.List<Location> result) {
				// Window.alert("the result size is: " + result.size());
				// for (Location location : result)
				// Window.alert(location.postal + " " + location.latitude
				// + " " + location.longitude);
				// }
				// });
				// } else
				if (source.equals(refresh)) {
					mobService.loadMakes(true, new AsyncCallback<List<Make>>() {
						public void onFailure(Throwable caught) {
							Window.alert("failed creating make");
						}

						public void onSuccess(List<Make> result) {
							// GWT.log("types size is: " + result.size());
							master = result;
							makesQuery.setItems(master);

							// set types query against the master set
							List<Make> values = makesQuery.getItems();
							typesQuery.setItems(DataUtil.getTypes(values.isEmpty() ? master : values));
						}
					});
				} else if (source.equals(makb)) {
					// String arg0 = null, arg1 = null;
					// //FilterType filter =
					// FilterType.valueOf(condition.getItem());
					//
					// switch (filter) {
					// case MILEAGE:
					// case MILEAGE_DESC:
					// case PRICE:
					// case PRICE_DESC:
					// case YEAR:
					// case YEAR_DESC:
					// arg0 = lo.getValue().isEmpty() ? null : lo.getValue();
					// arg1 = hi.getValue().isEmpty() ? null : hi.getValue();
					// break;
					// case LOCATION:
					// arg0 = lo.getValue();
					// arg1 = hi.getValue();
					// break;
					// case NONE:
					// default:
					// break;
					// }

					// if (dist == -1) {
					// // do not use distance
					// filter = FilterType.valueOf(sorts.getItem());
					// arg0 = lo.getValue().isEmpty() ? null : lo.getValue();
					// arg1 = hi.getValue().isEmpty() ? null : hi.getValue();
					// } else {
					// // use distance
					// filter = FilterType.LOCATION;
					// arg0 = lo.getValue();
					// arg1 = hi.getValue();
					// }

					// keys.addAll(DataUtil.getKeys(modelsQuery.getItems()));
					// dist = dist == 5 ? -1 : dist;
					GWT.log("mileage: " + mileage.getItem() + ", condition: " + condition.getItem() + ", price: "
							+ price.getItem());

					// List<Make> make = makesQuery.getItems();
					// Model model = modelsQuery.getItem();

					resultMode = ResultMode.QUERY;
					doSearch(0);

					// // query for models using types and makes
					// List<Key<Type>> typeKeys = new ArrayList<Key<Type>>();
					// for (String id : types.getStrings())
					// typeKeys.add(new Key<Type>(Type.class, id));
					//
					// List<Key<Make>> makeKeys = new ArrayList<Key<Make>>();
					// for (String id : makes.getStrings())
					// makeKeys.add(new Key<Make>(Make.class, id));

					// mobService.load(typesQuery.getKeys(),
					// makesQuery.getKeys(), null, new
					// AsyncCallback<Tuple<Make,Tuple<Car,Tuple<Key<Model>,
					// List<Model>>>>>() {
					//
					// public void onFailure(Throwable caught) {};
					//
					// public void
					// onSuccess(Tuple<Make,Tuple<Car,Tuple<Key<Model>,
					// List<Model>>>> result) {
					// List<Make> makes = result.list;
					// List<Model> models = result.item.item.item;
					//
					// // results
					// List<Car> cars = result.item.list;
					//
					// // selected but doesn't show
					// List<Key<Model>> keys = result.item.item.list;
					//
					// //rest.get(0);
					// }
					// });
					// mobService.load(0, 10, typesQuery.getKeys(),
					// makesQuery.getKeys(), new AsyncCallback<List<Car>>() {
					// public void onFailure(Throwable caught) {
					// Window.alert("could not get cars");
					// }
					//
					// public void onSuccess(List<Car> result) {
					// if (result.isEmpty())
					// Window.alert("no cars match criteria");
					//
					// for (Car car : result) {
					// Window.alert("car is: " + car.year + " " +
					// car.model.getName() + " " + car.price + " " +
					// car.getId());
					// for (String id : car.pics)
					// Window.alert(id);
					// }
					// }
					// });
				}
			}
		};

		makb.addClickHandler(handler);
		refresh.addClickHandler(handler);
		// locq.addClickHandler(handler);

		HorizontalPanel main = new HorizontalPanel();

		VerticalPanel one = new VerticalPanel();
		DOM.setStyleAttribute(one.getElement(), "marginTop", "10px");
		DOM.setStyleAttribute(one.getElement(), "marginLeft", "10px");
		// one.setSpacing(10);

		VerticalPanel vp = new VerticalPanel();

		HorizontalPanel queries = new HorizontalPanel();
		queries.add(typesQuery);
		queries.add(makesQuery);
		queries.add(modelsQuery);
		vp.add(queries);

		// HorizontalPanel locationPanel = new HorizontalPanel();
		// locationPanel.setSpacing(5);
		// locationPanel.add(distances);
		// vp.add(locationPanel);

		HorizontalPanel sortPanel = new HorizontalPanel();
		sortPanel.setSpacing(5);
		sortPanel.add(postal);
		sortPanel.add(distance);
		sortPanel.add(sorts);
		vp.add(sortPanel);

		sortPanel = new HorizontalPanel();
		sortPanel.setSpacing(5);
		sortPanel.add(condition);
		sortPanel.add(mileage);
		sortPanel.add(price);
		vp.add(sortPanel);

		HorizontalPanel bp = new HorizontalPanel();
		bp.setWidth("100%");
		bp.setSpacing(5);
		bp.add(refresh);
		bp.add(makb);
		bp.setCellHorizontalAlignment(makb, HasAlignment.ALIGN_RIGHT);
		vp.add(bp);

		DecoratorPanel dp = new DecoratorPanel();
		vp.setWidth("808px");
		dp.setWidget(vp);
		one.add(dp);

		// HorizontalPanel hp = new HorizontalPanel();
		// hp.add(dp);
		// hp.add(new HTML("<div style=\"width: 10px\"/>"));
		// hp.add(results);
		// hp.add(new HTML("<div style=\"width: 10px\"/>"));
		// hp.add(messages);

		// one.add(dp);
		one.add(new HTML("<div style=\"height: 10px\"/>"));

		one.add(cp = new CarPanel(this));
		cp.doors.setItems(doors);
		cp.model.addValueChangeHandler(this);
		cp.make.addValueChangeHandler(new ValueChangeHandler<Key<Make>>() {
			public void onValueChange(ValueChangeEvent<Key<Make>> event) {
				mobService.loadModels(event.getValue().getName(), new AsyncCallback<List<Model>>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert("failed to load models");
					}

					@Override
					public void onSuccess(List<Model> result) {
						cp.model.setItems(result);
						// model2Car.setItems(result);

						if (!result.isEmpty()) {
							cp.model.fireChange();
							// model2Car.fireChange();
						}
					}
				});
			}
		});
		// one.add(hp);
		one.add(new HTML("<div style=\"height: 10px\"/>"));

		HorizontalPanel hp = new HorizontalPanel();

		vp = new VerticalPanel();
		vp.add(new MakePanel(this));
		vp.add(new HTML("<div style=\"height: 10px\"/>"));
		vp.add(new EnginePanel(this));
		hp.add(vp);
		hp.add(new HTML("<div style=\"width: 10px\"/>"));

		vp = new VerticalPanel();
		vp.add(new TransmissionPanel(this));
		vp.add(new HTML("<div style=\"height: 10px\"/>"));
		vp.add(new ColorPanel(this));
		hp.add(vp);
		hp.add(new HTML("<div style=\"width: 10px\"/>"));

		vp = new VerticalPanel();
		vp.add(new LocationPanel(this));
		vp.add(new HTML("<div style=\"height: 10px\"/>"));
		vp.add(new EditPanel(this));
		hp.add(vp);

		one.add(hp);

		VerticalPanel two = new VerticalPanel();
		DOM.setStyleAttribute(two.getElement(), "marginTop", "10px");
		DOM.setStyleAttribute(two.getElement(), "marginLeft", "10px");

		hp = new HorizontalPanel();
		hp.add(results);
		hp.add(new HTML("<div style=\"width: 10px\"/>"));
		hp.add(messages);
		two.add(hp);

		two.add(new HTML("<div style=\"height: 10px\"/>"));

		two.add(mp = new ModelPanel(this));
		mp.make.addValueChangeHandler(new ValueChangeHandler<Key<Make>>() {
			public void onValueChange(ValueChangeEvent<Key<Make>> event) {
				mobService.loadModels(event.getValue().getName(), new AsyncCallback<List<Model>>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert("failed to load models");
					}

					@Override
					public void onSuccess(List<Model> result) {
						mp.model.setItems(result);
						// model2Car.setItems(result);

						if (!result.isEmpty()) {
							mp.model.fireChange();
							// model2Car.fireChange();
						}
					}
				});
			}
		});

		mp.model.addValueChangeHandler(new ValueChangeHandler<Model>() {
			@Override
			public void onValueChange(ValueChangeEvent<Model> event) {
				mp.setModel(event.getValue());
			}
		});

		two.add(new HTML("<div style=\"height: 10px\"/>"));
		two.add(ap = new AccountPanel(this));
		main.add(one);
		main.add(two);

		// VerticalPanel two = new VerticalPanel();
		// DOM.setStyleAttribute(two.getElement(), "margin", "5px");
		// two.setSpacing(10);

		// hp = new HorizontalPanel();
		// hp.add(results);
		// hp.add(new HTML("<div style=\"width: 10px\"/>"));
		// hp.add(messages);
		// two.add(hp);

		// hp = new HorizontalPanel();
		// hp.setSpacing(10);
		// TODO: put another horizontal panel here
		// hp.add(mp = new ModelPanel(this));
		// two.setSpacing(10);
		// two.add(mp = new ModelPanel(this));
		// two.add(hp);

		// main.add(two);

		// VerticalPanel three = new VerticalPanel();
		// three.setSpacing(10);
		// three.add(cp = new CarPanel(this));
		// hp.add(new HTML("<div style=\"width: 10px\"/>"));
		// hp.add(cp = new CarPanel(this));

		// three.add(new LocationPanel(this));
		// main.add(three);

		// two.add(dp);
		// two.add(hp);
		// four.add(dp);
		// main.add(four);

		pp.setWidget(this.vp = new ViewPanel(this));
		pp.setText("Details");

		message.setWidget(msgp = new MessagePanel(message) {
			public void doSend(long reference, String message) {
				sendMessage(reference, message);
			}
		});
		message.setText("Inquiry");

		reply.setWidget(repp = new MessagePanel(reply) {
			public void doSend(long reference, String message) {
				sendReply(reference, message);
			}
		});
		reply.setText("Response");

		msgView.setWidget(msgv = new MessagePanel(msgView) {
			public void doSend(long reference, String message) {
				// hide the message view
				msgView.hide();

				// set reply
				// repp.setReference(reference);

				// show reply
				reply.setPopupPosition(100, 100);
				reply.show();
			}
		});
		msgView.setText("View");

		alert.setWidget(altp = new MessagePanel(alert) {
			public void doSend(long reference, String message) {
				int point = Integer.parseInt(message.trim());
				mobService.updateAlert(reference, point, new AsyncCallback<Void>() {
					@Override
					public void onSuccess(Void result) {
						alert.hide();
					}
					
					@Override
					public void onFailure(Throwable caught) {
						Window.alert("price alert failed: " + caught.getClass());
					}
				});
			}
		});
		alert.setText("price alert");
		

		// main.add(new Button("populate", new ClickHandler() {
		// @Override
		// public void onClick(ClickEvent event) {
		// mobService.populate(new AsyncCallback<Void>() {
		// @Override
		// public void onFailure(Throwable caught) {
		// Window.alert("the thing did not populate");
		// }
		//					
		// @Override
		// public void onSuccess(Void result) {
		// Window.alert("starting population routine");
		// }
		// });
		// }
		// }));
		
		RootPanel.get().add(main);
		// RootPanel.get().add(query);
		// RootPanel.get().add(distance);
		// RootPanel.get().add(locq);

		// RootPanel.get().add(testPanel);

		// init
		init();
	}

	/** popup for the car view panel */
	private DialogBox pp = new DialogBox(true, false);

	/** popup for message viewer */
	private DialogBox msgView = new DialogBox(true, false);

	/** popup for inquiries */
	private DialogBox message = new DialogBox(true, false);

	/** popup for responses */
	private DialogBox reply = new DialogBox(true, false);
	
	/** popup for price alerts */
	private DialogBox alert = new DialogBox(true, false);

	private MessagePanel altp, msgp, msgv, repp;

	private ViewPanel vp;

	private CarPanel cp;

	private ModelPanel mp;

	private AccountPanel ap;

	private String cursor = null;

	public void loadBuying(String accountId) {
		this.accountId = accountId;// == null ? userId : accountId;
		resultMode = ResultMode.BUYING;
		results.load(null);
	}

	public void loadSelling(String accountId) {
		this.accountId = accountId;// == null ? userId : accountId;
		resultMode = ResultMode.SELLING;
		results.load(null);
		// mobService.loadSelling(accountId, 0, 25, callback)
	}

	/** this method will keep cycling through the results */
	public void loadPending() {
		mobService.loadPending(cursor, 25, new AsyncCallback<Result<List<Car>>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed to load ads");
			}

			@Override
			public void onSuccess(Result<List<Car>> result) {
				results.setTitle("Results (pending)");
				results.setItems(result.result);
				cursor = result.cursor;
			}
		});
	}

	public void updateAccount(Map<FieldAccount, Serializable> fields) {
		mobService.updateAccount(fields, vc);
	}

	public void updateAd(Map<Serializable, FieldAd> fields) {
		mobService.updateAd(fields, vc);
	}

	public void updateCar(Map<FieldCar, Serializable> fields) {
		mobService.updateCar(fields, vc);
	}

	public void updateModel(final Map<FieldModel, Serializable> fields) {
		mobService.updateModel(fields, new AsyncCallback<Void>() {
			public void onFailure(Throwable caught) {
				Window.alert("failed to create model");
			}

			public void onSuccess(Void result) {
				Key<Make> make = new Key<Make>(Make.class, (String) fields.get(FieldModel.MAKE));
				if (make != null && (cp.make.getItem().equals(make) || mp.make.getItem().equals(make))) {
					// update the models on cp
					mobService.loadModels(make.getName(), new AsyncCallback<List<Model>>() {
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("failed to load models");
						}

						@Override
						public void onSuccess(List<Model> result) {
							cp.model.setItems(result);
							// mp.model.setItems(result);
							// model2Car.setItems(result);

							if (!result.isEmpty()) {
								cp.model.fireChange();
								// mp.model.fireChange();
								// model2Car.fireChange();
							}
						}
					});
				}
				// model2Car.setItems(result);
				// cp.model.setItems(result);
				//
				// if (!result.isEmpty()) {
				// // model2Car.fireChange();
				// cp.model.fireChange();
				// }
			}
		});
	}

	public void createDrive(String drive) {
		mobService.createDrive(drive, new AsyncCallback<List<Key<Drivetrain>>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed to create drive");
			}

			@Override
			public void onSuccess(List<Key<Drivetrain>> result) {
				cp.drive.setItems(result);
				mp.drivetrains.setItems(result);
			}
		});
	}

	public void createEngine(String engine) {
		mobService.createEngine(engine, new AsyncCallback<List<Key<Engine>>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed to create drive");
			}

			@Override
			public void onSuccess(List<Key<Engine>> result) {
				cp.engine.setItems(result);
				mp.engines.setItems(result);
			}
		});
	}

	public void createColor(String color) {
		mobService.createColor(color, new AsyncCallback<List<Key<Color>>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed to create color");
			}

			@Override
			public void onSuccess(List<Key<Color>> result) {
				cp.exterior.setItems(result);
				cp.interior.setItems(result);
			}
		});
	}

	public void createLocation(String postal, String state, String city, double latitude, double longitude) {
		mobService.createLocation(postal, state, city, latitude, longitude, vc);
	}

	public void createMake(String make) {
		mobService.createMake(make, new AsyncCallback<List<Key<Make>>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed to create make");
			}

			@Override
			public void onSuccess(List<Key<Make>> result) {
				mp.make.setItems(result);
				cp.make.setItems(result);
			}
		});
	}

	public void createTransmission(String transmission) {
		// create type
		mobService.createTransmission(transmission, new AsyncCallback<List<Key<Transmission>>>() {
			public void onFailure(Throwable caught) {
				Window.alert("failed creating transmission");
			}

			public void onSuccess(List<Key<Transmission>> result) {
				cp.transmission.setItems(result);
				mp.transmissions.setItems(result);
			}
		});
	}

	public void createType(String type) {
		// create type
		mobService.createType(type, new AsyncCallback<List<Key<Type>>>() {
			public void onFailure(Throwable caught) {
				Window.alert("failed creating type");
			}

			public void onSuccess(List<Key<Type>> result) {
				mp.types.setItems(result);
			}
		});
	}

	/** the car that will be edited */
	private Car car;
	
	public void addBookmark(long carId) {
		mobService.addBookmark(carId, vc);
	}
	
	/**
	 * Set this <code>Car</code> as editable in the CarPanel view.
	 * @param car
	 */
	public void edit(Car car) {
		this.car = car;
		// set the car's make and fire changes
		cp.make.setSelected(car.make);
		cp.make.fireChange();
		//cp.setCar(car);
	}
	
	public void loadModel(String model) {
		// Key<Model> key = new Key<Model>(Model.class, model);
		mobService.loadModel(/* key */model, new AsyncCallback<Model>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("could not load model");
			}

			@Override
			public void onSuccess(Model result) {
				mp.setModel(result);
			}
		});
	}

	public void renameModel(String modelId, String make, String model) {
		mobService.renameModel(modelId, make, model, new AsyncCallback<Model>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("could not load model");
			}

			@Override
			public void onSuccess(Model result) {
				mp.setModel(result);
			}
		});
		//Window.alert("this is no longer implemented, use task queue instead");
	}

//	public void doLoadEdit(Long ad, String model) {
//		mobService.loadEdit(model, ad, new AsyncCallback<Tuple<Model, Ad>>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				Window.alert("failed to load edit");
//			}
//
//			public void onSuccess(Tuple<Model, Ad> result) {
//				// do nothing
//			}
//		});
//	}

	public void setAlert(Widget source, Car car) {
		altp.setHeader("reference: " + car.year + " " + car.model.getName());
		altp.setReference(car.id);
		alert.showRelativeTo(source);
	}
	
	public void setInquiry(Widget source, Car car) {
		msgp.setHeader("reference: " + car.year + " " + car.model.getName());
		msgp.setReference(car.id);
		// message.setPopupPosition(100, 100);
		// message.show();
		message.showRelativeTo(source);
	}

	// private String cursor = null;

	public void loadInquiries(Car car) {
		messageType = MessageType.INQUIRIES;
		carId = car.id;
		messages.setTitle("Messages (inquiries)");
		messages.load(null);
		// mobService.loadMessages(0, 25, MessageType.INQUIRIES, car.id,
		// new AsyncCallback<List<Message>>() {
		// @Override
		// public void onFailure(Throwable caught) {
		// Window.alert("failed to load messages "
		// + caught.getClass());
		// }
		//
		// public void onSuccess(List<Message> result) {
		// messages.setItems(result);
		// // cursor = result.cursor;
		// }
		// });
	}

	public void pay(final boolean enabled, long car) {
		mobService.activate(car, enabled, new AsyncCallback<Long>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed to set ad to " + enabled + ", " + caught.getClass());
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Long result) {
				// set the ad to the enabled state
				vp.setApproval(enabled);
			}
		});
	}

	public void sendMessage(long car, String message) {
		// send the message to the user
		mobService.inquire(car, message, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("sending message failed: " + caught.getClass());
			}

			@Override
			public void onSuccess(Void result) {
				// hide the message panel
				Admin.this.message.hide();
			}
		});
	}

	public void sendReply(long message, String reply) {
		// reply to the inquirer
		mobService.reply(message, reply, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("replying failed: " + caught.getClass());
			}

			@Override
			public void onSuccess(Void result) {
				// hide the message panel
				Admin.this.reply.hide();
			}
		});
	}

	// default values for all models
	private List<Integer> doors = Arrays.asList(2, 3, 4, 5, 6);
	private List<Key<Drivetrain>> drivetrains;
	private List<Key<Engine>> engines;
	private List<Key<Transmission>> transmissions;

	private void init() {

		// init enumerations
		mobService.loadEdits(new AsyncCallback<Map<FieldCar, List<?>>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed to load edits");
			}

			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(Map<FieldCar, List<?>> result) {
				for (Entry<FieldCar, List<?>> entry : result.entrySet())
					switch (entry.getKey()) {
					case EXTERIOR:
						// populate colors
						List<Key<Color>> colors = (List<Key<Color>>) entry.getValue();
						cp.exterior.setItems(colors);
						cp.interior.setItems(colors);
						break;
					case DRIVETRAIN:
						// populate drivetrains
						drivetrains = (List<Key<Drivetrain>>) entry.getValue();
						cp.drive.setItems(drivetrains);
						mp.drivetrains.setItems(drivetrains);
						break;
					case ENGINE:
						// populate engines
						engines = (List<Key<Engine>>) entry.getValue();
						cp.engine.setItems(engines);
						mp.engines.setItems(engines);
						break;
					case TRANSMISSION:
						// populate tranmissions
						transmissions = (List<Key<Transmission>>) entry.getValue();
						cp.transmission.setItems(transmissions);
						mp.transmissions.setItems(transmissions);
						break;
					default:
						break;
					}
			}
		});

		// mobService.loadDrivetrains(new AsyncCallback<List<Key<Drivetrain>>>()
		// {
		// @Override
		// public void onFailure(Throwable caught) {
		// Window.alert("faield to load interiors");
		// }
		//
		// @Override
		// public void onSuccess(List<Key<Drivetrain>> result) {
		// cp.drive.setItems(result);
		// }
		// });
		//
		// mobService.loadEngines(new AsyncCallback<List<Key<Engine>>>() {
		// @Override
		// public void onFailure(Throwable caught) {
		// Window.alert("failed to load engines");
		// }
		//
		// public void onSuccess(List<Key<Engine>> result) {
		// cp.engine.setItems(result);
		// }
		// });
		//
		// mobService.loadTransmissions(new
		// AsyncCallback<List<Key<Transmission>>>() {
		// @Override
		// public void onFailure(Throwable caught) {
		// Window.alert("faield to load interiors");
		// }
		//
		// @Override
		// public void onSuccess(List<Key<Transmission>> result) {
		// cp.transmission.setItems(result);
		// }
		// });
		//
		// mobService.loadColors(new AsyncCallback<List<Key<Color>>>() {
		// @Override
		// public void onFailure(Throwable caught) {
		// Window.alert("faield to load colors");
		// }
		//
		// @Override
		// public void onSuccess(List<Key<Color>> result) {
		// cp.exterior.setItems(result);
		// cp.interior.setItems(result);
		// }
		// });

		// init makes
		mobService.loadMakes(false, new AsyncCallback<List<Make>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed on loading makes");
			}

			@Override
			public void onSuccess(List<Make> result) {
				List<Key<Make>> keys = DataUtil.getKeys(result);
				// make2Model.setItems(keys);
				cp.make.setItems(keys);
				mp.make.setItems(keys);

				Key<Make> make = cp.make.getItem();

				if (make != null)
					mobService.loadModels(cp.make.getItem().getName(), new AsyncCallback<List<Model>>() {
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("failed to load models");
						}

						@Override
						public void onSuccess(List<Model> result) {
							cp.model.setItems(result);
							// model2Car.setItems(result);

							if (!result.isEmpty()) {
								cp.model.fireChange();
								// model2Car.fireChange();
							}
						}
					});
				// makesQuery.setItems(result);
			}
		});

		// init types
		mobService.loadTypes(new AsyncCallback<List<Type>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed to load types");
			}

			@Override
			public void onSuccess(List<Type> result) {
				// type2Model.setItems(result);
				mp.types.setItems(DataUtil.getKeys(result));
				// typesQuery.setItems(result);
			}
		});

		// init doors
		mp.doors.setItems(Arrays.asList(2, 3, 4, 5, 6));

		// load messages
		mobService.loadMessages(/*0*/null, 500, MessageType.ALL, null, new AsyncCallback<Result<List<Message>>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed to load messages " + caught.getClass());
			}

			public void onSuccess(Result<List<Message>> result) {
				messages.setItems(result.result);
				// cursor = result.cursor;
			}
		});

		// load vehicles
		mobService.loadInventory(/*0*/null, 500, SortType.NONE, MarkerType.STOCK, new AsyncCallback<Result<List<Tuple<Marker, Car>>>>() {
			public void onFailure(Throwable caught) {
				Window.alert("loading selling failed: " + caught.getClass());
			}

			public void onSuccess(Result<List<Tuple<Marker, Car>>> result) {
				results.setItems(DataUtil.getSecondTupleList(result.result));
			}
		});

		// load account info
		mobService.loadAccount(new AsyncCallback<Account>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("failed to load account: " + caught.getClass());
			}

			public void onSuccess(Account result) {
				ap.setAccount(result);
			}
		});
	}

	private void doSearch(int page) {
		Type type = typesQuery.getItem();

		mobService.loadCars(page * 25, 25, type == null ? null : type.getKey(),
				DataUtil.getKeys(makesQuery.getItems()), DataUtil.getKeys(modelsQuery.getItems()), price.getItem(),
				mileage.getItem(), condition.getItem(), sorts.getItem(), postal.getValue(), distance.getItem(), /*
																												 * callback
																												 * )
																												 * mobService
																												 * .
																												 * loadCars
																												 * (
																												 * 0
																												 * ,
																												 * 50
																												 * ,
																												 * DataUtil
																												 * .
																												 * getKeys
																												 * (
																												 * typesQuery
																												 * .
																												 * getItems
																												 * (
																												 * )
																												 * )
																												 * ,
																												 * DataUtil
																												 * .
																												 * getKeys
																												 * (
																												 * makesQuery
																												 * .
																												 * getItems
																												 * (
																												 * )
																												 * )
																												 * ,
																												 * DataUtil
																												 * .
																												 * getKeys
																												 * (
																												 * modelsQuery
																												 * .
																												 * getItems
																												 * (
																												 * )
																												 * )
																												 * ,
																												 * prices
																												 * .
																												 * getItem
																												 * (
																												 * )
																												 * ,
																												 * mileage
																												 * .
																												 * getItem
																												 * (
																												 * )
																												 * ,
																												 * condition
																												 * .
																												 * getItem
																												 * (
																												 * )
																												 * ,
																												 * lo
																												 * .
																												 * getValue
																												 * (
																												 * )
																												 * ,
																												 * hi
																												 * .
																												 * getInt
																												 * (
																												 * )
																												 * ,
																												 * sorts
																												 * .
																												 * getItem
																												 * (
																												 * )
																												 * ,
																												 */
				new AsyncCallback<MobResult>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert("could not load " + caught);
						caught.printStackTrace();
					}

					public void onSuccess(MobResult
					/*
					 * Tuple<Make, Tuple<Car, Tuple<Key<Model>, List<Model>>>>
					 */result) {
						// List<Make> makes =
						// result.makes;//result.list;
						// List<Model> models =
						// result.models;//result.item.item.item;
						List<Car> cars = result.cars;// result.item.list;

						// selected but doesn't show
						// List<Key<Model>> keys =
						// result.selected;//result.item.item.list;

						// if (cars.isEmpty())
						// Window.alert("there are no cars with that criteria");

						results.setTitle("Results (query)");
						results.setItems(cars);

						// for (Car car : cars) {
						// Window.alert("car is: " + car.year + " "
						// + car.model.getName() + " $" + car.price
						// + " "
						// + car.mileage + " miles, " + car.id);

						// if (!car.pics.isEmpty())
						// for (Entry<String, String> entry :
						// car.pics.entrySet())
						// Window.alert(entry.getKey() + " " +
						// entry.getValue());
						// }

						// GWT.log("the size of makes: " +
						// makes.size());
						// GWT.log("the size of models: " +
						// models.size());

						// typesQuery.setItems(result.types);

						// makesQuery.setItems(result.makes);
						modelsQuery.setItems(result.models);

						Type type = typesQuery.getItem();

						// init make facet; qualify against type
						// setQuery(makes, DataUtil.getMakes(type,
						// master));
						makesQuery.setItems(DataUtil.getMakes(type == null ? null : type.getKey(), master));

						// init types facet; qualify against
						// selected makes
						List<Make> values = makesQuery.getItems();
						typesQuery.setItems(DataUtil.getTypes(values.isEmpty() ? master : values));
						// setQuery(types,
						// DataUtil.getTypes(values.isEmpty() ?
						// master : values));

						// Make make = makesQuery.getItem();
						// typesQuery.setItems(DataUtil.getTypes(make
						// == null ? result.makes :
						// Arrays.asList(make)));
						// set the selected keys
						// Moblot.this.keys.clear();
						// Moblot.this.keys.addAll(DataUtil.getKeys(result.models));
					}
				});
	}

	@Override
	public void onValueChange(ValueChangeEvent<Model> event) {
		Model model = event.getValue();
		// year.setItems(model.years);

		// set years
		cp.year.setItems(model.years);

		// set doors
		if (model.doors.isEmpty())
			cp.doors.setItems(doors);
		else
			cp.doors.setItems(model.doors);

		// set drivetrains
		if (model.drivetrains.isEmpty())
			cp.drive.setItems(drivetrains);
		else
			cp.drive.setItems(model.drivetrains);

		// set engines
		if (model.engines.isEmpty())
			cp.engine.setItems(engines);
		else
			cp.engine.setItems(model.engines);

		// set transmissions
		if (model.transmissions.isEmpty())
			cp.transmission.setItems(transmissions);
		else
			cp.transmission.setItems(model.transmissions);
		
		if (car != null) {
			// we need to set the editable
			cp.setCar(car);
			car = null;
		}
	}
}
