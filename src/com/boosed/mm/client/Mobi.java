package com.boosed.mm.client;

import com.boosed.mm.client.gin.MobiGinjector;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

public class Mobi implements EntryPoint {

	@Override
	public void onModuleLoad() {
		// create injector
		MobiGinjector ginjector = GWT.create(MobiGinjector.class);
		
		RootPanel.get().add(ginjector.getDash());
	}
}