package com.boosed.mm.client.enums;

public enum ResultMode {

	QUERY, INVENTORY, SELLING, BUYING
}
