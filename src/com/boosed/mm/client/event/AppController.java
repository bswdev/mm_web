package com.boosed.mm.client.event;

import java.util.List;

import com.boosed.mm.client.event.enums.EventType;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.service.MobimotosRpcServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.Key;

@Singleton
public class AppController extends Composite {/*
											 * implements IAppController ,
											 * BaseEventHandler < EventHandler >
											 * {
											 */

	private MobimotosRpcServiceAsync service;

	@Inject
	public AppController(MobimotosRpcServiceAsync service) {
		this.service = service;

		// dash.setController(this);

		// add handlers
		addHandler(createTransmissionEventHandler());

		// initWidget(/*dash*/ new Button("event", new ClickHandler() {
		//
		// @Override
		// public void onClick(ClickEvent event) {
		// fireEvent(new TransmissionEvent());
		// }
		// }));

		initWidget(new Label("none"));
	}

	private TransmissionEventHandler createTransmissionEventHandler() {
		return new TransmissionEventHandler() {
			@Override
			public void onEvent(TransmissionEvent event) {
				switch (event.getType()) {
				case ADD:
					// add tranmission
					service.createTransmission((String) event.getPayload(),
							(new AsyncCallback<List<Key<Transmission>>>() {
								@Override
								public void onSuccess(List<Key<Transmission>> result) {
									Window.alert("successfully added transmission");
									fireEvent(new TransmissionEvent(EventType.RESULT, result));
								}

								@Override
								public void onFailure(Throwable caught) {
									Window.alert("failed adding transmission " + caught.toString());
								}
							}));
					break;
				case DELETE:
					GWT.log("attempting to delete");
					break;
				default:
					break;
				}
			}

			@Override
			public Type<TransmissionEventHandler> getType() {
				return TransmissionEvent.TYPE;
			}
		};
		// return null;
	}

	/**
	 * Add in all the handlers to the "event bus."
	 */
	@SuppressWarnings("unchecked")
	// @Override
	public <T extends GwtEvent<H>, H extends EventHandler> void addHandler(BaseEventHandler<T, H> handler) {
		addHandler((H) handler, handler.getType());
	}

	// @Override
	// public void onEvent(GwtEvent<EventHandler> event) {
	// GWT.log("something happened");
	// }
	//
	// @Override
	// public Type<EventHandler> getType() {
	// // this needs to be added to all types
	// return AddTransmissionEvent.TYPE;
	// }

	// @Override
	// public void onEvent(GwtEvent<AddTransmissionEventHandler> event) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public Type<AddTransmissionEventHandler> getType() {
	// // TODO Auto-generated method stub
	// return null;
	// }
}
