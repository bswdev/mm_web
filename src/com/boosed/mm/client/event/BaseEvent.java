package com.boosed.mm.client.event;

import com.boosed.mm.client.event.enums.EventType;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event dispatched when a contact is added.
 * 
 * @author dsumera
 */
public abstract class BaseEvent<T extends GwtEvent<H>, H extends BaseEventHandler<T, H>> extends GwtEvent<H> {

	private Object payload;

	private EventType type;

	// public BaseEvent() {
	// // TODO Auto-generated constructor stub
	// }

	public BaseEvent(EventType type, Object payload) {
		this.type = type;
		this.payload = payload;
	}

	public Object getPayload() {
		return payload;
	}

	public EventType getType() {
		return type;
	}

	// public static Type<BaseEventHandler> TYPE = new Type<BaseEventHandler>();

	// @Override
	// public Type<H> getAssociatedType() {
	// return TYPE;
	// }

	// @Override
	// protected void dispatch(H handler) {
	//
	// handler.onEvent(this);
	// }

	@SuppressWarnings("unchecked")
	@Override
	protected void dispatch(H handler) {
		handler.onEvent((T) this);
	}
}
