package com.boosed.mm.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

/**
 * Generic implementation of the event handler system.
 * 
 * @author dsumera
 * 
 * @param <T>
 */
public interface BaseEventHandler<T extends GwtEvent<H>, H extends EventHandler> extends EventHandler {

	/**
	 * Callback for an event which is fired.
	 * 
	 * @param event
	 */
	void onEvent(T event);
	
	Type<H> getType();
}