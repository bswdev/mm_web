package com.boosed.mm.client.event;

import com.boosed.mm.client.event.enums.EventType;

/**
 * Event dispatched when a <code>Transmission</code> is added.
 * 
 * @author assessor
 * 
 */
public class EngineEvent extends BaseEvent<EngineEvent, EngineEventHandler> {

	public static Type<EngineEventHandler> TYPE = new Type<EngineEventHandler>();

	public EngineEvent(EventType type, Object payload) {
		super(type, payload);
	}

	@Override
	public Type<EngineEventHandler> getAssociatedType() {
		return TYPE;
	}

//	@Override
//	protected void dispatch(EngineEventHandler handler) {
//		handler.onEvent(this);
//	}
}
