package com.boosed.mm.client.event;

import com.boosed.mm.client.event.enums.EventType;
import com.google.gwt.core.client.GWT;

/**
 * Event dispatched when a <code>Transmission</code> is added.
 * 
 * @author assessor
 * 
 */
public class TransmissionEvent extends BaseEvent<TransmissionEvent, TransmissionEventHandler> {
	
	public TransmissionEvent(EventType type, Object payload) {
		super(type, payload);
	}

	public static Type<TransmissionEventHandler> TYPE = new Type<TransmissionEventHandler>();

	@Override
	public Type<TransmissionEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(TransmissionEventHandler handler) {
		GWT.log("calling on event for the handler");
		handler.onEvent(this);
	}
}
