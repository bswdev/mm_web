package com.boosed.mm.client.event.enums;

public enum EventType {
	ADD, DELETE, RESULT
}
