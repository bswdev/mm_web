package com.boosed.mm.client.gin;

import com.boosed.mm.client.widget.Dash;
import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;

/**
 * Injector for mobi.
 * 
 * @author dsumera
 */
@GinModules(MobiModule.class)
public interface MobiGinjector extends Ginjector {

	Dash getDash();
}
