package com.boosed.mm.client.gin;

import com.google.inject.Inject;
import com.google.inject.Provider;

public class SizeProvider implements Provider<Integer> {
	
	private final int numberOfCards;

	//@Inject
	public SizeProvider() {
		numberOfCards = 5;
		// this.numberOfCards = rows*columns;
	}

	public Integer get() {
		return numberOfCards;
	}
}
