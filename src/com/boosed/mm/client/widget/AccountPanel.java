package com.boosed.mm.client.widget;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.boosed.mm.client.Admin;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.enums.FieldAccount;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class AccountPanel extends Composite {

	private static AccountPanelUiBinder uiBinder = GWT
			.create(AccountPanelUiBinder.class);

	private Admin ml;
	
	interface AccountPanelUiBinder extends UiBinder<Widget, AccountPanel> {
	}

	public AccountPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		
		main.setCellHeight(apply, "26px");
		main.setCellHorizontalAlignment(apply, HasAlignment.ALIGN_RIGHT);
		main.setCellVerticalAlignment(apply, HasAlignment.ALIGN_BOTTOM);
	}

	public AccountPanel(Admin ml) {
		this();
		this.ml = ml;
	}
	
	@UiField
	Label title;
	
	@UiField
	LabelBox name;
	
	@UiField
	LabelBox location;
	
	@UiField
	LabelBox phone;
	
	@UiField
	LabelBox url;
	
	@UiField
	LabelBox email;
	
	@UiField
	LabelCheck<Void> emailCheck;
	
	@UiField
	LabelCheck<Void> dealerCheck;
	
	@UiField
	VerticalPanel main;
	
	@UiField
	Button apply;
	
	@Override
	public void setTitle(String title) {
		this.title.setText(title);
	}
	
	@UiHandler("apply")
	public void apply(ClickEvent e) {
		Map<FieldAccount, Serializable> fields = new HashMap<FieldAccount, Serializable>();
		
		fields.put(FieldAccount.NAME, name.value.getText());
		fields.put(FieldAccount.URL, url.value.getText());
		fields.put(FieldAccount.PHONE, phone.value.getText());
		fields.put(FieldAccount.LOCATION, location.value.getText());
		fields.put(FieldAccount.DEALER, dealerCheck.value.getValue());
		fields.put(FieldAccount.EML_SHOW, emailCheck.value.getValue());
		fields.put(FieldAccount.EMAIL, email.value.getText());
		
		ml.updateAccount(fields);
	}
	
	public void setAccount(Account account) {
		name.value.setText(account.name);
		phone.value.setText(account.phone);
		url.value.setText(account.url);
		email.value.setText(account.email);
		
		if (account.postal != null)
			location.value.setText(account.postal);
		else
			location.value.setText(null);
		
		emailCheck.value.setValue(account.showEmail);
		dealerCheck.value.setValue(account.dealer);
	}
}
