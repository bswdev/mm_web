package com.boosed.mm.client.widget;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.boosed.mm.client.Admin;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class AdPanel extends Composite {

	private static AdPanelUiBinder uiBinder = GWT.create(AdPanelUiBinder.class);

	interface AdPanelUiBinder extends UiBinder<Widget, AdPanel> {
	}

	private Admin ml;

	public AdPanel() {
		type = new LabelList<String>(false) {
			@Override
			public String getName(String item) {
				return item;
			}
		};
		type.setItems(Arrays.asList("ad", "ad_app"));
		initWidget(uiBinder.createAndBindUi(this));
		main.setCellHorizontalAlignment(buttons, HasAlignment.ALIGN_RIGHT);
	}

	public AdPanel(Admin ml) {
		this();
		this.ml = ml;
	}

	@UiField
	VerticalPanel main;
	
	@UiField
	HorizontalPanel buttons;

	@UiField
	LabelBox ad;
	
	@UiField
	LabelBox car;
	
	@UiField
	LabelBox key;
	
	@UiField(provided = true)
	LabelList<String> type;

	@UiField
	Button button;
	
	@UiField
	Button pending;
	
	@UiField
	Button delete;

	@UiHandler("delete")
	void delete(ClickEvent e) {
		//ml.deleteCar(car.getLong());
	}
	
	@UiHandler("pending")
	void click(ClickEvent e) {
		ml.loadPending();
	}
	
	@UiHandler("button")
	void onClick(ClickEvent e) {
		Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();
		fields.put(ad.getLong(), FieldAd.ID_AD);
		String t = type.getItem();
		
		fields.put(car.getLong(), FieldAd.ID_CAR);
		
		// map the edits
		if ("ad".equals(t))
			fields.put(key.getValue(), FieldAd.AD);
		else if ("ad_app".equals(t))
			fields.put("", FieldAd.AD_APPROVE);
		
		ml.updateAd(fields);
	}
}