package com.boosed.mm.client.widget;

import com.boosed.mm.client.event.AppController;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Base class for simple add panels.
 * 
 * @author dsumera 
 */
public abstract class AddEnumPanel extends Composite {

	private static AddEnumPanelUiBinder uiBinder = GWT.create(AddEnumPanelUiBinder.class);

	protected AppController controller;

	interface AddEnumPanelUiBinder extends UiBinder<Widget, AddEnumPanel> {
	}

	public AddEnumPanel(AppController controller) {
		// set controller
		this.controller = controller;

		// create widgets and init
		initWidget(uiBinder.createAndBindUi(this));
		main.setCellHorizontalAlignment(button, HasAlignment.ALIGN_RIGHT);
	}

	public void setLabel(String label) {
		this.label.setText(label);
		value.setLabel(label);
	}

	@UiField
	Label label;

	@UiField
	VerticalPanel main;

	@UiField
	LabelBox value;

	@UiField
	Button button;

	@UiHandler("button")
	public void onClick(ClickEvent event) {
		// check string value
		String val = value.getValue().trim();

		// if value is not empty, execute
		if (val.length() != 0)
			execute(val);
	}

	public abstract void execute(String value);
}