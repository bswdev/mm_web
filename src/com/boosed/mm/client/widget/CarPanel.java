package com.boosed.mm.client.widget;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.boosed.mm.client.Admin;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Color;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.enums.FieldCar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.objectify.Key;

public class CarPanel extends Composite {

	private static CarPanelUiBinder uiBinder = GWT.create(CarPanelUiBinder.class);

	interface CarPanelUiBinder extends UiBinder<Widget, CarPanel> {
	}

	private Admin ml;

	public CarPanel() {
		condition = new LabelList<String>(false) {
			@Override
			public String getName(String item) {
				return item;
			}
		};
		condition.setItems(Arrays.asList("NEW", "CERTIFIED", "USED"));
		
		model = new LabelList<Model>(false) {
			@Override
			public String getName(Model item) {
				return item.toString();
			}
		};

		year = new LabelList<Integer>(false) {
			@Override
			public String getName(Integer item) {
				return item.toString();
			}
		};

		drive = new LabelList<Key<Drivetrain>>(false) {
			public String getName(Key<Drivetrain> item) {
				return item.getName();
			};
		};

		engine = new LabelList<Key<Engine>>(false) {
			public String getName(Key<Engine> item) {
				return item.getName();
			};
		};
		
		doors = new LabelList<Integer>(false) {
			public String getName(Integer item) {
				return item.toString();
			}
		};
		
		transmission = new LabelList<Key<Transmission>>(false) {
			public String getName(Key<Transmission> item) {
				return item.getName();
			};
		};

		exterior = new LabelList<Key<Color>>(false) {
			public String getName(Key<Color> item) {
				return item.getName();
			};
		};

		interior = new LabelList<Key<Color>>(false) {
			public String getName(Key<Color> item) {
				return item.getName();
			};
		};

		make = new LabelList<Key<Make>>(false) {
			@Override
			public String getName(Key<Make> item) {
				return item.getName();
			}
		};
		
		initWidget(uiBinder.createAndBindUi(this));
		
		// set panel within main
		main.setCellHeight(panel, "57px");
		//main.setCellHorizontalAlignment(panel, HasAlignment.ALIGN_RIGHT);
		main.setCellVerticalAlignment(panel, HasAlignment.ALIGN_BOTTOM);
		
		// set buttons within panel
		panel.setCellHorizontalAlignment(buttons, HasAlignment.ALIGN_RIGHT);
	}

	public CarPanel(Admin ml) {
		this();
		this.ml = ml;
	}

	@UiField
	VerticalPanel main;

	@UiField
	LabelBox mileage;

	@UiField
	LabelBox postal;

	@UiField
	LabelBox price;
	
	@UiField
	LabelBox trim;

	@UiField
	LabelBox stock;
	
	@UiField
	LabelBox vin;
	
	@UiField(provided = true)
	public LabelList<String> condition;
	
	@UiField(provided = true)
	public LabelList<Key<Make>> make;
	
	@UiField(provided = true)
	public LabelList<Key<Drivetrain>> drive;
	
	@UiField(provided = true)
	public LabelList<Key<Engine>> engine;

	@UiField(provided = true)
	public LabelList<Key<Transmission>> transmission;

	@UiField(provided = true)
	public LabelList<Key<Color>> interior;

	@UiField(provided = true)
	public LabelList<Key<Color>> exterior;

	@UiField(provided = true)
	public LabelList<Model> model;

	@UiField(provided = true)
	public LabelList<Integer> year;

	@UiField(provided = true)
	public LabelList<Integer> doors;
	
	@UiField
	VerticalPanel panel;
	
	@UiField
	HorizontalPanel buttons;
	
	@UiField
	Button button;
	
	@UiField
	Button update;
	
	private Map<FieldCar, Serializable> getFields() {
		Map<FieldCar, Serializable> fields = new HashMap<FieldCar, Serializable>();
		
		fields.put(FieldCar.YEAR, year.getItem());
		fields.put(FieldCar.CONDITION, condition.getSelectedIndex() + 1);
		fields.put(FieldCar.MAKE, model.getItem().make.getName());
		fields.put(FieldCar.MODEL, model.getItem().desc);
		fields.put(FieldCar.MILEAGE, mileage.getInt());
		fields.put(FieldCar.DOORS, doors.getItem());
		fields.put(FieldCar.PRICE, price.getInt());
		fields.put(FieldCar.TRIM, trim.getValue());
		fields.put(FieldCar.TRANSMISSION, transmission.getItem().getName());
		fields.put(FieldCar.DRIVETRAIN, drive.getItem().getName());
		fields.put(FieldCar.ENGINE, engine.getItem().getName());
		fields.put(FieldCar.INTERIOR, interior.getItem().getName());
		fields.put(FieldCar.EXTERIOR, exterior.getItem().getName());
		fields.put(FieldCar.LOCATION, postal.getValue());
		fields.put(FieldCar.STOCK, stock.getValue());
		fields.put(FieldCar.VIN, vin.getValue());
		
		return fields;
	}
	
	@UiHandler("update")
	void update(ClickEvent e) {
		if (car != null) {
			Map<FieldCar, Serializable> fields = getFields();
			fields.put(FieldCar.ID, car.id);
			ml.updateCar(fields);
		}
	}
	
	@UiHandler("button")
	void onClick(ClickEvent e) {
		ml.updateCar(getFields());
	}
	
	private Car car;
	
	public void setCar(Car car) {
		this.car = car;
		
		make.setSelected(car.make);
		Model mdl = new Model(car.model.getName());
		mdl.make = car.make;
		model.setSelected(mdl);
		// account for any
		condition.list.setSelectedIndex(car.conditions.get(0).ordinal() - 1);
		trim.setValue(car.trim);
		doors.setSelected(car.doors);
		year.setSelected(car.year);
		price.setValue(car.price.toString());
		mileage.setValue(car.mileage.toString());
		transmission.setSelected(car.transmission);
		drive.setSelected(car.drivetrain);
		engine.setSelected(car.engine);
		exterior.setSelected(car.exterior);
		interior.setSelected(car.interior);
		stock.setValue(car.stock);
		vin.setValue(car.vin);
		postal.setValue(car.postal);
	}
}