package com.boosed.mm.client.widget;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;

public abstract class CheckPanel<T> extends Composite {

	private VerticalPanel wrapped = new VerticalPanel();

	private List<LabelCheck<T>> checks = new ArrayList<LabelCheck<T>>();

	public CheckPanel() {
		initWidget(wrapped);
	}

	public void setItems(List<T> items) {
		checks.clear();
		for (int i = wrapped.getWidgetCount(); --i > -1;)
			wrapped.remove(i);

		for (T item : items) {
			// create for checks
			LabelCheck<T> check = new LabelCheck<T>(getName(item), item);
			checks.add(check);
			wrapped.add(check);
		}
	}

	public List<T> getItems() {
		List<T> rv = new ArrayList<T>();
		for (LabelCheck<T> check : checks)
			if (check.getValue())
				rv.add(check.getItem());
		return rv;
	}

	public void setChecked(Collection<T> items) {
		clear();

		//for (T item : items)
			for (LabelCheck<T> check : checks) {
				//if (item.equals(check.getItem())) {
				//	check.value.setValue(true);
				//	break;
				//}
				check.value.setValue(items.contains(check.getItem()));
			}
	}
	
	public void clear() {
		for (LabelCheck<T> check : checks)
			check.value.setValue(false);
	}
	
	public abstract String getName(T item);
}
