package com.boosed.mm.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public abstract class CheckboxList<T> extends Composite {

	private static CheckboxListUiBinder uiBinder = GWT.create(CheckboxListUiBinder.class);

	@SuppressWarnings("unchecked")
	interface CheckboxListUiBinder extends UiBinder<Widget, CheckboxList> {
	}

	public CheckboxList() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	private List<LabelCheck<T>> checks = new ArrayList<LabelCheck<T>>();

	@UiField
	VerticalPanel wrapped;

	@UiField
	Label label;

	public CheckboxList(String name) {
		initWidget(uiBinder.createAndBindUi(this));
		label.setText(name);
		
		wrapped.setVerticalAlignment(HasAlignment.ALIGN_TOP);
	}

	// @UiHandler("button")
	// void onClick(ClickEvent e) {
	// Window.alert("Hello!");
	// }
	//
	// public void setText(String text) {
	// button.setText(text);
	// }
	//
	// public String getText() {
	// return button.getText();
	// }

	public void setItems(List<T> items) {
		// retain the selected keys
		List<T> keys = getItems();
		
		checks.clear();
		for (int i = wrapped.getWidgetCount(); --i > -1;)
			wrapped.remove(i);
		
		for (T item : items) {
			LabelCheck<T> check = new LabelCheck<T>(getName(item), item);
			
			// set the check on if it was previously selected
			check.value.setValue(keys.contains(item));
			
			checks.add(check);
			wrapped.add(check);
		}
	}
	
	public void addCheck(LabelCheck<T> check) {
		checks.add(check);
		wrapped.add(check);
	}
	
	public abstract String getName(T item);
	
	public List<T> getItems() {
		List<T> rv = new ArrayList<T>();
		for (LabelCheck<T> check : checks)
			if (check.getValue())
				rv.add(check.getItem());
		return rv;
	}
	
	public T getItem() {
		List<T> rv = new ArrayList<T>();
		for (LabelCheck<T> check : checks)
			if (check.getValue())
				rv.add(check.getItem());
		return rv.isEmpty() ? null : rv.get(0);
	}
}
