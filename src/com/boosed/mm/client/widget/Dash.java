package com.boosed.mm.client.widget;

import com.boosed.mm.client.event.AppController;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

public class Dash extends Composite {

	private static DashUiBinder uiBinder = GWT.create(DashUiBinder.class);

	interface DashUiBinder extends UiBinder<Widget, Dash> {
	}

	// @Inject
	// private AppController controller;

	@UiField(provided = true)
	EngPanel eng;

	@UiField(provided = true)
	TransPanel trans;

	// @Inject
	// public Dash(TransmissionPanel trans) {
	// this.trans = trans;
	//
	// initWidget(uiBinder.createAndBindUi(this));
	// }

	@Inject
	public Dash(AppController controller) {
		eng = new EngPanel(controller);
		trans = new TransPanel(controller);

		// this.controller = controller;

		// controller.addHandler(trans);
		// set the services
		// this.trans.service = service;

		initWidget(uiBinder.createAndBindUi(this));
	}

	// @Override
	// public void onEvent(GwtEvent<TransmissionEventHandler> event) {
	// GWT.log("received the event");
	// }

	// public void setController(AppController controller) {
	// this.controller = controller;
	// }

	// @Override
	// public Type<EventHandler> getType() {
	//
	//
	// return new Type();
	// }

	// @Override
	// public Type<AddTransmissionEventHandler> getType() {
	// return AddTransmissionEvent.TYPE;
	// }
	// @Override
	// public void onEvent(GwtEvent<TransmissionEventHandler> event) {
	// GWT.log("I finally did something!");
	// }

	// @Override
	// public void onEvent(GwtEvent<EventHandler> event) {
	// GWT.log("I should get any event");
	// }

	// @Override
	// public void onEvent(GwtEvent<AddTransmissionEventHandler> event) {
	// GWT.log("I should get any event");
	// }
}