package com.boosed.mm.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.boosed.mm.client.Admin;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public abstract class DataTable<T> extends Composite implements ClickHandler {

	private static DataTableUiBinder uiBinder = GWT
			.create(DataTableUiBinder.class);

	private List<T> items;

	@SuppressWarnings("unchecked")
	interface DataTableUiBinder extends UiBinder<Widget, DataTable> {
	}

	public DataTable() {
		initWidget(uiBinder.createAndBindUi(this));

		// alignment of buttons
		main.setCellHorizontalAlignment(buttons, HasAlignment.ALIGN_RIGHT);
		buttons.setCellVerticalAlignment(page, HasAlignment.ALIGN_MIDDLE);
		
		// adjust the page textbox
		page.label.setWidth("50px");
		page.value.setWidth("50px");
		page.value.setText("0");
	}

	public DataTable(String title, String load, String reset, String process) {
		this();
		this.title.setText(title);
		this.load.setText(load);
		this.reset.setText(reset);
		this.process.setText(process);
		this.process.setEnabled(false);
		

		page.value.addKeyPressHandler(new KeyPressHandler() {

			@Override
			public void onKeyPress(KeyPressEvent event) {
				int code = event.getCharCode();
				if (code == KeyCodes.KEY_UP) {
					int value = page.getInt();
					value++;
					page.value.setText(value + "");
					load(value);
				} else if (code == KeyCodes.KEY_DOWN) {
					int value = page.getInt();
					value--;
					if (value < 0) {
						value = 0;
						page.value.setText(value + "");
					} else {
						page.value.setText(value + "");
						load(value);
					}
				} else if (code == KeyCodes.KEY_BACKSPACE
						|| code == KeyCodes.KEY_DELETE
						|| code == KeyCodes.KEY_RIGHT
						|| code == KeyCodes.KEY_LEFT) {
					// allow backspace, delete, left and right
				} else if ((code - '0') < 0 || (code - '0') > 9)
					// prevent anything else other than a number
					event.preventDefault();
			}
		});
	}

	@UiField
	Label title;

	@UiField
	VerticalPanel data;

	@UiField
	VerticalPanel main;

	@UiField
	HorizontalPanel buttons;

	@UiField
	Button load;
	
	@UiField
	Button reset;

	@UiField
	Button process;
	
	@UiField
	LabelBox page;

	@UiHandler("load")
	public void load(ClickEvent event) {
		int value = page.getInt();
		if (value < 0) {
			page.value.setText("0");
			load(0);
		} else
			load(value);
	}
	
	@SuppressWarnings("unchecked")
	@UiHandler("process")
	public void process(ClickEvent event) {
		List<T> items = new ArrayList<T>();

		for (int i = data.getWidgetCount(); --i > -1;) {
			LabelCheck<T> check = (LabelCheck<T>) data.getWidget(i);
			if (check.getValue())
				items.add(check.getItem());
		}

		process(items);
	}

	@UiHandler("reset")
	public void reset(ClickEvent event) {
		reset();
		load(null);
	}
	
	private boolean suppressProcess = false;

	public void setItems(List<T> items) {
		data.clear();

		LabelCheck<T> label;
		for (T item : items) {
			label = new LabelCheck<T>(format(item), item);
			label.sinkEvents(Event.ONCLICK);
			label.addClickHandler(this);
			label.value.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					suppressProcess = true;
				}
			});
			data.add(label);
		}

		if (items.isEmpty()) {
			data.add(new Label("no results"));
		}

		process.setEnabled(!items.isEmpty());

		this.items = items;
	}

	@Override
	public void setTitle(String title) {
		this.title.setText(title);
	}
	
	public abstract String format(T item);

	public abstract void processItem(Widget w, T item);

	public abstract void load(int page);
	
	public abstract void reset();
	
	public abstract void process(List<T> items);

	@Override
	public void onClick(ClickEvent event) {
		// suppress processing click for this item
		if (suppressProcess) {
			suppressProcess = false;
			return;
		}

		Widget source = (Widget) event.getSource();
		int index = data.getWidgetIndex(source);
		processItem(source, items.get(index));
	}
}