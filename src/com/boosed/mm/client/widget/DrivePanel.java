package com.boosed.mm.client.widget;

import com.boosed.mm.client.Admin;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class DrivePanel extends Composite {

	private static DrivePanelUiBinder uiBinder = GWT.create(DrivePanelUiBinder.class);

	interface DrivePanelUiBinder extends UiBinder<Widget, DrivePanel> {
	}

	private Admin ml;

	public DrivePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		main.setCellHorizontalAlignment(button, HasAlignment.ALIGN_RIGHT);
	}

	public DrivePanel(Admin ml) {
		this();
		this.ml = ml;
	}

	@UiField
	VerticalPanel main;

	@UiField
	LabelBox drive;

	@UiField
	Button button;

	@UiHandler("button")
	void onClick(ClickEvent e) {
		ml.createDrive(drive.getValue());
	}
}