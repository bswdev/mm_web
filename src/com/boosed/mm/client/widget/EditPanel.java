package com.boosed.mm.client.widget;

import com.boosed.mm.client.Admin;
import com.boosed.mm.shared.util.DataUtil;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class EditPanel extends Composite {

	private static EditPanelUiBinder uiBinder = GWT.create(EditPanelUiBinder.class);

	interface EditPanelUiBinder extends UiBinder<Widget, EditPanel> {
	}

	private Admin ml;

	public EditPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		
		main.setCellHorizontalAlignment(buttons, HasAlignment.ALIGN_RIGHT);
	}

	public EditPanel(Admin ml) {
		this();
		this.ml = ml;
	}

	@UiField
	VerticalPanel main;

	@UiField
	HorizontalPanel buttons;
	
	@UiField
	LabelBox user;

	@UiField
	Button selling;
	
	@UiField
	Button pending;
	
	@UiField
	Button buying;

	@UiHandler("selling")
	void selling(ClickEvent e) {
		String accountId = DataUtil.getNullString(user.getValue());
		ml.loadSelling(accountId);
	}
	
	@UiHandler("pending")
	void pending(ClickEvent e) {
		ml.loadPending();
	}
	
	@UiHandler("buying")
	void buying(ClickEvent e) {
		String accountId = DataUtil.getNullString(user.getValue());
		ml.loadBuying(accountId);
	}
}