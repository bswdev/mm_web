package com.boosed.mm.client.widget;

import com.boosed.mm.client.event.AppController;
import com.boosed.mm.client.event.EngineEvent;
import com.boosed.mm.client.event.enums.EventType;

public class EngPanel extends AddEnumPanel {

	public EngPanel(AppController controller) {
		super(controller);
	}

	@Override
	public void execute(String value) {
		controller.fireEvent(new EngineEvent(EventType.ADD, value));
	}
}
