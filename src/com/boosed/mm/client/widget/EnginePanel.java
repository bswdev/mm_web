package com.boosed.mm.client.widget;

import com.boosed.mm.client.Admin;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class EnginePanel extends Composite {

	private static EnginePanelUiBinder uiBinder = GWT.create(EnginePanelUiBinder.class);

	interface EnginePanelUiBinder extends UiBinder<Widget, EnginePanel> {
	}

	private Admin ml;

	public EnginePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		main.setCellHorizontalAlignment(button, HasAlignment.ALIGN_RIGHT);
	}

	public EnginePanel(Admin ml) {
		this();
		this.ml = ml;
	}

	@UiField
	VerticalPanel main;

	@UiField
	LabelBox engine;

	@UiField
	Button button;

	@UiHandler("button")
	void onClick(ClickEvent e) {
		ml.createEngine(engine.getValue());
	}
}