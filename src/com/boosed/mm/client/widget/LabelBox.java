package com.boosed.mm.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class LabelBox extends Composite {

	private static LabelBoxUiBinder uiBinder = GWT.create(LabelBoxUiBinder.class);

	interface LabelBoxUiBinder extends UiBinder<Widget, LabelBox> {
	}

	public LabelBox() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	Label label;

	@UiField
	TextBox value;

	public LabelBox(String label) {
		this();
		this.label.setText(label);
	}

	// @UiHandler("button")
	// void onClick(ClickEvent e) {
	// Window.alert("Hello!");
	// }
	//
	// public void setText(String text) {
	// button.setText(text);
	// }

	public void setLabel(String label) {
		this.label.setText(label);
	}
	
	public String getValue() {
		return value.getText();
	}

	public void setValue(String value) {
		this.value.setText(value);
	}
	
	public String[] getStrings() {
		String[] ids = getValue().split(",");
		if ("".equals(ids[0]))
			return new String[0];
		else
			return ids;
		// for (int i = ids.length; --i > -1;)
		// rv[i] = Long.parseLong(ids[i]);
		// return rv;
	}

	public int getInt() {
		String s = getValue();
		if (s.isEmpty())
			return -1;
		else
			return Integer.parseInt(s);
	}
	
	public double getDouble() {
		String s = getValue();
		if (s.isEmpty())
			return -1;
		else
			return Double.parseDouble(s);
	}
	
	public long getLong() {
		String s = getValue();
		if (s.isEmpty())
			return -1;
		else
			return Long.parseLong(s);
	}
}
