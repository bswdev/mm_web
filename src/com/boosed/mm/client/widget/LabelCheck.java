package com.boosed.mm.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class LabelCheck<T> extends Composite implements HasClickHandlers {

	private static LabelCheckUiBinder uiBinder = GWT.create(LabelCheckUiBinder.class);

	@SuppressWarnings("unchecked")
	interface LabelCheckUiBinder extends UiBinder<Widget, LabelCheck> {
	}

	public LabelCheck() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	Label label;

	@UiField
	CheckBox value;

	private T item;

	public LabelCheck(String label, T item) {
		this();
		this.label.setText(label);
		this.item = item;
	}
	
	public void setLabel(String label) {
		this.label.setText(label);
	}

	// @UiHandler("button")
	// void onClick(ClickEvent e) {
	// Window.alert("Hello!");
	// }
	//
	// public void setText(String text) {
	// button.setText(text);
	// }

	public boolean getValue() {
		return value.getValue();
	}

	public T getItem() {
		return item;
	}
	
	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return addHandler(handler, ClickEvent.getType());
	}
}
