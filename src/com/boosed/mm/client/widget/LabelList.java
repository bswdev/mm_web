package com.boosed.mm.client.widget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public abstract class LabelList<T> extends Composite implements HasValueChangeHandlers<T> {

	private static LabelListUiBinder uiBinder = GWT.create(LabelListUiBinder.class);

	@SuppressWarnings("unchecked")
	interface LabelListUiBinder extends UiBinder<Widget, LabelList> {
	}

	Comparator<T> comparator;
	
	public LabelList(boolean multiple) {
		list = new ListBox(multiple);
		if (multiple)
			list.setVisibleItemCount(5);
		
		initWidget(uiBinder.createAndBindUi(this));

		list.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				ValueChangeEvent.fire(LabelList.this, getItem());
			}
		});
	}

	public LabelList(String name, boolean multiple) {
		this(multiple);
		label.setText(name);
	}

	@UiField
	Label label;

	@UiField(provided = true)
	ListBox list;

	List<T> items = new ArrayList<T>();

	public void setName(String label) {
		this.label.setText(label);
	}

	public void addItems(T ... items) {
		for (T item : items)
			if (!this.items.contains(item))
				this.items.add(item);
		
		//Collections.sort(this.items, comparator);
		setItems(this.items);
	}
	
	public void setItems(List<T> items) {
		// do not set these items if the populating list is empty
		if (items.isEmpty())
			return;
		
		list.clear();
		
		if (comparator != null)
			Collections.sort(items, comparator);
		
		this.items = items;
		for (T item : items)
			list.addItem(getName(item));
	}

	public T getItem() {
		int index = list.getSelectedIndex();
		return index == -1 ? null : items.get(index);
	}

	public int getSelectedIndex() {
		return list.getSelectedIndex();
	}

	public void setSelected(T item) {
		list.setSelectedIndex(items.indexOf(item));
	}
	
	public void removeSelected() {
		List<Integer> selected = new ArrayList<Integer>();
		
		for (int i = list.getItemCount(); --i > -1;)
			if (list.isItemSelected(i))
				selected.add(i);
		
		for (int i : selected) {
			items.remove(i);
			list.removeItem(i);
		}
	}
	
	public abstract String getName(T item);

	@Override
	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<T> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	/**
	 * Notify listeners that a change has occurred and updates should be made.
	 */
	public void fireChange() {
		ValueChangeEvent.fire(LabelList.this, getItem());
	}
}
