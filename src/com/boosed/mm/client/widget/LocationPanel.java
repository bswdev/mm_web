package com.boosed.mm.client.widget;

import com.boosed.mm.client.Admin;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class LocationPanel extends Composite {

	private static LocationPanelUiBinder uiBinder = GWT.create(LocationPanelUiBinder.class);

	interface LocationPanelUiBinder extends UiBinder<Widget, LocationPanel> {
	}

	private Admin ml;

	public LocationPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		main.setCellHorizontalAlignment(button, HasAlignment.ALIGN_RIGHT);
	}

	public LocationPanel(Admin ml) {
		this();
		this.ml = ml;
	}

	@UiField
	VerticalPanel main;

	@UiField
	LabelBox latitude;

	@UiField
	LabelBox longitude;

	@UiField
	LabelBox zip;

	@UiField
	LabelBox city;

	@UiField
	LabelBox state;

	@UiField
	Button button;

	@UiHandler("button")
	void onClick(ClickEvent e) {
		ml.createLocation(zip.getValue(), state.getValue(), city.getValue(), latitude.getDouble(),
				longitude.getDouble());
	}
}