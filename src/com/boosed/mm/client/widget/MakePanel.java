package com.boosed.mm.client.widget;

import com.boosed.mm.client.Admin;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class MakePanel extends Composite {

	private static MakePanelUiBinder uiBinder = GWT.create(MakePanelUiBinder.class);

	interface MakePanelUiBinder extends UiBinder<Widget, MakePanel> {
	}

	private Admin ml;

	public MakePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		main.setCellHorizontalAlignment(button, HasAlignment.ALIGN_RIGHT);
	}

	public MakePanel(Admin ml) {
		this();
		this.ml = ml;
	}

	@UiField
	VerticalPanel main;

	@UiField
	LabelBox make;

	@UiField
	Button button;

	@UiHandler("button")
	void onClick(ClickEvent e) {
		ml.createMake(make.getValue());
	}
}