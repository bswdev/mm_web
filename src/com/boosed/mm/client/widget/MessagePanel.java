package com.boosed.mm.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public abstract class MessagePanel extends Composite {

	private DialogBox popup;

	private static MessagePanelUiBinder uiBinder = GWT
			.create(MessagePanelUiBinder.class);

	interface MessagePanelUiBinder extends UiBinder<Widget, MessagePanel> {
	}

	public MessagePanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public MessagePanel(DialogBox popup) {
		this();
		this.popup = popup;

		main.setCellHorizontalAlignment(buttons, HasAlignment.ALIGN_RIGHT);
	}

	private Long referenceId;

	@UiField
	Label reference;

	@UiField
	TextArea message;

	@UiField
	Button cancel;

	@UiField
	Button send;

	@UiField
	VerticalPanel main;

	@UiField
	HorizontalPanel buttons;

	@UiHandler("cancel")
	void cancel(ClickEvent e) {
		popup.hide();
	}

	@UiHandler("send")
	void send(ClickEvent e) {
		doSend(referenceId, message.getText());
		
		// clear the field
		message.setText("");
		// ml.sendMessage(referenceId, message.getText());
	}

	public void setReference(Long reference) {
		referenceId = reference;
		//this.reference.setText(reference + "");
	}
	
	public void setHeader(String text) {
		reference.setText(text);
	}
	
	public void setText(String text) {
		message.setText(text);
	}

	abstract public void doSend(long reference, String message);
}