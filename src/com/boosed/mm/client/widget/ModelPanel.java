package com.boosed.mm.client.widget;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.boosed.mm.client.Admin;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.FieldModel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.objectify.Key;

public class ModelPanel extends Composite {

	private static ModelPanelUiBinder uiBinder = GWT.create(ModelPanelUiBinder.class);

	interface ModelPanelUiBinder extends UiBinder<Widget, ModelPanel> {
	}

	private Admin ml;
	
	public ModelPanel() {
		make = new LabelList<Key<Make>>(false) {
			@Override
			public String getName(Key<Make> item) {
				return item.getName();
			}
		};
		
		model = new LabelList<Model>(false) {
			@Override
			public String getName(Model item) {
				return item.toString();
			}
		};
		
		doors = new CheckPanel<Integer>() {
			@Override
			public String getName(Integer item) {
				return item.toString();
			}
		};

		drivetrains = new CheckPanel<Key<Drivetrain>>() {
			@Override
			public String getName(Key<Drivetrain> item) {
				return item.getName();
			}
		};

		engines = new CheckPanel<Key<Engine>>() {
			@Override
			public String getName(Key<Engine> item) {
				return item.getName();
			}
		};
		
		transmissions = new CheckPanel<Key<Transmission>>() {
			@Override
			public String getName(Key<Transmission> item) {
				return item.getName();
			}
		};
		
		types = new CheckPanel<Key<Type>>() {
			@Override
			public String getName(Key<Type> item) {
				return item.getName();
			}
		};
		
		years = new LabelList<Integer>(true) {
			@Override
			public String getName(Integer item) {
				return item.toString();
			}
		};
		
		initWidget(uiBinder.createAndBindUi(this));
		main.setCellHorizontalAlignment(buttons1, HasAlignment.ALIGN_RIGHT);
		//main.setCellHorizontalAlignment(buttons2, HasAlignment.ALIGN_RIGHT);
		main.setCellHorizontalAlignment(buttons3, HasAlignment.ALIGN_RIGHT);
		
		make.comparator = new Comparator<Key<Make>>() {
			@Override
			public int compare(Key<Make> arg0, Key<Make> arg1) {
				return arg0.compareTo(arg1);
			}
		};
		
		model.comparator = new Comparator<Model>() {
			@Override
			public int compare(Model arg0, Model arg1) {
				return arg0.compareTo(arg1);
			}
		};
		
		years.comparator = new Comparator<Integer>() {
			@Override
			public int compare(Integer arg0, Integer arg1) {
				return arg0.compareTo(arg1);
			}
		};
		
		// buttons
		buttons4.setCellHorizontalAlignment(buttons2, HasAlignment.ALIGN_RIGHT);
		buttons2.setCellVerticalAlignment(button, HasAlignment.ALIGN_BOTTOM);
		buttons2.setCellVerticalAlignment(clear, HasAlignment.ALIGN_BOTTOM);
	}

	public ModelPanel(Admin ml) {
		this();
		this.ml = ml;
	}

	public void setModel(Model model) {
		if (model == null) {
			years.setItems(new ArrayList<Integer>());
			types.clear();
			return;
		}
		
		// set the doors
		doors.setChecked(model.doors);
		
		// set the drivetrains
		drivetrains.setChecked(model.drivetrains);
		
		// set the engines
		engines.setChecked(model.engines);
		
		// set the transmissions
		transmissions.setChecked(model.transmissions);
		
		// set the types
		types.setChecked(model.types);
		
		// set the years
		years.setItems(model.years);
		
		// set the name
		name.value.setText(model.toString());
	}
	
	@UiField
	VerticalPanel main;
	
	@UiField
	LabelBox name;
	
	@UiField
	LabelBox rename1;
	
	@UiField
	LabelBox rename2;

	@UiField
	LabelBox year;
	
	@UiField(provided = true)
	public LabelList<Integer> years;
	
	@UiField(provided = true)
	public LabelList<Key<Make>> make;
	
	@UiField(provided = true)
	public LabelList<Model> model;
	
	@UiField(provided = true)
	public CheckPanel<Integer> doors;

	@UiField(provided = true)
	public CheckPanel<Key<Drivetrain>> drivetrains;
	
	@UiField(provided = true)
	public CheckPanel<Key<Engine>> engines;
	
	@UiField(provided = true)
	public CheckPanel<Key<Transmission>> transmissions;
	
	@UiField(provided = true)
	public CheckPanel<Key<Type>> types;
	
	@UiField
	HorizontalPanel buttons1;
	
	@UiField
	HorizontalPanel buttons2;
	
	@UiField
	HorizontalPanel buttons3;
	
	@UiField
	HorizontalPanel buttons4;
	
	@UiField
	Button button;
	
	@UiField
	Button query;
	
	@UiField
	Button clear;
	
	@UiField
	Button add;
	
	@UiField
	Button remove;
	
	@UiField
	Button reset;
	
	@UiField
	Button change;
	
	@UiHandler("change")
	void rename(ClickEvent e) {
		ml.renameModel(model.getItem().desc, rename1.getValue(), rename2.getValue());
	}
	
	@UiHandler("reset")
	void reset(ClickEvent e) {
		years.setItems(new ArrayList<Integer>());
	}
	
	@UiHandler("clear")
	void clear(ClickEvent e) {
		name.value.setText("");
		year.value.setText("");
		years.setItems(new ArrayList<Integer>());
		drivetrains.clear();
		engines.clear();
		transmissions.clear();
		types.clear();
		doors.clear();
	}
	
	@UiHandler("query")
	void query(ClickEvent e) {
		ml.loadModel(make.getItem().getName() + " " + name.getValue());
	}
	
	@UiHandler("add")
	void click(ClickEvent e) {
		List<Integer> values = new ArrayList<Integer>();
		
		// handle years
		String[] periods = year.getValue().split(",");
		for (String period : periods) {
			if (period.contains("-")) {
				String[] span = period.split("-");
				int low = Integer.parseInt(span[0]) - 1;
				for (int i = Integer.parseInt(span[1]); i > low; i--)
					values.add(i);
			} else
				values.add(Integer.parseInt(period));
		}
		
		years.addItems(values.toArray(new Integer[0]));
	}
	
	@UiHandler("remove")
	void remove(ClickEvent e) {
		years.removeSelected();
	}
	
	@UiHandler("button")
	void onClick(ClickEvent e) {
		Map<FieldModel, Serializable> fields = new HashMap<FieldModel, Serializable>();
		
		// actually this is the id
		fields.put(FieldModel.ID, make.getItem().getName() + " " + name.getValue());
		
		// make
		fields.put(FieldModel.MAKE, make.getItem().getName());
		
		//Model model = new Model(name.getValue(), make.getItem());
		fields.put(FieldModel.TYPES, (Serializable) new HashSet<Key<Type>>(types.getItems()));
		
		// doors
		fields.put(FieldModel.DOORS, (Serializable) doors.getItems());
		
		// drivetrains
		fields.put(FieldModel.DRIVETRAINS, (Serializable) drivetrains.getItems());
		
		// engines
		fields.put(FieldModel.ENGINES, (Serializable) engines.getItems());
		
		// transmissions
		fields.put(FieldModel.TRANSMISSIONS, (Serializable) transmissions.getItems());
		
		//model.addTypes(types.getItems());

//		List<Integer> yearz = new ArrayList<Integer>();
//		for (String year : years.getStrings())
//			yearz.add(Integer.parseInt(year));
//		years.get
		fields.put(FieldModel.YEARS, (Serializable) years.items);
		
		ml.updateModel(fields);
	}
}