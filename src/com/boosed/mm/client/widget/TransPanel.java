package com.boosed.mm.client.widget;

import com.boosed.mm.client.event.AppController;
import com.boosed.mm.client.event.TransmissionEvent;
import com.boosed.mm.client.event.enums.EventType;

public class TransPanel extends AddEnumPanel {

	public TransPanel(AppController controller) {
		super(controller);
	}

	@Override
	public void execute(String value) {
		controller.fireEvent(new TransmissionEvent(EventType.ADD, value));
	}
}
