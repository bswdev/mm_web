package com.boosed.mm.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;

public class UploadPanel extends FormPanel {

	private final Button submit = new Button("Submit");

	private TextBox adId;

	public UploadPanel() {

		// setAction(GWT.getModuleBaseURL() + "images/upload");
		// setAction(GWT.getHostPageBaseURL() + "upload");
		setAction(GWT.getModuleBaseURL() + "upload");
		// System.out.println(GWT.getHostPageBaseURL() + "images/upload");
		// file input uses the POST method, and multipart MIME encoding
		setEncoding(FormPanel.ENCODING_MULTIPART);
		setMethod(FormPanel.METHOD_POST);

		final HorizontalPanel uploadPanel = new HorizontalPanel();
		uploadPanel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
		//uploadPanel.setSpacing(2);
		setWidget(uploadPanel);

		adId = new TextBox();
		adId.setVisible(false);
		adId.setName("ad");
		uploadPanel.add(adId);
		FileUpload upload = new FileUpload();
		upload.setName("pic");
		// add the preview panel
		// uploadPanel.add(pav);

		// VerticalPanel inputPanel = new VerticalPanel();
		// inputPanel.add(upload);

		submit.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submit();
				// UploadPanel.this.parent.hide();
			}
		});

		uploadPanel.add(upload);

		uploadPanel.setHorizontalAlignment(HasAlignment.ALIGN_RIGHT);
		uploadPanel.add(submit);
		// create and add the file upload elements
		// for (int i = 0; i < mediators.length; i++) {
		// mediators[i] = new PhotoMediator(i + 1, photoStatus);
		// // mediators[i].addChangeListener(this);
		// mediators[i].addValueChangeHandler(this);
		// // uploadPanel.add(uploads[i]);
		// inputPanel.add(mediators[i].getWidget());
		// }

		// inputPanel.add(createControls());

		// IE7 does not allow padding on tables, must nest in a div
		// PaddedPanel paddedPanel = new PaddedPanel();
		// paddedPanel.addStyleDependentName("bordered");
		// paddedPanel.setPadding(3);
		// paddedPanel.setWidth(502);

		// // add controls
		// paddedPanel.setWidget(inputPanel);
		// uploadPanel.add(paddedPanel);

		// Add an event handler to the form.
		addSubmitHandler(new SubmitHandler() {
			public void onSubmit(SubmitEvent event) {
				GWT.log("submitting");
				// this event is fired before the form is submitted
				// photoStatus.display(messages.photoUpload(), Status.WAIT,
				// false);
				// example validation routine
				// if (tb.getText().length() == 0) {
				// Window.alert("The text box must not be empty");
				// event.setCancelled(true);
				// }
			}
		});

		addSubmitCompleteHandler(new SubmitCompleteHandler() {
			public void onSubmitComplete(SubmitCompleteEvent event) {

				// When the form submission is successfully completed, this
				// event is
				// fired. Assuming the service returned a response of type
				// text/plain,
				// we can get the result text here (see the FormPanel
				// documentation for
				// further explanation).
				String result = event.getResults();
				if (result.indexOf("error") == -1) {
					GWT.log("success uploading");
					// refresh(messages.photoSubmissionSuccess());
				} else {
					// strip off tag if it is present
					int index = result.indexOf('>');
					if (index != -1)
						result = result.substring(index + 1);

					// remove "error - " if present
					if (result.startsWith("error"))
						result = result.substring(8);

					GWT.log("something bad happened");
					// photoStatus.display(result, Status.ERROR, true);
				}
			}
		});
	}

	public void setId(String id) {
		adId.setText(id);
	}
}