package com.boosed.mm.client.widget;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.boosed.mm.client.Admin;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class ViewPanel extends Composite implements ClickHandler {

	private static ViewPanelUiBinder uiBinder = GWT.create(ViewPanelUiBinder.class);

	interface ViewPanelUiBinder extends UiBinder<Widget, ViewPanel> {
	}

	private Admin ml;

	public ViewPanel(Admin ml) {
		this.ml = ml;
		up = new UploadPanel();
		
		initWidget(uiBinder.createAndBindUi(this));

		//approved = new ListBox(true);
		approved.setMultipleSelect(true);
		approved.addClickHandler(this);
		approved.setVisibleItemCount(5);
		
		//unapproved = new ListBox(true);
		unapproved.setMultipleSelect(true);
		unapproved.addClickHandler(this);
		unapproved.setVisibleItemCount(5);
		
		main.add(new HTML("<br/>"));
		main.add(new Label("image upload:"));
		main.add(up);
		
		// alignment
		control.setCellVerticalAlignment(ids, HasAlignment.ALIGN_MIDDLE);
		main.setCellHorizontalAlignment(buttons, HasAlignment.ALIGN_RIGHT);
		//main.setCellHorizontalAlignment(control, HasAlignment.ALIGN_RIGHT);
		
		// TODO: remove this setting
		//image.setVisible(false);
		image.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				image.setVisible(false);
			}
		});
	}

	private UploadPanel up;
	@UiField
	Image image;

	@UiField
	Label distance;
	
	@UiField
	Label mileage;

	@UiField
	Label price;

	@UiField
	Label year;

	@UiField
	Label model;

	@UiField
	Label interior;

	@UiField
	Label exterior;

	@UiField
	Label location;

	@UiField
	Label vin;

	@UiField
	Label condition;
	
	@UiField
	Label doors;

	@UiField
	Label engine;

	@UiField
	Label transmission;

	@UiField
	Label drivetrain;

	@UiField
	Label content;
	
	@UiField
	Label name;
	
	@UiField
	Label email;
	
	@UiField
	Label accLocation;
	
	@UiField
	Label address;
	
	@UiField
	Label phone;
	
	@UiField
	Label url;
	
	@UiField
	TextArea pending;
	
	@UiField
	ListBox approved;

	@UiField
	ListBox unapproved;

	@UiField
	Button alert;
	
	@UiField
	Button approve;

	@UiField
	Button reject;

	@UiField
	Button snapshot;

	@UiField
	Button delete;
	
	@UiField
	Button modify;
	
	@UiField
	Button confirm;
	
	@UiField
	Button deny;
	
	@UiField
	Button inquiry;
	
	@UiField
	Button approval;
	
	@UiField
	Button bookmark;

	@UiField
	Button edit;
	
	@UiField
	Label ids;

	@UiField
	VerticalPanel main;
	
	@UiField
	HorizontalPanel buttons;
	
	@UiField
	HorizontalPanel control;

	private Long ad;
	private Car car;

	@UiHandler("alert")
	public void alert(ClickEvent e) {
		ml.setAlert((Widget) e.getSource(), car);
	}

	@UiHandler("edit")
	public void edit(ClickEvent e) {
		ml.edit(car);
	}
	
	@UiHandler("snapshot")
	public void snapshot(ClickEvent e) {
		// set image as car snapshot
		Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();
		fields.put(ad, FieldAd.ID_AD);
		fields.put(car.id, FieldAd.ID_CAR);
		fields.put(approved.getItemText(approved.getSelectedIndex()), FieldAd.IMG_CAR);

		ml.updateAd(fields);
	}

	@UiHandler("delete")
	public void delete(ClickEvent e) {
		// delete the approved image
		Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();
		fields.put(ad, FieldAd.ID_AD);
		
		// add all approved images
		for (int i = approved.getItemCount(); --i > -1;)
			if (approved.isItemSelected(i))
				fields.put(approved.getItemText(i), FieldAd.IMG_DELETE);

		ml.updateAd(fields);
	}

	@UiHandler("approve")
	public void approve(ClickEvent e) {
		// approve the image
		Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();
		fields.put(ad, FieldAd.ID_AD);
		
		// add all unapproved images
		for (int i = unapproved.getItemCount(); --i > -1;)
			if (unapproved.isItemSelected(i))
				fields.put(unapproved.getItemText(i), FieldAd.IMG_APPROVE);

		ml.updateAd(fields);
	}

	@UiHandler("reject")
	void reject(ClickEvent e) {
		// reject the unapproved image
		Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();
		fields.put(ad, FieldAd.ID_AD);
		fields.put(car.id, FieldAd.ID_CAR);
		
		// add all unapproved images
		for (int i = unapproved.getItemCount(); --i > -1;)
			if (unapproved.isItemSelected(i))
				fields.put(unapproved.getItemText(i), FieldAd.IMG_REJECT);

		ml.updateAd(fields);
	}

	@UiHandler("modify")
	void modify(ClickEvent e) {
		// submit pending text
		Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();
		fields.put(ad, FieldAd.ID_AD);
		fields.put(pending.getText(), FieldAd.AD);

		ml.updateAd(fields);
	}
	
	@UiHandler("confirm")
	void confirm(ClickEvent e) {
		// confirm pending text
		Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();
		fields.put(ad, FieldAd.ID_AD);
		fields.put(confirm.hashCode(), FieldAd.AD_APPROVE);
		
		ml.updateAd(fields);
	}
	
	@UiHandler("deny")
	void deny(ClickEvent e) {
		// deny pending text
		Map<Serializable, FieldAd> fields = new HashMap<Serializable, FieldAd>();
		fields.put(ad, FieldAd.ID_AD);
		fields.put(car.id, FieldAd.ID_CAR);
		fields.put(deny.hashCode(), FieldAd.AD_REJECT);
		
		ml.updateAd(fields);
	}
	
	@UiHandler("inquiry")
	void inquiry(ClickEvent e) {
		ml.setInquiry((Widget) e.getSource(), car);
	}
	
	@UiHandler("approval")
	void approval(ClickEvent e) {
		// set opposite of the current state
		ml.pay(!enabled, car.id);
	}
	
	@UiHandler("bookmark")
	void bookmark(ClickEvent e) {
		// add as a bookmark
		//Map<FieldAccount, Serializable> fields = new HashMap<FieldAccount, Serializable>();
		//fields.put(FieldAccount.CAR_BUY, car.id);
		//ml.updateAccount(fields);
		ml.addBookmark(car.id);
	}
	
	public void setAction(String action) {
		up.setAction(action);
	}
	
	//private Key<Model> modelKey;

	private boolean enabled;
	
	public void setApproval(boolean enabled) {
		// set the status
		this.enabled = enabled;
		
		// set the proper text for the button
		approval.setText(enabled ? "disable" : "enable");
	}
	
	public Car getCar() {
		return car;
	}
	
	public void setCar(Car car) {
		this.car = car;
	
		//modelKey = car.model;
		distance.setText("distance: " + car.distance + " miles");
		mileage.setText("mileage: " + car.mileage + " miles");
		price.setText("price: $" + car.price);
		year.setText("year: " + car.year);
		condition.setText("condition: " + car.conditions.get(0).toString());
		model.setText("model: " + car.model.getName());
		interior.setText("interior: " + car.interior.getName());
		exterior.setText("exterior: " + car.exterior.getName());
		location.setText("car location: " + car.location);
		
		// vin
		if (car.vin != null) {
			vin.setText("vin: " + car.vin);
			vin.setVisible(true);
		} else
			vin.setVisible(false);
		
		doors.setText("doors: " + car.doors);
		engine.setText("engine: " + car.engine.getName());
		transmission.setText("trans: " + car.transmission.getName());
		drivetrain.setText("drive: " + car.drivetrain.getName());

		if (!"".equals(car.image))
			image.setUrl(car.image);
		
		// set the approval state of this ad
		setApproval(car.isActive());
	}

//	List<String> appImages = new ArrayList<String>();
//	List<String> unImages = new ArrayList<String>();

	public void setAccount(Account account) {
		name.setText("name: " + account.name + (account.dealer ? " (dealer)" : ""));
		
		// email
		if (account.email != null) {
			email.setText("email: " + account.email);
			email.setVisible(true);
		} else
			email.setVisible(false);
		
		// phone
		if (account.phone != null) {
			phone.setText("phone: " + account.phone);
			phone.setVisible(true);
		} else
			phone.setVisible(false);
		
		// address
		if (account.address != null) {
			address.setText("address: " + account.address);
			address.setVisible(true);
		} else
			address.setVisible(false);
		
		// location
		if (account.postal != null) {
			accLocation.setText("location: " + account.postal);
			accLocation.setVisible(true);
		} else
			accLocation.setVisible(false);
		
		// url
		if (account.url != null) {
			url.setText("url: " + account.url);
			url.setVisible(true);
		} else
			url.setVisible(false);
	}
	
	public void setAd(Ad ad) {
		this.ad = ad.id;

		ids.setText("car: " + car.id + ", ad: " + this.ad);

		// set content
		String content = ad.getContent();
		if (content == null)
			this.content.setText("none");
		else
			this.content.setText(content);
		
		// set pending
		pending.setText(ad.getPending());
		
		//int i = 0;
		approved.clear();
		//appImages.clear();
		for (Entry<String, String> entry : ad.getImages(true).entrySet()) {
			approved.addItem(entry.getKey(), entry.getValue());
			//appImages.add(entry.getValue());
		}

		//i = 0;
		unapproved.clear();
		//unImages.clear();
		for (Entry<String, String> entry : ad.getImages(false).entrySet()) {
			unapproved.addItem(entry.getKey(), entry.getValue());
			//unImages.add(entry.getValue());
		}

		// set ad id on upload
		up.setId(this.ad + "");
	}

	@Override
	public void onClick(ClickEvent event) {
		ListBox rv = (ListBox) event.getSource();
		int index = rv.getSelectedIndex();
		if (index != -1) {
			//String url = rv.equals(approved) ? appImages.get(index) : unImages.get(index);
			String url = rv.getValue(rv.getSelectedIndex());
			url = url.replace("0.0.0.0", "127.0.0.1");
			image.setUrl(url);
			image.setVisible(true);
		}
	}
	
//	@Override
//	public void onChange(ChangeEvent event) {
//		String url = appImages.get(approved.getSelectedIndex());
//		image.setUrl(url);
//	}
}