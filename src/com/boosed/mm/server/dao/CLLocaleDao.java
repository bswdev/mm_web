package com.boosed.mm.server.dao;

import com.boosed.gae.server.dao.GenericDao;
import com.boosed.mm.shared.db.CLLocale;

public interface CLLocaleDao extends GenericDao<CLLocale> {

}
