package com.boosed.mm.server.dao;

import com.boosed.gae.server.dao.GenericDao;
import com.boosed.mm.shared.db.Engine;

public interface EngineDao extends GenericDao<Engine> {

}
