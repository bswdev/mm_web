package com.boosed.mm.server.dao;

import com.boosed.gae.server.dao.GenericDao;
import com.boosed.mm.shared.db.Make;

public interface MakeDao extends GenericDao<Make> {

}
