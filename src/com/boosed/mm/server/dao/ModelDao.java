package com.boosed.mm.server.dao;

import java.util.List;

import com.boosed.gae.server.dao.GenericDao;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Type;
import com.googlecode.objectify.Key;

public interface ModelDao extends GenericDao<Model> {

//	/**
//	 * Find a <code>List</code> of <code>Model</code>s by <code>Make</code>.
//	 * 
//	 * @param make
//	 * @return
//	 */
//	public List<Model> findByMakes(List<Key<Make>> makes, boolean filterEmpty);

//	/**
//	 * Find a <code>List</code> of <code>Model</code>s by <code>Type</code>s.
//	 * 
//	 * @param types
//	 * @return
//	 */
//	public List<Model> findByTypes(List<Key<Type>> types, boolean filterEmpty);

	/**
	 * Find a <code>List</code> of <code>Model</code>s by a <code>Type</code> and a
	 * <code>Make</code>.
	 * 
	 * @param type
	 * @param make
	 * @param filterEmpty
	 * @return
	 */
	public List<Model> findByTypeAndMake(Key<Type> type, Key<Make> make,
			boolean filterEmpty);
	
	/**
	 * Find a <code>List</code> of <code>Model</code>s by <code>Type</code>s and
	 * <code>Make</code>s.
	 * 
	 * @param types
	 * @param makes
	 * @param limits
	 * @return
	 */
	public List<Model> findByTypesAndMakes(List<Key<Type>> types, List<Key<Make>> makes,
			boolean filterEmpty, int ... limits);

//	/**
//	 * Find a <code>List</code> of <code>Model</code>s <code>Key</code>s by
//	 * <code>Make</code>.
//	 * 
//	 * @param make
//	 * @return
//	 */
//	public List<Key<Model>> findKeysByMakes(List<Key<Make>> makes, boolean filterEmpty);

//	/**
//	 * Find a <code>List</code> of <code>Model</code>s <code>Key</code>s by
//	 * <code>Type</code>s.
//	 * 
//	 * @param types
//	 * @return
//	 */
//	public List<Key<Model>> findKeysByTypes(List<Key<Type>> types, boolean filterEmpty);

	/**
	 * Find a <code>List</code> of <code>Model</code>s <code>Key</code>s by
	 * <code>Type</code>s and <code>Make</code>s.
	 * 
	 * @param types
	 * @param makes
	 * @return
	 */
	public List<Key<Model>> findKeysByTypesAndMakes(List<Key<Type>> types, List<Key<Make>> makes, boolean filterEmpty);
}
