package com.boosed.mm.server.dao;

import com.boosed.gae.server.dao.GenericDao;
import com.boosed.mm.shared.db.Transmission;

public interface TransmissionDao extends GenericDao<Transmission> {

}
