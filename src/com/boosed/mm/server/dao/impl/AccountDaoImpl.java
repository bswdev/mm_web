package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.AccountDao;
import com.boosed.mm.shared.db.Account;

public class AccountDaoImpl extends GenericDaoImpl<Account> implements AccountDao {

	public AccountDaoImpl() {
		super(Account.class);
	}
}
