package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.AdDao;
import com.boosed.mm.shared.db.Ad;

public class AdDaoImpl extends GenericDaoImpl<Ad> implements AdDao {

	public AdDaoImpl() {
		super(Ad.class);
	}
}
