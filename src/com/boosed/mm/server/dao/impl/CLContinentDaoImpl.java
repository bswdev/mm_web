package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.CLContinentDao;
import com.boosed.mm.shared.db.CLContinent;

public class CLContinentDaoImpl extends GenericDaoImpl<CLContinent> implements CLContinentDao {

	public CLContinentDaoImpl() {
		super(CLContinent.class);
	}
}
