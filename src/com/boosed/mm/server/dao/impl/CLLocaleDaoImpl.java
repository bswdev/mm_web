package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.CLLocaleDao;
import com.boosed.mm.shared.db.CLLocale;

public class CLLocaleDaoImpl extends GenericDaoImpl<CLLocale> implements CLLocaleDao {

	public CLLocaleDaoImpl() {
		super(CLLocale.class);
	}
}
