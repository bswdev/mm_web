package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.CLStateDao;
import com.boosed.mm.shared.db.CLState;

public class CLStateDaoImpl extends GenericDaoImpl<CLState> implements CLStateDao {

	public CLStateDaoImpl() {
		super(CLState.class);
	}
}
