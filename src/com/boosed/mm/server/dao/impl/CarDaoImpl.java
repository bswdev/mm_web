package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.CarDao;
import com.boosed.mm.shared.db.Car;

public class CarDaoImpl extends GenericDaoImpl<Car> implements CarDao {

	public CarDaoImpl() {
		super(Car.class);
	}
}