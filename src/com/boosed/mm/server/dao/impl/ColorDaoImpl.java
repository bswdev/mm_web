package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.ColorDao;
import com.boosed.mm.shared.db.Color;

public class ColorDaoImpl extends GenericDaoImpl<Color> implements ColorDao {

	public ColorDaoImpl() {
		super(Color.class);
	}
}
