package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.DrivetrainDao;
import com.boosed.mm.shared.db.Drivetrain;

public class DrivetrainDaoImpl extends GenericDaoImpl<Drivetrain> implements DrivetrainDao {

	public DrivetrainDaoImpl() {
		super(Drivetrain.class);
	}
}
