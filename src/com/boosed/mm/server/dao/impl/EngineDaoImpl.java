package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.EngineDao;
import com.boosed.mm.shared.db.Engine;

public class EngineDaoImpl extends GenericDaoImpl<Engine> implements EngineDao {

	public EngineDaoImpl() {
		super(Engine.class);
	}
}
