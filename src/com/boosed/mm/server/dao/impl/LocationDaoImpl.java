package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.LocationDao;
import com.boosed.mm.shared.db.Location;

public class LocationDaoImpl extends GenericDaoImpl<Location> implements LocationDao {

	public LocationDaoImpl() {
		super(Location.class);
	}
}
