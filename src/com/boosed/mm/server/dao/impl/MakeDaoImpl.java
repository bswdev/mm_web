package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.MakeDao;
import com.boosed.mm.shared.db.Make;

public class MakeDaoImpl extends GenericDaoImpl<Make> implements MakeDao {

	public MakeDaoImpl() {
		super(Make.class);
	}
}
