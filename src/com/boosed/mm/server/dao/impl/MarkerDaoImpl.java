package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.MarkerDao;
import com.boosed.mm.shared.db.Marker;

public class MarkerDaoImpl extends GenericDaoImpl<Marker> implements MarkerDao {

	public MarkerDaoImpl() {
		super(Marker.class);
	}
}
