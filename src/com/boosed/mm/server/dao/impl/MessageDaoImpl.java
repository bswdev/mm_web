package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.MessageDao;
import com.boosed.mm.shared.db.Message;

public class MessageDaoImpl extends GenericDaoImpl<Message> implements MessageDao {

	public MessageDaoImpl() {
		super(Message.class);
	}
}