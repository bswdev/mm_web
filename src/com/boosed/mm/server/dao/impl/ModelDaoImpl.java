package com.boosed.mm.server.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.criterion.EQCriterion;
import com.boosed.gae.server.criterion.InCriterion;
import com.boosed.gae.server.criterion.NotZeroCriterion;
import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.ModelDao;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Type;
import com.googlecode.objectify.Key;

public class ModelDaoImpl extends GenericDaoImpl<Model> implements ModelDao {

	public ModelDaoImpl() {
		super(Model.class);
	}

//	@Override
//	public List<Model> findByMakes(List<Key<Make>> makes, boolean filterEmpty) {
//		return findByConditions(createCriteria(null, makes, filterEmpty));
//	}

//	@Override
//	public List<Model> findByTypes(List<Key<Type>> types, boolean filterEmpty) {
//		return findByConditions(createCriteria(types, null, filterEmpty));
//	}

	@Override
	public List<Model> findByTypeAndMake(Key<Type> type, Key<Make> make, boolean filterEmpty) {
		// prepare criterion
		List<Criterion> criteria = new ArrayList<Criterion>();
		criteria.add(new EQCriterion("types", type));
		criteria.add(new EQCriterion("make", make));
		criteria.add(new NotZeroCriterion("total"));
		
		// return matches
		return findAllByCriteria(criteria);
	}
	
	@Override
	public List<Model> findByTypesAndMakes(List<Key<Type>> types, List<Key<Make>> makes,
			boolean filterEmpty, int... limits) {
		// return empty list if makes is empty (then this will return all models or all models of a given type!)
		if (makes.isEmpty())
			return new ArrayList<Model>();
		
		List<Criterion> criteria = createCriteria(types, makes, filterEmpty);

//		if (limits.length != 0) {
//			// offset
//			criteria.add(new OffsetCriterion(limits[0]));
//
//			// limit
//			criteria.add(new LimitCriterion(limits[1]));
//		}

		return findAllByCriteria(criteria);
	}

//	@Override
//	public List<Key<Model>> findKeysByMakes(List<Key<Make>> makes, boolean filterEmpty) {
//		return findKeysByConditions(createCriteria(null, makes, filterEmpty));
//	}

//	@Override
//	public List<Key<Model>> findKeysByTypes(List<Key<Type>> types, boolean filterEmpty) {
//		return findKeysByConditions(createCriteria(types, null, filterEmpty));
//	}

	@Override
	public List<Key<Model>> findKeysByTypesAndMakes(List<Key<Type>> types, List<Key<Make>> makes,
			boolean filterEmpty) {
		return findKeysByCriteria(createCriteria(types, makes, filterEmpty));
	}

	private List<Criterion> createCriteria(List<Key<Type>> types, List<Key<Make>> makes,
			boolean filterEmpty) {
		List<Criterion> criteria = new ArrayList<Criterion>();

		// only filter by types if not null
		if (types != null)
			criteria.add(new InCriterion<Key<Type>>("types", types));

		// only filter by makes if not null
		if (makes != null)
			criteria.add(new InCriterion<Key<Make>>("make", makes));

		// filter out empty models
		if (filterEmpty)
			criteria.add(new NotZeroCriterion("total"));

		return criteria;
	}
}