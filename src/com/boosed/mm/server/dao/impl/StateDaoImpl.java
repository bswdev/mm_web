package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.StateDao;
import com.boosed.mm.shared.db.State;

public class StateDaoImpl extends GenericDaoImpl<State> implements StateDao {

	public StateDaoImpl() {
		super(State.class);
	}
}
