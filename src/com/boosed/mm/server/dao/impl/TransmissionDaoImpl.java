package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.TransmissionDao;
import com.boosed.mm.shared.db.Transmission;

public class TransmissionDaoImpl extends GenericDaoImpl<Transmission> implements TransmissionDao {

	public TransmissionDaoImpl() {
		super(Transmission.class);
	}
}
