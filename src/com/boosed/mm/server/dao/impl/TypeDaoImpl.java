package com.boosed.mm.server.dao.impl;

import com.boosed.gae.server.dao.impl.GenericDaoImpl;
import com.boosed.mm.server.dao.TypeDao;
import com.boosed.mm.shared.db.Type;

public class TypeDaoImpl extends GenericDaoImpl<Type> implements TypeDao {

	public TypeDaoImpl() {
		super(Type.class);
	}
}
