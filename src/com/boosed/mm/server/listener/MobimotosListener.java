package com.boosed.mm.server.listener;

import com.boosed.mm.server.module.ServerModule;
import com.boosed.mm.server.module.ServletModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

public class MobimotosListener extends GuiceServletContextListener {

	// private Logger log = Logger.getLogger(getClass().getName());
	public static Injector INJECTOR;

	@Override
	protected Injector getInjector() {
		if (INJECTOR == null)
			INJECTOR = Guice.createInjector(new ServerModule(), new ServletModule());
		
		return INJECTOR;
	}
}
