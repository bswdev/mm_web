package com.boosed.mm.server.mapper;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;

import com.boosed.mm.server.listener.MobimotosListener;
import com.boosed.mm.server.service.CarService;
import com.boosed.mm.shared.db.Car;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.tools.mapreduce.AppEngineMapper;

/**
 * 
 * This Mapper deletes all Entities of a given kind. It simulates the DROP TABLE
 * functionality asked for by developers.
 * 
 * @author Ikai Lan
 * 
 */
public class DeleteCars extends AppEngineMapper<Key, Entity, NullWritable, NullWritable> {
	
//	private AdService adService;
//	private AccountService accountService;
	private CarService carService;
//	private MakeService makeService;
//	private ModelService modelService;
	//private TypeService typeService;

	public void taskSetup(Context context) throws IOException, InterruptedException {
//		accountService = MoblotListener.INJECTOR.getInstance(AccountService.class);
//		adService = MoblotListener.INJECTOR.getInstance(AdService.class);
		carService = MobimotosListener.INJECTOR.getInstance(CarService.class);
		// makeService = MoblotListener.INJECTOR.getInstance(MakeService.class);
		// modelService =
		// MoblotListener.INJECTOR.getInstance(ModelService.class);
	//	typeService = MoblotListener.INJECTOR.getInstance(TypeService.class);
	}

	@Override
	public void map(Key key, Entity value, Context context) {
		// log.info("Adding key to deletion pool: " + key);
		//DatastoreMutationPool mutationPool = this.getAppEngineContext(context).getMutationPool();
		//mutationPool.delete(key);
		
		long id = key.getId();
		Car car = carService.get(id);
		
//		// remove ad
//		adService.drop(car.ad);
//		
//		// decrement make
//		//makeService.decrement(car.make, car.types);
//		
//		// decrement model
//		modelService.decrement(car.model);
//		
//		// decrement type
//		//for (com.googlecode.objectify.Key<Type> type : car.types)
//		makeService.decrement(car.make, car.types);
//		
//		// remove from seller
//		//Account account = accountService.get(car.owner);
//		//account.selling.remove(new com.googlecode.objectify.Key<Car>(Car.class, id));
//		//accountService.save(account);
		
		// delete the car
		carService.drop(/*id*/car);
	}
}