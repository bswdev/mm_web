package com.boosed.mm.server.mapper;

import java.io.IOException;
import java.util.logging.Logger;

import org.apache.hadoop.io.NullWritable;

import com.beoui.geocell.GeocellManager;
import com.beoui.geocell.model.Point;
import com.boosed.mm.server.listener.MobimotosListener;
import com.boosed.mm.server.service.LocationService;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.State;
import com.google.appengine.tools.mapreduce.AppEngineMapper;
import com.google.appengine.tools.mapreduce.BlobstoreRecordKey;
import com.googlecode.objectify.Key;

/**
 * 
 * This Mapper imports from a CSV file in the Blobstore. The CSV assumes it's in
 * the MaxMind format for cities, states, zipcodes and lat/long.
 * 
 * 
 * @author Ikai Lan
 * 
 */
public class ImportLocation extends
		AppEngineMapper<BlobstoreRecordKey, byte[], NullWritable, NullWritable> {
	private static final Logger log = Logger.getLogger(ImportLocation.class.getName());

	private LocationService ls;
	
	// private static StateService ss =
	// MoblotListener.INJECTOR.getInstance(StateService.class);

	private static String[] states = { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL",
			"GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN",
			"MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR",
			"PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY", "AA",
			"AE", "AP", "PR", "VI", "AS", "GU", "PW", "FM", "MP", "MH" };

	public void taskSetup(Context context)
			throws IOException, InterruptedException {
		ls = MobimotosListener.INJECTOR.getInstance(LocationService.class);
	}

	@Override
	public void map(BlobstoreRecordKey key, byte[] segment, Context context) {

		String line = new String(segment);

		log.info("At offset: " + key.getOffset());
		log.info("Got value: " + line);

		// Line format looks like this:
		// 10644,"US","VA","Tazewell","24651",37.0595,-81.5220,559,276
		// We're also assuming no errant commas in this simple example

		// 00501,Holtsville ,33,1,40.922325,-72.637080

		String[] values = line.split(",");
		// long postal = Long.parseLong(values[0]);
		int state = Integer.parseInt(values[2]);
		Double lat = Double.parseDouble(values[4]);
		Double lon = Double.parseDouble(values[5]);
		Location location = new Location(values[0], lat, lon, GeocellManager.generateGeoCell(new Point(lat, lon)));
		location.city = values[1].trim();
		location.state = new Key<State>(State.class, states[state - 1]);

		ls.save(location);

		// if(!zipcode.isEmpty()) {
		// Entity zip = new Entity("Zip", zipcode);
		// //zip.setp
		// //zip.setProperty("state", state);
		// zip.setProperty("city", cityName);
		// zip.setProperty("latitude", latitude);
		// zip.setProperty("longitute", longitude);
		//			
		// // Entity city = new Entity("City", cityName);
		// // city.setProperty("state", state);
		// // city.setUnindexedProperty("zip", zipcode);
		//			
		// DatastoreMutationPool mutationPool =
		// this.getAppEngineContext(context)
		// .getMutationPool();
		// mutationPool.put(zip);
		// //mutationPool.put(city);
		// }

	}
}