package com.boosed.mm.server.mapper;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;

import com.boosed.mm.server.listener.MobimotosListener;
import com.boosed.mm.server.service.MakeService;
import com.boosed.mm.shared.db.Make;
import com.google.appengine.tools.mapreduce.AppEngineMapper;
import com.google.appengine.tools.mapreduce.BlobstoreRecordKey;

public class ImportMake extends AppEngineMapper<BlobstoreRecordKey, byte[], NullWritable, NullWritable> {
	// private static final Logger log =
	// Logger.getLogger(ImportMake.class.getName());

	private MakeService service;

	public void taskSetup(Context context) throws IOException, InterruptedException {
		service = MobimotosListener.INJECTOR.getInstance(MakeService.class);
	}

	@Override
	public void map(BlobstoreRecordKey key, byte[] segment, Context context) {

		String line = new String(segment);

		// log.info("At offset: " + key.getOffset());
		// log.info("Got value: " + line);

		// 1,Alabama,AL

		// String[] values = line.split(",");
		// String abbr = values[2];
		// String state = values[1];

		// state = state.trim();
		// abbr = abbr.trim();

		service.save(new Make(line));

		// if (!state.isEmpty()) {
		// Entity e = new Entity("State", abbr);
		// e.setUnindexedProperty("state", state);
		//
		// DatastoreMutationPool mutationPool =
		// this.getAppEngineContext(context)
		// .getMutationPool();
		// mutationPool.put(e);
		// }
	}
}