package com.boosed.mm.server.mapper;

import java.io.IOException;
import java.util.Collections;
import java.util.logging.Logger;

import org.apache.hadoop.io.NullWritable;

import com.boosed.mm.server.listener.MobimotosListener;
import com.boosed.mm.server.service.MakeService;
import com.boosed.mm.server.service.ModelService;
import com.boosed.mm.server.service.TypeService;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Type;
import com.google.appengine.tools.mapreduce.AppEngineMapper;
import com.google.appengine.tools.mapreduce.BlobstoreRecordKey;
import com.googlecode.objectify.Key;

public class ImportModel extends AppEngineMapper<BlobstoreRecordKey, byte[], NullWritable, NullWritable> {

	private static final Logger log = Logger.getLogger(ImportMake.class.getName());

	private ModelService mos;
	private MakeService mas;
	private TypeService ts;

	public void taskSetup(Context context) throws IOException, InterruptedException {
		mas = MobimotosListener.INJECTOR.getInstance(MakeService.class);
		mos = MobimotosListener.INJECTOR.getInstance(ModelService.class);
		ts = MobimotosListener.INJECTOR.getInstance(TypeService.class);
	}

	@Override
	public void map(BlobstoreRecordKey key, byte[] segment, Context context) {

		String line = new String(segment);

		// make, model, years, types
		String[] parts = line.split(":");

		if (parts.length != 4) {
			// must only be four parts... typo
			log.severe("unable to parse: " + line);
			return;
		}
		
		// create make
		String name = parts[0];
		Make make = mas.get(name);
		if (make == null) {
			log.info("make does not exist: " + line);
			make = mas.save(new Make(name));
		}
		
		// create model
		name = parts[0] + " " + parts[1];
		Model model = mos.get(name);
		if (model == null)
			model = new Model(name);
		else {
			//log.severe("model alread exists: " + line);
			return;
		}

		
		// handle years
		String[] periods = parts[2].split(",");
		for (String period : periods) {
			if (period.contains("-")) {
				String[] span = period.split("-");
				int low = Integer.parseInt(span[0]);
				int high = Integer.parseInt(span[1]);
				
				// check ordering
				if (low >= high) {
					log.severe("incorrect range: " + line);
					return;
				}
				
				// check bounds
				if (!checkYear(low) || !checkYear(high)) {
					log.severe("incorrect year: " + line);
					return;
				}
				
				low -= 1;
				for (int i = high; i > low; i--)
					model.years.add(i);
			} else {
				int year = Integer.parseInt(period);
				
				// check bounds
				if (!checkYear(year)) {
					log.severe("incorrect year: " + line);
					return;
				}
				
				model.years.add(Integer.parseInt(period));
			}
		}
		Collections.sort(model.years);
		
		// handle types
		for (String period : parts[3].split(",")) {
			period = period.trim();
			
			if (period.isEmpty()) {
				log.severe("the type key is empty: " + line);
				return;
			}
			
			Key<Type> type = new Key<Type>(Type.class, period);
			if (ts.get(type) == null) {
				log.severe("type does not exist: " + line);
				return;
			}
			
			// add the type
			model.types.add(type);
		}
		
		// make sure there is a type
		if (model.types.isEmpty()) {
			log.severe("model does not have type: " + line);
			return;
		}
		
		// set the make
		model.make = make.getKey();
		
		// save the model
		mos.save(model);
		
		StringBuilder sb = new StringBuilder(model.desc);
		sb.append(" ").append(model.years);
		sb.append(" ").append(model.types);
		log.info(sb.toString());
	}
	
	private boolean checkYear(int year) {
		// check bounds
		return year <= 2100 && year >= 1950;
	}
}