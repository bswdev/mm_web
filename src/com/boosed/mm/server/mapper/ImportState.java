package com.boosed.mm.server.mapper;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;

import com.boosed.mm.server.listener.MobimotosListener;
import com.boosed.mm.server.service.StateService;
import com.boosed.mm.shared.db.State;
import com.google.appengine.tools.mapreduce.AppEngineMapper;
import com.google.appengine.tools.mapreduce.BlobstoreRecordKey;

/**
 * 
 * This Mapper imports from a CSV file in the Blobstore. The CSV assumes it's in
 * the MaxMind format for cities, states, zipcodes and lat/long.
 * 
 * 
 * @author Ikai Lan
 * 
 */
public class ImportState extends AppEngineMapper<BlobstoreRecordKey, byte[], NullWritable, NullWritable> {

	// private static final Logger log =
	// Logger.getLogger(ImportState.class.getName());

	private StateService ss;

	public void taskSetup(Context context) throws IOException, InterruptedException {
		ss = MobimotosListener.INJECTOR.getInstance(StateService.class);
	}

	@Override
	public void map(BlobstoreRecordKey key, byte[] segment, Context context) {

		String line = new String(segment);

		// log.info("At offset: " + key.getOffset());
		// log.info("Got value: " + line);

		// 1,Alabama,AL

		String[] values = line.split(",");
		String abbr = values[2];
		String state = values[1];

		state = state.trim();
		abbr = abbr.trim();

		ss.save(new State(abbr, state));

		// if (!state.isEmpty()) {
		// Entity e = new Entity("State", abbr);
		// e.setUnindexedProperty("state", state);
		//
		// DatastoreMutationPool mutationPool =
		// this.getAppEngineContext(context)
		// .getMutationPool();
		// mutationPool.put(e);
		// }
	}
}