package com.boosed.mm.server.mapper;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;

import com.boosed.mm.server.listener.MobimotosListener;
import com.boosed.mm.server.service.ColorService;
import com.boosed.mm.server.service.DrivetrainService;
import com.boosed.mm.server.service.EngineService;
import com.boosed.mm.server.service.TransmissionService;
import com.boosed.mm.server.service.TypeService;
import com.boosed.mm.shared.db.Color;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.Type;
import com.google.appengine.tools.mapreduce.AppEngineMapper;
import com.google.appengine.tools.mapreduce.BlobstoreRecordKey;

public class ImportTypes extends AppEngineMapper<BlobstoreRecordKey, byte[], NullWritable, NullWritable> {

	private ColorService cs;
	private DrivetrainService ds;
	private EngineService es;
	private TransmissionService trs;
	private TypeService tys;

	public void taskSetup(Context context) throws IOException, InterruptedException {
		cs = MobimotosListener.INJECTOR.getInstance(ColorService.class);
		ds = MobimotosListener.INJECTOR.getInstance(DrivetrainService.class);
		es = MobimotosListener.INJECTOR.getInstance(EngineService.class);
		trs = MobimotosListener.INJECTOR.getInstance(TransmissionService.class);
		tys = MobimotosListener.INJECTOR.getInstance(TypeService.class);
	}

	@Override
	public void map(BlobstoreRecordKey key, byte[] segment, Context context) {
		String[] s = new String(segment).split(":");
		
		if (s[0].equals("type"))
			tys.save(new Type(s[1].trim()));
		else if (s[0].equals("drive"))
			ds.save(new Drivetrain(s[1].trim()));
		else if (s[0].equals("color"))
			cs.save(new Color(s[1].trim()));
		else if (s[0].equals("trans"))
			trs.save(new Transmission(s[1].trim()));
		else if (s[0].equals("engine"))
			es.save(new Engine(s[1].trim()));
	}
}