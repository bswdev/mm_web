package com.boosed.mm.server.mapper;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;

import com.boosed.mm.server.listener.MobimotosListener;
import com.boosed.mm.server.service.CarService;
import com.boosed.mm.server.service.MarkerService;
import com.boosed.mm.server.service.MessageService;
import com.boosed.mm.server.service.ModelService;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Model;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.tools.mapreduce.AppEngineMapper;

/**
 * Rename a model.
 * 
 * @author dsumera
 */
public class RenameModel extends AppEngineMapper<Key, Entity, NullWritable, NullWritable> {

	private CarService carService;

	private MarkerService markerService;
	
	private MessageService messageService;
	
	private ModelService modelService;

	private Model newModel = null;
	
	private com.googlecode.objectify.Key<Model> oldModel = null;

	public void taskSetup(Context context) throws IOException, InterruptedException {
		carService = MobimotosListener.INJECTOR.getInstance(CarService.class);
		markerService = MobimotosListener.INJECTOR.getInstance(MarkerService.class);
		messageService = MobimotosListener.INJECTOR.getInstance(MessageService.class);
		modelService = MobimotosListener.INJECTOR.getInstance(ModelService.class);

		// NOTE: BE SURE TO USE ONLY A SINGLE SHARD!
		if (oldModel == null && newModel == null) {
			Configuration cfg = context.getConfiguration();

			Model model = modelService.get(cfg.get("current"));

			// delete the old model
			oldModel = model.getKey();
			modelService.drop(model);

			// change the model "id"
			model.desc = model.make.getName() + " " + cfg.get("new");

			// save the new model
			newModel = modelService.save(model);
		}
	}

	@Override
	public void map(Key key, Entity value, Context context) {
		// log.info("Adding key to deletion pool: " + key);
		// DatastoreMutationPool mutationPool =
		// this.getAppEngineContext(context).getMutationPool();
		// mutationPool.delete(key);

		// retrieve car
		Car car = carService.get(key.getId());

		// check if car is the old model
		if (car.model.equals(oldModel)) {
			// set new model key
			car.model = newModel.getKey();
			
			// save car
			car = carService.save(car);
			
			// update model name on all markers
			markerService.updateModel(car.id, newModel.toString());
			
			// change all the message references
			messageService.updateIdentifiers(car);
			
			// StringBuilder sb = new StringBuilder();
			// sb.append(car.year).append(" ").append(car.model.getName());
			// String referenceName = sb.toString();
			//			
			// List<Message> messages = messageService.getAll(new
			// EQCriterion("reference", car.getKey()));
			// for (Message message : messages)
			// message.referenceName = referenceName;
			//			
			// // save messages
			// messageService.save(messages.toArray(new Message[0]));
		}
	}
}