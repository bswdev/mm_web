package com.boosed.mm.server.mapper;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;

import com.boosed.mm.server.listener.MobimotosListener;
import com.boosed.mm.server.service.MakeService;
import com.boosed.mm.shared.db.Make;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.tools.mapreduce.AppEngineMapper;

/**
 * Zero out the totals wrt a given make.
 * 
 * @author dsumera
 */
public class ZeroCount extends AppEngineMapper<Key, Entity, NullWritable, NullWritable> {

	private MakeService makeService;

	public void taskSetup(Context context) throws IOException, InterruptedException {
		makeService = MobimotosListener.INJECTOR.getInstance(MakeService.class);
	}

	@Override
	public void map(Key key, Entity value, Context context) {
		// log.info("Adding key to deletion pool: " + key);
		// DatastoreMutationPool mutationPool =
		// this.getAppEngineContext(context).getMutationPool();
		// mutationPool.delete(key);

		long id = key.getId();
		if (id != 0) {
			Make make = makeService.get(id);
			make.totals.clear();

			// save make
			makeService.save(make);
		}
	}
}