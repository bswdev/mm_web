package com.boosed.mm.server.module;

import com.boosed.mm.server.dao.AccountDao;
import com.boosed.mm.server.dao.AdDao;
import com.boosed.mm.server.dao.CLContinentDao;
import com.boosed.mm.server.dao.CLLocaleDao;
import com.boosed.mm.server.dao.CLStateDao;
import com.boosed.mm.server.dao.CarDao;
import com.boosed.mm.server.dao.ColorDao;
import com.boosed.mm.server.dao.DrivetrainDao;
import com.boosed.mm.server.dao.EngineDao;
import com.boosed.mm.server.dao.LocationDao;
import com.boosed.mm.server.dao.MakeDao;
import com.boosed.mm.server.dao.MarkerDao;
import com.boosed.mm.server.dao.MessageDao;
import com.boosed.mm.server.dao.ModelDao;
import com.boosed.mm.server.dao.StateDao;
import com.boosed.mm.server.dao.TransactionDao;
import com.boosed.mm.server.dao.TransmissionDao;
import com.boosed.mm.server.dao.TypeDao;
import com.boosed.mm.server.dao.impl.AccountDaoImpl;
import com.boosed.mm.server.dao.impl.AdDaoImpl;
import com.boosed.mm.server.dao.impl.CLContinentDaoImpl;
import com.boosed.mm.server.dao.impl.CLLocaleDaoImpl;
import com.boosed.mm.server.dao.impl.CLStateDaoImpl;
import com.boosed.mm.server.dao.impl.CarDaoImpl;
import com.boosed.mm.server.dao.impl.ColorDaoImpl;
import com.boosed.mm.server.dao.impl.DrivetrainDaoImpl;
import com.boosed.mm.server.dao.impl.EngineDaoImpl;
import com.boosed.mm.server.dao.impl.LocationDaoImpl;
import com.boosed.mm.server.dao.impl.MakeDaoImpl;
import com.boosed.mm.server.dao.impl.MarkerDaoImpl;
import com.boosed.mm.server.dao.impl.MessageDaoImpl;
import com.boosed.mm.server.dao.impl.ModelDaoImpl;
import com.boosed.mm.server.dao.impl.StateDaoImpl;
import com.boosed.mm.server.dao.impl.TransactionDaoImpl;
import com.boosed.mm.server.dao.impl.TransmissionDaoImpl;
import com.boosed.mm.server.dao.impl.TypeDaoImpl;
import com.boosed.mm.server.service.AccountService;
import com.boosed.mm.server.service.AdService;
import com.boosed.mm.server.service.CLService;
import com.boosed.mm.server.service.CarService;
import com.boosed.mm.server.service.ColorService;
import com.boosed.mm.server.service.DrivetrainService;
import com.boosed.mm.server.service.EngineService;
import com.boosed.mm.server.service.LocationService;
import com.boosed.mm.server.service.MakeService;
import com.boosed.mm.server.service.MarkerService;
import com.boosed.mm.server.service.MessageService;
import com.boosed.mm.server.service.ModelService;
import com.boosed.mm.server.service.PostService;
import com.boosed.mm.server.service.StateService;
import com.boosed.mm.server.service.TaskService;
import com.boosed.mm.server.service.TransactionService;
import com.boosed.mm.server.service.TransmissionService;
import com.boosed.mm.server.service.TypeService;
import com.boosed.mm.server.service.impl.AccountServiceImpl;
import com.boosed.mm.server.service.impl.AdServiceImpl;
import com.boosed.mm.server.service.impl.CLServiceImpl;
import com.boosed.mm.server.service.impl.CarServiceImpl;
import com.boosed.mm.server.service.impl.ColorServiceImpl;
import com.boosed.mm.server.service.impl.DrivetrainServiceImpl;
import com.boosed.mm.server.service.impl.EngineServiceImpl;
import com.boosed.mm.server.service.impl.LocationServiceImpl;
import com.boosed.mm.server.service.impl.MakeServiceImpl;
import com.boosed.mm.server.service.impl.MarkerServiceImpl;
import com.boosed.mm.server.service.impl.MessageServiceImpl;
import com.boosed.mm.server.service.impl.ModelServiceImpl;
import com.boosed.mm.server.service.impl.PostServiceImpl;
import com.boosed.mm.server.service.impl.StateServiceImpl;
import com.boosed.mm.server.service.impl.TaskServiceImpl;
import com.boosed.mm.server.service.impl.TransactionServiceImpl;
import com.boosed.mm.server.service.impl.TransmissionServiceImpl;
import com.boosed.mm.server.service.impl.TypeServiceImpl;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.mail.MailService;
import com.google.appengine.api.mail.MailServiceFactory;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Names;

public class ServerModule extends AbstractModule {

	@Override
	protected void configure() {
		// daos
		bind(AccountDao.class).to(AccountDaoImpl.class).asEagerSingleton();
		bind(AdDao.class).to(AdDaoImpl.class).asEagerSingleton();
		bind(CarDao.class).to(CarDaoImpl.class).asEagerSingleton();
		bind(ColorDao.class).to(ColorDaoImpl.class).asEagerSingleton();
		bind(CLContinentDao.class).to(CLContinentDaoImpl.class).asEagerSingleton();
		bind(CLLocaleDao.class).to(CLLocaleDaoImpl.class).asEagerSingleton();
		bind(CLStateDao.class).to(CLStateDaoImpl.class).asEagerSingleton();
		bind(DrivetrainDao.class).to(DrivetrainDaoImpl.class).asEagerSingleton();
		bind(EngineDao.class).to(EngineDaoImpl.class).asEagerSingleton();
		bind(LocationDao.class).to(LocationDaoImpl.class).asEagerSingleton();
		bind(MakeDao.class).to(MakeDaoImpl.class).asEagerSingleton();
		bind(MarkerDao.class).to(MarkerDaoImpl.class).asEagerSingleton();
		bind(MessageDao.class).to(MessageDaoImpl.class).asEagerSingleton();
		bind(ModelDao.class).to(ModelDaoImpl.class).asEagerSingleton();
		//bind(PriceAlertDao.class).to(PriceAlertDaoImpl.class).asEagerSingleton();
		bind(StateDao.class).to(StateDaoImpl.class).asEagerSingleton();
		bind(TransactionDao.class).to(TransactionDaoImpl.class).asEagerSingleton();
		bind(TransmissionDao.class).to(TransmissionDaoImpl.class).asEagerSingleton();
		bind(TypeDao.class).to(TypeDaoImpl.class).asEagerSingleton();

		// services
		bind(AccountService.class).to(AccountServiceImpl.class).asEagerSingleton();
		bind(AdService.class).to(AdServiceImpl.class).asEagerSingleton();
		bind(CarService.class).to(CarServiceImpl.class).asEagerSingleton();
		bind(ColorService.class).to(ColorServiceImpl.class).asEagerSingleton();
		bind(CLService.class).to(CLServiceImpl.class).asEagerSingleton();
		bind(DrivetrainService.class).to(DrivetrainServiceImpl.class).asEagerSingleton();
		bind(EngineService.class).to(EngineServiceImpl.class).asEagerSingleton();
		bind(LocationService.class).to(LocationServiceImpl.class).asEagerSingleton();
		bind(MakeService.class).to(MakeServiceImpl.class).asEagerSingleton();
		bind(MarkerService.class).to(MarkerServiceImpl.class).asEagerSingleton();
		bind(MessageService.class).to(MessageServiceImpl.class).asEagerSingleton();
		bind(ModelService.class).to(ModelServiceImpl.class).asEagerSingleton();
		//bind(PriceService.class).to(PriceServiceImpl.class).asEagerSingleton();
		bind(PostService.class).to(PostServiceImpl.class).asEagerSingleton();
		bind(StateService.class).to(StateServiceImpl.class).asEagerSingleton();
		bind(TaskService.class).to(TaskServiceImpl.class).asEagerSingleton();
		bind(TransactionService.class).to(TransactionServiceImpl.class).asEagerSingleton();
		bind(TransmissionService.class).to(TransmissionServiceImpl.class).asEagerSingleton();
		bind(TypeService.class).to(TypeServiceImpl.class).asEagerSingleton();

		// values
		// bind(Long.class).annotatedWith(Names.named("expiry")).toInstance(7200000L);
		// bind(String.class).annotatedWith(Names.named("channel")).toInstance("com.boosed.cc.user.id.");

		// distances
		bind(Integer[].class).annotatedWith(Names.named("distances")).toInstance(DISTANCES);
		
		// limits
		bind(Integer.class).annotatedWith(Names.named("alertMax")).toInstance(ALERT_MAX);
		bind(Integer.class).annotatedWith(Names.named("bookmarkMax")).toInstance(BOOKMARK_MAX);
		
		// edit time (in hours)
		bind(Integer.class).annotatedWith(Names.named("editTime")).toInstance(EDIT_TIME);
		
		// geocell resolution
		bind(Integer.class).annotatedWith(Names.named("geocellMax")).toInstance(GEOCELL_MAX);
		
		// paypal
		bind(String.class).annotatedWith(Names.named("ipn")).toInstance(PP_TEST);

		// messages
		bind(String.class).annotatedWith(Names.named("activation")).toInstance(ACTIVE);
		bind(String.class).annotatedWith(Names.named("priceAlert")).toInstance(PRICE_ALERT);
		bind(String.class).annotatedWith(Names.named("rejectImage")).toInstance(IMG_REJ);
		bind(String.class).annotatedWith(Names.named("rejectAd")).toInstance(AD_REJ);
		bind(String.class).annotatedWith(Names.named("truncateAd")).toInstance(AD_TRUNC);
		
		// email
		bind(String.class).annotatedWith(Names.named("domain")).toInstance(DOMAIN);
		bind(String.class).annotatedWith(Names.named("from")).toInstance(FROM);
		
		// image quotas
		bind(Integer.class).annotatedWith(Names.named("quotaBasic")).toInstance(QUOTA_BASIC);
		bind(Integer.class).annotatedWith(Names.named("quotaStandard")).toInstance(QUOTA_STD);
	}

	private static final int QUOTA_BASIC = 4;
	private static final int QUOTA_STD = 20;
	
	/** price alert limit */
	private static final Integer ALERT_MAX = 100;
	
	/** bookmark limit */
	private static final Integer BOOKMARK_MAX = 100;
	
	/** edit time before ad becomes locked, in hours */
	private static final Integer EDIT_TIME = 48;
	
	// this gives us the smallest geocell set w/o regions of resolution == 1
	/** the desired geocell set size representing the bounded region used in location query */
	private static final Integer GEOCELL_MAX = /*9*/10;
	
	/** distances user is able to query */
	private static final Integer[] DISTANCES = new Integer[] { 10, 25, 50, 75, 100,
		150, 200, 250, 300, 500 };
	
	/** email domain for the application */
	private static final String DOMAIN = "mobimotosbsw.appspotmail.com";
	
	/** the generic from email address */
	private static final String FROM = "noreply@mobimotos.com";
	
	/** activated ad message */
	private static final String ACTIVE = "Your ad has been activated. If this is a new ad, you will have up to 48 hours to correct the vehicle's vin and year, "
			+ "the time after which they will no longer be editable. If the vin is not set within "
			+ "the allotted 48 hours, it will remain empty until initially set.\n\nWith the exception of make and model (in addition to the "
			+ "above mentioned fields), your entire ad will always be editable as long as your account is in good standing.\n\nPlease keep in "
			+ "mind that ad duration runs for 60 days, the time after which it will have to be renewed (free) to be searchable again.";

	/** activated ad message */
	private static final String ACTIVE_OLD = "Your ad has been activated. If this is a new ad, you will have up to 48 hours to correct the vehicle's vin and year, "
			+ "the time after which they will no longer be editable. If the vin is not set within "
			+ "the allotted 48 hours, it will remain empty until initially set.\n\nWith the exception of make and model (in addition to the "
			+ "above mentioned fields), your entire ad will always be editable as long as your account is in good standing. Newly submitted "
			+ "photos and details will not be made publicly visible until they are verified to be in accordance with the terms of service.\n\nPlease keep in "
			+ "mind that ad duration runs for 60 days, the time after which it will have to be renewed (free of charge) to be searchable again.";
	
	/** details rejection */
	private static final String AD_REJ = "Your ad description was rejected due to inappropriate language and/or information as specified "
			+ "by the conditions of the terms of service. Please ensure any future submissions comply with these terms.";
	
	/** ad truncation */
	private static final String AD_TRUNC = "Your ad activation has resulted in the deletion of some images due to quota constraints. As a reminder, "
			+ "basic ads are permitted to post " + QUOTA_BASIC + " pictures total, while standard ads increase this quota to " + QUOTA_STD + ". If your ad needs require more images "
			+ "or you desire increased privacy through our direct ad messaging service, please consider upgrading to a standard ad.";
	
	/** image rejection */
	private static final String IMG_REJ = "One or more of your images were deleted due to violation of the terms of service. "
			+ "Please ensure any future submissions are in accordance with the photo guidelines specified therein.";

	/** alert met */
	private static final String PRICE_ALERT = "Congratulations, your target price has been met! Please verify the data below with the seller "
			+ "to ensure the pricing (as of this message date) is still valid. Since the price point for this vehicle has been met, its "
			+ "associated alert has been deactivated.\n\n";
	
	/** paypal processor */
	private static final String PP = "https://www.paypal.com/cgi-bin/webscr";
	
	/** paypal testing */
	private static final String PP_TEST = "https://www.sandbox.paypal.com/cgi-bin/webscr";

	/** service for storing blobs */
	private static BlobstoreService blobstoreService;

	@Provides
	BlobstoreService provideBlobstoreService() {
		if (blobstoreService == null)
			blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

		return blobstoreService;
	}

	private static FileService fileService;

	/** service for uploading files */
	@Provides
	FileService provideFileService() {
		if (fileService == null)
			fileService = FileServiceFactory.getFileService();

		return fileService;
	}

	/** service for modifying and serving images */
	private static ImagesService imagesService;

	@Provides
	ImagesService provideImagesService() {
		if (imagesService == null)
			imagesService = ImagesServiceFactory.getImagesService();

		return imagesService;
	}

	/** service for sending emails */
	private static MailService mailService;
	
	@Provides
	MailService provideMailService() {
		if (mailService == null)
			mailService = MailServiceFactory.getMailService();
		
		return mailService;
	}
	
	/** service for accessing/verifying users */
	private static UserService userService;

	@Provides
	UserService provideUserService() {
		if (userService == null)
			userService = UserServiceFactory.getUserService();

		return userService;
	}
}