package com.boosed.mm.server.module;

import com.boosed.mm.server.rpc.MobimotosRpcServiceImpl;
import com.boosed.mm.server.servlet.FileServlet;
import com.boosed.mm.server.servlet.ImageServlet;
import com.boosed.mm.server.servlet.IpnServlet;
import com.boosed.mm.server.servlet.MobimotosServlet;
import com.boosed.mm.server.servlet.QueueServlet;

public class ServletModule extends com.google.inject.servlet.ServletModule {

	@Override
	protected void configureServlets() {
		// rpc
		serve("/mm/rpc").with(MobimotosRpcServiceImpl.class);
		//serve("/mobi/rpc").with(MobRpcServiceImpl.class);
		
		// upload a file
		serve("/file").with(FileServlet.class);

		// upload an image
		serve("/image").with(ImageServlet.class);

		// task queue
		serve("/queue").with(QueueServlet.class);
		
		// IPN
		serve("/abipn").with(IpnServlet.class);

		// we do this only for injection purposes (to static fields)
		serve("/injected").with(MobimotosServlet.class);
	}
}