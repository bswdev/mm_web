package com.boosed.mm.server.rpc;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beoui.geocell.GeocellManager;
import com.beoui.geocell.GeocellUtils;
import com.beoui.geocell.model.Point;
import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.criterion.EQCriterion;
import com.boosed.gae.server.criterion.InCriterion;
import com.boosed.gae.server.criterion.SortCriterion;
import com.boosed.mm.server.service.AccountService;
import com.boosed.mm.server.service.AdService;
import com.boosed.mm.server.service.CLService;
import com.boosed.mm.server.service.CarService;
import com.boosed.mm.server.service.ColorService;
import com.boosed.mm.server.service.DrivetrainService;
import com.boosed.mm.server.service.EngineService;
import com.boosed.mm.server.service.LocationService;
import com.boosed.mm.server.service.MakeService;
import com.boosed.mm.server.service.MarkerService;
import com.boosed.mm.server.service.MessageService;
import com.boosed.mm.server.service.ModelService;
import com.boosed.mm.server.service.StateService;
import com.boosed.mm.server.service.TaskService;
import com.boosed.mm.server.service.TransmissionService;
import com.boosed.mm.server.service.TypeService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.CLContinent;
import com.boosed.mm.shared.db.CLLocale;
import com.boosed.mm.shared.db.CLState;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Color;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.db.MobResult;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.State;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.AdType;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.FieldAccount;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.boosed.mm.shared.db.enums.FieldCar;
import com.boosed.mm.shared.db.enums.FieldModel;
import com.boosed.mm.shared.db.enums.MarkerType;
import com.boosed.mm.shared.db.enums.MessageState;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.db.enums.MileageType;
import com.boosed.mm.shared.db.enums.PriceType;
import com.boosed.mm.shared.db.enums.SortType;
import com.boosed.mm.shared.exception.InsufficientCreditException;
import com.boosed.mm.shared.exception.InvalidLocationException;
import com.boosed.mm.shared.exception.InvalidPriceException;
import com.boosed.mm.shared.exception.NotFoundException;
import com.boosed.mm.shared.exception.OwnerException;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.exception.RestrictedException;
import com.boosed.mm.shared.exception.SessionTimeoutException;
import com.boosed.mm.shared.exception.UnauthorizedException;
import com.boosed.mm.shared.service.MobimotosRpcService;
import com.boosed.mm.shared.util.DataUtil;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.googlecode.objectify.Key;

/**
 * The server side implementation of the RPC service.
 */
@Singleton
public class MobimotosRpcServiceImpl extends RemoteServiceServlet implements MobimotosRpcService {

	protected Logger log = Logger.getLogger(getClass().getName());

	private final Integer[] distances;

	@Inject
	public MobimotosRpcServiceImpl(@Named("distances") Integer[] distances) {
		this.distances = distances;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
			IOException {
		// get redirects to the main page (i.e., index.html)
		req.getRequestDispatcher("/").forward(req, resp);
	}

	// @Override
	// public void populate() {
	// taskService.populateCraigslist();
	// }

	// CREATE

	@Override
	public void createLocation(String postal, String state, String city, double latitude,
			double longitude) throws RemoteServiceFailureException {
		ensureAdmin();

		// delete any existing location
		locationService.drop(postal);

		// generate the new location with geocell data
		Location location = new Location(postal, latitude, longitude);
		location.city = city;
		location.state = new Key<State>(State.class, state);
		location.geocells = GeocellManager.generateGeoCell(new Point(latitude, longitude));
		locationService.save(location);
	}

	@Override
	public List<Key<Drivetrain>> createDrive(String drive) throws RemoteServiceFailureException {
		ensureAdmin();
		drivetrainService.save(new Drivetrain(drive));
		return drivetrainService.getAllKeys();
	}

	@Override
	public List<Key<Engine>> createEngine(String engine) throws RemoteServiceFailureException {
		ensureAdmin();
		engineService.save(new Engine(engine));
		return engineService.getAllKeys();
	}

	@Override
	public List<Key<Color>> createColor(String color) throws RemoteServiceFailureException {
		ensureAdmin();
		colorService.save(new Color(color));
		return colorService.getAllKeys();
	}

	@Override
	public List<Key<Make>> createMake(String make) throws RemoteServiceFailureException {
		ensureAdmin();
		makeService.save(new Make(make));
		return makeService.getAllKeys();
	}

	@Override
	public List<Key<Transmission>> createTransmission(String transmission)
			throws RemoteServiceFailureException {
		ensureAdmin();
		transmissionService.save(new Transmission(transmission));
		return transmissionService.getAllKeys();
	}

	@Override
	public List<Key<Type>> createType(String type) throws RemoteServiceFailureException {
		ensureAdmin();
		typeService.save(new Type(type));
		return typeService.getAllKeys();
	}

	// @Override
	// public List<Model> createModel(Model model)
	// throws RemoteServiceFailureException {
	// ensureAdmin();
	// modelService.save(model);
	// return modelService.getAll();
	// }

	// @Override
	// public void payAd(long carId, boolean enabled) throws
	// RemoteServiceFailureException {
	// // essentially index the time so that the vehicle is now searchable
	// Car car = carService.get(carId);
	//
	// // if car not found, just return
	// if (car == null)
	// return;
	//
	// // check if this is redundant (don't let them pay twice)
	// if (car.time != null && enabled)
	// return;
	// else if (car.time == null && !enabled)
	// return;
	// else
	// car.time = enabled ? System.currentTimeMillis() : null;
	//
	// // update the counts
	// Model m = modelService.get(car.model);
	//
	// if (enabled) {
	// // makeService.increment(m.make, m.types);
	// modelService.increment(car.model);
	// for (Key<Type> type : m.types)
	// typeService.increment(type);
	// } else {
	// // makeService.decrement(m.make, m.types);
	// modelService.decrement(car.model);
	// for (Key<Type> type : m.types)
	// typeService.decrement(type);
	// }
	//
	// carService.save(car);
	// }

	@Override
	public Model renameModel(String modelId, String make, String model)
			throws RemoteServiceFailureException {

		// ensure user is admin
		ensureAdmin();

		// rename model (queued to task)
		taskService.renameModel(modelId, make, model);

		// faked model to immediately send back to the client
		Model rv = modelService.get(modelId);
		rv.desc = make + " " + model;
		return rv;

		// // cars to change to the new model
		// List<Car> cars = carService.getAll(new EQCriterion("model",
		// model.getKey()));
		//
		// // delete the old model
		// modelService.drop(model);
		//
		// // change the model "id"
		// model.desc = model.make.getName() + " " + name;
		//
		// // save the new model
		// model = modelService.save(model);
		// Key<Model> key = model.getKey();
		//
		// // replace the cars
		// for (Car car : cars)
		// car.model = key;
		//
		// carService.save(cars.toArray(new Car[0]));
		//
		// return model;
	}

	// END CREATE

	// LOCATIONS

	// @Override
	// public List<Location> loadLocations(String zip, int distance) throws
	// RemoteServiceFailureException {
	// Location loc = locationService.get(zip);
	// return locationService.getByDistance(loc.latitude, loc.longitude,
	// distance);
	// }

	// END LOCATIONS

	@Override
	public String loadAction() throws RemoteServiceFailureException {
		return blobService.createUploadUrl("/iprocess");
	}

	@Override
	public Account loadAccount() throws RemoteServiceFailureException {
		return accountService.get(ensureAccountId());
	}

	@Override
	public Ad loadAd(long adId) throws RemoteServiceFailureException {
		String id = ensureAccountId();
		Ad ad = adService.get(adId);

		if (!id.equals(ad.owner.getName()))
			// not the owner of specified ad
			throw new UnauthorizedException();

		return ad;
	}

	@Override
	public Tuple<Ad, Account> loadAd(long adId, boolean sanitize)
			throws RemoteServiceFailureException {
		Ad ad = adService.get(adId);
		if (ad == null)
			// the ad was probably recently deleted
			throw new NotFoundException();

		Account account = accountService.get(ad.owner);
		boolean isOwner = account.id.equals(getUserId());

		if (sanitize) {
			// requesting sanitized (e.g., user viewing vehicle)

			// track ad views
			if (!isOwner) {
				// not owner, increment ad views
				ad.views++;
				ad = adService.save(ad);
			}

			// sanitize objects (even if this is owner)
			ad.sanitize();
			account.sanitize();
		} else if (!isOwner && !isUserAdmin())
			// requesting unsanitized (e.g., editing photos, admin modification)

			// user requesting unsanitized version and is not owner or admin
			throw new UnauthorizedException();
		// }

		// // sanitize or not an admin and not the owner
		// if (sanitize || (!isUserAdmin() && !isOwner)) {
		//
		// if (!isOwner) {
		// // not owner, increment ad views
		// ad.views++;
		// ad = adService.save(ad);
		// }
		//
		// // sanitize objects
		// ad.sanitize();
		// account.sanitize();
		// } else if (!sanitize && !isOwner)
		// // user requesting unsanitized version and is not owner
		// throw new UnauthorizedException();

		// return result
		return new Tuple<Ad, Account>(ad, account);
	}

	// @Override
	// public/* Tuple<Make, Tuple<Car, Tuple<Key<Model>, List<Model>>>>
	// */MobResult loadCars(int offset, int limit,
	// List<Key<Type>> types, List<Key<Make>> makes, List<Key<Model>> models,
	// /* String zip, int distance, String sort, int lo, int hi */FilterType
	// filter, String arg0, String arg1)
	// throws RemoteServiceFailureException {

	@Override
	public Car loadCar(long carId) throws RemoteServiceFailureException {
		return carService.get(carId);
	}

	@Override
	@Deprecated
	public MobResult loadCars(int offset, int limit, List<Key<Type>> types, List<Key<Make>> makes,
			List<Key<Model>> models, PriceType price, MileageType mileage, ConditionType condition,
			String postal, int distance, SortType sort) throws RemoteServiceFailureException {

		// selectable types
		List<Type> rtypes = typeService.getAll(true);
		List<Make> rmakes = new ArrayList<Make>();
		List<Model> rmodels = new ArrayList<Model>();
		List<Car> rcars;
		List<Criterion> criteria = new ArrayList<Criterion>();

		// if (types.isEmpty())
		// clear out selected models
		// models.clear();
		// else if (makes.isEmpty()) {
		if (!types.isEmpty()) {
			// selectable makes by types
			rmakes = makeService.getByTypes(types, true);

			// filter selected makes by selectable makes
			makes.retainAll(DataUtil.getKeys(rmakes));

			if (makes.isEmpty()) {
				// list of cars by selected types
				// CANNOT use models since it's likely subqueries > 30
				criteria.add(new InCriterion<Key<Type>>("types", types));

				// clear out selected models
				// models.clear();
			} else {
				// selectable models by types and makes (make sure only 30
				// subqueries!)
				rmodels = modelService.getByTypesAndMakes(types, makes, true);

				// filter selected models by selectable models
				List<Key<Model>> keys = DataUtil.getKeys(rmodels);

				// retain only selectable models w/in selected
				models.retainAll(keys);

				if (rmodels.size() > 30) {
					log.warning("subqueries could exceed maximum of 30 < " + rmodels.size());

					// return a subset of the models available; can't use
					// sublist
					rmodels = new ArrayList<Model>(rmodels.subList(0, Math.min(30, rmodels.size())));
					// if (rmodels.size() > limit--)
					// for (int i = rmodels.size(); --i > limit;)
					// rmodels.remove(i);
				}

				// if selected is empty, just use the selectable models
				criteria
						.add(new InCriterion<Key<Model>>("model", models.isEmpty() ? keys : models));
			}
		}

		// price, mileage, year
		// if (price != PriceType.ANY)
		criteria.add(new EQCriterion("prices", price));

		// if (mileage != MileageType.ANY)
		criteria.add(new EQCriterion("mileages", mileage));

		// if (condition != ConditionType.ANY)
		criteria.add(new EQCriterion("conditions", condition));

		// sort
		if (postal != null && !postal.isEmpty()) {
			// if (filter == FilterType.LOCATION/* distance != -1 */) {
			// get the vehicles by location
			// if (arg0 == null || arg1 == null)
			// throw new InvalidLocationException();

			Location location = locationService.get(postal);
			if (location == null)
				throw new InvalidLocationException();

			// when performing location search, the SORT has no effect
			rcars = carService.getByLocation(location, /*
														 * Integer.parseInt(arg1)
														 */distance, criteria);

			// create the sublist from offset & limit
			rcars = new ArrayList<Car>(rcars.subList(offset, offset
					+ Math.min(limit, rcars.size() - offset)));

			// calculate distance for all these vehicles
			for (Car car : rcars)
				car.distance = DataUtil.getDistance(car, location);

			// List<Car> cars = rcars.subList(offset, offset + Math.min(limit,
			// rcars.size() - offset));
			// rcars.clear();
			// for (Car car : cars)
			// rcars.add(car);
			// rcars.addAll(cars);

			// if (rmodels.size() > limit--)
			// for (int i = rmodels.size(); --i > limit;)
			// rmodels.remove(i);
		} else {
			// Integer lo = arg0 == null ? null : Integer.parseInt(arg0);
			// Integer hi = arg1 == null ? null : Integer.parseInt(arg1);
			// switch (filter) {
			// case MILEAGE:
			// case MILEAGE_DESC:
			// case PRICE:
			// case PRICE_DESC:
			// case YEAR:
			// case YEAR_DESC:
			// criteria.add(new SortCriterion(filter.toString()));
			// criteria.add(new RangeCriterion(filter.getProperty(), lo, hi));
			// break;
			// case LOCATION:
			// case NONE:
			// break;
			// }

			// if (!sort.isEmpty()) {
			// // perform a range filter
			// criteria.add(new SortCriterion(sort));
			//
			// if (sort.startsWith("-"))
			// sort = sort.substring(1);
			//
			// // add ranges (-1 means do not factor into range)
			// criteria.add(new RangeCriterion(sort, lo == -1 ? null : lo, hi ==
			// -1 ? null : hi));
			// }

			// sort by given field
			// if (sort != SortType.NONE)
			criteria.add(new SortCriterion(sort.toString()));

			// final sort by time
			criteria.add(new SortCriterion("-time"));

			// add offset & limit criteria
			// criteria.add(new OffsetCriterion(offset));
			// criteria.add(new LimitCriterion(limit));

			// retrieve cars
			rcars = carService.getAll(offset, limit, criteria);
		}

		// return createTuple(rmakes, rcars, models, rmodels);
		return new MobResult(rtypes, rmakes, rcars, rmodels);
	}

	// private static final int[] distances = new int[] { 10, 25, 50, 75, 100,
	// 150, 200, 250, 300, 500 };

	private static final Key<Type> TYPE_ANY = new Key<Type>(Type.class, "any");

	@SuppressWarnings("unchecked")
	@Override
	public MobResult loadCars(int offset, int limit, Key<Type> type, List<Key<Make>> makes,
			List<Key<Model>> models, PriceType price, MileageType mileage, ConditionType condition,
			SortType sort, String postal, int distance) throws RemoteServiceFailureException {

		// selectable types
		// List<Make> rmakes = type == null ? makeService.getAll(true) :
		// makeService.getByType(type,
		// true);

		// selectable models; return empty list if make is not specified (if
		// type is null, be sure to use an empty list)
		List<Model> rmodels = /* make == null *//*
											 * makes.isEmpty() ? new
											 * ArrayList<Model>() :
											 */modelService.getByTypesAndMakes(
				type == null ? new ArrayList<Key<Type>>() : Arrays.asList(type), makes, true);

		List<Car> rcars;
		List<Criterion> criteria = new ArrayList<Criterion>();

		// model and make must be specified
		models.retainAll(DataUtil.getKeys(rmodels));
		if (!models.isEmpty())
			criteria.add(new InCriterion<Key<Model>>("model", models));
		else {
			// included type to reduce the number of stored indexes
			// if (type != null)
			// criteria.add(new InCriterion<Key<Type>>("types",
			// Arrays.asList(type)));

			criteria.add(new EQCriterion("types", type == null ? TYPE_ANY : type));

			// if (!makes.isEmpty() /* != null */)
			// criteria.add(new EQCriterion("make", make));
			criteria.add(new InCriterion<Key<Make>>("make", makes));
		}

		// price, mileage, year (these are list properties, so watch for
		// exploding indexes)
		criteria.add(new EQCriterion("prices", price));
		criteria.add(new EQCriterion("mileages", mileage));

		// omit criteria if condition selected is any
		// if (condition != ConditionType.ANY)
		criteria.add(new EQCriterion("conditions", condition));

		// sort by given field
		// if (sort != SortType.NONE)
		criteria.add(new SortCriterion(sort.toString()));

		// TODO check if this works properly, modify indices
		// sort by priority
		criteria.add(new SortCriterion("-priority"));

		// final sort by time
		criteria.add(new SortCriterion("-time"));

		// postal
		if (postal != null && !postal.isEmpty()) {
			// if (filter == FilterType.LOCATION/* distance != -1 */) {
			// get the vehicles by location
			// if (arg0 == null || arg1 == null)
			// throw new InvalidLocationException();

			Location location = locationService.get(postal);
			if (location == null)
				throw new InvalidLocationException();

			// when performing location search, the SORT has no effect
			// rcars = carService.getByLocation(location,
			// /*Integer.parseInt(arg1)*/distances[distance], criteria);
			int dist = distances[distance];
			// log.warning("the distance is: " + dist);

			rcars = carService.getByGeocells(offset, limit, location, dist, criteria);

			// create the sublist from offset & limit
			// rcars = new ArrayList<Car>(rcars.subList(offset, offset +
			// Math.min(limit, rcars.size() - offset)));

			// calculate distance for all these vehicles
			String cell = location.geocells.get(4);
			// for (Car car : rcars)
			for (Iterator<Car> it = rcars.iterator(); it.hasNext();) {
				Car car = it.next();
				// or use actual user's location here
				BigDecimal bd = BigDecimal.valueOf(GeocellUtils.pointDistance(cell, new Point(
						car.latitude, car.longitude)));// DataUtil.getDistance(car,
				// location);
				car.distance = bd.setScale(1, RoundingMode.HALF_UP).doubleValue();//bd.setScale(0, RoundingMode.HALF_UP).intValue();
				if (car.distance > dist)
					it.remove();
			}

			// List<Car> cars = rcars.subList(offset, offset + Math.min(limit,
			// rcars.size() - offset));
			// rcars.clear();
			// for (Car car : cars)
			// rcars.add(car);
			// rcars.addAll(cars);

			// if (rmodels.size() > limit--)
			// for (int i = rmodels.size(); --i > limit;)
			// rmodels.remove(i);
		} else {
			// retrieve cars
			rcars = carService.getAll(offset, limit, criteria);
		}

		// return createTuple(rmakes, rcars, models, rmodels);
		return new MobResult(null, /* rmakes */null, rcars, rmodels);
	}

	@Override
	public List<Account> loadDealers(int offset, int limit, String postal, int distance, SortType sort) throws RemoteServiceFailureException {

		// prepare critera
		List<Criterion> criteria = new ArrayList<Criterion>();
		criteria.add(new EQCriterion("dealer", true));
		criteria.add(new SortCriterion(sort.toString()));
		
		// get location
		if (postal != null) {
			Location location = locationService.get(postal);
			if (location == null)
				throw new InvalidLocationException();
			
			// distance
			int dist = distances[distance];

			List<Account> dealers = accountService.getByGeocells(offset, limit, location, dist, criteria);
			
			// calculate distance for all these vehicles
			String cell = location.geocells.get(4);
			// for (Car car : rcars)
			for (Iterator<Account> it = dealers.iterator(); it.hasNext();) {
				Account dealer = it.next();
				dealer.sanitize();
				
				// or use actual user's location here
				BigDecimal bd = BigDecimal.valueOf(GeocellUtils.pointDistance(cell, new Point(
						dealer.latitude, dealer.longitude)));// DataUtil.getDistance(car,
				// location);
				dealer.distance = bd.setScale(1, RoundingMode.HALF_UP).doubleValue();
				if (dealer.distance > dist)
					it.remove();
			}

			return dealers;
		} else {
			List<Account> dealers = accountService.getAll(offset, limit, criteria);
			for (Iterator<Account> it = dealers.iterator(); it.hasNext();) {
				Account dealer = it.next();
				
				// only include if dealer has provided location
				if (dealer.latitude == 0)
					it.remove();
				else
					// sanitize result
					dealer.sanitize();
			}
			
			// return the specified results
			return dealers;
		}
	}
	
	// @Override
	// public List<Key<Color>> loadColors() throws RemoteServiceFailureException
	// {
	// return colorService.getAllKeys();
	// }
	//
	// @Override
	// public List<Key<Drivetrain>> loadDrivetrains() throws
	// RemoteServiceFailureException {
	// return drivetrainService.getAllKeys();
	// }
	//
	// @Override
	// public List<Key<Engine>> loadEngines() throws
	// RemoteServiceFailureException {
	// return engineService.getAllKeys();
	// }
	//
	// @Override
	// public List<Key<Transmission>> loadTransmissions() throws
	// RemoteServiceFailureException {
	// return transmissionService.getAllKeys();
	// }

	// @Override
	// public com.boosed.ml.shared.db.Tuple<Model, Ad> loadEdit(Key<Model>
	// model,
	// Key<Ad> ad) throws RemoteServiceFailureException {
	// return new com.boosed.ml.shared.db.Tuple<Model, Ad>(
	// modelService.get(model), adService.get(ad));
	// }

	// @Override
	// public Tuple<Model, Ad> loadEdit(String modelId, long adId) throws
	// RemoteServiceFailureException {
	// return new Tuple<Model, Ad>(modelService.get(modelId),
	// adService.get(adId));
	// }

	@Override
	public Map<FieldCar, List<?>> loadEdits() throws RemoteServiceFailureException {
		Map<FieldCar, List<?>> rv = new HashMap<FieldCar, List<?>>();
		rv.put(FieldCar.EXTERIOR, colorService.getAllKeys());
		rv.put(FieldCar.DRIVETRAIN, drivetrainService.getAllKeys());
		rv.put(FieldCar.ENGINE, engineService.getAllKeys());
		rv.put(FieldCar.TRANSMISSION, transmissionService.getAllKeys());
		return rv;
	}

	@Override
	public Location loadLocation(String postal) throws RemoteServiceFailureException {
		Location rv = locationService.get(postal);
		if (rv == null)
			// location is not known
			throw new InvalidLocationException();

		return rv;
	}

	@Override
	public List<Key<CLLocale>> loadLocales(Key<CLState> state) throws RemoteServiceFailureException {
		return clService.getLocales(state);
	}

	@Override
	public List<Key<CLState>> loadStates() throws RemoteServiceFailureException {
		return clService.getStates(new Key<CLContinent>(CLContinent.class, "US"));
	}

	// @Override
	// public <T> Map<String, List<Key>> loadEdits() throws
	// RemoteServiceFailureException {
	// Map<String, List> rv = new HashMap<String, List>();
	// rv.put("colors", colorService.getAllKeys());
	// }

	@Override
	public List<Type> loadTypes() throws RemoteServiceFailureException {
		// return makeService.getAllKeys();
		return typeService.getAll();
	}

	// @Override
	// public Model loadModel(Key<Model> model)
	// throws RemoteServiceFailureException {
	// return modelService.get(model);
	// }

	@Override
	public Model loadModel(String modelId) throws RemoteServiceFailureException {
		return modelService.get(modelId);
	}

	@Override
	public List<Model> loadModels(String makeId) throws RemoteServiceFailureException {
		// return models with given make
		return modelService.getAll(new EQCriterion("make", new Key<Make>(Make.class, makeId)));
	}

	// private int[] distances = new int[] { 25, 50, 100, 250, 500 };

	// @Override
	// public List<Model> loadModels(int offset, int limit, List<Key<Type>>
	// types, List<Key<Make>> makes)
	// throws RemoteServiceFailureException {
	// return modelService.getByTypesAndMakes(types, makes, true, offset,
	// limit);
	// }

	@Override
	public List<Make> loadMakes(boolean filterEmpty) throws RemoteServiceFailureException {
		// // distances
		// Integer[] distances = new Integer[] { 10, 25, 50, 75, 100, 150, 200,
		// 250, 300, 500 };
		//
		// // hooked this to test geo stuff
		// int single = 0;
		// int max = 0;
		// int maxlength = 0;
		// int coarse = 0;
		// List<Location> locations = locationService.getAll();
		// System.out.println("set size: " + locations.size());
		//
		// // Set<String> marked = new HashSet<String>();
		// int[] res = new int[5];
		//
		// for (Location location : locations)
		// for (int distance : distances) {
		// // System.out.println(location.postal + ": " +
		// // location.geocells.size());
		//
		// RangeCriterion rangeLat =
		// locationService.getLatitudeCriterion(location.latitude, distance);
		// RangeCriterion rangeLon =
		// locationService.getLongitudeCriteiron(location.latitude,
		// location.longitude,
		// distance);
		// BoundingBox bb = new BoundingBox((Double) rangeLat.hi, (Double)
		// rangeLon.hi, (Double) rangeLat.lo,
		// (Double) rangeLon.lo);
		// List<String> cells = GeocellManager.bestBboxSearchCells(bb, /*
		// * null
		// */
		// (CostFunction) carService);
		// max = Math.max(max, cells.size());
		//
		// // mark case for resolution of geocell list
		// res[cells.get(0).length() - 1]++;
		//
		// for (String cell : cells) {
		// int len = cell.length();
		// maxlength = Math.max(maxlength, len);
		// if (len == 1)
		// single++;
		// else if (len == 2) {
		// // maxlength = Math.max(maxlength, cell.length());
		// // System.out.println(location.postal + ", " + distance
		// // + ", " + cells);
		// coarse++;
		// break;
		// }
		//
		// }
		// }
		//
		// // sSBkwJ4EuSNn32bfZBpdig
		//
		// Location location = locationService.get("99950");
		// RangeCriterion rangeLat =
		// locationService.getLatitudeCriterion(location.latitude, 500);
		// RangeCriterion rangeLon =
		// locationService.getLongitudeCriteiron(location.latitude,
		// location.longitude, 500);
		// BoundingBox bb = new BoundingBox((Double) rangeLat.hi, (Double)
		// rangeLon.hi, (Double) rangeLat.lo,
		// (Double) rangeLon.lo);
		// List<String> cells = GeocellManager.bestBboxSearchCells(bb, /*
		// * null
		// */
		// (CostFunction) carService);
		// System.out.println(location.postal + ", " + 500 + ", " + cells);
		//
		// System.out
		// .println("finished! coarse queries: " + coarse + ", top length: " +
		// maxlength + ", max terms: " + max);
		// // Location loc = locationService.get("27708");
		// // System.out.println("location: " + loc.postal + " " + loc.geocells
		// +
		// // " " + loc.latitude + ", " + loc.longitude);
		//
		// System.out.println("resolution counts: [1] - " + res[0] + ", [2] - "
		// + res[1] + ", [3] - " + res[2]
		// + ", [4] - " + res[3] + ", [5] - " + res[4]);
		//
		// return new ArrayList<Make>();

		return makeService.getAll(filterEmpty);
	}

	@Override
	public Result<List<Car>> loadPending(String cursor, int limit)
			throws RemoteServiceFailureException {
		ensureAdmin();

		Result<List<Ad>> ads = adService.getPending(cursor, limit);

		List<Key<Car>> cars = new ArrayList<Key<Car>>();

		for (Ad ad : ads.result)
			cars.add(ad.car);

		return new Result<List<Car>>(new ArrayList<Car>(carService.get(cars).values()), ads.cursor);

		// return adService.getPending();
	}

	@Override
	public Set<Key<Car>> loadBookmarks() throws RemoteServiceFailureException {
		// return accountService.get(ensureAccountId()).buying;
		// Set<Key<Car>> rv = new HashSet<Key<Car>>();
		//
		// markerService.getAllParentKeys(null);
		// for (Marker marker :
		// markerService.getAllParentKeysByType(ensureAccountId(),
		// MarkerType.BOOKMARK))
		// rv.add(marker.car);
		//
		// return rv;
		// criteria.add(new EQCriterion("account", new
		// Key<Account>(Account.class, accountId)));
		// criteria.add(new EQCriterion("type", type));
		List<Criterion> criteria = new ArrayList<Criterion>();
		criteria
				.add(new EQCriterion("account", new Key<Account>(Account.class, ensureAccountId())));
		criteria.add(new EQCriterion("type", MarkerType.BOOKMARK));
		return markerService.getAllParentKeys(criteria);
	}

	@Override
	public Result<List<Tuple<Marker, Car>>> loadInventory(String cursor, int limit, SortType sort,
			MarkerType type) throws RemoteServiceFailureException {
		// Account account = accountService.get(ensureAccountId());
		// Map<Key<Car>, Car> cars = carService.get(account.buying);
		//
		// // remove the keys of cars which are no longer for sale
		// for (Iterator<Key<Car>> it = account.buying.iterator();
		// it.hasNext();)
		// if (!cars.containsKey(it.next()))
		// it.remove();
		// accountService.save(account);
		//
		// // result size
		// int size = cars.size();
		//
		// // no more results to return
		// if (size <= offset)
		// return new ArrayList<Car>();
		//
		// // return offset w/ limits
		// List<Car> rv = new ArrayList<Car>(cars.values());
		// return new ArrayList<Car>(rv.subList(offset, Math.min(offset + limit,
		// size)));
		return loadInv(ensureAccountId(), cursor, limit, sort, type, false);
	}

	@Override
	public Result<List<Tuple<Marker, Car>>> loadInventory(String accountId, String cursor,
			int limit, SortType sort, MarkerType type) throws RemoteServiceFailureException {
		// ensureAccountId();

		//boolean isAdmin = false;

		// ensure account exists
		if (accountService.get(accountId) == null)
			throw new NotFoundException();

		// TODO modify this so that admins can see inactive STOCK as well 
		
		// if other than stock retrieval, make sure user is admin
//		else if (type != MarkerType.STOCK) {
//			ensureAdmin();
//			isAdmin = true;
//		}

		// filter inactive items if requesting user is not an admin
		return loadInv(accountId, cursor, limit, sort, type, !/*isAdmin*/isUserAdmin());
		// // remove the keys of cars which are no longer for sale
		// Map<Key<Car>, Car> cars = carService.get(account.buying);
		// for (Iterator<Key<Car>> it = account.buying.iterator();
		// it.hasNext();)
		// if (!cars.containsKey(it.next()))
		// it.remove();
		// accountService.save(account);
		//
		// // result size
		// int size = cars.size();
		//
		// // no more results to return
		// if (size <= offset)
		// return new ArrayList<Car>();
		//
		// // return offset w/ limits
		// List<Car> rv = new ArrayList<Car>(cars.values());
		// return new ArrayList<Car>(rv.subList(offset, Math.min(offset + limit,
		// size)));
	}

	// private Result<List<Car>> ldBuying(String accountId, String cursor, int
	// limit, SortType sort) {
	// // // get the account's bookmarks
	// // Result<List<Marker>> markers = markerService.getAllByType(cursor,
	// limit, accountId, MarkerType.BOOKMARK);
	// //
	// // // get the corresponding car keys
	// // List<Key<Car>> keys = new ArrayList<Key<Car>>();
	// // for (Marker marker : markers.result)
	// // keys.add(marker.car);
	// //
	// // // create the return value
	// // return new Result<List<Car>>(new
	// ArrayList<Car>(carService.get(keys).values()), markers.cursor);
	//
	// List<Criterion> criteria = new ArrayList<Criterion>();
	// criteria.add(new EQCriterion("account", new Key<Account>(Account.class,
	// accountId)));
	// criteria.add(new EQCriterion("type", MarkerType.BOOKMARK));
	//
	// if (sort != SortType.NONE)
	// criteria.add(new SortCriterion(sort.toString()));
	// else
	// criteria.add(new SortCriterion("-time"));
	//
	// return markerService.getAllParents(cursor, limit, criteria);
	// //return markerService.getAllParents(cursor, limit, accountId,
	// MarkerType.BOOKMARK);
	//
	// // // load the map of cars
	// // Map<Key<Car>, Car> result = carService.get(keys);
	//
	// // // remove any markers that returned results; marker.result will be
	// // used
	// // // to delete any invalid markers
	// // for (Iterator<Marker> it = markers.result.iterator(); it.hasNext();)
	// // if (result.containsKey(it.next().car))
	// // // remove the existing marker from list
	// // it.remove();
	// //
	// // // delete the invalid markers
	// // markerService.drop(markers.result.toArray(new Marker[0]));
	// }

	// @Override
	// public Result<List<Car>> loadSelling(String cursor, int limit, SortType
	// sort) throws RemoteServiceFailureException {
	// return loadInventory(ensureAccountId(), cursor, limit, MarkerType.STOCK,
	// sort);
	// // Account account = accountService.get(ensureAccountId());
	// // Set<Key<Car>> selling = account.selling;
	// //
	// // // result size
	// // int size = selling.size();
	// //
	// // // no more results to return
	// // if (size <= offset)
	// // return new ArrayList<Car>();
	// //
	// // // sublist of keys (offset w/ limits)
	// // List<Key<Car>> keys = new
	// // ArrayList<Key<Car>>(selling).subList(offset, Math.min(offset
	// // + limit, size));
	// //
	// // // return items corresponding to keys
	// // return new ArrayList<Car>(carService.get(keys).values());
	// // // List<Car> rv = new
	// // ArrayList<Car>(carService.get(selling).values());
	// // // return new ArrayList<Car>(rv.subList(offset, Math.min(offset +
	// // limit,
	// // // rv.size())));
	// }
	//
	// @Override
	// public Result<List<Car>> loadSelling(String accountId, String cursor, int
	// limit)
	// throws RemoteServiceFailureException {
	// // only accessible by admins
	// ensureAdmin();
	//
	// if (accountService.get(accountId) == null)
	// throw new NotFoundException();
	//
	// return loadInventory(accountId, cursor, limit, MarkerType.STOCK,
	// SortType.NONE);
	// // Set<Key<Car>> selling = account.selling;
	// //
	// // // result size
	// // int size = selling.size();
	// //
	// // // no more results to return
	// // if (size <= offset)
	// // return new ArrayList<Car>();
	// //
	// // // sublist of keys (offset w/ limits)
	// // List<Key<Car>> keys = new
	// // ArrayList<Key<Car>>(selling).subList(offset, Math.min(offset
	// // + limit, size));
	// //
	// // // return items corresponding to keys
	// // return new ArrayList<Car>(carService.get(keys).values());
	// // // List<Car> rv = new
	// // ArrayList<Car>(carService.get(selling).values());
	// // // return new ArrayList<Car>(rv.subList(offset, Math.min(offset +
	// // limit,
	// // // rv.size())));
	// }

	@Override
	public Marker loadPrice(long carId) throws RemoteServiceFailureException {
		String accountId = ensureAccountId();

		if (markerService.get(accountId, carId, MarkerType.STOCK) != null)
			// user owns vehicle
			throw new OwnerException();
		else
			// return only the price marker or null (user may also have bookmark
			// as well)
			return markerService.get(accountId, carId, MarkerType.PRICE);
	}

	@Override
	public void postCg(long carId, Key<CLLocale> locale) throws RemoteServiceFailureException {
		// get user id
		String accountId = ensureAccountIdWithPrivileges();

		// retrieve car
		Car car = carService.get(carId);

		if (car == null)
			// car doesn't exist
			throw new NotFoundException();
		else if (!accountId.equals(car.owner.getName()) || car.time == null)
			// user is not the owner
			// user has already published to craigslist - IGNORED
			// ad is not searchable
			throw new UnauthorizedException();

		// post car to the ad network
		taskService.postAd(carId, clService.get(locale).url);
	}

	private Result<List<Tuple<Marker, Car>>> loadInv(String accountId, String cursor, int limit,
			SortType sort, MarkerType type, boolean filterInactive) {
		List<Criterion> criteria = new ArrayList<Criterion>();
		criteria.add(new EQCriterion("account", new Key<Account>(Account.class, accountId)));
		criteria.add(new EQCriterion("type", type));

		// add sort criterion
		criteria.add(new SortCriterion(sort.toString()));

		// if sorting not by oldest
		if (sort != SortType.TIME)
			// secondary sort by time (descending)
			criteria.add(new SortCriterion("-time"));

		Result<List<Tuple<Marker, Car>>> rv = markerService.getAllParents(cursor, limit, criteria);

		// should we remove if not price marker?
		if (type != MarkerType.PRICE)
			// remove marker from tuples
			for (Tuple<Marker, Car> item : rv.result)
				item.a = null;

		// remove all inactive items
		if (filterInactive)
			for (Iterator<Tuple<Marker, Car>> it = rv.result.iterator(); it.hasNext();)
				if (!it.next().b.isActive())
					it.remove();

		return rv;

		// return markerService.getAllParents(cursor, limit, accountId,
		// MarkerType.STOCK);

		// // get the account's bookmarks
		// Result<List<Marker>> markers = markerService.getAllByType(cursor,
		// limit, accountId, MarkerType.STOCK);
		//
		// // get the corresponding car keys
		// List<Key<Car>> keys = new ArrayList<Key<Car>>();
		// for (Marker marker : markers.result)
		// keys.add(marker.car);
		//
		// // create the return value
		// return new Result<List<Car>>(new
		// ArrayList<Car>(carService.get(keys).values()), markers.cursor);

		// load the map of cars
		// Map<Key<Car>, Car> result = carService.get(keys);

		// // remove any markers that returned results; marker.result will be
		// used
		// // to delete any invalid markers
		// for (Iterator<Marker> it = markers.result.iterator(); it.hasNext();)
		// if (result.containsKey(it.next().car))
		// // remove the existing marker from list
		// it.remove();
		//
		// // delete the invalid markers
		// markerService.drop(markers.result.toArray(new Marker[0]));
	}

	// EDIT

	@Override
	public Long activate(long carId, boolean active) throws RemoteServiceFailureException {
		// essentially index the time so that the vehicle is now searchable
		Car car = carService.get(carId);

		// if car not found, just return
		if (car == null)
			throw new NotFoundException();

		if (isUserAdmin()/* userService.isUserAdmin() */)
			// permit admin to activate/deactivate ad
			return carService.activateAd(car, active);
		else {
			// vet the user
			String accountId = ensureAccountIdWithPrivileges(); // ensureAccountId();

			// check that account is owner
			if (!car.owner.getName().equals(accountId))
				throw new UnauthorizedException();

			// user activation
			if (active) {
				switch (car.adType) {
				case STANDARD:
					if (!car.isActive())
						// only debit if this is the first activation
						debitAccount(car.owner);
					break;
				case BASIC:
					// users can activate free ads
					break;
				default:
					break;
				}

				// prune the ad accordingly
				adService.applyLimits(car);

				return carService.activateAd(car, true);
			}
			// don't allow users to deactivate their ads
			/* else carService.activateAd(car, activated); */

			return 0L;
		}
	}

	// @Override
	// public Set<Key<Car>> addBookmark(long carId) throws
	// RemoteServiceFailureException {
	// Account account = accountService.get(ensureAccountId());
	//
	// // check if this car exists?
	// account.buying.add(new Key<Car>(Car.class, carId));
	// accountService.save(account);
	//
	// return account.buying;
	// }

	@Override
	public void removeAlert(long carId) throws RemoteServiceFailureException {
		// find price alert
		Marker marker = markerService.get(ensureAccountId(), carId, MarkerType.PRICE);

		if (marker == null)
			// marker does not exist
			throw new NotFoundException();

		// delete the price alert
		markerService.drop(marker.getKey());
	}

	@Override
	public void removeCar(long carId) throws RemoteServiceFailureException {

		// Account account = null;
		Car car = null;

		if (isUserAdmin()) {
			// retrieve car
			car = carService.get(new Key<Car>(Car.class, carId));

			// check if car exists
			if (car == null)
				throw new NotFoundException();

			// retrieve the account of owner
			// account = accountService.get(car.owner);
		} else {
			// retrieve the account and ensure user owns vehicle
			// account = accountService.get(ensureAccountId());

			// don't need to specify type since user cannot bookmark own vehicle
			Marker marker = markerService.get(ensureAccountId(), carId, MarkerType.STOCK);

			// no account exists or user does not own vehicle
			if (/* account == null || !account.selling.contains(key) */marker == null)
				throw new UnauthorizedException();

			// initialize car (no need to null check since an account cannot
			// contain a non-existent car)
			car = carService.get(new Key<Car>(Car.class, carId));
		}

		carService.drop(car);
	}

	@Override
	public void addBookmark(long carId) throws RemoteServiceFailureException {
		String accountId = ensureAccountId();

		Car car = carService.get(carId);

		if (car == null)
			// car doesn't exist
			throw new NotFoundException();
		else if (accountId.equals(car.owner.getName()))
			// user is the owner
			throw new OwnerException();
		else
			// add bookmark
			markerService.addBookmark(accountId, car);
	}

	@Override
	public void removeBookmark(long carId) throws RemoteServiceFailureException {
		String accountId = ensureAccountId();

		// check if car exists
		if (carService.get(carId) == null)
			throw new NotFoundException();

		markerService.removeBookmark(accountId, carId);
	}

	@Override
	public void suggest(String postal, String locality, String admin, double latitude,
			double longitude) {
		if (locationService.get(postal) == null) {
			// check if "admin" is appropriate state identifier
			State state = stateService.get(admin);
			if (state != null) {
				// add location (use munged id so it is not yet searchable)
				Location location = new Location(postal, latitude, longitude);
				location.city = locality;
				location.state = state.getKey();

				// don't save this
				// locationService.save(location);

				messageService.validateLocation(location);
			}
		}
	}

	@Override
	public void updateAccount(Map<FieldAccount, Serializable> fields)
			throws RemoteServiceFailureException {
		// get account
		// Account account = accountService.get(ensureAccountId());

		// if (account == null)
		// throw new UnauthorizedException();

		accountService.update(accountService.get(ensureAccountIdWithPrivileges()), fields);
	}

	public void updateAd(Map<Serializable, FieldAd> fields) throws RemoteServiceFailureException {

		// get user id
		String accountId = ensureAccountId();

		Ad ad = null;
		Car car = null;

		// init the car and ad
		for (Entry<Serializable, FieldAd> entry : fields.entrySet()) {
			switch (entry.getValue()) {
			case ID_AD:
				ad = adService.get(new Key<Ad>(Ad.class, (Long) entry.getKey()));
				if (ad == null)
					throw new NotFoundException();
				break;
			case ID_CAR:
				car = carService.get(new Key<Car>(Car.class, (Long) entry.getKey()));
				if (car == null)
					throw new NotFoundException();
				break;
			default:
				break;
			}
		}

		boolean isAdmin = isUserAdmin();//userService.isUserAdmin();

		if (!isAdmin && /* ad != null && */!accountId.equals(ad.owner.getName()))
			// ensure that the user is the ad owner (if not an admin)
			throw new UnauthorizedException();
		else if (!isAdmin && car != null && !accountId.equals(car.owner.getName()))
			// ensure that the user is the car owner (if car exists)
			throw new UnauthorizedException();

		adService.update(ad, car, fields, isAdmin);
	}

	@Override
	public void updateAlert(long carId, int point) throws RemoteServiceFailureException {
		String accountId = ensureAccountId();

		// find marker by car, account & type
		// List<Criterion> criteria = new ArrayList<Criterion>();
		// criteria.add(new EQCriterion("car", new Key<Car>(Car.class, carId)));
		// criteria.add(new EQCriterion("account", new
		// Key<Account>(Account.class, accountId)));
		// criteria.add(new EQCriterion("type", MarkerType.PRICE));

		// find existing alert
		Marker alert = markerService.get(accountId, carId, MarkerType.PRICE);

		if (alert == null) {
			// create a new one

			// check if car exists
			Car car = carService.get(carId);

			if (car == null)
				// car is not found
				throw new NotFoundException();
			else if (accountId.equals(car.owner.getName()))
				// user owns vehicle
				throw new OwnerException();
			else if (car.price <= point)
				// point already met
				throw new InvalidPriceException();
			else
				// add a new alert
				markerService.createAlert(accountId, car, point);
		} else {
			// modify alert

			if (point >= alert.price)
				// point already met
				throw new InvalidPriceException();
			else {
				// change the price point
				alert.point = point;

				if (!alert.active) {
					// reactivating/reusing an alert

					// zero the change
					alert.delta = 0;

					// set the price
					alert.price = carService.get(carId).price;

					// mark alert active
					alert.active = true;

					// reestablish new time data
					alert.time = System.currentTimeMillis();
					alert.created = alert.time;
					// alert.time = System.currentTimeMillis();
				}

				// should not be able to update inactive markers
				// enable the marker
				// alert.active = true;

				markerService.save(alert);
			}
		}
	}

	@Override
	public void updateCar(Map<FieldCar, Serializable> fields) throws RemoteServiceFailureException {

		// get user id
		String accountId = ensureAccountIdWithPrivileges();

		// retrieve the car we are modifying or create a new one
		Long id = (Long) fields.get(FieldCar.ID);

		if (id == null)
			// car is new
			carService.update(accountId, new Car(), fields);
		else {
			// fetch car
			Car car = carService.get(id);

			if (car == null)
				// car doesn't exist
				throw new NotFoundException();
			else if (!accountId.equals(car.owner.getName()))
				// user is not the owner
				throw new UnauthorizedException();

			carService.update(accountId, car, fields);
		}
	}

	@Override
	public void updateModel(Map<FieldModel, Serializable> fields)
			throws RemoteServiceFailureException {
		// only admin can do this
		ensureAdmin();

		// retrieve the model by id
		String id = (String) fields.get(FieldModel.ID);
		Model model = modelService.get(id);

		// create a new model if necessary
		if (model == null)
			model = new Model(id);

		// save the model
		modelService.update(model, fields);
	}

	@Override
	public void upgrade(long carId) throws RemoteServiceFailureException {
		// essentially index the time so that the vehicle is now searchable
		Car car = carService.get(carId);

		// if car not found, just return
		if (car == null)
			throw new NotFoundException();

		if (isUserAdmin()) {
			// adjust the ad type for admin
			car.adType = AdType.STANDARD;
			carService.save(car);
		} else {
			// vet the user
			String accountId = ensureAccountIdWithPrivileges(); // ensureAccountId();

			// check that account is owner, car is already active and ad is free
			if (!car.owner.getName().equals(accountId) || !car.isActive()
					|| car.adType != AdType.BASIC)
				// not owner, not active or not free
				throw new UnauthorizedException();

			// debit the account
			debitAccount(car.owner);

			// upgrade type
			car.adType = AdType.STANDARD;
			carService.save(car);

			// TODO: send an upgrade message
			// messageService.createUpgrade(car);
		}
	}

	// END EDIT

	// MESSAGING

	@Override
	public void inquire(long carId, String message) throws RemoteServiceFailureException {
		// ensure user has proper privileges
		String senderId = ensureAccountIdWithPrivileges();

		// get the owner for the vehicle
		Car car = carService.get(carId);
		// Key<Account> owner = car.owner;

		// owner is making an inquiry on own car, throw owner exception
		if (senderId.equals(car.owner.getName()))
			throw new OwnerException();

		// check if ad allows messaging (cannot inquire against free ads)
		if (car.adType == AdType.BASIC)
			throw new UnauthorizedException();

		// create inquiry
		messageService.createInquiry(accountService.get(senderId), /*
																	 * accountService
																	 * .
																	 * get(car.
																	 * owner )
																	 */car, message);
		// messageService.save(new Message(new Key<Account>(Account.class,
		// senderId), owner, reference, message));
	}

	@Override
	public void reply(long messageId, String reply) throws RemoteServiceFailureException {
		Message message = messageService.get(new EQCriterion("responseTo", messageId));

		if (message == null)
			// use message identified by messageId to create new reply
			messageService.createReply(ensureAccountIdWithPrivileges(), messageService
					.get(messageId), reply);
		else {
			// use existing thread that already has the same responseTo field &
			// add message
			message.addMessage(reply, new Key<Account>(Account.class,
					ensureAccountIdWithPrivileges()));
			messageService.save(message);
		}

		// Message msg = new Message(new Key<Account>(Account.class,
		// ensureAccountId()), reference.sender, reference.reference,
		// message);
		// messageService.save(msg);
	}

	@Override
	public Tuple<Long, Integer> loadCount() throws RemoteServiceFailureException {
		Account account = accountService.get(ensureAccountId());

		if (account == null)
			throw new NotFoundException();

		// return the count of new messages
		return new Tuple<Long, Integer>(account.update, account.count);
	}

	@Override
	public Result<List<Message>> loadMessages(String cursor, int limit, MessageType type, Long carId)
			throws RemoteServiceFailureException {
		// String accountId = ensureAccountId();

		// add cursor/offset
		// criteria.add(new CursorCriterion(cursor));

		// add limit
		// criteria.add(new LimitCriterion(limit));

		// the recipient
		Key<Account> recipient = new Key<Account>(Account.class, ensureAccountId());

		List<Criterion> criteria = new ArrayList<Criterion>();
		criteria.add(new EQCriterion("recipient", recipient));
		// criteria.add(new EQCriterion("recipientDelete", Boolean.FALSE));

		switch (type) {
		case INQUIRIES:
			// must be owner to load inquiries
			criteria.add(new EQCriterion("owner", recipient));
			// criteria.add(new SortCriterion("owner"));
			// criteria.add(new SortCriterion("recipient"));
			// criteria.add(new SortCriterion("owner"));
			break;
		case RESPONSES:
			// must be customer to load responses
			criteria.add(new EQCriterion("customer", recipient));
			// criteria.add(new NEQCriterion("owner", recipient));
			// criteria.add(new SortCriterion("owner"));
			break;
		case ALERTS:
		case ALL:
		default:
			break;
		}

		// add reference
		if (carId != null)
			criteria.add(new EQCriterion("reference", new Key<Car>(Car.class, carId)));

		// add sorts
		criteria.add(new SortCriterion("-time"));

		return messageService.getAll(cursor, limit, criteria);
		// return messageService.getAll(cursor, limit, criteria);
	}

	@Override
	public void mark(long messageId, MessageState state, boolean enabled)
			throws RemoteServiceFailureException {
		String id = ensureAccountId();
		Message message = messageService.get(messageId);

		// message does not exist
		if (message == null)
			throw new NotFoundException();

		// make sure user is recipient
		if (message.recipient.getName().equals(id)) {
			message.setState(state, enabled);

			if (state == MessageState.READ)
				// decrement the account
				accountService.decrement(new Key<Account>(Account.class, id), 1);

			messageService.save(message);
		} else
			throw new UnauthorizedException();
	}

	@Override
	public void removeMessages(List<Long> messageIds) throws RemoteServiceFailureException {
		String id = ensureAccountId();

		// get ids
		Long[] ids = messageIds.toArray(new Long[0]);

		// count the number of unread messages and decrement count
		int unread = 0;
		for (Entry<Long, Message> entry : messageService.get(messageIds.toArray(new Long[0]))
				.entrySet())
			unread += entry.getValue().getState(MessageState.READ) ? 0 : 1;

		if (unread > 0)
			accountService.decrement(new Key<Account>(Account.class, id), unread);

		// drop messages
		messageService.drop(ids);
	}

	//
	// @Override
	// public List<Model> loadModels() {
	// return modelService.getAll();
	// }
	//
	// @Override
	// public List<Key<Type>> loadTypeKeys(boolean filterEmpty) {
	// return typeService.getAllKeys();
	// }

	// @Override
	// public List<Car> loadByMakes(List<Key<Make>> makes) {
	// List<Key<Model>> models = modelService.getKeysByMakes(makes, true);
	// // log.severe("the model is: " + models.get(0).desc);
	// return carService.getByModels(models);
	// }
	//
	// @Override
	// public List<Car> loadByTypes(List<Key<Type>> types) {
	// // return carService.getByTypes(types);
	// return null;
	// }

	// @Override
	// public List<Car> loadCars(int offset, int limit, List<Key<Type>> types,
	// List<Key<Make>> makes) {
	// List<Key<Model>> models = modelService.getKeysByTypesAndMakes(types,
	// makes, true);
	// return carService.getByModels(offset, limit, models);
	// }

	// @Override
	// public void checkCreds() throws RemoteServiceFailureException {
	// // will throw an exception if the user is not logged in
	// ensureAccountId();
	// }

	@Override
	public String initialize() {
		// if the user is not logged in, return false
		if (!userService.isUserLoggedIn())
			return null;

		User user = userService.getCurrentUser();
		String id = user.getUserId();

		Account account = accountService.get(id);
		if (account == null) {
			account = new Account(id, user.getNickname(), user.getEmail());
			accountService.save(account);
		}

		// set the user privileges
		// getThreadLocalRequest().getSession().setAttribute("enabled",
		// account.enabled);

		return id;
	}

	// /**
	// * Escape an html string. Escaping data received from the client helps to
	// * prevent cross-site script vulnerabilities.
	// *
	// * @param html
	// * the html string to escape
	// * @return the escaped string
	// */
	// private String escapeHtml(String html) {
	// if (html == null) {
	// return null;
	// }
	// return html.replaceAll("&", "&amp;").replaceAll("<",
	// "&lt;").replaceAll(">", "&gt;");
	// }

	/**
	 * Debit an <code>Account</code> for any activated/upgraded <code>Ad</code>
	 * s.
	 * 
	 * @param account
	 * @throws InsufficientCreditException
	 */
	private void debitAccount(Key<Account> account) throws InsufficientCreditException {
		// TODO: free activation of paid ads; uncomment when billing implemented

		// // check that account has credits
		// Account acct = accountService.get(account);
		// if (acct.credits == 0)
		// throw new InsufficientCreditException();
		// else {
		// // reduce credit
		// acct.credits--;
		// accountService.save(acct);
		// }
	}

	/**
	 * Retrieve the <code>User</code>'s id.
	 * 
	 * @return
	 * @throws SessionTimeoutException
	 */
	private String ensureAccountId() throws SessionTimeoutException {
		User user = userService.getCurrentUser();

		if (user == null)
			// throw an exception
			throw new SessionTimeoutException();

		return user.getUserId();
	}

	/**
	 * Like <code>ensureAccountId</code>, but also checks if the user has
	 * sufficient privilege to perform actions such as activating ads, sending
	 * messages, etc.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	private String ensureAccountIdWithPrivileges() throws RemoteServiceFailureException {
		String rv = ensureAccountId();

		if (!/*
			 * (Boolean)
			 * getThreadLocalRequest().getSession().getAttribute("enabled")
			 */accountService.get(rv).enabled)
			// user is restricted from performing action
			throw new RestrictedException();
		else
			// return the account id
			return rv;
	}

	/**
	 * Enusre that the user has administrative credentials.
	 * 
	 * @throws UnauthorizedException
	 */
	private void ensureAdmin() throws RemoteServiceFailureException {
		if (!userService.isUserLoggedIn())
			throw new SessionTimeoutException();
		else if (!userService.isUserAdmin())
			throw new UnauthorizedException();
	}

	/**
	 * Get user's id.
	 * 
	 * @return <code>null</code> if user is not logged in
	 */
	private String getUserId() {
		User user = userService.getCurrentUser();

		if (user == null)
			return null;

		return user.getUserId();
	}

	/**
	 * Check if the user is an admin.
	 * 
	 * @return
	 */
	private boolean isUserAdmin() {
		try {
			return userService.isUserAdmin();
		} catch (IllegalStateException e) {
			return false;
		}
	}

	@Inject
	private AccountService accountService;

	@Inject
	private AdService adService;

	@Inject
	private CarService carService;

	@Inject
	private ColorService colorService;

	@Inject
	private CLService clService;

	@Inject
	private DrivetrainService drivetrainService;

	@Inject
	private EngineService engineService;

	@Inject
	private LocationService locationService;

	@Inject
	private MakeService makeService;

	@Inject
	private MarkerService markerService;

	@Inject
	private MessageService messageService;

	@Inject
	private ModelService modelService;

	@Inject
	private StateService stateService;

	@Inject
	private TransmissionService transmissionService;

	@Inject
	private TypeService typeService;

	@Inject
	private BlobstoreService blobService;

	@Inject
	private TaskService taskService;

	@Inject
	private UserService userService;

	// private <A, B, C, D> Tuple<A, Tuple<B, Tuple<C, List<D>>>>
	// /*Tuple<A,B,C,D>*/ createTuple(List<A> a, List<B> b, List<C> c, List<D>
	// d) {
	// return new Tuple<A, Tuple<B, Tuple<C, List<D>>>>(a, new Tuple<B, Tuple<C,
	// List<D>>>(b, new Tuple<C, List<D>>(c,
	// d)));
	// //return new Tuple<A,B,C,D>(a,b,c,d);
	// }

	// private <A, B, C, D> Tuple<A, Tuple<B, Tuple<C, List<D>>>>
	// createTuple(List<A> a, List<B> b,
	// List<C> c, List<D> d) {
	// return new Tuple<A, Tuple<B, Tuple<C, List<D>>>>(a, new Tuple<B, Tuple<C,
	// List<D>>>(b,
	// new Tuple<C, List<D>>(c, d)));
	// }
}