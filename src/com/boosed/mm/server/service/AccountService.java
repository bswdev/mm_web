package com.boosed.mm.server.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.enums.FieldAccount;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.googlecode.objectify.Key;

public interface AccountService extends GenericService<Account> {

	/**
	 * Add credits to the given <code>Account</code>.
	 * 
	 * @param accountId
	 * @param credits
	 */
	public void addCredit(String accountId, int credits);

	/**
	 * Decrement the count of unread <code>Message</code>s for the given account
	 * id.
	 * 
	 * @param account
	 */
	public void decrement(Key<Account> account, int count);
	
	/**
	 * Increment the count of unread <code>Message</code>s for the given account
	 * id.
	 * 
	 * @param account
	 */
	public void increment(Key<Account> account, int count);

	/**
	 * Use geocells to further qualify a <code>List</code> of
	 * <code>Criterion</code>.
	 * 
	 * @param offset
	 * @param limit
	 * @param location
	 * @param distance
	 * @param criteria
	 * @return
	 */
	public List<Account> getByGeocells(int offset, int limit, Location location, int distance,
			List<Criterion> criteria);
	
	/**
	 * Update an <code>Account</code> wrt <code>fields</code>.
	 * 
	 * @param account
	 * @param fields
	 * @throws RemoteServiceFailureException
	 */
	public void update(Account account, Map<FieldAccount, Serializable> fields) throws RemoteServiceFailureException;
}