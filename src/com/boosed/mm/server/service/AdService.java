package com.boosed.mm.server.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.boosed.mm.shared.exception.PrimaryImageException;

public interface AdService extends GenericService<Ad> {

	/**
	 * Apply any limits wrt <code>AdType</code> before activating an
	 * <code>Ad</code>.
	 * 
	 * @param car
	 *            supplies <code>Key</code> and <code>AdType</code>
	 */
	public void applyLimits(Car car);

	/**
	 * Load all <code>Ad</code>s with pending editions.
	 * 
	 * @return
	 */
	public Result<List<Ad>> getPending(String cursor, int limit);

	/**
	 * Update an <code>Ad</code>.
	 * 
	 * @param ad
	 * @param car
	 * @param fields
	 * @param isAdmin
	 * @throws PrimaryImageException
	 */
	public void update(Ad ad, Car car, Map<Serializable, FieldAd> fields, boolean isAdmin) throws PrimaryImageException;
}