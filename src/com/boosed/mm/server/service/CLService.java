package com.boosed.mm.server.service;

import java.util.List;

import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.CLContinent;
import com.boosed.mm.shared.db.CLLocale;
import com.boosed.mm.shared.db.CLState;
import com.googlecode.objectify.Key;

public interface CLService extends GenericService<CLLocale> {

	/**
	 * Retrieve all <code>CLContinent</code> <code>Key</code>s.
	 * 
	 * @return
	 */
	public List<Key<CLContinent>> getContinents();

	/**
	 * Add a <code>CLContinent</code> by the given name.
	 * 
	 * @param name
	 * @return
	 */
	public Key<CLContinent> saveContinent(String name);

	/**
	 * Retrieve all <code>CLState</code>s by the given parent <code>Key</code>.
	 * 
	 * @param parent
	 * @return
	 */
	public List<Key<CLState>> getStates(Key<CLContinent> parent);

	/**
	 * Add a <code>CLState</code> by the given name.
	 * 
	 * @param parent
	 * @param name
	 * @return
	 */
	public Key<CLState> saveState(Key<CLContinent> parent, String name);

	/**
	 * Retrieve all <code>CLLocale</code>s by the given parent <code>Key</code>.
	 * 
	 * @param parent
	 * @return
	 */
	public List<Key<CLLocale>> getLocales(Key<CLState> parent);

	/**
	 * Add a <code>CLLocale</code> by the given name.
	 * 
	 * @param parent
	 * @param name
	 * @param url
	 * @return
	 */
	public Key<CLLocale> saveLocale(Key<CLState> parent, String name, String url);

	// /**
	// * Populate all the values from craigslist.
	// *
	// * @throws Exception
	// */
	// public void populate() throws Exception;
}