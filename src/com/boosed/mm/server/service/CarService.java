package com.boosed.mm.server.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.enums.FieldCar;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;

public interface CarService extends GenericService<Car> {

	/**
	 * Activate the <code>Ad</code> corresponding to the given
	 * <code>carId</code>.
	 * 
	 * @param car
	 * @param activated
	 * @return
	 */
	public long activateAd(Car car, boolean activated) throws RemoteServiceFailureException;

//	/**
//	 * Remove the <code>Car</code> and <code>Ad</code> associated with the given
//	 * <code>Account</code>.
//	 * 
//	 * @param car
//	 */
//	public void drop(Car car);

	/**
	 * Use geocells to further qualify a <code>List</code> of
	 * <code>Criterion</code>.
	 * 
	 * @param offset
	 * @param limit
	 * @param location
	 * @param distance
	 * @param criteria
	 * @return
	 */
	public List<Car> getByGeocells(int offset, int limit, Location location, int distance,
			List<Criterion> criteria);

	/**
	 * @deprecated
	 * 
	 *             Use <code>Location</code> to further qualify a
	 *             <code>List</code> of <code>Criterion</code>.
	 * 
	 * @param location
	 * @param distance
	 * @param criteria
	 * @return
	 */
	public List<Car> getByLocation(Location location, int distance, List<Criterion> criteria);

	/**
	 * Update the <code>Car</code> wrt <code>fields</code>.
	 * 
	 * @param accountId
	 * @param car
	 * @param fields
	 * @throws RemoteServiceFailureException
	 */
	public void update(String accountId, Car car, Map<FieldCar, Serializable> fields)
			throws RemoteServiceFailureException;
}