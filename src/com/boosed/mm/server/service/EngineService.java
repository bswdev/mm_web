package com.boosed.mm.server.service;

import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Engine;

public interface EngineService extends GenericService<Engine> {
}