package com.boosed.mm.server.service;

import java.util.List;

import com.boosed.gae.server.criterion.RangeCriterion;
import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Location;
import com.googlecode.objectify.Key;

public interface LocationService extends GenericService<Location> {

	/**
	 * Retrieve a <code>List</code> of <code>Location</code>s by the given
	 * distance.
	 * 
	 * @param latitude
	 * @param longitude
	 * @param distance
	 * @return
	 */
	List<Location> getByDistance(double latitude, double longitude, int distance);

	/**
	 * Return <code>Key</code>s by distance.
	 * 
	 * @param latitude
	 * @param longitude
	 * @param distance
	 * @return
	 */
	List<Key<Location>> getKeysByDistance(double latitude, double longitude, int distance);
	
	/**
	 * Get a <code>RangeCriterion</code> for a given latitude and distance.
	 * 
	 * @param latitude
	 * @param distance
	 * @return
	 */
	public RangeCriterion getLatitudeCriterion(double latitude, int distance);

	/**
	 * Get a <code>RangeCriterion</code> for a given longitude and distance.
	 * 
	 * @param latitude
	 * @param longitude
	 * @param distance
	 * @return
	 */
	public RangeCriterion getLongitudeCriteiron(double latitude, double longitude, int distance);
}