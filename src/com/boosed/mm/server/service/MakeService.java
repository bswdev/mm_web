package com.boosed.mm.server.service;

import java.util.List;
import java.util.Set;

import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Type;
import com.googlecode.objectify.Key;

public interface MakeService extends GenericService<Make> {

	/**
	 * Retrieve all available <code>Make</code>s. Choose to filter out
	 * <code>Make</code>s with no <code>Ad</code>s.
	 * 
	 * @param filterEmpty
	 * @return
	 */
	List<Make> getAll(boolean filterEmpty);

	/**
	 * Retrieve all <code>Make</code>s by the desired <code>Type</code>. Choose
	 * to filter out <code>Make</code>s which have no corresponding
	 * <code>Ad</code>s.
	 * 
	 * @param type
	 * @param filterEmpty
	 * @return
	 */
	public List<Make> getByType(Key<Type> type, boolean filterEmpty);
	
	/**
	 * Retrieve all <code>Make</code>s by the desired <code>Type</code>s. Choose
	 * to filter out <code>Make</code>s which have no corresponding
	 * <code>Ad</code>s.
	 * 
	 * @param types
	 * @param filterEmpty
	 * @return
	 * @deprecated
	 */
	public List<Make> getByTypes(List<Key<Type>> types, boolean filterEmpty);

	// /**
	// * Lower the <code>Type</code> counts on this <code>Make</code>.
	// *
	// * @param make
	// * @param types
	// */
	// public void decrement(Key<Make> make, Set<Key<Type>> types);
	//
	// /**
	// * Increase the <code>Type</code> counts on this <code>Make</code>.
	// *
	// * @param make
	// * @param types
	// */
	// public void increment(Key<Make> make, Set<Key<Type>> types);
	
	/**
	 * Lower the total count on these <code>Type</code>s for given <code>Make</code>.
	 * 
	 * @param make
	 * @param types
	 * @param count
	 */
	public void decrement(Key<Make> make, Set<Key<Type>> types, int count);
	
	/**
	 * Increase the total count on these <code>Type</code>s for given <code>Make</code>.
	 * 
	 * @param make
	 * @param types
	 * @param count
	 */
	public void increment(Key<Make> make, Set<Key<Type>> types, int count);
}