package com.boosed.mm.server.service;

import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.enums.MarkerType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.exception.ResourceLimitException;
import com.googlecode.objectify.Key;

public interface MarkerService extends GenericService<Marker> {

	/**
	 * Associate a <code>Marker</code> of type "bookmark" with user's
	 * <code>Account</code> referencing the given <code>Car</code>.
	 * 
	 * @param accountId
	 * @param car
	 * @throws RemoteServiceFailureException
	 */
	public void addBookmark(String accountId, Car car) throws RemoteServiceFailureException;

	/**
	 * Dissociate a <code>Marker</code> of type "bookmark" with user's
	 * <code>Account</code> referencing the given <code>Car</code>.
	 * 
	 * @param accountId
	 * @param carId
	 * @throws RemoteServiceFailureException
	 */
	public void removeBookmark(String accountId, long carId) throws RemoteServiceFailureException;

	/**
	 * Creates a new price <code>Marker</code> for this <code>Account</code>.
	 * The number of price <code>Marker</code>s is limited.
	 * 
	 * @param accountId
	 * @param car
	 * @param point
	 * @throws ResourceLimitException
	 */
	public void createAlert(String accountId, Car car, int point) throws ResourceLimitException;

	/**
	 * Drop all <code>Marker</code>s associated with the given <code>Car</code>.
	 * 
	 * @param car
	 */
	public void dropCar(Key<Car> car);

	/**
	 * Retrieve a <code>Marker</code>.
	 * 
	 * @param accountId
	 * @param carId
	 * @param type
	 * @return
	 */
	public Marker get(String accountId, long carId, MarkerType type);

	// /**
	// * Retrieve all parent entities given the <code>accountId</code> and
	// * <code>MarkerType</code>.
	// *
	// * @param <P>
	// * @param accountId
	// * @param type
	// * @return
	// */
	// public <P> Map<Key<P>, P> getAllParentsByType(String accountId,
	// MarkerType type);

	// /**
	// * Retrieve all parent entities given the <code>accountId</code> and
	// * <code>MarkerType</code> using a cursor and limit.
	// *
	// * @param <P>
	// * @param cursor
	// * @param limit
	// * @param accountId
	// * @param type
	// * @return
	// */
	// public <P> Result<List<P>> getAllParentsByType(String cursor, int limit,
	// String accountId,
	// MarkerType type);

	// /**
	// * Retrieve all <code>Marker</code>s associated with the given
	// * <code>MarkerType</code> and <code>Account</code>.
	// *
	// * @param accountId
	// * @param type
	// * @return
	// */
	// public Set<Key<Car>> getAllParentKeysByType(List<Criterion> criteria);

	// /**
	// * Supply a cursor and limit to fetch <code>Marker</code>s.
	// *
	// * @param cursor
	// * @param accountId
	// * @param type
	// * @return
	// */
	// public Result<List<Marker>> getAllByType(String cursor, int limit, String
	// accountId,
	// MarkerType type);

	// /**
	// * Retrieve the <code>Marker</code> identified by the given car and
	// account
	// * id.
	// *
	// * @param carId
	// * @param accountId
	// * @return
	// */
	// public Marker getByCar(long carId, String accountId);

	/**
	 * Update all associated <code>Marker</code>s according to the new updated
	 * model name set by admin.
	 * 
	 * @param carId
	 * @param modelName
	 */
	public void updateModel(Long carId, String modelName);

	/**
	 * Update all associated <code>Marker</code>s according to the new updated
	 * price an owner has set.
	 * 
	 * TODO QUEUE ELIGIBLE
	 * 
	 * @param car
	 *            the car which has received a price update
	 */
	public void updatePrice(Car car);
}