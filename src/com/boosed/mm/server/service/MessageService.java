package com.boosed.mm.server.service;

import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.exception.InvalidRecipientException;
import com.googlecode.objectify.Key;

public interface MessageService extends GenericService<Message> {

	/**
	 * Creates an alert <code>Message</code> notifying the owner of this
	 * <code>Car</code> that the associated <code>Ad</code> has been activated.
	 * 
	 * @param car
	 */
	public void createActivation(Car car);

	/**
	 * Generate an inquiry for a particular vehicle given by the
	 * <code>Car</code> reference.
	 * 
	 * @param sender
	 * @param car
	 * @param message
	 */
	public void createInquiry(Account sender, Car car, String message);

	/**
	 * Generate a reply for a particular vehicle given by the <code>Car</code>
	 * reference.
	 * 
	 * @param accountId
	 * @param original
	 * @param message
	 * @throws InvalidRecipientException
	 */
	public void createReply(String sender, Message original, String reply)
			throws InvalidRecipientException;

	/**
	 * Delete all <code>Message</code>s associated with the referenced
	 * <code>Car</code>.
	 * 
	 * @param reference
	 */
	public void dropCar(Key<Car> reference);

	// /**
	// * Notify the admins of a particular problem.
	// *
	// * @param user
	// * @param subject
	// * @param message
	// */
	// public void notifyAdmins(String user, String subject, String message);

	/**
	 * Notify the user that a specified price <code>Marker</code> has been
	 * satisfied.
	 * 
	 * @param alert
	 * @param car
	 */
	public void confirmPrice(Marker alert, Car car);

	/**
	 * Create a reject message for an <code>Ad</code> given the <code>Car</code>
	 * reference and the owner <code>Account</code>.
	 * 
	 * @param owner
	 * @param car
	 */
	public void rejectAd(Key<Account> owner, Car car);

	/**
	 * Create a reject message for an <code>Ad</code>'s image(s) given the
	 * <code>Car</code> reference and the owner <code>Account</code>.
	 * 
	 * @param owner
	 * @param car
	 */
	public void rejectImages(Key<Account> owner, Car car);

	/**
	 * Inform the <code>owner</code> that a recently activated <code>Ad</code>
	 * has been truncated through removal of images.
	 * 
	 * @param owner
	 * @param car
	 */
	public void truncateAd(Key<Account> owner, Car car);

	/**
	 * Like <code>save</code>, but also updates the message count associated
	 * with the recipient and sends email notifications if requested.
	 * 
	 * @param messge
	 * @return
	 */
	public Message update(Message messge);

	/**
	 * Updates all identifying information transposed from the <code>Car</code>
	 * due to changes in its information (e.g., stock number, vin, model,
	 * etc.).
	 * 
	 * TODO QUEUE ELIGIBLE
	 * 
	 * @param car
	 */
	public void updateIdentifiers(Car car);

	/**
	 * Notify admins to validate a suggested <code>Location</code>.
	 * 
	 * @param location
	 */
	public void validateLocation(Location location);
}