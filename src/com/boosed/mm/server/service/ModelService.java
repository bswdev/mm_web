package com.boosed.mm.server.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.FieldModel;
import com.googlecode.objectify.Key;

public interface ModelService extends GenericService<Model> {

	// public List<Model> getByMakes(List<Key<Make>> makes, boolean
	// filterEmpty);
	//
	// public List<Key<Model>> getKeysByMakes(List<Key<Make>> makes, boolean
	// filterEmpty);
	//
	// public List<Model> getByTypes(List<Key<Type>> types, boolean
	// filterEmpty);
	//
	// public List<Key<Model>> getKeysByTypes(List<Key<Type>> types, boolean
	// filterEmpty);

	/**
	 * Retrieve a <code>List</code> of <code>Model</code>s given the desired
	 * <code>Type</code> and <code>Make</code>. Choose to filter out
	 * <code>Model</code>s that don't have <code>Ad</code>s.
	 * 
	 * @param type
	 * @param make
	 * @param filterEmpty
	 * @return
	 */
	public List<Model> getByTypeAndMake(Key<Type> type, Key<Make> make, boolean filterEmpty);

	/**
	 * Retrieve a <code>List</code> of <code>Model</code>s given the desired
	 * <code>Type</code>s and <code>Make</code>s. Choose to filter out
	 * <code>Model</code>s that don't have <code>Ad</code>s.
	 * 
	 * @param types
	 * @param makes
	 * @param filterEmpty
	 * @return
	 */
	public List<Model> getByTypesAndMakes(List<Key<Type>> types, List<Key<Make>> makes,
			boolean filterEmpty);

	/**
	 * Retrieve a <code>List</code> of <code>Key</code>s of <code>Model</code>s
	 * given the desired <code>Type</code>s and <code>Make</code>s. Choose to
	 * filter out <code>Model</code>s that don't have <code>Ad</code>s.
	 * 
	 * @param types
	 * @param makes
	 * @param filterEmpty
	 * @return
	 */
	public List<Key<Model>> getKeysByTypesAndMakes(List<Key<Type>> types, List<Key<Make>> makes,
			boolean filterEmpty);

	/**
	 * Lower the total count on this <code>Model</code>.
	 * 
	 * @param model
	 */
	public void decrement(Key<Model> model, int count);

	/**
	 * Increase the total count on this <code>Model</code>.
	 * 
	 * @param model
	 * @param count
	 */
	public void increment(Key<Model> model, int count);

	/**
	 * Update specified <code>Model</code>.
	 * 
	 * @param fields
	 */
	public void update(Model model, Map<FieldModel, Serializable> fields);

	/**
	 * Rename the <code>Model</code> specified by <code>oldName</code> to the
	 * current <code>Make</code> plus <code>newName</code>.
	 * 
	 * TODO QUEUE ELIGIBLE
	 * 
	 * @param modelId
	 * @param make
	 */
	public void rename(String modelId, String make, String model);
}