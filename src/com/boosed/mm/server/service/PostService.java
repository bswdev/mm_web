package com.boosed.mm.server.service;

import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Car;

public interface PostService extends GenericService<Car> {

	/**
	 * Post this <code>Car</code> to provided advertising network.
	 * 
	 * @param carId
	 * @param network
	 */
	public void post(long carId, String network);
}