package com.boosed.mm.server.service;

/**
 * Service for enqueuing tasks onto task queues provided by app engine.
 * 
 * @author dsumera
 */
public interface TaskService {

//	/**
//	 * Populates the data items associated with craigslist.
//	 */
//	public void populateCraigslist();

	/**
	 * Repost the current <code>Car</code> ad to another advertising network.
	 * 
	 * @param carId
	 * @param network
	 */
	public void postAd(long carId, String network);

	/**
	 * Change the name of an existing <code>Model</code>.
	 * 
	 * @param modelId
	 * @param make
	 * @param model
	 */
	public void renameModel(String modelId, String make, String model);

	/**
	 * Update the <code>Message</code>s associated with a <code>Car</code> due
	 * to changes in the car's info (i.e., stock number, vin or model).
	 * 
	 * @param carId
	 */
	public void updateMessages(long carId);

	/**
	 * Update price alerts associated with a <code>Car</code> due to changes in
	 * price.
	 * 
	 * @param carId
	 */
	public void updatePriceAlerts(long carId);
}
