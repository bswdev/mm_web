package com.boosed.mm.server.service;

import java.util.List;

import com.boosed.gae.server.service.GenericService;
import com.boosed.mm.shared.db.Type;

public interface TypeService extends GenericService<Type> {

	/**
	 * Retrieve all available <code>Type</code>s. Choose to filter out
	 * <code>Type</code>s with no <code>Ad</code>s.
	 * 
	 * @param filterEmpty
	 * @return
	 */
	List<Type> getAll(boolean filterEmpty);

	// /**
	// * Retrieve all the <code>Key</code>s of available <code>Type</code>s.
	// * Choose to filter out <code>Type</code>s with no <code>Ad</code>s.
	// *
	// * @param filterEmpty
	// * @return
	// */
	// List<Key<Type>> getAllKeys(boolean filterEmpty);

//	/**
//	 * Lower the total count on this <code>Type</code>.
//	 * 
//	 * @param type
//	 */
//	public void decrement(Key<Type> type);
//
//	/**
//	 * Increase the total count on this <code>Type</code>.
//	 * 
//	 * @param type
//	 */
//	public void increment(Key<Type> type);
}
