package com.boosed.mm.server.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.beoui.geocell.GeocellManager;
import com.beoui.geocell.model.BoundingBox;
import com.beoui.geocell.model.CostFunction;
import com.beoui.geocell.model.Point;
import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.criterion.InCriterion;
import com.boosed.gae.server.criterion.RangeCriterion;
import com.boosed.gae.server.dao.GenericDao;
import com.boosed.gae.server.dao.Transactable;
import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.AccountDao;
import com.boosed.mm.server.service.AccountService;
import com.boosed.mm.server.service.LocationService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.enums.FieldAccount;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.exception.IncompleteException;
import com.boosed.mm.shared.exception.InvalidLocationException;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.util.DataUtil;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.googlecode.objectify.Key;

public class AccountServiceImpl extends GenericServiceImpl<Account, AccountDao> implements AccountService, CostFunction {

	/** the number of cells desired to approximate a bounded region */
	private final int geocellMax;
	
	@Inject
	public AccountServiceImpl(AccountDao dao, @Named("geocellMax") int geocellMax) {
		super(dao);
		this.geocellMax = geocellMax;
	}

	@Override
	public void addCredit(String accountId, int credits) {
		Account account = get(accountId);
		account.credits += credits;
		save(account);
	}

	@Override
	public void decrement(final Key<Account> account, final int count) {
		dao.repeatInTransaction(new Transactable<Account>() {
			@Override
			public void run(GenericDao<Account> dao) {
				Account acct = dao.findByKey(account);

				// decrement count
				acct.count -= count;

				if (acct.count < 0)
					acct.count = 0;
				// else
				// acct.update = System.currentTimeMillis();

				dao.persist(acct);
			}
		});
	}

	@Override
	public void increment(final Key<Account> account, final int count) {
		dao.repeatInTransaction(new Transactable<Account>() {
			@Override
			public void run(GenericDao<Account> dao) {
				Account acct = dao.findByKey(account);

				// increment count
				acct.count += count;
				acct.update = System.currentTimeMillis();

				dao.persist(acct);
			}
		});
	}

	@Override
	public List<Account> getByGeocells(int offset, int limit, Location location, int distance,
			List<Criterion> criteria) {
		RangeCriterion rangeLat = locationService.getLatitudeCriterion(location.latitude, distance);
		RangeCriterion rangeLon = locationService.getLongitudeCriteiron(location.latitude,
				location.longitude, distance);
		BoundingBox bb = new BoundingBox((Double) rangeLat.hi, (Double) rangeLon.hi,
				(Double) rangeLat.lo, (Double) rangeLon.lo);
		List<String> cells = GeocellManager.bestBboxSearchCells(bb, this);
		criteria.add(new InCriterion<String>("geocells", cells));

		return dao.findAllByCriteria(offset, limit, criteria);
	}
	
	@Override
	public void update(Account account, Map<FieldAccount, Serializable> fields) throws RemoteServiceFailureException {

		/** whether user has provided coordinates for location */
		boolean locationExplicit = false;

		for (Entry<FieldAccount, Serializable> entry : fields.entrySet()) {
			switch (entry.getKey()) {
			case ADDRESS:
				account.address = DataUtil.getNullString((String) entry.getValue());
				break;
			// case CAR_BUY:
			// Key<Car> key = new Key<Car>(Car.class, (Long) entry.getValue());
			//
			// // user is selling this car already; do not add to "buying" set
			// if (account.selling.contains(key))
			// throw new OwnerException();
			//
			// // check the buying limits
			// if (account.buying.size() >= bookmarkMax)
			// // exceeded maximum permitted
			// throw new ResourceLimitException();
			//
			// account.buying.add(key);
			// break;
			// case CAR_SELL:
			// account.selling.add(new Key<Car>(Car.class, (Long)
			// entry.getKey()));
			// break;
			// case CAR_REMOVE:
			// // marks removal of bookmarks; for removing inventory, see
			// // CarServiceImpl.drop
			// account.buying.remove(new Key<Car>(Car.class, (Long)
			// entry.getValue()));
			// break;
			case DEALER:
				account.dealer = (Boolean) entry.getValue();
				break;
			case EMAIL:
				// TODO: validate the email address
				// String email = DataUtil.getNullString((String)
				// entry.getValue());
				account.email = DataUtil.getNullString((String) entry.getValue());
				if (account.email == null)
					throw new IncompleteException();
				break;
			case EML_SHOW:
				account.showEmail = (Boolean) entry.getValue();
				break;
			case LATITUDE:
			case LONGITUDE:
				locationExplicit = true;
				break;
			case LOCATION:
				String location = DataUtil.getNullString((String) entry.getValue());
				if (location == null)
					account.postal = null;
				else {
					// retrieve this location
					Location loc = locationService.get(location);
					if (loc == null)
						throw new InvalidLocationException();

					// set the value to the location's key
					// account.location = loc.getKey();
					account.postal = loc.postal;
				}
				break;
			case NAME:
				account.name = DataUtil.getNullString((String) entry.getValue());
				if (account.name == null)
					throw new IncompleteException();
				break;
			case NOTIFY_ALT:
				account.setNotify(MessageType.ALERTS, (Boolean) entry.getValue());
				break;
			case NOTIFY_INQ:
				account.setNotify(MessageType.INQUIRIES, (Boolean) entry.getValue());
				break;
			case NOTIFY_RES:
				account.setNotify(MessageType.RESPONSES, (Boolean) entry.getValue());
				break;
			case PHONE:
				account.phone = DataUtil.getNullString((String) entry.getValue());
				break;
			case URL:
				account.url = DataUtil.getNullString((String) entry.getValue());
				break;
			default:
				break;
			}

			if (account.dealer && locationExplicit) {
				// set the geocells for this account
				account.latitude = (Double) fields.get(FieldAccount.LATITUDE);
				account.longitude = (Double) fields.get(FieldAccount.LONGITUDE);
				account.geocells = GeocellManager.generateGeoCell(new Point(account.latitude, account.longitude));
			} else {
				// will not be indexed/searchable
				account.geocells = null;
				account.latitude = 0;
				account.longitude = 0;
			}

			save(account);
		}
	}

	@Override
	public double defaultCostFunction(int numCells, int resolution) {
		return numCells > geocellMax ? Double.MAX_VALUE : 0;
	}
	
	@Inject
	private LocationService locationService;
}