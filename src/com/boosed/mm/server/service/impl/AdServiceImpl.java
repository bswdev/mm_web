package com.boosed.mm.server.service.impl;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.criterion.EQCriterion;
import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.AdDao;
import com.boosed.mm.server.service.AdService;
import com.boosed.mm.server.service.CarService;
import com.boosed.mm.server.service.MessageService;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Result;
import com.boosed.mm.shared.db.Tuple;
import com.boosed.mm.shared.db.enums.FieldAd;
import com.boosed.mm.shared.exception.PrimaryImageException;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class AdServiceImpl extends GenericServiceImpl<Ad, AdDao> implements AdService {

	/** free and paid quotas */
	private final int quotaBasic, quotaStandard;
	
	@Inject
	public AdServiceImpl(AdDao dao, @Named("quotaBasic") int quotaBasic,
			@Named("quotaStandard") int quotaStandard) {
		super(dao);
		this.quotaBasic = quotaBasic;
		this.quotaStandard = quotaStandard;
	}

	@Override
	public void applyLimits(Car car) {
		Ad ad = get(car.ad);
		Set<Entry<String, Tuple<Boolean, String>>> images = ad.getImages();
		int remove = images.size();

		switch (car.adType) {
		case BASIC:
			// permitted 2 images
			remove -= quotaBasic;
			break;
		case STANDARD:
			// permitted 20 images
			remove -= quotaStandard;
			break;
		}

		// reduce the images accordingly
		if (remove > 0) {
			for (Iterator<Entry<String, Tuple<Boolean, String>>> it = images.iterator(); remove > 0
					&& it.hasNext();)
				// make sure that the removed image is not the primary ad image
				if (!it.next().getKey().equals(ad.imageKey)) {
					// remove item and decrement the count
					it.remove();
					remove--;
				}

			// send a message to user that ad has been truncated
			messageService.truncateAd(ad.owner, car);

			// save the ad
			save(ad);
		}
	}

	@Override
	public Result<List<Ad>> getPending(String cursor, int limit) {
		// find all ads with edit set to true
		return dao.findAllByCriteria(cursor, limit, Arrays.asList((Criterion) new EQCriterion(
				"edit", Boolean.TRUE)));
	}

	@Override
	public void update(Ad ad, Car car, Map<Serializable, FieldAd> fields, boolean isAdmin)
			throws PrimaryImageException {
		boolean imageReject = false;
		for (Entry<Serializable, FieldAd> entry : fields.entrySet()) {
			switch (entry.getValue()) {
			case AD:
				// set the ad content
				ad.setContent((String) entry.getKey());
				
				// TODO: approve for now
				ad.approveContent();
				break;
			case AD_APPROVE:
				// approve the ad content
				// limit to admin
				if (isAdmin)
					ad.approveContent();
				break;
			case AD_REJECT:
				// reject the ad content
				if (isAdmin) {
					ad.rejectContent();

					// ensure car info is included when rejecting ad
					messageService.rejectAd(ad.owner, car);
					// Message message = new Message(null, "", recipient)//new
					// Message(null, ad.owner, "ad was rejected");
					// message.reference = car.getKey();
					// messageService.save(message);
				}
				break;
			case IMG_APPROVE:
				// approve the image
				// limit to admin
				if (isAdmin)
					ad.approveImage((String) entry.getKey());
				// log.warning("approving the image w/ key: " + entry.getKey());
				break;
			case IMG_REJECT:
				if (isAdmin) {
					// delete a photo & corresponding blob
					String key = (String) entry.getKey();
					ad.removeImage(key);
					blobService.delete(new BlobKey(key));

					// send message to user (ensure car is referenced)
					imageReject |= true;
				}
				break;
			case IMG_CAR:
				// set this key as the snapshot (update car)
				String key = (String) entry.getKey();
				String url = ad.getUrl(key);
				if (url != null) {
					// update car url
					car.image = url;
					carService.save(car);

					// update ad key
					ad.imageKey = key;
					save(ad);
				} else
					log.warning("file image does not exist");
				break;
			case IMG_DELETE:
				// delete a photo & corresponding blob
				key = (String) entry.getKey();
				if (ad.removeImage(key))
					blobService.delete(new BlobKey(key));
				else {
					log.warning("cannot delete main image");
					throw new PrimaryImageException();
				}
				break;
			default:
				break;
			}
		}

		if (imageReject) {
			// include vehicle when rejecting images
			messageService.rejectImages(/* accountService.get(ad.owner) */ad.owner, car);
			// Message message = new Message(null, ad.owner,
			// "images were rejected");
			// message.reference = car.getKey();
			// messageService.save(message);
		}

		// update the ad
		save(ad);
	}

	// @Inject
	// private AccountService accountService;

	@Inject
	private BlobstoreService blobService;

	@Inject
	private CarService carService;

	@Inject
	private MessageService messageService;
}