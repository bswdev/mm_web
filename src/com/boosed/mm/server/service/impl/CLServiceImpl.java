package com.boosed.mm.server.service.impl;

import java.util.List;

import com.boosed.gae.server.criterion.AncestorCriterion;
import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.CLContinentDao;
import com.boosed.mm.server.dao.CLLocaleDao;
import com.boosed.mm.server.dao.CLStateDao;
import com.boosed.mm.server.service.CLService;
import com.boosed.mm.shared.db.CLContinent;
import com.boosed.mm.shared.db.CLLocale;
import com.boosed.mm.shared.db.CLState;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.inject.Inject;
import com.googlecode.objectify.Key;

public class CLServiceImpl extends GenericServiceImpl<CLLocale, CLLocaleDao> implements CLService {

	@Inject
	public CLServiceImpl(CLLocaleDao dao) {
		super(dao);
	}

	@Override
	public List<Key<CLContinent>> getContinents() {
		return continentDao.findAllKeys();
	}

	@Override
	public Key<CLContinent> saveContinent(String name) {
		CLContinent rv = new CLContinent(name);
		continentDao.persist(rv);
		return rv.getKey();
	}

	@Override
	public List<Key<CLState>> getStates(Key<CLContinent> parent) {
		return stateDao.findKeysByCriteria(new AncestorCriterion<CLContinent>(parent));
	}

	@Override
	public Key<CLState> saveState(Key<CLContinent> parent, String name) {
		CLState rv = new CLState(parent, name);
		stateDao.persist(rv);
		return rv.getKey();
	}

	@Override
	public List<Key<CLLocale>> getLocales(Key<CLState> parent) {
		return getAllKeys(new AncestorCriterion<CLState>(parent));
	}

	@Override
	public Key<CLLocale> saveLocale(Key<CLState> parent, String name, String url) {
		return save(new CLLocale(parent, name, url)).getKey();
	}
	
	//@Override
	@SuppressWarnings("unchecked")
	public void populate() throws Exception {
		// sites
		//Map<String, Map<String, Map<String, String>>> sites = new HashMap<String, Map<String, Map<String, String>>>();

		final WebClient webClient = new WebClient();

		// Get the first page
		final HtmlPage cl = webClient.getPage("http://www.craigslist.org/about/sites");

		// final HtmlTextInput textField = form.getInputByName("userid");

		// Change the value of the text field
		// textField.setValueAttribute("root");

		// find continents
		for (HtmlElement continent : (List<HtmlElement>) cl
				.getByXPath("//h1[@class='continent_header']")) {
			// create continent
			Key<CLContinent> cont = saveContinent(continent.getTextContent());

			// find assoc countries
			for (HtmlElement country : (List<HtmlElement>) cl.getByXPath(continent.getNextSibling().getCanonicalXPath()
					+ "//div[@class='state_delimiter']")) {
				// create country map
				String state = country.getTextContent();
				if (state.isEmpty())
					state = "Other";

				// create state
				Key<CLState> st = saveState(cont, state);

				// find assoc locales
				for (HtmlElement locale : (List<HtmlElement>) cl.getByXPath(country.getNextSibling()
						.getCanonicalXPath() + "//a"))
					// create locale entry
					saveLocale(st, locale.getTextContent(), locale.getAttribute("href"));
			}
		}

		webClient.closeAllWindows();
	}
	
	@Inject
	private CLContinentDao continentDao;

	@Inject
	private CLStateDao stateDao;
}