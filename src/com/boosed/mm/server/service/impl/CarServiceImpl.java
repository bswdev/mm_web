package com.boosed.mm.server.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.beoui.geocell.GeocellManager;
import com.beoui.geocell.model.BoundingBox;
import com.beoui.geocell.model.CostFunction;
import com.beoui.geocell.model.Point;
import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.criterion.InCriterion;
import com.boosed.gae.server.criterion.RangeCriterion;
import com.boosed.gae.server.criterion.SortCriterion;
import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.CarDao;
import com.boosed.mm.server.service.AdService;
import com.boosed.mm.server.service.CarService;
import com.boosed.mm.server.service.LocationService;
import com.boosed.mm.server.service.MakeService;
import com.boosed.mm.server.service.MarkerService;
import com.boosed.mm.server.service.MessageService;
import com.boosed.mm.server.service.ModelService;
import com.boosed.mm.server.service.TaskService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Color;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.AdType;
import com.boosed.mm.shared.db.enums.ConditionType;
import com.boosed.mm.shared.db.enums.FieldCar;
import com.boosed.mm.shared.db.enums.MarkerType;
import com.boosed.mm.shared.exception.IncompleteException;
import com.boosed.mm.shared.exception.InvalidLocationException;
import com.boosed.mm.shared.exception.NoOpException;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.util.DataUtil;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

public class CarServiceImpl extends GenericServiceImpl<Car, CarDao> implements CarService,
		CostFunction {

	/** time after activation upon which certain fields become locked */
	private final int editTime;

	/** the number of cells desired to approximate a bounded region */
	private final int geocellMax;

	@Inject
	public CarServiceImpl(CarDao dao, @Named("editTime") int editTime,
			@Named("geocellMax") int geocellMax) {
		super(dao);
		this.editTime = editTime;
		this.geocellMax = geocellMax;
	}

	@Override
	public long activateAd(Car car, boolean active) throws NoOpException {
		// // check if this is redundant (don't let them pay twice)
		// if (car.isActive() && active)
		// // activating already activated ad
		// throw new NoOpException();
		// else if (!car.isActive() && !active)
		// // deactivating alraedy deactivated ad
		// throw new NoOpException();
		// else

		// car.time = activated ? System.currentTimeMillis() : null;

		// get the previous state
		boolean delta = car.time == null;
		
		// send the user an activation message
		boolean send = !car.isActive();
		
		// set activation of car (this just updates the time)
		car.setActive(active);

		// see if change occurred (XOR)
		delta ^= car.time == null;

		// no change, throw exception
		if (!delta)
			throw new NoOpException();

		// update the car
		save(car);

		// send the owner an activation message (mention change limits,
		// etc.), first time activation only
		if (send)
			messageService.createActivation(car);
		
		// update the counts
		if (active) {
			// makeService.increment(m.make, m.types);
			modelService.increment(car.model, 1);
			makeService.increment(car.make, car.types, 1);
		} else {
			// makeService.decrement(m.make, m.types);
			modelService.decrement(car.model, 1);
			makeService.decrement(car.make, car.types, 1);
		}

		// this throws an rpc exception when returning null
		return car.time == null ? 0L : car.time;
	}

	/**
	 * Override the default implementation which only deletes the car.
	 */
	@Override
	public void drop(Car... cars) {
		for (Car car : cars) {
			// remove the car from account
			// account.selling.remove(carKey);
			// accountService.save(account);

			// // retrieve car if not already retrieved
			// if (car == null)
			// car = get(carKey);

			// retrieve ad
			Ad ad = adService.get(car.ad);

			// delete photos associated with ad
			List<BlobKey> keys = new ArrayList<BlobKey>();

			// add approved keys
			for (String blob : ad.getImages(true).keySet())
				keys.add(new BlobKey(blob));

			// add unapproved keys
			for (String blob : ad.getImages(false).keySet())
				keys.add(new BlobKey(blob));

			// delete all blobs (images) associated with ad
			blobService.delete(keys.toArray(new BlobKey[0]));

			// delete the ad
			adService.drop(ad);

			// only decrement if car was active (otherwise it was never
			// accounted
			// for in the search statistics)
			if (car.isActive()) {

				// decrement model
				modelService.decrement(car.model, 1);

				// decrement types
				makeService.decrement(car.make, car.types, 1);
			}

			// delete any associated messages
			messageService.dropCar(car.getKey());

			// delete any associated markers
			markerService.dropCar(car.getKey());

			// delete the car
			super.drop(car);
		}
	}

	@Override
	public List<Car> getByGeocells(int offset, int limit, Location location, int distance,
			List<Criterion> criteria) {
		RangeCriterion rangeLat = locationService.getLatitudeCriterion(location.latitude, distance);
		RangeCriterion rangeLon = locationService.getLongitudeCriteiron(location.latitude,
				location.longitude, distance);
		BoundingBox bb = new BoundingBox((Double) rangeLat.hi, (Double) rangeLon.hi,
				(Double) rangeLat.lo, (Double) rangeLon.lo);
		List<String> cells = GeocellManager.bestBboxSearchCells(bb, /* null */this);
		criteria.add(new InCriterion<String>("geocells", cells));

		// location.geocells.get(GeocellManager.MAX_GEOCELL_RESOLUTION - 1)
		return dao.findAllByCriteria(offset, limit, criteria);
	}

	@Override
	@Deprecated
	public List<Car> getByLocation(Location location, int distance, List<Criterion> criteria) {

		// sort by latitude
		SortCriterion geo = new SortCriterion("latitude");
		criteria.add(geo);

		// this doesn't work or do anything
		// if (sort != SortType.NONE)
		// criteria.add(new SortCriterion(sort.toString()));

		// final sort by time
		criteria.add(new SortCriterion("-time"));

		// double lat = distance / 68.708;
		// double lon = distance / (69.171 *
		// Math.cos(Math.toRadians(Math.abs(location.latitude))));
		//
		// // discriminate latitude
		// double low = location.latitude - lat;
		// double high = location.latitude + lat;
		// log.info("latitude: " + low + " to " + high);

		// RangeCriterion range =
		// locationService.getLatitudeCriterion(location.latitude, distance);
		// new RangeCriterion("latitude", low, high);

		criteria.add(locationService.getLatitudeCriterion(location.latitude, distance));
		List<Car> rv = getAll(criteria);

		// // discriminate longitude
		// low = location.longitude - lon;
		// high = location.longitude + lon;
		// log.info("longitude: " + low + " to " + high);

		// modify the sort and range criteria
		// sort by longitude
		geo.property = "longitude";

		// new range
		// range.property = "longitude";
		// range.lo = low;
		// range.hi = high;

		// remove last criterion and add another range
		criteria.remove(criteria.size() - 1);
		criteria.add(locationService.getLongitudeCriteiron(location.latitude, location.longitude,
				distance));

		// this should be all vehicles within the given range (location)
		rv.retainAll(getAll(criteria));

		// sort cars to give a predictable ordering
		Collections.sort(rv);

		return rv;
	}

	@Override
	public void update(String accountId, Car car, Map<FieldCar, Serializable> fields)
			throws RemoteServiceFailureException {

		/** whether the user has performed a model change */
		Model model = null;

		/** whether user has provided coordinates for location */
		boolean locationExplicit = false;

		/** whether ad has exceeded permitted time for updates */
		boolean editable = !DataUtil.exceededTime(car, editTime);

		/** whether price alerts should be updated */
		boolean updatePrice = false;

		/** whether messages should be updated */
		boolean updateMessages = false;

		for (Entry<FieldCar, Serializable> entry : fields.entrySet()) {
			switch (entry.getKey()) {
			case CONDITION:
				// check if a primary condition has been set
				if (hasPrimaryCondition(car.conditions))
					// remove the first element
					car.conditions.remove(0);

				car.conditions.add(0, ConditionType.values()[(Integer) entry.getValue()]);
				break;
			case DOORS:
				car.doors = (Integer) entry.getValue();
				break;
			case DRIVETRAIN:
				car.drivetrain = new Key<Drivetrain>(Drivetrain.class, (String) entry.getValue());
				break;
			case ENGINE:
				car.engine = new Key<Engine>(Engine.class, (String) entry.getValue());
				break;
			case EXTERIOR:
				car.exterior = new Key<Color>(Color.class, (String) entry.getValue());
				break;
			case INTERIOR:
				car.interior = new Key<Color>(Color.class, (String) entry.getValue());
				break;
			case LATITUDE:
				// setting location explicitly
				locationExplicit = true;
				car.latitude = (Double) fields.get(FieldCar.LATITUDE);
				break;
			case LONGITUDE:
				// setting location explicitly
				locationExplicit = true;
				car.longitude = (Double) fields.get(FieldCar.LONGITUDE);
				break;
			case LOCATION:
				// get the postal code
				car.postal = DataUtil.getNullString((String) entry.getValue());
				if (car.postal == null)
					throw new InvalidLocationException();

				// retrieve the location
				Location location = locationService.get(car.postal);
				if (location == null)
					throw new InvalidLocationException();

				car.location = location.toString();

				if (!locationExplicit) {
					// just use lat/long/geocells of the location
					car.latitude = location.latitude;
					car.longitude = location.longitude;
					car.geocells = location.geocells;
				}

				break;
			case MILEAGE:
				car.mileage = (Integer) entry.getValue();
				car.mileages = DataUtil.getMileages(car.mileage);
				break;
			case MAKE:
				// if (exceededTime)
				// break;
				// cannot change after activation
				if (!car.isActive())
					car.make = new Key<Make>(Make.class, (String) entry.getValue());

				break;
			case MODEL:
				// if (exceededTime)
				// break;

				// capture old model
				// oldModel = car.model;

				// cannot change after activation
				if (!car.isActive()) {
					// set new model
					model = modelService.get((String) entry.getValue());
					updateMessages = true;

					// set model onto car
					car.model = model.getKey();
					car.types = model.types;
					car.types.add(new Key<Type>(Type.class, "any"));
				}

				break;
			case PRICE:
				Integer price = (Integer) entry.getValue();
				if (!price.equals(car.price)) {
					// price has changed, update
					car.price = price;
					car.prices = DataUtil.getPrices(price);
					updatePrice = true;
				}

				// update price only if car is active
				// updatePrice = true;// car.isActive();
				break;
			case STOCK:
				String stock = ((String) entry.getValue()).trim();
				if (!stock.equals(car.stock)) {
					// updated stock, update messages
					car.stock = stock;
					updateMessages = true;
				}

				break;
			case TRANSMISSION:
				car.transmission = new Key<Transmission>(Transmission.class, (String) entry
						.getValue());
				break;
			case TRIM:
				car.trim = DataUtil.getNullString((String) entry.getValue());
				break;
			case VIN:
				// cannot change after expiration (if also not null)
				if (car.vin == null || editable) {
					String vin = DataUtil.getNullString((String) entry.getValue());
					if (vin != null && !vin.equals(car.vin)) {
						// updated vin, update messages
						car.vin = vin;
						updateMessages = true;
					}
				}

				break;
			case YEAR:
				// cannot change after expiration
				if (editable) {

					// set the year
					car.year = (Integer) entry.getValue();

					// check if a primary condition has been set
					if (hasPrimaryCondition(car.conditions)) {
						// retrieve the initial condition (NEW, CERTIFIED, USED)
						ConditionType condition = car.conditions.get(0);

						// retain first condition
						// car.conditions.clear();
						car.conditions.retainAll(Arrays.asList(condition));

						// re-add original condition
						// car.conditions.add(condition);
					} else
						// clear out conditions
						car.conditions.clear();

					// add the year-based conditions
					car.conditions.addAll(DataUtil.getConditions(Calendar.getInstance().get(
							Calendar.YEAR)
							- car.year));
				}

				break;
			// case ID:
			case AD_TYPE:
				// only allow change if the ad has not been activated
				if (!car.isActive())
					car.adType = AdType.values()[(Integer) entry.getValue()];

				break;
			default:
				break;
			}
		}

		// annotate whether car location was set explicitly
		if (car.explicit = locationExplicit)
			// if lat/lon provided, set location explicitly
			car.geocells = GeocellManager.generateGeoCell(new Point(car.latitude, car.longitude));

		// account to save car against
		// Key<Account> accountKey = null;

		if (car.id == null) {
			// perform validation for any new entries
			if (car.postal == null || car.mileage == null || car.price == null)
				throw new IncompleteException();

			// add the owner of the vehicle
			Key<Account> account = new Key<Account>(Account.class, accountId);
			car.owner = account;

			// save car and generate an id
			save(car);

			// create a new ad
			Ad ad = new Ad(car);
			ad = adService.save(ad);
			
			// save the stringified key
			ad.key = ObjectifyService.factory().keyToString(ad.getKey());
			adService.save(ad);
			
			// associate ad with car
			car.ad = ad.getKey();

			// save car again with ad reference
			save(car);

			// add stock marker to account
			markerService.save(new Marker(account, car, model, MarkerType.STOCK));
		} else {
			// car is not new (but may be active or inactive)

			// save the car
			car = save(car);

			if (model != null) {
				// update the marker to new model (car is not active)
				// markerService.updateModel(car.id, model.toString());
				Marker marker = markerService.get(accountId, car.id, MarkerType.STOCK);
				marker.model = model.toString();
				markerService.save(marker);
			}

			if (updateMessages)
				// update associated messages
				taskService.updateMessages(car.id);
				//Queue q = QueueFactory.getDefaultQueue();
				//q.add(Builder.withUrl("/))
			    
			if (updatePrice)
				// update the price (car is active)
				taskService.updatePriceAlerts(car.id);
		}

		// // for new cars, add stock marker for account
		// if (accountKey != null) // {
		// // Account account = accountService.get(accountKey);
		// // account.selling.add(new Key<Car>(Car.class, car.id));
		// // accountService.save(account);
		// markerService.save(new Marker(accountKey, car.getKey(),
		// MarkerType.STOCK));
		// // }
	}

	@Override
	public double defaultCostFunction(int numCells, int resolution) {
		return numCells > geocellMax ? Double.MAX_VALUE : 0;
		// return numCells > Math.pow(GeocellUtils.GEOCELL_GRID_SIZE, 2) ?
		// Double.MAX_VALUE : 0;
	}

	private boolean hasPrimaryCondition(List<ConditionType> conditions) {
		if (conditions.isEmpty())
			return false;
		else
			// check first condition
			return conditions.get(0).isPrimary();
	}

	@Inject
	private AdService adService;

	@Inject
	private BlobstoreService blobService;

	@Inject
	private LocationService locationService;

	// @Inject
	// private PriceService priceService;

	@Inject
	private MakeService makeService;

	@Inject
	private MarkerService markerService;

	@Inject
	private MessageService messageService;

	@Inject
	private ModelService modelService;

	@Inject
	private TaskService taskService;
	// @Inject
	// private TypeService typeService;
}