package com.boosed.mm.server.service.impl;

import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.ColorDao;
import com.boosed.mm.server.service.ColorService;
import com.boosed.mm.shared.db.Color;
import com.google.inject.Inject;

public class ColorServiceImpl extends GenericServiceImpl<Color, ColorDao> implements ColorService {

	@Inject
	public ColorServiceImpl(ColorDao dao) {
		super(dao);
	}
}