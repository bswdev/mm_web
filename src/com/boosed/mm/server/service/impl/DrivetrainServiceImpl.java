package com.boosed.mm.server.service.impl;

import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.DrivetrainDao;
import com.boosed.mm.server.service.DrivetrainService;
import com.boosed.mm.shared.db.Drivetrain;
import com.google.inject.Inject;

public class DrivetrainServiceImpl extends GenericServiceImpl<Drivetrain, DrivetrainDao> implements DrivetrainService {
	
	@Inject
	public DrivetrainServiceImpl(DrivetrainDao dao) {
		super(dao);
	}
}