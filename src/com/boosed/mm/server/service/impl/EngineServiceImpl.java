package com.boosed.mm.server.service.impl;

import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.EngineDao;
import com.boosed.mm.server.service.EngineService;
import com.boosed.mm.shared.db.Engine;
import com.google.inject.Inject;

public class EngineServiceImpl extends GenericServiceImpl<Engine, EngineDao> implements EngineService {

	@Inject
	public EngineServiceImpl(EngineDao dao) {
		super(dao);
	}
}