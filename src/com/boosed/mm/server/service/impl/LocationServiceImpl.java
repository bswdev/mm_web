package com.boosed.mm.server.service.impl;

import java.util.List;

import com.boosed.gae.server.criterion.RangeCriterion;
import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.LocationDao;
import com.boosed.mm.server.service.LocationService;
import com.boosed.mm.shared.db.Location;
import com.google.inject.Inject;
import com.googlecode.objectify.Key;

public class LocationServiceImpl extends GenericServiceImpl<Location, LocationDao> implements LocationService {

	@Inject
	public LocationServiceImpl(LocationDao dao) {
		super(dao);
	}

	@Deprecated
	@Override
	public List<Location> getByDistance(double latitude, double longitude, int distance) {

		// double lat = distance / 68.708;
		// double lon = distance / (69.171 *
		// Math.cos(Math.toRadians(Math.abs(latitude))));

		// List<Criterion> criteria = new ArrayList<Criterion>();

		// double lo = latitude - lat;
		// double hi = latitude + lat;
		// log.info("latitude: " + lo + " to " + hi);

		// criteria.add(new RangeCriterion("latitude", lo, hi));
		List<Location> locations = dao.findAllByCriteria(/*
													 * new
													 * RangeCriterion("latitude"
													 * , lo, hi)
													 */getLatitudeCriterion(latitude, distance));

		// lo = longitude - lon;
		// hi = longitude + lon;
		// log.info("longitude: " + lo + " to " + hi);

		// criteria.clear();
		// criteria.add(new RangeCriterion("longitude", lo, hi));

		// make Google do the work!
		locations.retainAll(dao.findAllByCriteria(getLongitudeCriteiron(latitude, longitude, distance)/*
																									 * new
																									 * RangeCriterion
																									 * (
																									 * "longitude"
																									 * ,
																									 * lo
																									 * ,
																									 * hi
																									 * )
																									 */));

		// for (Iterator<Location> it = locations.iterator(); it.hasNext();) {
		// double loc = it.next().longitude;
		// if (loc > hi || loc < lo)
		// it.remove();
		// }

		// return abs(lt1-lt2) &lt;= miles/68.708 and
		// abs(lg1-lg2) &lt;= miles/(69.171 *
		// cos(radians(greatest(abs(lt1),abs(lt2)))))
		return locations;
	}

	@Deprecated
	@Override
	public List<Key<Location>> getKeysByDistance(double latitude, double longitude, int distance) {

		// double lat = distance / 68.708;
		// // List<Criterion> criteria = new ArrayList<Criterion>();
		//
		// double lo = latitude - lat;
		// double hi = latitude + lat;
		// log.info("latitude: " + lo + " to " + hi);

		// criteria.add(new RangeCriterion("latitude", lo, hi));
		List<Key<Location>> locations = dao.findKeysByCriteria(getLatitudeCriterion(latitude, distance)/*
																										 * new
																										 * RangeCriterion
																										 * (
																										 * "latitude"
																										 * ,
																										 * lo
																										 * ,
																										 * hi
																										 * )
																										 */);

		// double lon = distance / (69.171 *
		// Math.cos(Math.toRadians(Math.abs(latitude))));
		//
		// lo = longitude - lon;
		// hi = longitude + lon;
		// log.info("longitude: " + lo + " to " + hi);

		// criteria.clear();
		// criteria.add(new RangeCriterion("longitude", lo, hi));

		locations.retainAll(dao.findKeysByCriteria(getLongitudeCriteiron(latitude, longitude, distance)/*
																										 * new
																										 * RangeCriterion
																										 * (
																										 * "longitude"
																										 * ,
																										 * lo
																										 * ,
																										 * hi
																										 * )
																										 */));

		return locations;
	}
	
	/**
	 * Get a <code>RangeCriterion</code> for a given latitude and distance.
	 * 
	 * @param latitude
	 * @param distance
	 * @return
	 */
	@Override
	public RangeCriterion getLatitudeCriterion(double latitude, int distance) {
		double lat = distance / 68.708;
		return new RangeCriterion("latitude", latitude - lat, latitude + lat);
	}

	/**
	 * Get a <code>RangeCriterion</code> for a given longitude and distance.
	 * 
	 * @param latitude
	 * @param longitude
	 * @param distance
	 * @return
	 */
	@Override
	public RangeCriterion getLongitudeCriteiron(double latitude, double longitude, int distance) {
		double lon = distance / (69.171 * Math.cos(Math.toRadians(Math.abs(latitude))));
		return new RangeCriterion("longitude", longitude - lon, longitude + lon);
	}
}