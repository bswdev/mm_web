package com.boosed.mm.server.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.criterion.InCriterion;
import com.boosed.gae.server.criterion.NotZeroCriterion;
import com.boosed.gae.server.dao.GenericDao;
import com.boosed.gae.server.dao.Transactable;
import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.MakeDao;
import com.boosed.mm.server.service.MakeService;
import com.boosed.mm.server.service.ModelService;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Type;
import com.google.inject.Inject;
import com.googlecode.objectify.Key;

public class MakeServiceImpl extends GenericServiceImpl<Make, MakeDao> implements MakeService {

	@Inject
	private ModelService modelService;

	@Inject
	public MakeServiceImpl(MakeDao dao) {
		super(dao);
	}

	@Override
	public List<Make> getAll(boolean filterEmpty) {
		if (filterEmpty)
			// filter out the empty results
			return dao.findAllByCriteria(new NotZeroCriterion("total"));
		else
			// return all results
			return dao.findAll();
	}

	@Override
	public List<Make> getByType(Key<Type> type, boolean filterEmpty) {
		List<Make> rv = getAll(filterEmpty);

		// remove all the makes that do not have the desired type
		for (Iterator<Make> it = rv.iterator(); it.hasNext();)
			if (!it.next().totals.containsKey(type))
				it.remove();

		return rv;
	}

	@Deprecated
	@Override
	public List<Make> getByTypes(List<Key<Type>> types, boolean filterEmpty) {
		// get the models with the given types (filter empty models out)
		List<Criterion> criteria = new ArrayList<Criterion>();

		// filter types
		criteria.add(new InCriterion<Key<Type>>("types", types));

		// filter empty
		if (filterEmpty)
			criteria.add(new NotZeroCriterion("total"));

		// retrieve all models with given type and total > 0
		List<Model> models = modelService.getAll(criteria);
		// modelService.getByTypesAndMakes(types, new ArrayList<Key<Make>>(),
		// filterEmpty);

		// get the makes of these available models, keep tallies for makes
		// Set<Key<Make>> makes = new HashSet<Key<Make>>();
		Map<Key<Make>, Integer> totals = new HashMap<Key<Make>, Integer>();
		for (Model model : models) {
			if (totals.containsKey(model.make))
				// add to the existing total
				totals.put(model.make, model.total + totals.get(model.make));
			else
				// create a new entry with model's total
				totals.put(model.make, model.total);
		}

		// sort & count makes
		List<Make> rv = Arrays.asList(get(totals.keySet()).values().toArray(new Make[0]));
		Collections.sort(rv);
		for (Make make : rv)
			make.total = totals.get(make.getKey());

		return rv;
	}

	// @Override
	// public void decrement(final Key<Make> make, final Set<Key<Type>> types) {
	// Transactable<Make> task = new Transactable<Make>() {
	// @Override
	// public void run(GenericDao<Make> dao) {
	// Make m = dao.findByKey(make);
	// for (Key<Type> type : types) {
	// String name = type.getName();
	// if (name.equals("convertible"))
	// m.convertible--;
	// else if (name.equals("coupe"))
	// m.coupe--;
	// else if (name.equals("crossover"))
	// m.crossover--;
	// else if (name.equals("diesel"))
	// m.diesel--;
	// else if (name.equals("hatchback"))
	// m.hatchback--;
	// else if (name.equals("hybrid"))
	// m.hybrid--;
	// else if (name.equals("luxury"))
	// m.luxury--;
	// else if (name.equals("sedan"))
	// m.sedan--;
	// else if (name.equals("suv"))
	// m.suv--;
	// else if (name.equals("truck"))
	// m.truck--;
	// else if (name.equals("van"))
	// m.van--;
	// else if (name.equals("wagon"))
	// m.wagon--;
	// }
	// //m.total--;
	// dao.persist(m);
	// }
	// };
	//		
	// // repeat this transaction until it completes
	// dao.repeatInTransaction(task);
	// }

	// @Override
	// public void increment(final Key<Make> make, final Set<Key<Type>> types) {
	// Transactable<Make> task = new Transactable<Make>() {
	// @Override
	// public void run(GenericDao<Make> dao) {
	// Make m = dao.findByKey(make);
	// for (Key<Type> type : types) {
	// String name = type.getName();
	// if (name.equals("convertible"))
	// m.convertible++;
	// else if (name.equals("coupe"))
	// m.coupe++;
	// else if (name.equals("crossover"))
	// m.crossover++;
	// else if (name.equals("diesel"))
	// m.diesel++;
	// else if (name.equals("hatchback"))
	// m.hatchback++;
	// else if (name.equals("hybrid"))
	// m.hybrid++;
	// else if (name.equals("luxury"))
	// m.luxury++;
	// else if (name.equals("sedan"))
	// m.sedan++;
	// else if (name.equals("suv"))
	// m.suv++;
	// else if (name.equals("truck"))
	// m.truck++;
	// else if (name.equals("van"))
	// m.van++;
	// else if (name.equals("wagon"))
	// m.wagon++;
	// }
	// //m.total++;
	// dao.persist(m);
	// }
	// };
	//		
	// // repeat this transaction until it completes
	// dao.repeatInTransaction(task);
	// }

	@Override
	public void decrement(final Key<Make> make, Set<Key<Type>> types, final int count) {
		// repeat for each type
		for (final Key<Type> type : types)
			if (!type.getName().equals("any"))
				// repeat this transaction until it completes
				dao.repeatInTransaction(new Transactable<Make>() {
					
					// store count to closure
					private int cnt = count;
					
					@Override
					public void run(GenericDao<Make> dao) {
						Make m = dao.findByKey(make);
						
						// if total is 0, remove from map
						int total = m.totals.get(type) - cnt;
						if (total <= 0)
							// remove the type from map
							m.totals.remove(type);
						else
							// enter in the new total
							m.totals.put(type, total);
						
						dao.persist(m);
					}
				});

		// once for total makes
		dao.repeatInTransaction(new Transactable<Make>() {
			
			// store count to closure
			private int cnt = count;
			
			@Override
			public void run(GenericDao<Make> dao) {
				Make m = dao.findByKey(make);
				m.total -= cnt;
				dao.persist(m);
			}
		});
	}

	@Override
	public void increment(final Key<Make> make, Set<Key<Type>> types, final int count) {
		for (final Key<Type> type : types)
			if (!type.getName().equals("any"))
				// repeat this transaction until it completes
				dao.repeatInTransaction(new Transactable<Make>() {
					
					// store count to closure
					private int cnt = count;
					
					@Override
					public void run(GenericDao<Make> dao) {
						Make m = dao.findByKey(make);

						// determine if type is already being tallied
						Integer total = m.totals.get(type);
						m.totals.put(type, total == null ? cnt : (total + cnt));
						
						dao.persist(m);
					}
				});

		// once for total makes
		dao.repeatInTransaction(new Transactable<Make>() {
			
			// store count to closure
			private int cnt = count;
			
			@Override
			public void run(GenericDao<Make> dao) {
				Make m = dao.findByKey(make);
				m.total += cnt;
				dao.persist(m);
			}
		});
	}
}