package com.boosed.mm.server.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.boosed.gae.server.criterion.AncestorCriterion;
import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.criterion.EQCriterion;
import com.boosed.gae.server.criterion.SortCriterion;
import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.MarkerDao;
import com.boosed.mm.server.service.MarkerService;
import com.boosed.mm.server.service.MessageService;
import com.boosed.mm.server.service.ModelService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.enums.MarkerType;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.boosed.mm.shared.exception.ResourceLimitException;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.googlecode.objectify.Key;

public class MarkerServiceImpl extends GenericServiceImpl<Marker, MarkerDao> implements
		MarkerService {

	private final int alertMax, bookmarkMax;

	@Inject
	public MarkerServiceImpl(MarkerDao dao, @Named("alertMax") int alertMax,
			@Named("bookmarkMax") int bookmarkMax) {
		super(dao);
		this.alertMax = alertMax;
		this.bookmarkMax = bookmarkMax;
	}

	@Override
	public void addBookmark(String accountId, Car car) throws RemoteServiceFailureException {
		// check if car exists

		// retrieve bookmark for this car
		Marker marker = dao.findByCriteria(createCriteria(accountId, car.id, MarkerType.BOOKMARK));

		if (marker == null) {
			// bookmark doesn't exist

			if (count(createCriteria(accountId, null, MarkerType.BOOKMARK)) >= bookmarkMax)
				// exceeded maximum permitted
				throw new ResourceLimitException();

			// create a new bookmark
			save(new Marker(new Key<Account>(Account.class, accountId), car, modelService
					.get(car.model), MarkerType.BOOKMARK));
		}
		// else user already has a bookmark, do nothing
	}

	@Override
	public void removeBookmark(String accountId, long carId) throws RemoteServiceFailureException {
		// retrieve marker for this car
		Marker marker = dao.findByCriteria(createCriteria(accountId, carId, MarkerType.BOOKMARK));

		if (marker != null)
			// remove the bookmark
			drop(marker);
	}

	@Override
	public void createAlert(String accountId, Car car, int point) throws ResourceLimitException {
		// check the bookmark limits
		List<Criterion> criteria = createCriteria(accountId, null, MarkerType.PRICE);

		// add criteria for account
		// criteria.add(new EQCriterion("account", new
		// Key<Account>(Account.class, accountId)));
		// criteria.add(new EQCriterion("type", MarkerType.BOOKMARK));

		if (count(criteria) >= alertMax)
			// exceeded maximum permitted
			throw new ResourceLimitException();
		else {
			// save a new alert
			Marker marker = new Marker(new Key<Account>(Account.class, accountId), car,
					modelService.get(car.model), MarkerType.PRICE);
			marker.point = point;
			// marker.price = car.price;
			save(marker);
		}
	}

	@Override
	public void dropCar(Key<Car> car) {
		dao.deleteByKeys(dao.findKeysByCriteria(new AncestorCriterion<Car>(car)));
	}

	@Override
	public Marker get(String accountId, long carId, MarkerType type) {
		return get(createCriteria(accountId, carId, type));
	}

	@Override
	public void updateModel(Long carId, String modelName) {
		// change all markers associated with car as well
		List<Marker> markers = getAll(createCriteria(null, carId, null));
		
		for (Marker marker : markers)
			// change the marker model
			marker.model = modelName;
		
		// save markers
		save(markers.toArray(new Marker[0]));	
	}
	
	@Override
	public void updatePrice(Car car) {
		// get the current time once
		long time = System.currentTimeMillis();

		// set up criteria
		//List<Criterion> criteria = createCriteria(null, car.id, null);

		// only PRICE markers are ever active
		//criteria.add(new EQCriterion("active", true));

		// new ArrayList<Criterion>();
		// // car is ancestor
		// criteria.add(new AncestorCriterion<Car>(car.getKey()));
		// // marker is active
		// criteria.add(new EQCriterion("active", true));
		// // sort by latest
		// criteria.add(new SortCriterion("-time"));

		// get all markers
		for (Marker marker : getAll(createCriteria(null, car.id, null))) {
			switch (marker.type) {
			case PRICE:
				// update price marker according to the car's new pricing
				
				if (!marker.active)
					// inactive price marker, do nothing
					break;

				// update active alert
				
				// set the current time
				marker.time = time;

				// set the change
				marker.delta = car.price - marker.price;

				// set the new price
				marker.price = car.price;

				// check if point is met
				if (marker.point >= marker.price) {
					// send user an alert that price is met
					messageService.confirmPrice(marker, car);

					// "deactivate" the alert
					marker.active = false;
				}

				// save the alert
				save(marker);
				break;
			case BOOKMARK:
			case STOCK:
				// set the new price
				marker.price = car.price;
				
				// save the alert
				save(marker);
				break;
			default:
				break;
			}

			// // set the current time
			// marker.time = time;
			//
			// // set the change
			// marker.delta = car.price - marker.price;
			//
			// // set the new price
			// marker.price = car.price;
			//
			// // check if point is met
			// if (marker.point >= marker.price) {
			// // send user an alert that price is met
			// messageService.confirmPrice(marker, car);
			//
			// // "deactivate" the alert
			// marker.active = false;
			// }

//			// save the alert
//			save(marker);
		}

//		// update all other markers; remove active criteria
//		criteria.remove(1);
//
//		for (Marker marker : getAll(criteria))
//			// modify all markers except price (both active and inactive)
//			if (marker.type != MarkerType.PRICE) {
//				marker.price = car.price;
//				save(marker);
//			}
	}

	private List<Criterion> createCriteria(String accountId, Long carId, MarkerType type) {
		List<Criterion> criteria = new ArrayList<Criterion>();

		if (accountId != null)
			criteria.add(new EQCriterion("account", new Key<Account>(Account.class, accountId)));

		if (carId != null)
			criteria.add(new AncestorCriterion<Car>(new Key<Car>(Car.class, carId)));

		if (type != null)
			criteria.add(new EQCriterion("type", type));

		// add sort criterion?
		criteria.add(new SortCriterion("-time"));

		return criteria;
	}

	@Inject
	private MessageService messageService;

	@Inject
	private ModelService modelService;
}