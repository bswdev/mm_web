package com.boosed.mm.server.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.boosed.gae.server.criterion.EQCriterion;
import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.MessageDao;
import com.boosed.mm.server.service.AccountService;
import com.boosed.mm.server.service.MessageService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.Marker;
import com.boosed.mm.shared.db.Message;
import com.boosed.mm.shared.db.enums.MessageState;
import com.boosed.mm.shared.db.enums.MessageType;
import com.boosed.mm.shared.exception.InvalidRecipientException;
import com.boosed.mm.shared.util.DataUtil;
import com.google.appengine.api.mail.MailService;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.googlecode.objectify.Key;

public class MessageServiceImpl extends GenericServiceImpl<Message, MessageDao> implements MessageService {

	/** types of messages */
	private final String activation, priceAlert, rejectAd, rejectImage, truncateAd;

	/** email addresses for notifications */
	private final String from, domain;

	// private static SimpleDateFormat FMT_DATE = new
	// SimpleDateFormat("M/d/yy, h:mm aa Z");

	@Inject
	public MessageServiceImpl(MessageDao dao, @Named("activation") String activation,
			@Named("priceAlert") String priceAlert, @Named("rejectAd") String rejectAd,
			@Named("rejectImage") String rejectImage, @Named("truncateAd") String truncateAd,
			@Named("from") String from, @Named("domain") String domain) {
		super(dao);
		this.activation = activation;
		this.priceAlert = priceAlert;
		this.rejectAd = rejectAd;
		this.rejectImage = rejectImage;
		this.truncateAd = truncateAd;
		this.from = from;
		this.domain = domain;
	}

	@Override
	public void createActivation(Car car) {
		update(new Message(car.owner, car, activation));
	}

	@Override
	public void createInquiry(Account sender, Car car, String message) {
		update(new Message(sender, car, message));
	}

	@Override
	public void createReply(String sender, Message original, String reply) throws InvalidRecipientException {
		if (original == null || original.sender == null)
			throw new InvalidRecipientException();

		// delete message to which original was in response
		if (original.responseTo != null)
			drop(original.responseTo);

		// mark original as having been replied to
		original.setState(MessageState.REPLIED, true);
		save(original);

		// need to get account to ensure we get the latest name available
		Account account = accountService.get(sender);
		Message message = new Message(account, original);

		// set the message
		// message.message = reply + "\n\n" + original.senderName
		// + (original.getType() == MessageType.INQUIRIES ? " inquired (" :
		// " responded (")
		// + FMT_DATE.format(new Date(original.time)) + "):" + "\n" +
		// original.message;
		message.addMessage(reply, account.getKey());

		// save reply to original message
		update(message);
	}

	@Override
	public void dropCar(Key<Car> reference) {
		// get all messages with the given reference
		List<Message> messages = dao.findAllByCriteria(new EQCriterion("reference", reference));

		// map of affected accounts to decrement value
		Map<Key<Account>, Integer> decrements = new HashMap<Key<Account>, Integer>();

		// adjust the number of unread messages for the account
		for (Message message : messages)
			if (!message.getState(MessageState.READ))
				// message is unread, decrement the count for recipient
				if (decrements.containsKey(message.recipient))
					// increase decrement
					decrements.put(message.recipient, 1 + decrements.get(message.recipient));
				else
					// add new account
					decrements.put(message.recipient, 1);

		// decrement all the accounts
		for (Entry<Key<Account>, Integer> entry : decrements.entrySet())
			accountService.decrement(entry.getKey(), entry.getValue());

		// delete all messages associated to reference
		// dao.delete(messages.toArray(new Message[0]));
		dao.deleteByKeys(DataUtil.getKeys(messages));
	}

	// @Override
	// public void notifyAdmins(String user, String subject, String message) {
	// // com.google.appengine.api.mail.MailService.Message msg = new
	// // com.google.appengine.api.mail.MailService.Message();
	// // msg.setSender(from);
	// // msg.setSubject(subject);
	// // msg.setHtmlBody(message);
	// //
	// // try {
	// // // send the message
	// // mailService.sendToAdmins(msg);
	// // } catch (Exception e) {
	// // log.warning("could not send message: " + e.getClass());
	// // }
	// }

	@Override
	public void confirmPrice(Marker alert, Car car) {
		StringBuilder sb = new StringBuilder(priceAlert);
		sb.append("price point: ").append('$').append(alert.point).append("\n");
		sb.append("car price: ").append('$').append(alert.price).append("\n");
		sb.append("trend: ").append(alert.delta);

		// create notification
		Message message = new Message(alert.account, car, sb.toString());

		// add message to indicate creation date (this is a bit hacky)
		message.messages.put(alert.created, Message.PRE_SND + "Price alert was created.");

		update(message);
	}

	@Override
	public void rejectAd(Key<Account> owner, Car car) {
		update(new Message(owner, car, rejectAd));
	}

	@Override
	public void rejectImages(Key<Account> owner, Car car) {
		update(new Message(owner, car, /* "image(s) was/were rejected" */rejectImage));
	}

	@Override
	public void truncateAd(Key<Account> owner, Car car) {
		update(new Message(owner, car, truncateAd));
	}

	@Override
	public Message update(Message t) {
		// update the message count (new messages)
		accountService.increment(t.recipient, 1);

		// check if recipient elects to be notified
		Account account = accountService.get(t.recipient);
		MessageType type = t.getType();

		if (account.getNotify(type))
			try {
				// send an email message to user
				StringBuilder content = new StringBuilder(t.getReference());
				content.append("\n").append(t.identifier);
				content.append("\n\n").append(t.senderName).append(" wrote:\n");
				// this only prints the first message
				content.append(t.messages.values().toArray(new String[0])[0].substring(1));
				content.append("\n--- end message ---\n\n");
				content.append("You can manage your messages and notification preferences through the mobimotos client installed on your android device. Thanks for using mobimotos!");

				MailService.Message email = new MailService.Message(from, account.email, type.toString().toLowerCase()
						+ " notification", account.name + ",\nYou have received a new message.\n\n" + content);
				mailService.send(email);
			} catch (IOException e) {
				log.warning("could not send email to: " + account.email);
			}

		// save message
		return save(t);
	}

	@Override
	public void updateIdentifiers(Car car) {
		// get all messages with the given reference
		List<Message> messages = dao.findAllByCriteria(new EQCriterion("reference", car.getKey()));
		
		// no messages to modify
		if (messages.isEmpty())
			return;
		
		// modify identifiers (stock & vin)
		StringBuilder sb = new StringBuilder();
		sb.append("st: ").append(car.stock.equals("") ? "n/a" : car.stock);
		sb.append(", vin: ").append(car.vin == null ? "n/a" : car.vin);
		String identifier = sb.toString();
		
		// modify the reference name
		sb = new StringBuilder();
		sb.append(car.year).append(" ").append(car.model.getName());
		String referenceName = sb.toString();
		
		for (Message message : messages) {
			// set new identifier and reference name
			message.identifier = identifier;
			message.referenceName = referenceName;
		}
		
		// save
		save(messages.toArray(new Message[0]));
	}
	
	@Override
	public void validateLocation(Location location) {
		MailService.Message email = new MailService.Message();
		email.setSender("validate@" + domain);
		email.setSubject("validate location");

		// when user replies, needs to add lat,lon coordinates
		email.setTextBody(location.postal + "," + location.city + "," + location.state.getName() + ","
				+ location.latitude + "," + location.longitude);

		try {
			mailService.sendToAdmins(email);
		} catch (IOException e) {
			log.warning("could not send location validation email");
		}
	}

	@Inject
	private AccountService accountService;

	@Inject
	private MailService mailService;
}