package com.boosed.mm.server.service.impl;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.boosed.gae.server.criterion.Criterion;
import com.boosed.gae.server.criterion.EQCriterion;
import com.boosed.gae.server.dao.GenericDao;
import com.boosed.gae.server.dao.Transactable;
import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.ModelDao;
import com.boosed.mm.server.service.CarService;
import com.boosed.mm.server.service.MakeService;
import com.boosed.mm.server.service.MarkerService;
import com.boosed.mm.server.service.MessageService;
import com.boosed.mm.server.service.ModelService;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.Drivetrain;
import com.boosed.mm.shared.db.Engine;
import com.boosed.mm.shared.db.Make;
import com.boosed.mm.shared.db.Model;
import com.boosed.mm.shared.db.Transmission;
import com.boosed.mm.shared.db.Type;
import com.boosed.mm.shared.db.enums.FieldModel;
import com.google.inject.Inject;
import com.googlecode.objectify.Key;

public class ModelServiceImpl extends GenericServiceImpl<Model, ModelDao> implements ModelService {

	@Inject
	public ModelServiceImpl(ModelDao dao) {
		super(dao);
	}

	// @Override
	// public List<Model> getByMakes(List<Key<Make>> makes, boolean filterEmpty)
	// {
	// return dao.findByMakes(makes, filterEmpty);
	// }
	//
	// @Override
	// public List<Model> getByTypes(List<Key<Type>> types, boolean filterEmpty)
	// {
	// return dao.findByTypes(types, filterEmpty);
	// }

	@Override
	public List<Model> getByTypeAndMake(Key<Type> type, Key<Make> make, boolean filterEmpty) {
		return dao.findByTypeAndMake(type, make, filterEmpty);
	}

	@Override
	public List<Model> getByTypesAndMakes(List<Key<Type>> types, List<Key<Make>> makes, boolean filterEmpty) {
		return dao.findByTypesAndMakes(types, makes, filterEmpty);
	}

	// @Override
	// public List<Key<Model>> getKeysByMakes(List<Key<Make>> makes, boolean
	// filterEmpty) {
	// return dao.findKeysByMakes(makes, filterEmpty);
	// }

	// @Override
	// public List<Key<Model>> getKeysByTypes(List<Key<Type>> types, boolean
	// filterEmpty) {
	// return dao.findKeysByTypes(types, filterEmpty);
	// }

	@Override
	public List<Key<Model>> getKeysByTypesAndMakes(List<Key<Type>> types, List<Key<Make>> makes, boolean filterEmpty) {
		return dao.findKeysByTypesAndMakes(types, makes, filterEmpty);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void update(Model model, Map<FieldModel, Serializable> fields) {
		for (Entry<FieldModel, Serializable> entry : fields.entrySet()) {
			switch (entry.getKey()) {
			case MAKE:
				model.make = new Key<Make>(Make.class, (String) entry.getValue());
				break;
			case TYPES:
				// Set<Key<Type>> s = new HashSet<Key<Type>>((List<Key<Type>>)
				// entry.getValue());
				model.types = (Set<Key<Type>>) entry.getValue();
				break;
			case YEARS:
				model.years = (List<Integer>) entry.getValue();
				Collections.sort(model.years);
				break;
			case DOORS:
				model.doors = (List<Integer>) entry.getValue();
				Collections.sort(model.doors);
				break;
			case DRIVETRAINS:
				model.drivetrains = (List<Key<Drivetrain>>) entry.getValue();
				Collections.sort(model.drivetrains);
				break;
			case ENGINES:
				model.engines = (List<Key<Engine>>) entry.getValue();
				Collections.sort(model.engines);
				break;
			case TRANSMISSIONS:
				model.transmissions = (List<Key<Transmission>>) entry.getValue();
				Collections.sort(model.transmissions);
				break;
			default:
				break;
			}
		}

		save(model);
	}

	@Override
	public void decrement(final Key<Model> model, final int count) {
		Transactable<Model> task = new Transactable<Model>() {

			// store variable to closure
			private int cnt = count;

			@Override
			public void run(GenericDao<Model> dao) {
				Model m = dao.findByKey(model);
				m.total -= cnt;
				dao.persist(m);
			}
		};

		// repeat this transaction until it completes
		dao.repeatInTransaction(task);
	}

	@Override
	public void increment(final Key<Model> model, final int count) {
		Transactable<Model> task = new Transactable<Model>() {

			// store variable to closure
			private int cnt = count;

			@Override
			public void run(GenericDao<Model> dao) {
				Model m = dao.findByKey(model);
				m.total += cnt;
				dao.persist(m);
			}
		};

		// repeat this transaction until it completes
		dao.repeatInTransaction(task);
	}

	@Override
	public void rename(String modelId, String make, String model) {
		// retrieve existing model
		Model mdl = get(modelId);

		// old make
		Key<Make> oldMake = mdl.make;

		// get the current model's key and create eq criterion
		Criterion criterion = new EQCriterion("model", mdl.getKey());

		// delete the model
		drop(mdl);

		// re-key the make if also changing the associated make
		mdl.make = new Key<Make>(Make.class, make);

		// change the model "id"
		mdl.desc = mdl.make.getName() + " " + model;

		// save as the new model
		mdl = save(mdl);

		// get the new key and new name
		Key<Model> key = mdl.getKey();
		String name = mdl.toString();

		// retrieve all cars with matching old model and change the name
		List<Car> cars = carService.getAll(criterion);

		// if switching over makes, be sure to decrement the old make and
		// increment the new make accordingly
		if (!mdl.make.equals(oldMake)) {
			int count = cars.size();
			makeService.decrement(oldMake, mdl.types, count);
			makeService.increment(mdl.make, mdl.types, count);
		}
		
		for (Car car : cars) {
			// set new model key
			car.model = key;

			// save car
			car = carService.save(car);

			// update model name on all markers
			markerService.updateModel(car.id, name);

			// change all the message references
			messageService.updateIdentifiers(car);
		}
	}

	@Inject
	private CarService carService;

	@Inject
	private MakeService makeService;

	@Inject
	private MarkerService markerService;

	@Inject
	private MessageService messageService;
}