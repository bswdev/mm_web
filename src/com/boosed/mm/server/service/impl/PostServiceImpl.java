package com.boosed.mm.server.service.impl;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;

import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.CarDao;
import com.boosed.mm.server.service.AccountService;
import com.boosed.mm.server.service.AdService;
import com.boosed.mm.server.service.CarService;
import com.boosed.mm.server.service.PostService;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Car;
import com.boosed.mm.shared.db.enums.AdNetwork;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlFileInput;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlRadioButtonInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.google.inject.Inject;

public class PostServiceImpl extends GenericServiceImpl<Car, CarDao> implements PostService {

	@Inject
	public PostServiceImpl(CarDao dao) {
		super(dao);
	}

	@Override
	public void post(long carId, String network) {
		Car car = dao.findById(carId);

		// if ("craigslist".equals(network))
		postCg(car, network);
	}

	@SuppressWarnings("unchecked")
	private void postCg(Car car, String network) {
		try {
			final WebClient client = new WebClient();
			client.setActiveXNative(false);
			client.setJavaScriptEnabled(false);
			// request.getMimeHeaders().removeHeader("Content-Length");
			// webClient.removeRequestHeader("Content-Length");
			// webClient.removeRequestHeader("Content-Encoding");

			// get the link for posting
			final HtmlPage post = client.getPage(network);
			HtmlElement element = ((List<HtmlElement>) post.getByXPath("//ul[@id='postlks']//a")).get(0);

			// https://post.craigslist.org/c/sat?lang=en
			// to https://post.craigslist.org/c/sat/S?lang=en
			String url = element.getAttribute("href");
			url = url.substring(0, url.indexOf('?')) + "/S?lang=en";

			// Get the first page
			HtmlPage page = client.getPage(url);

			// Get the form that we are dealing with and within that form,
			// find the submit button and the field that we want to change.
			HtmlForm form = page.getForms().get(0);

			// // get div which has a 'name' attribute of 'John'
			// final HtmlRadioButtonInput byOwner = (HtmlRadioButtonInput)
			// page1.getByXPath("//input[@value='145']")
			// .get(0);
			// final HtmlRadioButtonInput byDealer = (HtmlRadioButtonInput)
			// page1.getByXPath("//input[@value='146']").get(
			// 0);
			Account account = accountService.get(car.owner);

			// set by owner checked
			// byOwner.setChecked(true);
			try {
				if (account.dealer)
					// dealer click
					((HtmlRadioButtonInput) page.getByXPath("//input[@value='146']").get(0)).setChecked(true);
				// .click();
				else
					// owner click
					((HtmlRadioButtonInput) page.getByXPath("//input[@value='145']").get(0)).setChecked(true);
				// .click();

				// submit click
				form.getButtonByName("go").click();
			} catch (Throwable t) {
				// log.severe("caught!!!");
			}

			log.severe("the ad is at: " + form.getActionAttribute());

			url = form.getActionAttribute();

			// work through additional dialogs
			page = client.getPage(url);
			form = page.getForms().get(0);
			if (!"postingForm".equals(form.getId())) {
				try {
					// select first section radio button & submit
					((HtmlRadioButtonInput) page.getByXPath("//input[@type='radio']").get(0)).setChecked(true);
					form.getButtonByName("go").click();
				} catch (Throwable t) {
					// log.severe("caught!!!");
				}

				page = client.getPage(url);
				form = page.getForms().get(0);
				if (!"postingForm".equals(form.getId())) {
					try {
						// select bypass for neighborhood & submit
						((HtmlRadioButtonInput) page.getByXPath("//input[@value='0']").get(0)).setChecked(true);
						form.getButtonByName("go").click();
					} catch (Throwable t) {
						// log.severe("caught!!!");
					}
				
					page = client.getPage(url);
					form = page.getForms().get(0);
				}
			}

			// URL decaptcherPostURL = new URL("http://poster.decaptcher.com/");
			// WebRequestSettings request = new
			// WebRequestSettings(decaptcherPostURL, HttpMethod.POST);
			// request.setEncodingType(FormEncodingType.MULTIPART);
			// List<NameValuePair> params = new ArrayList<NameValuePair>();
			// params.add(new NameValuePair("function", "picture2"));
			// params.add(new NameValuePair("username", username));
			// params.add(new NameValuePair("password", password));
			//
			// //I added this block in
			// File file = new File("captcha.png");
			// new KeyDataPair()
			// params.add(new KeyDataPair("pict", capFile, "png", "utf-8"));
			// //----------------------
			//
			// params.add(new NameValuePair("pict_to", "0"));
			// params.add(new NameValuePair("pict_type", "0"));
			// request.setRequestParameters(params);
			// request.setUrl(decaptcherPostURL);
			//
			// HtmlPage page = webClient.getPage(request);

			//HtmlPage page2 = client.getPage(url);
			client.addRequestHeader("Connection", "Keep-Alive");
			// try {
			// page2 = byOwner.click();
			// } catch (ScriptException e) {
			// page2 = e.getPage();
			// }

			// log.severe("page1 uri is: " + form.getAcceptAttribute());

			// // Now submit the form by clicking the button and get back the
			// // second
			// // page.
			// final HtmlPage page2 = /*
			// * (HtmlPage)
			// * form.submit((SubmittableElement)
			// * page1.getElementByName("go"));
			// */webClient.getPage("https://post.craigslist.org/k/wnG0Yxol4RGCfJrEFsguSA/7PojZ");
			//
			//form = page2.getForms().get(0);

			// get list of all texts (title, price, location, from email,
			// confirm email)
			Map<String, String> nvp = new HashMap<String, String>();
			Map<String, byte[]> files = new HashMap<String, byte[]>();

			final List<HtmlTextInput> texts = (List<HtmlTextInput>) page.getByXPath("//input[@type='text']");

			// title
			texts.get(0).setValueAttribute(car.year + " " + car.model.getName());
			nvp.put(texts.get(0).getNameAttribute(), car.year + " " + car.model.getName());

			// price
			texts.get(1).setValueAttribute(car.price.toString());
			nvp.put(texts.get(1).getNameAttribute(), car.price.toString());

			// location
			texts.get(2).setValueAttribute(car.location);
			nvp.put(texts.get(2).getNameAttribute(), car.location);

			// email
			String email = accountService.get(car.owner).email;
			texts.get(3).setValueAttribute(email);
			texts.get(4).setValueAttribute(email);
			nvp.put(texts.get(3).getNameAttribute(), email);
			nvp.put(texts.get(4).getNameAttribute(), email);

			// description

			// header (year make model trim)
			StringBuilder desc = new StringBuilder("<table><tr><th colspan=\"2\" align=\"left\">");
			desc.append(car.year + " " + car.model.getName());
			if (car.trim != null)
				desc.append(" " + car.trim);
			desc.append("</th></tr>");

			// stock & vin
			if (!car.stock.isEmpty())
				addField(desc, "stock", car.stock);

			if (car.vin != null)
				addField(desc, "vin", car.vin);

			// condition
			addField(desc, "condition", car.conditions.get(0).toString());

			// price
			addField(desc, "price", "$" + car.price);

			// mileage
			addField(desc, "mileage", Integer.toString(car.mileage));

			// color
			addField(desc, "color", car.exterior.getName() + "/" + car.interior.getName());

			// transmission
			addField(desc, "transmission", car.transmission.getName());

			// doors
			addField(desc, "doors", Integer.toString(car.doors));

			// drivetrain
			addField(desc, "drivetrain", car.drivetrain.getName());

			// engine
			addField(desc, "engine", car.engine.getName());

			// end table
			desc.append("</table>");

			// ad
			Ad ad = adService.get(car.ad);

			// content
			desc.append("<p>");
			if (ad.getContent() != null)
				desc.append(ad.getContent());
			desc.append("</p>");

			// contact
			// Account account = accountService.get(ad.owner);

			// contact details
			desc.append("Contact ").append(account.name).append(" for more details");

			// email
			if (account.showEmail)
				// a href="mailto:astark1@unl.edu,ASTARK1@UNL.EDU">.
				desc.append("<br/>").append("<a href=\"mailto:").append(account.email).append("\">")
						.append(account.email).append("</a>");

			// phone
			if (account.phone != null)
				desc.append("<br/>").append(account.phone);

			// url
			if (account.url != null) {
				if (!account.url.startsWith("http"))
					account.url = "http://" + account.url;
				desc.append("<br/>").append("<a href=\"").append(account.url).append("\">").append(account.url)
						.append("</a>");
			}

			// // signature
			// desc.append("<br/><br/>").append("template by <b>mobimotos for android</b>");
			// String qr =
			// "<img src=\"http://qrcode.kaywa.com/img.php?s=5&d=http%3A%2F%2Fmarket.android.com%2Fdetails%3Fid%3Dcom.boosed.mm\" />";
			desc.append("<br/>").append("-----").append("<br/>android owner? inquire directly through ")
					.append("<a href=\"http://market.android.com/details?id=com.boosed.mm\">")
					.append("<b>mobimotos</b>").append("</a>");

			// set description
			((HtmlTextArea) form.getByXPath("//textarea").get(0)).setText(desc.toString());
			nvp.put(((HtmlTextArea) form.getByXPath("//textarea").get(0)).getNameAttribute(), desc.toString());

			// provide imagery
			final List<HtmlFileInput> inputs = (List<HtmlFileInput>) page.getByXPath("//input[@type='file']");

			int count = 0;
			for (Entry<String, String> image : ad.getImages(true).entrySet()) {
				HtmlFileInput input = inputs.get(count);

				input.setValueAttribute("file" + count + ".txt");
				input.setContentType("image/jpg");

				URL u = new URL(image.getValue());
				InputStream is = u.openConnection().getInputStream();
				byte[] data = IOUtils.toByteArray(is);
				is.close();
				// byte[] data = ImagesServiceFactory.makeImageFromBlob(new
				// BlobKey(image.getKey()))
				// .getImageData();
				input.setData(data);

				files.put(input.getNameAttribute(), data);

				// up to four images
				if (++count > 3)
					break;
			}

			// submit ad
			try {
				// form.getButtonByName("go").click();

				// add all hidden params
				final List<HtmlInput> hidden = (List<HtmlInput>) page.getByXPath("//input[@type='hidden']");
				for (HtmlInput input : hidden)
					// params.add(new NameValuePair(input.getNameAttribute(),
					// input.getValueAttribute()));
					nvp.put(input.getNameAttribute(), input.getValueAttribute());

				// radio buttons
				final List<HtmlRadioButtonInput> radios = (List<HtmlRadioButtonInput>) page
						.getByXPath("//input[@type='radio']");
				for (HtmlRadioButtonInput input : radios)
					if (input.getValueAttribute().equals("C"))
						// params.add(new
						// NameValuePair(input.getNameAttribute(),
						// input.getValueAttribute()));
						nvp.put(input.getNameAttribute(), input.getValueAttribute());

				// form.submit((SubmittableElement)
				// page2.getElementByName("go"));
				post(url, nvp, files);
			} catch (Throwable e) {
				log.severe("caught!!! " + e.getClass().toString());
			}

			// final submission
			try {
				// last click to submit
				((HtmlPage) client.getPage(url)).getForms().get(0).getButtonByName("go").click();
			} catch (Throwable e) {
				log.severe("caught!!! " + e.getClass().toString());
			}

			// close all windows
			client.closeAllWindows();

			// mark car as craiglist'd
			car.setNetwork(AdNetwork.CLIST, true);
			carService.save(car);
		} catch (MalformedURLException e) {

		} catch (IOException e) {

		}
	}

	private void post(String url, Map<String, String> nvp, Map<String, byte[]> files) {
		String boundary = "*****************************************";
		String newLine = "\r\n";

		try {
			// set cookies through sync proxy
			// SessionManager manager = SyncProxy.getDefaultSessionManager();
			HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setUseCaches(false);
			con.setRequestMethod("POST");
			con.setRequestProperty("Connection", "Keep-Alive");
			con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			DataOutputStream dos = new DataOutputStream(con.getOutputStream());

			// write the name/value pairs
			for (Entry<String, String> entry : nvp.entrySet()) {
				dos.writeBytes("--" + boundary + newLine);
				dos.writeBytes("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + newLine + newLine
						+ entry.getValue());
				dos.writeBytes(newLine);
			}

			// write the files
			for (Entry<String, byte[]> entry : files.entrySet()) {
				dos.writeBytes("--" + boundary + newLine);
				dos.writeBytes("Content-Disposition: form-data; " + "name=\"" + entry.getKey() + "\"; filename=\"" + ""
						+ "\"" + newLine + newLine);
				dos.write(entry.getValue());
				dos.writeBytes(newLine);
			}

			// the end
			dos.writeBytes("--" + boundary + "--" + newLine);

			// flush & close the output
			dos.flush();
			dos.close();

			// read the response (this actually performs the post!)
			if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				// OK
				log.severe("the response was received");
			} else {
				// Server returned HTTP error code.
				log.severe("not completed: " + con.getResponseCode());
			}

			// BufferedReader rd = new BufferedReader(new
			// InputStreamReader(con.getInputStream()));
			//
			// // detect the fail string
			// String line;
			// while ((line = rd.readLine()) != null) {
			// // do nothing
			// }

			// ret.content += line + "\r\n";
			// // get headers
			// Map<String, List<String>> headers = con.getHeaderFields();
			// Set<Entry<String, List<String>>> hKeys = headers.entrySet();
			// for (Iterator<Entry<String, List<String>>> i = hKeys.iterator();
			// i
			// .hasNext();) {
			// Entry<String, List<String>> m = i.next();
			// Log.w("HEADER_KEY", m.getKey() + "");
			// ret.headers.put(m.getKey(), m.getValue().toString());
			// if (m.getKey().equals("set-cookie"))
			// ret.cookies.put(m.getKey(), m.getValue().toString());
			// }
			// dos.close();
			// rd.close();

			// retrieve ad with latest updates and return
			// return service.loadAd(adId);
			// } catch (MalformedURLException me) {
			// Log.e("mm", "upload exception: " + me.toString());
			// return null;
			// } catch (IOException ie) {
			// Log.e("mm", "Exception: " + ie.toString());
			// return null;
			// } catch (Exception e) {
			// Log.e("chat", "Exception: " + e.toString());
			// e.printStackTrace();
			// return null;
			// }
		} catch (Exception e) {
			log.severe("post failed: " + e.getMessage());
			// Log.e("mm", "upload exception: " + e.toString());
			// return null;
		}
	}

	private void addField(StringBuilder sb, String field, String value) {
		// field
		sb.append("<tr><td>").append(field).append("</td>");

		// value
		sb.append("<td>").append(value).append("</td></tr>");
	}

	@Inject
	private AccountService accountService;

	@Inject
	private AdService adService;

	@Inject
	private CarService carService;
}