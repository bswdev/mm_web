package com.boosed.mm.server.service.impl;

import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.StateDao;
import com.boosed.mm.server.service.StateService;
import com.boosed.mm.shared.db.State;
import com.google.inject.Inject;

public class StateServiceImpl extends GenericServiceImpl<State, StateDao> implements StateService {

	@Inject
	public StateServiceImpl(StateDao dao) {
		super(dao);
	}
}