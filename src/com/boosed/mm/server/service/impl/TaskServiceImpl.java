package com.boosed.mm.server.service.impl;

import com.boosed.mm.server.service.TaskService;
import com.boosed.mm.server.servlet.QueueServlet;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions.Builder;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

public class TaskServiceImpl implements TaskService {

	/** default queue for managing tasks */
	private Queue queue;

	public TaskServiceImpl() {
		queue = QueueFactory.getDefaultQueue();
	}

//	@Override
//	public void populateCraigslist() {
//		queue.add(Builder.withUrl("/queue").param(QueueServlet.PARAM_OP, QueueServlet.OP_CRAIGSLIST));
//	}
	
	@Override
	public void postAd(long carId, String network) {
		queue.add(Builder.withUrl("/queue").param(QueueServlet.PARAM_OP, QueueServlet.OP_POST)
				.param(QueueServlet.PARAM_ID, Long.toString(carId)).param(QueueServlet.PARAM_NETWORK, network));
	}

	@Override
	public void renameModel(String modelId, String make, String model) {
		queue.add(Builder.withUrl("/queue").param(QueueServlet.PARAM_OP, QueueServlet.OP_RENAME)
				.param(QueueServlet.PARAM_ID, modelId).param(QueueServlet.PARAM_MAKE, make)
				.param(QueueServlet.PARAM_MODEL, model));
	}

	@Override
	public void updateMessages(long carId) {
		// queue task to update message metadata associated with modified car
		queue.add(Builder.withUrl("/queue").param(QueueServlet.PARAM_OP, QueueServlet.OP_MESSAGE)
				.param(QueueServlet.PARAM_ID, Long.toString(carId)).method(Method.POST));
	}

	@Override
	public void updatePriceAlerts(long carId) {
		// queue task to update price and send notifications
		queue.add(Builder.withUrl("/queue").param(QueueServlet.PARAM_OP, QueueServlet.OP_PRICE)
				.param(QueueServlet.PARAM_ID, Long.toString(carId)).method(Method.POST));
	}
}
