package com.boosed.mm.server.service.impl;

import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.TransactionDao;
import com.boosed.mm.server.service.TransactionService;
import com.boosed.mm.shared.db.Transaction;
import com.google.inject.Inject;

public class TransactionServiceImpl extends GenericServiceImpl<Transaction, TransactionDao> implements
		TransactionService {

	@Inject
	public TransactionServiceImpl(TransactionDao dao) {
		super(dao);
	}
}