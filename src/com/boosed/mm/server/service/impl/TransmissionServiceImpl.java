package com.boosed.mm.server.service.impl;

import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.TransmissionDao;
import com.boosed.mm.server.service.TransmissionService;
import com.boosed.mm.shared.db.Transmission;
import com.google.inject.Inject;

public class TransmissionServiceImpl extends GenericServiceImpl<Transmission, TransmissionDao> implements TransmissionService {

	@Inject
	public TransmissionServiceImpl(TransmissionDao dao) {
		super(dao);
	}
}