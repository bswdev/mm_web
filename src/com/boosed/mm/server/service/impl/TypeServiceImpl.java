package com.boosed.mm.server.service.impl;

import java.util.List;

import com.boosed.gae.server.criterion.NotZeroCriterion;
import com.boosed.gae.server.service.impl.GenericServiceImpl;
import com.boosed.mm.server.dao.TypeDao;
import com.boosed.mm.server.service.TypeService;
import com.boosed.mm.shared.db.Type;
import com.google.inject.Inject;

public class TypeServiceImpl extends GenericServiceImpl<Type, TypeDao> implements TypeService {

	@Inject
	public TypeServiceImpl(TypeDao dao) {
		super(dao);
	}

	@Override
	public List<Type> getAll(boolean filterEmpty) {
		if (filterEmpty)
			// filter all empty results
			return dao.findAllByCriteria(new NotZeroCriterion("total"));
		else
			// return all results
			return dao.findAll();
	}
}