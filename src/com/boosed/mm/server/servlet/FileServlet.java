package com.boosed.mm.server.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Singleton;

/**
 * Class used for simple blob upload of files.
 * 
 * @author dsumera
 */
@Singleton
public class FileServlet extends MobimotosServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		PrintWriter writer = resp.getWriter();
		writer.write("<html><head>");
		writer.write("<title>File Upload</title>");
		writer.write("</head><body>");
		writer.write("<form action=\"" + blobstoreService.createUploadUrl("/fprocess")
				+ "\" method=\"post\" enctype=\"multipart/form-data\">");
		writer.write("<input type=\"file\" name=\"file\"><br/>");
		writer.write("<input type=\"submit\" value=\"Submit\">");
		writer.write("</form></body></html>");
		writer.flush();
	}
}