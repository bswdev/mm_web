package com.boosed.mm.server.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Singleton;

@Singleton
public class ImageServlet extends MobimotosServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		PrintWriter writer = resp.getWriter();
		writer.write("<html><head>");
		writer.write("<title>Upload Test</title>");
		writer.write("</head><body>");
		writer.write("<form action=\"" + blobstoreService.createUploadUrl("/iprocess")
				+ "\" method=\"post\" enctype=\"multipart/form-data\">");
		writer.write("<input type=\"text\" name=\"ad\"><br/>");
		writer.write("<input type=\"file\" name=\"pic\"><br/>");
		writer.write("<input type=\"submit\" value=\"Submit\">");
		writer.write("</form></body></html>");
		writer.flush();
	}
}