package com.boosed.mm.server.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
public class IpnServlet extends MobimotosServlet {

	@Named("ipn")
	private String ipn;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// map of parameters
		final Map<String, String> params = (Map<String, String>) req.getParameterMap();

		// read post from PayPal system and add 'cmd'
		Enumeration<String> en = req.getParameterNames();
		String str = "cmd=_notify-validate";
		// for (Entry<String, String> entry : params.entrySet()) {
		while (en.hasMoreElements()) {
			String paramName = /* entry.getKey();// */en.nextElement();
			String paramValue = /* entry.getValue();// */req.getParameter(paramName);
			str = str + "&" + paramName + "=" + URLEncoder.encode(paramValue, "UTF-8");
		}

		// post back to PayPal system to validate
		// NOTE: change http: to https: in the following URL to verify using SSL
		// (for increased security).
		// using HTTPS requires either Java 1.4 or greater, or Java Secure
		// Socket Extension (JSSE)
		// and configured for older versions.
		URLConnection uc = new URL(ipn).openConnection();
		uc.setDoOutput(true);
		uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		PrintWriter pw = new PrintWriter(uc.getOutputStream());
		pw.println(str);
		pw.close();

		BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
		String res = in.readLine();
		in.close();

		// seller
		String receiverEmail = params.get("receiver_email");// request.getParameter("receiver_email");
		String receiverId = params.get("receiver_id");

		// transaction
		String txnId = params.get("txn_id");// request.getParameter("txn_id");
		String txnType = params.get("txn_type");
		String txnSubject = params.get("transaction_subject");

		// buyer
		String payerEmail = params.get("payer_email");// request.getParameter("payer_email");
		String payerId = params.get("payer_id");
		String payerStatus = params.get("payer_status");

		// buyer - extra
		String firstName = params.get("first_name");
		String lastName = params.get("last_name");
		String addressStreet = params.get("address_street");
		String addressCity = params.get("address_city");
		String addressCountry = params.get("address_country_code");
		String addressState = params.get("address_state");
		String addressPostal = params.get("address_zip");
		String addressStatus = params.get("address_status");

		// payment
		String itemName = params.get("item_name");// request.getParameter("item_name");
		String itemNumber = params.get("item_number");// request.getParameter("item_number");
		String paymentStatus = params.get("payment_status");// request.getParameter("payment_status");
		String paymentAmount = params.get("mc_gross");// request.getParameter("mc_gross");
		String paymentCurrency = params.get("mc_currency");// request.getParameter("mc_currency");
		String paymentFee = params.get("payment_fee");
		String paymentTax = params.get("tax");
		String paymentGross = params.get("payment_gross");
		String paymentType = params.get("payment_type");
		String quanity = params.get("quantity");

		// receiver_email = gm_1231902686_biz@paypal.com Check email address to
		// make
		// sure that this is not a spoof
		// receiver_id = S8XGHLYDW9T3S
		// residence_country = US
		// Information about the transaction:
		// test_ipn = 1 Testing with the Sandbox
		// transaction_subject =
		// txn_id = 61E67681CH3238416 Keep this ID to avoid processing the
		// transaction twice
		// txn_type = express_checkout Type of transaction
		// Information about your buyer:
		// payer_email = gm_1231902590_per@paypal.com
		// payer_id = LPLWNMTBWMFAY
		// payer_status = verified
		// first_name = Test
		// last_name = User
		// address_city = San Jose
		// address_country = United States
		// address_country_code = US
		// address_name = Test User
		// address_state = CA
		// address_status = confirmed
		// Introducing IPN
		// A Sample IPN Message and Response
		// 16 July, 2010 IPN Guide
		// Before you can trust the contents of the message, you must first
		// verify
		// that the message came
		// from PayPal. To verify the message, you must send back the contents
		// in
		// the exact order they
		// were received and precede it with the command _notify-validate, as
		// follows:
		// https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_notifyvalidate&
		// mc_gross=19.95&protection_eligibility=Eligible&address_status=conf
		// irmed&payer_id=LPLWNMTBWMFAY&tax=0.00&...&payment_gross=19.95&shipping=0.00
		// address_street = 1 Main St
		// address_zip = 95131
		// Information about the payment:
		// custom = Your custom field
		// handling_amount = 0.00
		// item_name =
		// item_number =
		// mc_currency = USD
		// mc_fee = 0.88
		// mc_gross = 19.95
		// payment_date = 20:12:59 Jan 13, 2009 PST
		// payment_fee = 0.88
		// payment_gross = 19.95
		// payment_status = Completed Status, which determines whether the
		// transaction is
		// complete
		// payment_type = instant Kind of payment
		// protection_eligibility = Eligible
		// quantity = 1
		// shipping = 0.00
		// tax = 0.00
		// Other information about the transaction:
		// notify_version = 2.6 IPN version; can be ignored
		// charset = windows-1252
		// verify_sign

		// check notification validation
		if (res.equals("VERIFIED")) {
			// check that paymentStatus=Completed
			// check that txnId has not been previously processed
			// check that receiverEmail is your Primary PayPal email
			// check that paymentAmount/paymentCurrency are correct
			// process payment
		} else if (res.equals("INVALID")) {
			// log for investigation
		} else {
			// error
		}
	}
}