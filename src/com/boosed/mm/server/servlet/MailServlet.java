package com.boosed.mm.server.servlet;

import java.io.IOException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beoui.geocell.GeocellManager;
import com.beoui.geocell.model.Point;
import com.boosed.mm.shared.db.Account;
import com.boosed.mm.shared.db.Ad;
import com.boosed.mm.shared.db.Location;
import com.boosed.mm.shared.db.State;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

/**
 * This servlet handles all incoming mail and processes it accordingly.
 * 
 * @author dsumera
 */
public class MailServlet extends MobimotosServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		Session session = Session.getDefaultInstance(new Properties(), null);

		try {
			MimeMessage message = new MimeMessage(session, req.getInputStream());

			// this is the appspotmail address
			String recipient = message.getRecipients(RecipientType.TO)[0].toString();
			recipient = recipient.substring(0, recipient.indexOf('@'));
			log.warning("the recipient is: " + recipient);

			if ("details".equals(recipient)) {
				// process details
				
				// get the ad
				String key = message.getSubject();
				Key<Ad> adKey = ObjectifyService.factory().stringToKey(key);
				Ad ad = adService.get(adKey);
				
				if (ad == null)
					// ad does not exist, return
					return;
				
				// get associated account
				Account account = accountService.get(ad.owner);
				
				// sender
				String sender = ((InternetAddress) message.getFrom()[0]).getAddress();
				
				if (!sender.equals(account.email))
					// user emailing is not the owner of the ad, return
					return;
				
				// get the details
				Object data = message.getContent();

				String details = "";
				if (data instanceof MimeMultipart)
					// get message if email was mime multipart
					details = ((MimeMultipart) data).getBodyPart(0).getContent().toString();
				else
					// get message if email was plain text
					details = (String) message.getContent();

				// remove all formating
				details = details.replaceAll("\\n", " ");
				
				// set the details on the ad
				ad.setContent(details);
				ad.approveContent();
				adService.save(ad);
			} else if ("validate".equals(recipient)) {
				// validate a new location
				
				// sender, ensure it is an admin
				String sender = ((InternetAddress) message.getFrom()[0]).getAddress();
				if (!sender.equals("dennis@boosedsoft.com")) {
					log.warning("the sender is: " + sender);
					
					// not an admin, do not process
					return;
				}
				
				// this is the reply subject from admin
				String subject = message.getSubject();
				log.warning("the subject is: " + subject);
				
				// need to validate this address
				Object data = message.getContent();

				String value = "";
				if (data instanceof MimeMultipart)
					// get message if email was mime multipart
					value = ((MimeMultipart) data).getBodyPart(0).getContent().toString();
				else
					// get message if email was plain text
					value = (String) message.getContent();

				// for (int i = content.getCount(); --i > -1;) {
				// log.warning("part " + i + " is: " +
				// content.getBodyPart(i).getContent());
				// }

				// postal, city, state, latitude, longitude
				String[] values = value.split(",");

				// create a new location from the data
				Location location = new Location(values[0], Double.parseDouble(values[3]),
						Double.parseDouble(values[4]));
				location.city = values[1];
				location.state = new Key<State>(State.class, values[2]);
				location.geocells = GeocellManager.generateGeoCell(new Point(location.latitude, location.longitude));
				locationService.save(location);
			}
		} catch (MessagingException e) {
			log.warning("something happened with mail parsing: " + e.getMessage());
		}
	}
}