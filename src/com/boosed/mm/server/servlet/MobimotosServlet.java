package com.boosed.mm.server.servlet;

import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;

import com.boosed.mm.server.service.AccountService;
import com.boosed.mm.server.service.AdService;
import com.boosed.mm.server.service.CLService;
import com.boosed.mm.server.service.CarService;
import com.boosed.mm.server.service.LocationService;
import com.boosed.mm.server.service.MarkerService;
import com.boosed.mm.server.service.MessageService;
import com.boosed.mm.server.service.ModelService;
import com.boosed.mm.server.service.PostService;
import com.boosed.mm.shared.exception.RemoteServiceFailureException;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

/**
 * Base servlet which contains references to all leveraged services.
 * 
 * @author dsumera
 * 
 */
@Singleton
public class MobimotosServlet extends HttpServlet {

	protected Logger log = Logger.getLogger(getClass().getName());

	protected static AccountService accountService;

	protected static AdService adService;

	protected static BlobstoreService blobstoreService;

	protected static FileService fileService;

	protected static ImagesService imagesService;

	protected static UserService userService;

	protected static CarService carService;

	protected static CLService clService;

	protected static LocationService locationService;

	protected static MarkerService markerService;

	protected static MessageService messageService;

	protected static ModelService modelService;

	protected static PostService postService;

	protected static int quotaBasic;

	protected static int quotaStandard;

	public MobimotosServlet() {
		// default no-arg constructor, used by derived classes
	}

	@Inject
	public MobimotosServlet(AccountService accountService, AdService adService, CarService carService,
			CLService clService, BlobstoreService blobstoreService, FileService fileService,
			ImagesService imagesService, LocationService locationService, MarkerService markerService,
			MessageService messageService, ModelService modelService, PostService postService, UserService userService,
			@Named("quotaBasic") int quotaBasic, @Named("quotaStandard") int quotaStandard) {

		if (MobimotosServlet.accountService == null)
			MobimotosServlet.accountService = accountService;

		if (MobimotosServlet.adService == null)
			MobimotosServlet.adService = adService;

		if (MobimotosServlet.blobstoreService == null)
			MobimotosServlet.blobstoreService = blobstoreService;

		if (MobimotosServlet.carService == null)
			MobimotosServlet.carService = carService;

		if (MobimotosServlet.clService == null)
			MobimotosServlet.clService = clService;
		
		if (MobimotosServlet.fileService == null)
			MobimotosServlet.fileService = fileService;

		if (MobimotosServlet.imagesService == null)
			MobimotosServlet.imagesService = imagesService;

		if (MobimotosServlet.locationService == null)
			MobimotosServlet.locationService = locationService;

		if (MobimotosServlet.markerService == null)
			MobimotosServlet.markerService = markerService;

		if (MobimotosServlet.messageService == null)
			MobimotosServlet.messageService = messageService;

		if (MobimotosServlet.modelService == null)
			MobimotosServlet.modelService = modelService;

		if (MobimotosServlet.postService == null)
			MobimotosServlet.postService = postService;

		if (MobimotosServlet.userService == null)
			MobimotosServlet.userService = userService;

		// image quotas
		MobimotosServlet.quotaBasic = quotaBasic;
		MobimotosServlet.quotaStandard = quotaStandard;
	}

	/**
	 * Retrieve the account id.
	 * 
	 * @return
	 */
	protected String ensureAccountId() {
		User user = userService.getCurrentUser();

		if (user == null)
			return null;

		return user.getUserId();
	}

	/**
	 * Like <code>ensureAccountId</code>, but also checks if the user has
	 * sufficient privilege to perform actions such as activating ads, sending
	 * messages, etc.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	protected String ensureAccountIdWithPrivileges() {
		String rv = ensureAccountId();

		if (rv != null && accountService.get(rv).enabled)
			// user is logged in an enabled
			return rv;
		else
			// user is restricted from performing action
			return null;
	}

	/**
	 * Check if the user is an admin.
	 * 
	 * @return
	 */
	protected boolean isUserAdmin() {
		try {
			return userService.isUserAdmin();
		} catch (IllegalStateException e) {
			return false;
		}
	}
}