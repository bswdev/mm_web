package com.boosed.mm.server.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;

public class ProcessFileServlet extends MobimotosServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// doPost(req, resp);
		// String url = ;
		resp.setHeader("Content-Type", "text/html");

		// This is a bit hacky, but it'll work
		resp.getWriter().println(req.getParameter("file"));
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		// get the key for the pic
		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);

		BlobKey key = blobs.get("file").get(0);

		if (key == null) {
			resp.sendRedirect("/fprocess?file=null-blob-key");
			return;
		}

		// send back to the get handler of this servlet
		resp.sendRedirect("/fprocess?file=" + key.getKeyString());
	}
}