package com.boosed.mm.server.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.boosed.mm.shared.db.Ad;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileWriteChannel;
import com.google.appengine.api.images.Image;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.Transform;
import com.google.appengine.api.images.ImagesService.OutputEncoding;
import com.google.apphosting.api.ApiProxy.ApiDeadlineExceededException;

public class ProcessImageServlet extends MobimotosServlet {

	// maximum size in bytes for images
	// private static int IMAGE_MAX = 262144;

	private static int IMG_WIDTH = 400;
	private static int IMG_HEIGHT = 240;

	// /** limit number of photos to 10 */
	// private static int LMT_PHOTO = 10;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// doPost(req, resp);
		// String url = ;
		resp.setHeader("Content-Type", "text/html");

		// This is a bit hacky, but it'll work
		resp.getWriter().println(req.getParameter("url"));
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		// get the key for the pic
		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);

		BlobKey key = blobs.get("pic").get(0);

		if (key == null) {
			resp.sendRedirect("/iprocess?url=null-blob-key");
			return;
		}

		// flag to check for stock photo upload
		boolean isStock = false;

		// check if the user is the owner
		String accountId = ensureAccountIdWithPrivileges();
		Ad ad = adService.get(Long.parseLong(req.getParameter("ad")));

		// if ad is null
		if (ad == null) {
			if (isUserAdmin())
				// admin is registering a stock image
				isStock = true;
			else {
				log.warning("ad does not exist");

				// delete the uploaded blob
				blobstoreService.delete(key);

				// return a page reading failed
				PrintWriter writer = resp.getWriter();
				writer.print("failed, ad id: " + req.getParameter("ad") + " does not exist");
				writer.flush();
				return;
			}
		} else {// if (ad == null && userService.isUserAdmin())
			// user != owner AND not admin
			if (!ad.owner.getName().equals(accountId) && !isUserAdmin()) {
				log.warning(accountId + " is not an admin and doesn't own this ad");

				// delete the uploaded blob
				blobstoreService.delete(key);

				// return a page reading failed
				PrintWriter writer = resp.getWriter();
				writer.print(
						"failed upload (unauthorized), ad id: " + req.getParameter("ad") + ", ad.owner: "
								+ ad.owner.getName() + ", accountId: " + accountId);
				writer.flush();
				writer.close();
				return;
			} else {
				// check ad limits against the ad type

				// retrieve the ad type and check photo limits
				int allowed = 0;
				switch (carService.get(ad.car).adType) {
				case STANDARD:
					allowed = quotaStandard;
					break;
				case BASIC:
					allowed = quotaBasic;
					break;
				}

				if (ad.getImages().size() >= allowed) {
					// photo limit met or exceeded
					log.warning("ad id : " + ad.id + " has met photo limit (" + allowed + ")");

					// delete the uploaded blob
					blobstoreService.delete(key);

					// return a page upload failed
					PrintWriter writer = resp.getWriter();
					writer.print(
							"failed upload (photo limit), ad id: " + req.getParameter("ad") + ", ad.owner: "
									+ ad.owner.getName() + ", accountId: " + accountId);
					writer.flush();
					writer.close();
					return;
				}
			}
		}

		// get the old image
		Image image = ImagesServiceFactory.makeImageFromBlob(key);

		// need to shrink the image
		// log.warning("the image is: " + image.getWidth());
		// log.warning("the bytes are: " + image.getImageData());
		// if (image.getWidth() > IMG_WIDTH) {

		// resize transform, crop to a square (240, 400)
		Transform resize = ImagesServiceFactory.makeResize(IMG_WIDTH, IMG_WIDTH);
		// Transform crop = ImagesServiceFactory.makeCrop(0, 0.1666667, 0,
		// 0.833333);
		// Transform composite =
		// ImagesServiceFactory.makeCompositeTransform(Arrays.asList(resize,
		// crop));

		// apply a transform
		image = imagesService.applyTransform(resize, image, OutputEncoding.JPEG);

		// if height exceeds 240, crop the top and bottom
		int height = image.getHeight();
		if (height > IMG_HEIGHT) {
			double delta = height - IMG_HEIGHT;
			delta /= (2.0 * height);
			Transform crop = ImagesServiceFactory.makeCrop(0, delta, 1, 1 - delta);
			image = imagesService.applyTransform(crop, image, OutputEncoding.JPEG);
		}

		// AppEngineFile aef = fileService.getBlobFile(blobKey);

		// String format = image.getFormat().toString();
		// log.warning("the format is: " + format);

		// delete the old blob
		blobstoreService.delete(key);

		// NEW
		// Get a file service
		// FileService fileService = FileServiceFactory.getFileService();
		// Create a new Blob file with mime-type "text/plain"
		AppEngineFile file = fileService.createNewBlobFile("image/jpg");
		// Open a channel to write to it
		boolean lock = true;
		FileWriteChannel writeChannel = fileService.openWriteChannel(file, lock);
		// Different standard Java ways of writing to the channel
		// are possible. Here we use a PrintWriter:
		// PrintWriter out = new
		// PrintWriter(Channels.newWriter(writeChannel, "UTF8"));
		// out.println("The woods are lovely dark and deep.");
		// out.println("But I have promises to keep.");
		// Close without finalizing and save the file path for writing later
		// out.close();
		// String path = file.getFullPath();
		// Write more to the file in a separate request:
		// file = new AppEngineFile(path);
		// This time lock because we intend to finalize
		// lock = true;
		// writeChannel = fileService.openWriteChannel(file, lock);
		// This time we write to the channel using standard Java
		// writeChannel.write(ByteBuffer.wrap("And miles to go before I sleep.".getBytes()));
		writeChannel.write(ByteBuffer.wrap(image.getImageData()));
		// Now finalize
		writeChannel.closeFinally();
		// Later, read from the file using the file API
		// lock = false;
		// Let other people read at the same time
		// FileReadChannel readChannel = fileService.openReadChannel(file,
		// false);
		// Again, different standard Java ways of reading from the channel.
		// BufferedReader reader = new
		// BufferedReader(Channels.newReader(readChannel, "UTF8"));
		// String line = reader.readLine();
		// line = "The woods are lovely dark and deep."
		// readChannel.close();
		// Now read from the file using the Blobstore API
		key = fileService.getBlobKey(file);
		// BlobstoreService blobStoreService =
		// BlobstoreServiceFactory.getBlobstoreService();
		// String segment = new String(blobStoreService.fetchData(blobKey,
		// 30, 40));

		// END NEW
		// }

		try {
			String url = imagesService.getServingUrl(key);
	
			// get the car with the given id
			// log.warning("the serving url is: " + url);
	
			// add pic to car
			// ad.pics.put(key.getKeyString(), new Picture(url));
			if (!isStock) {
				String blobKey = key.getKeyString();
				
				// add image
				ad.addImage(blobKey, url);
				
				// TODO: remove this for flagged? approve images automatically
				ad.approveImage(blobKey);
				
				adService.save(ad);
			}
	
			// send back to the get handler of this servlet
			resp.sendRedirect("/iprocess?url=" + url);
		} catch (ApiDeadlineExceededException e) {
			// image uploaded but url was not generated
			// The API call images.GetUrlBase() took too long to respond and was cancelled.
			
			// delete image
			blobstoreService.delete(key);
			
			// send back to the get handler of this servlet
			resp.sendRedirect("/iprocess?url=none");
		}

		// String key = "";
		// for (Entry<String, BlobKey> entry : blobs.entrySet())
		// key = entry.getKey();

		// BlobKey blobKey = blobs.get("myFile");

		// writer.println("the key is: " + key + "<br/>");
		// writer.println("use this link to get somehwere <a href=\"\\serve?key="
		// + key + "\">link</a>");
		// writer.flush();
		// res.flushBuffer();
	}
}