package com.boosed.mm.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Singleton;

/**
 * Class used for simple blob upload of files.
 * 
 * @author dsumera
 */
@Singleton
public class QueueServlet extends MobimotosServlet {

	public static final String PARAM_OP = "com.boosed.mm.param.op";

	public static final String PARAM_ID = "com.boosed.mm.param.id";

	public static final String PARAM_MAKE = "com.boosed.mm.param.make";
	
	public static final String PARAM_MODEL = "com.boosed.mm.param.model";

	public static final String PARAM_NETWORK = "com.boosed.mm.param.network";
	
	public static final String OP_CG = "com.boosed.mm.op.clist";
	
	public static final String OP_MESSAGE = "com.boosed.mm.op.message";

	public static final String OP_POST = "com.boosed.mm.op.post";
	
	public static final String OP_PRICE = "com.boosed.mm.op.price";

	public static final String OP_RENAME = "com.boosed.mm.op.rename";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
			IOException {
		doPost(req, resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		// get the operation
		String operation = req.getParameter(PARAM_OP);
		
		if (OP_MESSAGE.equals(operation))
			// update the vin, model, stock in messages
			messageService.updateIdentifiers(carService.get(Long.parseLong(req
					.getParameter(PARAM_ID))));
		else if (OP_POST.equals(operation))
			// post this vehicle to the ad network
			postService.post(Long.parseLong(req.getParameter(PARAM_ID)), req.getParameter(PARAM_NETWORK));
		else if (OP_PRICE.equals(operation))
			// send notifications for price updates
			markerService.updatePrice(carService.get(Long.parseLong(req.getParameter(PARAM_ID))));
		else if (OP_RENAME.equals(operation))
			// rename the model & update all messages
			modelService.rename(req.getParameter(PARAM_ID), req.getParameter(PARAM_MAKE), req.getParameter(PARAM_MODEL));
//		else if (OP_CG.equals(operation))
//			// populate the craigslist items
//			try {
//				clService.populate();
//			} catch (Exception e) {
//				log.severe("could not parse craigslist data");
//			}
	}
}