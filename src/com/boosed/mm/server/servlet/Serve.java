package com.boosed.mm.server.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.inject.Singleton;

@Singleton
public class Serve extends MobimotosServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		BlobKey blobKey = new BlobKey(req.getParameter("key"));
		blobstoreService.serve(blobKey, res);
	}
}