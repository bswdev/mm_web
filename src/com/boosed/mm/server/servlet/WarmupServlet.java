package com.boosed.mm.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.boosed.mm.server.module.ServerModule;
import com.boosed.mm.server.module.ServletModule;
import com.google.inject.Guice;

public class WarmupServlet extends MobimotosServlet {

	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException,
			IOException {
		super.service(request, response);

		// initialize the services
		// ServiceFactory.getCrowdService();
		// ServiceFactory.getMemberService();
		Guice.createInjector(new ServerModule(), new ServletModule());
		
		log.info("initializing the warmup serlvet");
	}
}